package apiserver

import (
	"gitlab.com/asdfakl/gallery/app/jwt"
)

type TokenPayload struct {
	jwt.JWTPayload
}
