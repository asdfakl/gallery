package main

import (
	"context"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type Object struct {
	Abs     string
	Base    string
	Size    int64
	ModTime time.Time
	SHA1    [sha1.Size]byte
}

type ReadJob struct {
	Abs  string
	Info os.FileInfo
}

type ReaderService struct {
	cfg *Configuration

	objectc    chan<- *Object
	once       sync.Once
	dedup      map[[sha1.Size]byte]struct{}
	dedupMutex sync.Mutex
}

func NewReaderService(cfg *Configuration, objectc chan<- *Object) *ReaderService {
	return &ReaderService{
		cfg:     cfg,
		objectc: objectc,
		dedup:   make(map[[sha1.Size]byte]struct{}),
	}
}

func (r *ReaderService) Start(ctx context.Context, errg *errgroup.Group) {
	r.once.Do(func() {
		errg.Go(func() error {
			return utils.Wrap(r.Run(ctx), "reader service")
		})
	})
}

func (r *ReaderService) Run(ctx context.Context) error {
	defer close(r.objectc)

	wg := &sync.WaitGroup{}
	jobc := make(chan *ReadJob, r.cfg.Concurrency.ReadWorkerCount)

	wg.Add(r.cfg.Concurrency.ReadWorkerCount)
	for i := 0; i < r.cfg.Concurrency.ReadWorkerCount; i++ {
		go r.Work(wg, jobc)
	}

	defer wg.Wait()

	defer close(jobc)

	walkFn := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if err = ctx.Err(); err != nil {
			return err
		}

		if !r.cfg.IncludeHidden() && strings.HasPrefix(info.Name(), ".") {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		if info.Mode()&os.ModeType != 0 {
			return nil
		}

		if info.Size() <= 0 {
			return nil
		}

		r.cfg.Metrics.Total.Increment(info.Size())
		jobc <- &ReadJob{
			Abs:  path,
			Info: info,
		}

		return nil
	}

	for _, target := range r.cfg.Targets {
		if err := ctx.Err(); err != nil {
			return err
		}

		target, err := filepath.EvalSymlinks(target)
		if err != nil {
			return fmt.Errorf("evaluate target symlinks: %w", err)
		}

		target, err = filepath.Abs(target)
		if err != nil {
			return fmt.Errorf("evaluate target absolute path: %w", err)
		}

		if utils.IsDirectory(target) {
			if err = filepath.Walk(target, walkFn); err != nil {
				return fmt.Errorf("recursively read target directory '%s': %w", target, err)
			}
		} else if utils.IsRegularFile(target) {
			info, err := os.Lstat(target)
			if err != nil {
				return fmt.Errorf("stat target file '%s': %w", target, err)
			}
			r.cfg.Metrics.Total.Increment(info.Size())
			jobc <- &ReadJob{
				Abs:  target,
				Info: info,
			}
		} else {
			return fmt.Errorf("target '%s' is not a directory or regular file", target)
		}
	}

	r.cfg.Success("Read service complete")

	return nil
}

func (r *ReaderService) Work(wg *sync.WaitGroup, jobc <-chan *ReadJob) {
	defer wg.Done()

	send := func(obj *Object) {
		r.dedupMutex.Lock()
		defer r.dedupMutex.Unlock()

		_, exists := r.dedup[obj.SHA1]
		if exists {
			r.cfg.Metrics.ReadDuplicate.Increment(obj.Size)
			r.cfg.Info(fmt.Sprintf("%s - ignoring duplicate", obj.Base))
			return
		}

		r.dedup[obj.SHA1] = struct{}{}

		r.objectc <- obj
	}

	for job := range jobc {
		obj, err := r.Read(job)
		if err != nil {
			r.cfg.Metrics.ReadFailure.Increment(job.Info.Size())
			r.cfg.Error(fmt.Sprintf("read file '%s': %v", job.Abs, err))
			continue
		}
		r.cfg.Metrics.ReadSuccess.Increment(obj.Size)
		send(obj)
	}
}

func (r *ReaderService) Read(job *ReadJob) (*Object, error) {
	f, err := os.Open(job.Abs)
	if err != nil {
		return nil, fmt.Errorf("open file '%s': %w", job.Abs, err)
	}
	defer f.Close()

	h := sha1.New()

	n, err := io.Copy(h, f)
	if err != nil {
		return nil, fmt.Errorf("read file '%s': %w", job.Abs, err)
	}
	if n != job.Info.Size() {
		return nil, fmt.Errorf(
			"%s: expecting read of %d bytes, got %d, possible concurrent modification",
			job.Abs,
			job.Info.Size(),
			n,
		)
	}

	obj := &Object{
		Abs:     job.Abs,
		Base:    filepath.Base(job.Abs),
		Size:    n,
		ModTime: job.Info.ModTime(),
	}
	copy(obj.SHA1[:], h.Sum(nil))

	return obj, nil
}
