package apiserver

import (
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/postgres"
)

type HandlerFn func(*APIServer, *http.Request, httprouter.Params) (Handler, error)

type Handler interface {
	CanAccess(*postgres.UserView) bool
	SetUser(*postgres.UserView)
	User() *postgres.UserView
	SetRequest(*http.Request)
	Request() *http.Request
}

type GETHandler interface {
	Handler
	HandleGET(*APIServer, http.ResponseWriter, *http.Request)
}

type POSTHandler interface {
	Handler
	HandlePOST(*APIServer, http.ResponseWriter, *http.Request)
}

type Route struct {
	user *postgres.UserView
	r    *http.Request
}

func (route *Route) SetUser(user *postgres.UserView) {
	route.user = user
}

func (route *Route) User() *postgres.UserView {
	return route.user
}

func (page *Route) SetRequest(r *http.Request) {
	page.r = r
}

func (page *Route) Request() *http.Request {
	return page.r
}
