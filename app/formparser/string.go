package formparser

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

type FormStringField struct {
	parser *FormParser
	key    string
	value  *string
}

func (parser *FormParser) String(key string) (field *FormStringField) {
	field = &FormStringField{
		parser: parser,
		key:    key,
	}

	vals, err := parser.src(key)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse form: %w", err))
		return
	}

	if len(vals) == 0 {
		return
	}

	if len(vals) > 1 {
		parser.registerError(key, ErrFormParserAmbiguous)
		return
	}

	if !utf8.ValidString(vals[0]) {
		parser.registerError(key, ErrFormParserInvalidUTF8)
		return
	}

	field.value = &vals[0]
	return
}

func (field *FormStringField) Required() *FormStringField {
	if field.value == nil {
		field.parser.registerError(field.key, ErrFormParserRequired)
	}
	return field
}

func (field *FormStringField) Trim() *FormStringField {
	if field.value == nil {
		return field
	}

	trimmed := strings.TrimSpace(*field.value)
	return &FormStringField{
		parser: field.parser,
		key:    field.key,
		value:  &trimmed,
	}
}

func (field *FormStringField) NonEmpty() *FormStringField {
	if field.value == nil {
		return field
	}

	if *field.value == "" {
		field.parser.registerError(field.key, ErrFormParserEmpty)
	}
	return field
}

func (field *FormStringField) MaxLength(maxLength int) *FormStringField {
	if field.value == nil {
		return field
	}

	if runeLen := len([]rune(*field.value)); runeLen > maxLength {
		field.parser.registerError(field.key, ErrFormParserMaxLengthExceeded)
	}
	return field
}

func (field *FormStringField) Options(opts []string) *FormStringField {
	if field.value == nil {
		return field
	}

	match := false
	for _, opt := range opts {
		if *field.value == opt {
			match = true
			break
		}
	}

	if !match {
		field.parser.registerError(field.key, ErrFormParserDoesNotMatchOptions)
	}
	return field
}

func (field *FormStringField) Value() string {
	field.parser.mustHaveErrChecked()

	return *field.value
}

func (field *FormStringField) OptionalValue() *string {
	field.parser.mustHaveErrChecked()

	return field.value
}

func (field *FormStringField) ValueOr(def string) string {
	field.parser.mustHaveErrChecked()

	if field.value == nil {
		return def
	}
	return *field.value
}
