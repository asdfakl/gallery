package appserver

import (
	"errors"
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type ChangeSecretMasterPasswordPage struct {
	Page

	Secret          *postgres.Secret
	MasterPassword  *postgres.MasterPassword
	MasterPasswords postgres.MasterPasswordList
}

func ChangeSecretMasterPasswordPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("secret not found")
	}

	// load
	secret, err := srv.pg.GetSecretByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get secret by ext_id")
	}
	if secret == nil {
		return nil, ErrNotFound("secret not found")
	}

	password, err := srv.pg.GetMasterPasswordByID(srv.appCtx, secret.MasterPasswordID)
	if err != nil {
		return nil, utils.Wrap(err, "get master password by id")
	}
	if password == nil {
		return nil, ErrNotFound("master password not found")
	}

	return &ChangeSecretMasterPasswordPage{
		Secret:         secret,
		MasterPassword: password,
	}, nil
}

func (page *ChangeSecretMasterPasswordPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.MasterPassword.Owner
}

func (page *ChangeSecretMasterPasswordPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	passwords, err := srv.pg.GetMasterPasswordListByOwner(srv.appCtx, page.User().ID)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get master password list by owner"))
		return
	}

	passwords = passwords.Filter(func(mp *postgres.MasterPassword) bool {
		return mp.ID != page.MasterPassword.ID
	})
	if len(passwords) == 0 {
		srv.handleError(w, ErrConflict("only one master password"))
		return
	}

	page.MasterPasswords = passwords

	srv.executePageTemplate(w, "secret_change_master_password_form.html", page, http.StatusOK)
}

func (page *ChangeSecretMasterPasswordPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		fromPasswordField = parser.String("from_password").Required().NonEmpty().MaxLength(64)
		toPasswordField   = parser.String("to_password").Required().NonEmpty().MaxLength(64)
		toPasswordIDField = parser.UUID("to_password_id").Required()
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	toPassword, err := srv.pg.GetMasterPasswordByExtID(srv.appCtx, toPasswordIDField.Value())
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get master password by ext_id"))
		return
	}
	if toPassword == nil {
		srv.handleError(w, ErrNotFound("to master password not found"))
		return
	}
	if toPassword.Owner != page.User().ID {
		srv.handleError(w, ErrForbidden("to master password access denied"))
		return
	}
	if toPassword.ID == page.MasterPassword.ID {
		srv.handleError(w, ErrConflict("from and to master passwords are the same"))
		return
	}

	if err = bcrypt.CompareHashAndPassword([]byte(toPassword.Password), []byte(toPasswordField.Value())); err != nil {
		srv.handleError(w, ErrForbidden("to password mismatch"))
		return
	}

	plaintext, err := srv.pg.DecryptSecretByID(srv.appCtx, page.Secret.ID, fromPasswordField.Value())
	if err != nil {
		if errors.Is(err, postgres.ErrDecrypt) {
			srv.handleError(w, ErrForbidden("from password mismatch"))
		} else {
			srv.handleError(w, utils.Wrap(err, "decrypt secret by id"))
		}
		return
	}

	ciphertext, nonce, err := utils.EncryptAES256(plaintext, toPasswordField.Value())
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "encrypt aes256"))
		return
	}

	page.Secret.MasterPasswordID = toPassword.ID

	if err = srv.pg.ModifySecret(srv.appCtx, page.Secret, ciphertext, nonce); err != nil {
		srv.handleError(w, utils.Wrap(err, "modify secret"))
		return
	}

	srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
}
