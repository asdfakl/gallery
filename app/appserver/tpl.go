package appserver

import (
	"bytes"
	"crypto/md5"
	"embed"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/utils"
)

//go:embed tpl/*.html
var tplFS embed.FS

//go:embed static
var staticFS embed.FS

func (srv *AppServer) tplFuncs() template.FuncMap {
	return template.FuncMap{
		"V": func() string {
			if config.Version == "development" {
				return srv.cfg.StartTime.In(time.UTC).Format("dev-2006-01-02T15:04")
			}
			return config.Version
		},
		"path": func(path string) string { return srv.path(path) },
		"csrf": func(page Handler, path string, ttl time.Duration) string {
			session := page.Request().Context().Value(ContextKeySession{}).(*redismain.Session)

			return srv.issueCSRFToken(session.Key, path, ttl)
		},
		"concat": func(xs ...string) string {
			return strings.Join(xs, "")
		},
		"string_slice": func(args ...string) []string { return args },
		"fa_content_type": func(ct string) string {
			switch ct {
			case "image":
				return "fas fa-file-image"
			case "video":
				return "fas fa-file-video"
			case "audio":
				return "fas fa-file-audio"
			default:
				return ""
			}
		},
		"datetime": func(t time.Time) string {
			return t.Format("2006-01-02 15:04")
		},
		"human": func(size int64) string {
			return utils.HumanFilesize(size)
		},
		"pagination": func(count, offset, totalCount uint) *Pagination {
			return &Pagination{
				Count:      count,
				Offset:     offset,
				TotalCount: totalCount,
			}
		},
		"minutes": func(minutes int) time.Duration {
			return time.Duration(minutes) * time.Minute
		},
		"user_by_id": func(id int64) *postgres.UserView {
			user, err := srv.cachedUserViewByID(id)
			if err != nil {
				panic(err)
			}
			return user
		},
		"gravatar": func(username string) *url.URL {
			h := md5.New()
			h.Write([]byte(strings.ToLower(strings.TrimSpace(username))))

			query := url.Values{}
			query.Add("size", "64")
			query.Add("d", "robohash")

			return &url.URL{
				Scheme:   "https",
				Host:     "www.gravatar.com",
				Path:     "/avatar/" + hex.EncodeToString(h.Sum(nil)),
				RawQuery: query.Encode(),
			}
		},
		"join": strings.Join,
		"json": func(val interface{}) string {
			b, err := json.Marshal(val)
			if err != nil {
				panic(err)
			}
			return string(b)
		},
		"next_redirect": func(redirect string) *JSONResponse {
			return &JSONResponse{
				Redirect: redirect,
			}
		},
		"next_refresh": func() *JSONResponse {
			return &JSONResponse{
				Refresh: true,
			}
		},
		"next_reload": func() *JSONResponse {
			return &JSONResponse{
				Reload: true,
			}
		},
	}
}

func (srv *AppServer) loadTemplates() error {
	tpls, err := template.New("").Funcs(srv.tplFuncs()).ParseFS(tplFS, "tpl/*.html")
	if err != nil {
		return err
	}
	srv.tpls = tpls

	return nil
}

func (srv *AppServer) executePageTemplate(w http.ResponseWriter, name string, page Handler, statusCode int) {
	srv.executeTemplate(w, name, page, statusCode)
}

func (srv *AppServer) executeTemplate(w http.ResponseWriter, name string, data interface{}, statusCode int) {
	if srv.cfg.TemplateDirectory == "" {
		t := srv.tpls.Lookup(name)
		if t == nil {
			panic(fmt.Errorf("no such template: '%s'", name))
		}

		b := &bytes.Buffer{}

		if err := t.Execute(b, data); err != nil {
			panic(fmt.Errorf("execute template '%s': %v", name, err))
		}

		w.Header().Add("Content-Type", "text/html")
		w.Header().Add("Cache-Control", "no-store")

		w.WriteHeader(statusCode)
		if _, err := io.Copy(w, b); err != nil {
			srv.logger.Warn("response write error", log.Err(err))
		}
	} else {
		tpls, err := template.New("").Funcs(srv.tplFuncs()).ParseFS(os.DirFS(srv.cfg.TemplateDirectory), "*.html")
		if err != nil {
			panic(fmt.Errorf("load templates from '%s': %v", srv.cfg.TemplateDirectory, err))
		}

		t := tpls.Lookup(name)
		if t == nil {
			panic(fmt.Errorf("no such template: '%s'", name))
		}

		b := &bytes.Buffer{}

		if err := t.Execute(b, data); err != nil {
			panic(fmt.Errorf("execute template '%s': %v", name, err))
		}

		w.Header().Add("Content-Type", "text/html")
		w.Header().Add("Cache-Control", "no-store")

		w.WriteHeader(statusCode)
		if _, err := io.Copy(w, b); err != nil {
			srv.logger.Warn("response write error", log.Err(err))
		}
	}
}
