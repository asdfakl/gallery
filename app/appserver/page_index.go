package appserver

import (
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/postgres"
)

type IndexPage struct {
	Page

	TagFrequencies postgres.TagFrequencyList
}

func IndexPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &IndexPage{}, nil
}

func (page *IndexPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *IndexPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	freqs, err := srv.allCachedTagFrequencies()
	if err != nil {
		srv.handleError(w, err)
		return
	}
	page.TagFrequencies = freqs

	srv.executePageTemplate(w, "index.html", page, http.StatusOK)
}
