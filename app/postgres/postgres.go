package postgres

import (
	"context"
	"time"

	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/asdfakl/gallery/app/config"
)

const (
	ChunkSize = 100
)

type PostgresConnection struct {
	cp *pgxpool.Pool
}

type BoundedTimerange [2]time.Time

func New(ctx context.Context, cfg *config.PostgresConfiguration) (*PostgresConnection, error) {
	cp, err := pgxpool.Connect(ctx, cfg.ConnectionString())
	if err != nil {
		return nil, err
	}

	return &PostgresConnection{
		cp: cp,
	}, nil
}
