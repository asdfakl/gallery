package postgres

import (
	"context"
	"crypto/sha1"
	"errors"

	"github.com/jackc/pgx/v4"

	"gitlab.com/asdfakl/gallery/app/utils"
)

func (pg *PostgresConnection) UpsertStorageObject(ctx context.Context, storageID int64, obj *Object) (err error) {
	tx, err := pg.cp.BeginTx(ctx, pgx.TxOptions{
		IsoLevel:       pgx.ReadCommitted,
		AccessMode:     pgx.ReadWrite,
		DeferrableMode: pgx.Deferrable,
	})
	if err != nil {
		return
	}
	//nolint:errcheck
	defer tx.Rollback(ctx)

	id := int64(0)
	if err = tx.QueryRow(ctx, `SELECT id FROM gallery.object WHERE sha1 = $1`, obj.SHA1[:]).Scan(&id); err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return utils.Wrap(err, "query object id")
	}

	if id == 0 {
		qObject := `
			INSERT INTO
				gallery.object (
					sha1,
					basename,
					size,
					content_type,
					content_subtype,
					content_subtype_params,
					modified_at,
					owner,
					tags
				)
			VALUES
				($1, $2, $3, $4, $5, $6, $7, $8, $9)
			RETURNING id
		`

		if err = tx.QueryRow(
			ctx,
			qObject,
			obj.SHA1[:],
			obj.Basename,
			obj.Size,
			obj.ContentType,
			obj.ContentSubtype,
			obj.ContentSubtypeParams,
			obj.ModifiedAt,
			obj.Owner,
			obj.Tags,
		).Scan(&id); err != nil {
			return utils.Wrap(err, "insert object")
		}
	}
	obj.ID = id

	qObjectStorage := `
		INSERT INTO
			gallery.object_storage (object_id, storage_id)
		VALUES
			($1, $2)
		ON CONFLICT ON CONSTRAINT object_storage_pkey
		DO NOTHING
	`
	_, err = tx.Exec(ctx, qObjectStorage, obj.ID, storageID)
	if err != nil {
		return err
	}

	return tx.Commit(ctx)
}

func (pg *PostgresConnection) DeleteStorageObject(ctx context.Context, storageID, objectID int64) error {
	q := `
		DELETE FROM
			gallery.object_storage
		WHERE
			object_id = $1
			AND storage_id = $2
	`

	_, err := pg.cp.Exec(ctx, q, objectID, storageID)
	return err
}

func (pg *PostgresConnection) GetStorageObjectListBySHA1(ctx context.Context, storageID int64, hashes [][sha1.Size]byte) (objects ObjectList, err error) {
	q := `
		SELECT
			o.id,
			o.sha1,
			o.basename,
			o.size,
			o.content_type,
			o.content_subtype,
			o.content_subtype_params,
			o.modified_at,
			o.added_at,
			o.owner,
			o.public,
			o.tags,
			o.deleted
		FROM
			gallery.object o
		INNER JOIN
			gallery.object_storage os ON (o.id = os.object_id AND os.storage_id = $1)
		WHERE
			o.sha1 = ANY ($2)
	`

	rows, err := pg.cp.Query(ctx, q, storageID, objectSHA1ArraysToSlices(hashes))
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var object *Object

		object, err = scanObjectFromRows(rows)
		if err != nil {
			return
		}

		objects = append(objects, object)
	}

	err = rows.Err()
	return
}

func (pg *PostgresConnection) ListEveryStorageObject(ctx context.Context, storageID int64, callback func(ObjectList) error) error {
	q := `
		SELECT
			o.id,
			o.sha1,
			o.basename,
			o.size,
			o.content_type,
			o.content_subtype,
			o.content_subtype_params,
			o.modified_at,
			o.added_at,
			o.owner,
			o.public,
			o.tags,
			o.deleted
		FROM
			gallery.object o
		INNER JOIN
			gallery.object_storage os ON (o.id = os.object_id)
		WHERE
			os.storage_id = $1
	`

	rows, err := pg.cp.Query(ctx, q, storageID)
	if err != nil {
		return err
	}
	defer rows.Close()

	var objs ObjectList
	for rows.Next() {
		obj, err := scanObjectFromRows(rows)
		if err != nil {
			return err
		}

		objs = append(objs, obj)

		if len(objs) >= ChunkSize {
			if err = callback(objs); err != nil {
				return err
			}
			objs = nil
		}
	}

	if len(objs) > 0 {
		if err = callback(objs); err != nil {
			return err
		}
	}

	return rows.Err()
}
