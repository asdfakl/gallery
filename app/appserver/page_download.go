package appserver

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type DownloadPage struct {
	Page

	Filepath string
}

func DownloadPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	fpath := filepath.Join(srv.cfg.DownloadDirectory, ps.ByName("filepath"))

	// TODO REMOVE
	srv.logger.Debug("TODO", log.String("fpath", fpath))

	abs, err := filepath.Abs(fpath)
	if err != nil {
		return nil, utils.Wrap(err, "evaluate absolute filepath")
	}

	if !strings.HasPrefix(abs, srv.cfg.DownloadDirectory) {
		return nil, ErrNotFound("File not found")
	}

	return &DownloadPage{
		Filepath: abs,
	}, nil
}

func (page *DownloadPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *DownloadPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	info, err := os.Lstat(page.Filepath)
	if err != nil {
		srv.handleError(w, ErrNotFound("File not found"))
		return
	}
	if info.Mode()&os.ModeType != 0 {
		srv.handleError(w, ErrNotFound("File not found"))
		return
	}

	f, err := os.Open(page.Filepath)
	if err != nil {
		srv.handleError(w, ErrNotFound("File not found"))
		return
	}
	defer f.Close()

	http.ServeContent(w, r, info.Name(), info.ModTime(), f)
}
