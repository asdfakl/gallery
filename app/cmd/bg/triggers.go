package main

import (
	"context"
	"sync"
	"time"

	"gitlab.com/asdfakl/gallery/app/bg"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

func handleTriggers(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	wg *sync.WaitGroup,
	fatalf func(error),
	executor *bg.Executor,
	logger *log.Logger,
) {

	defer wg.Done()
	defer logger.System("stop")

	ticker := time.NewTicker(time.Minute)
	defer ticker.Stop()

	runningJobs := make(map[string]struct{})
	donec := make(chan string)
	defer close(donec) // not necessary but exposes wait failures

	logger.System("run")

trigger_select_loop:
	for {
		select {
		case <-ticker.C:
			triggers, err := pg.GetEveryBackgroundJobTrigger(ctx)
			if err != nil {
				fatalf(utils.Wrap(err, "query postgres background job triggers"))
				return
			}

			for _, trigger := range triggers {
				key := trigger.Key

				isSet, err := rmc.BackgroundTriggerIsSet(ctx, key)
				if err != nil {
					fatalf(utils.Wrap(err, "query redis background job trigger is set"))
					return
				}
				if !isSet {
					continue
				}
				if _, running := runningJobs[key]; running {
					logger.Debug("ignore trigger for running job", log.String("key", key))
					continue
				}

				runningJobs[key] = struct{}{}
				go executor.Execute(ctx, trigger.Func, pg, rmc, thumbs, trigger.Opts, func() {
					donec <- key
				})
			}
		case <-ctx.Done():
			break trigger_select_loop
		case key := <-donec:
			delete(runningJobs, key)
		}
	}

	for len(runningJobs) > 0 {
		delete(runningJobs, <-donec)
	}
}
