package bg

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"sync/atomic"
	"time"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type JobIDKey struct{}

type JobFn func(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error

type Executor struct {
	fnMap  map[postgres.BackgroundJobFnKey]JobFn
	logger *log.Logger
	jobID  int64
}

var fnKeyExp = regexp.MustCompile(`^[a-z]+(_[a-z]+)*$`)

func NewExecutor(m map[postgres.BackgroundJobFnKey]JobFn, logger *log.Logger) (*Executor, error) {
	x := &Executor{
		fnMap:  make(map[postgres.BackgroundJobFnKey]JobFn),
		logger: logger,
	}

	for k, fn := range m {
		if !fnKeyExp.MatchString(string(k)) {
			return nil, fmt.Errorf("fn key '%s' does not match expression %v", k, fnKeyExp)
		}

		if fn == nil {
			return nil, fmt.Errorf("empty job fn for function key '%s'", k)
		}

		x.fnMap[k] = fn
	}

	return x, nil
}

func (x *Executor) Execute(
	ctx context.Context,
	fnKey postgres.BackgroundJobFnKey,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	donef func(),
) {
	startTime := time.Now()
	jobID := atomic.AddInt64(&x.jobID, 1)
	logger := x.logger.With(
		log.Int64("job_id", jobID),
		log.String("fn", string(fnKey)),
	)

	ctx = context.WithValue(ctx, JobIDKey{}, jobID)

	defer func() {
		if donef != nil {
			donef()
		}
		logger.Info("stop", log.Duration("elapsed", time.Since(startTime)))
	}()

	logger.Info("run")

	fn := x.fnMap[fnKey]
	if fn == nil {
		logger.Warn("unknown job")
		return
	}

	func() {
		defer func() {
			if r := recover(); r != nil {
				logger.Error("panic", log.Err(fmt.Errorf("%v", r)))
			}
		}()

		if err := fn(ctx, pg, rmc, thumbs, opts, logger); err != nil {
			logger.Error("failed", log.Err(err))
		}
	}()
}

func TempDir() (string, error) {
	dir, err := tempDirName()
	if err != nil {
		return "", err
	}

	if err = os.MkdirAll(dir, utils.DEFAULT_DIRECTORY_PERMISSION_BITS); err != nil {
		return "", err
	}

	return dir, nil
}

func tempDirName() (string, error) {
	b := make([]byte, 16)
	n, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	if n != 16 {
		return "", fmt.Errorf("expected read of %d bytes, got %d", len(b), n)
	}

	return filepath.Join("/var/tmp/gallerybg", "tmp-"+base64.RawURLEncoding.EncodeToString(b)), nil
}
