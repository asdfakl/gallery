package appserver

import (
	"net/http"
	"path"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type PublishObjectPage struct {
	Page

	Object *postgres.ObjectView
	Public bool
}

func PublishObjectPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	sum, err := utils.ParseHexEncodedSHA1String(ps.ByName("sha1"))
	if err != nil {
		return nil, ErrNotFound("object not found")
	}

	base := path.Base(r.URL.Path)

	var public bool
	switch base {
	case "publish":
		public = true
	case "unpublish":
		public = false
	default:
		return nil, ErrNotFound("unsupported request path")
	}

	// load
	obj, err := srv.pg.GetObjectViewBySHA1(srv.appCtx, sum)
	if err != nil {
		return nil, utils.Wrap(err, "get object view by sha1")
	}
	if obj == nil {
		return nil, ErrNotFound("object not found")
	}

	return &PublishObjectPage{
		Object: obj,
		Public: public,
	}, nil
}

func (page *PublishObjectPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.Object.Owner
}

func (page *PublishObjectPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		srv.handleError(w, ErrBadRequest(err, "invalid form data"))
		return
	}

	if err := srv.validateCSRFToken(r); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	if err := srv.pg.SetObjectPublicByID(srv.appCtx, page.Object.ID, page.Public); err != nil {
		srv.handleError(w, err)
		return
	}

	// TODO accept "next" query parameter
	srv.renderJSON(w, &JSONResponse{Refresh: true}, http.StatusOK)
}
