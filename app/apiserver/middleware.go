package apiserver

import (
	"context"
	"fmt"
	"net/http"
	"path"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/asdfakl/gallery/app/jwt"
)

type ContextKeyUser struct{}
type ContextKeyToken struct{}

type AuthMiddleWare struct {
	srv            *APIServer
	handler        http.Handler
	publicPatterns []string
}

func NewAuthMiddleware(srv *APIServer, handler http.Handler) http.Handler {
	return &AuthMiddleWare{
		srv:     srv,
		handler: handler,
		publicPatterns: []string{
			srv.path("/token"),
		},
	}
}

func (mw *AuthMiddleWare) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, pattern := range mw.publicPatterns {
		if match, _ := path.Match(pattern, r.URL.Path); match {
			mw.handler.ServeHTTP(w, r)
			return
		}
	}

	authorization := r.Header.Get("Authorization")
	if !strings.HasPrefix(authorization, "Bearer ") {
		mw.srv.handleError(w, ErrUnauthorized(ERR_CODE_MISSING_TOKEN, "missing bearer token"))
		return
	}

	bearer := []byte(strings.TrimSpace(strings.TrimPrefix(authorization, "Bearer")))
	token := &TokenPayload{}

	if !jwt.VerifyJWTToken(bearer, []byte(mw.srv.cfg.Secret), token) {
		mw.srv.handleError(w, ErrUnauthorized(ERR_CODE_INVALID_TOKEN, "invalid jwt token"))
		return
	}
	if !token.Validate([]string{mw.srv.cfg.Origin}) {
		mw.srv.handleError(w, ErrUnauthorized(ERR_CODE_EXPIRED_OR_UNTRUSTED_TOKEN, "expired or untrusted jwt token"))
		return
	}

	extUserID, err := uuid.Parse(token.Subject)
	if err != nil {
		mw.srv.handleError(w, ErrUnauthorized(ERR_CODE_UNAUTHORIZED, "invalid subject"))
		return
	}

	user, err := mw.srv.pg.GetUserViewByExtID(mw.srv.appCtx, extUserID)
	if err != nil {
		mw.srv.handleError(w, fmt.Errorf("get userview by ext id: %w", err))
		return
	}
	if user == nil {
		mw.srv.handleError(w, ErrUnauthorized(ERR_CODE_UNAUTHORIZED, "user not found"))
		return
	}

	ctx := context.WithValue(
		r.Context(),
		ContextKeyUser{},
		user,
	)
	ctx = context.WithValue(
		ctx,
		ContextKeyToken{},
		token,
	)

	rr := r.Clone(ctx)

	mw.handler.ServeHTTP(w, rr)
}
