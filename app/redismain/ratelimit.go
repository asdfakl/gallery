package redismain

import (
	"context"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	RATE_LIMIT_MAX_LEN                 = 1000000
	RATE_LIMIT_CACHE_TTL time.Duration = time.Minute
)

type rateLimitCache struct {
	rdb    *redis.Client
	logger *log.Logger
	mutex  sync.RWMutex
	cached map[string]*rateLimitCacheResource
}

type rateLimitCacheResource struct {
	rdb          *redis.Client
	logger       *log.Logger
	mutex        sync.RWMutex
	resource     string
	key          string
	lastModified time.Time
	values       map[string]int
}

func (c *rateLimitCache) lookup(resource, key string) *rateLimitCacheResource {
	cacheKey := validateKey(resource + ":" + key)

	c.mutex.RLock()
	res := c.cached[cacheKey]
	c.mutex.RUnlock()

	if res != nil {
		return res
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()

	res = c.cached[cacheKey]
	if res != nil {
		return res // loaded white waiting for exclusive lock
	}

	c.logger.Debug("initialize rate limit cache resource", log.String("resource", resource), log.String("key", key))

	c.cached[cacheKey] = &rateLimitCacheResource{
		rdb:      c.rdb,
		logger:   c.logger,
		resource: resource,
		key:      key,
		values:   make(map[string]int),
	}

	return c.cached[cacheKey]
}

func (r *rateLimitCacheResource) lookup(ctx context.Context, value string, duration time.Duration) (int, error) {
	r.mutex.RLock()
	count, lastModified := r.values[value], r.lastModified
	r.mutex.RUnlock()

	if time.Since(lastModified) < RATE_LIMIT_CACHE_TTL {
		return count, nil
	}

	r.mutex.Lock()
	defer r.mutex.Unlock()

	now := time.Now()

	if now.Sub(r.lastModified) < RATE_LIMIT_CACHE_TTL {
		return r.values[value], nil // loaded while waiting for exclusive lock
	}

	k := ns.rateLimit(r.resource)

	startTime := now.Add(-duration)
	stopTime := now

	startUnixMilli := strconv.FormatInt(startTime.UnixNano()/int64(time.Millisecond), 10)
	stopUnixMilli := strconv.FormatInt(stopTime.UnixNano()/int64(time.Millisecond), 10)

	xrange := r.rdb.XRange(ctx, k, startUnixMilli, stopUnixMilli)

	msgs, err := xrange.Result()
	if err != nil {
		return 0, utils.Wrap(err, "xrange")
	}

	r.logger.Debug(
		"refresh rate limit cache resource",
		log.String("resource", r.resource),
		log.String("key", r.key),
		log.Int("message_count", len(msgs)),
	)

	r.values = make(map[string]int)
	r.lastModified = now

	for _, msg := range msgs {
		// assumption: castable to string
		v := msg.Values[r.key].(string)
		r.values[v]++
	}

	return r.values[value], nil
}

func (rmc *RedisMainConnection) IsRateLimited(ctx context.Context, resource, key, value string, max int, duration time.Duration) (bool, error) {
	k := ns.rateLimit(resource)

	if err := rmc.rdb.XAdd(ctx, &redis.XAddArgs{
		Stream:       k,
		MaxLenApprox: RATE_LIMIT_MAX_LEN,
		ID:           "*",
		Values:       []string{key, value},
	}).Err(); err != nil {
		return false, utils.Wrap(err, "xadd")
	}

	res := rmc.cachedRateLimits.lookup(resource, key)

	count, err := res.lookup(ctx, value, duration)

	return count > max, err
}
