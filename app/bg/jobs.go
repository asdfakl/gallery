package bg

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/objectstorage"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

var DefaultFunctionMapping = map[postgres.BackgroundJobFnKey]JobFn{
	postgres.BG_JOB_FN_KEY_CLEAN_UPLOADS:      JobCleanUploads,
	postgres.BG_JOB_FN_KEY_GENERATE_THUMBNAIL: JobGenerateThumbnail,
	postgres.BG_JOB_FN_KEY_IMPORT_OBJECTS:     JobImportObjects,
	postgres.BG_JOB_FN_KEY_IMPORT_UPLOAD:      JobImportUpload,
	postgres.BG_JOB_FN_KEY_SLEEP:              JobSleep,
	postgres.BG_JOB_FN_KEY_SYNC_STORAGE:       JobSyncStorage,
}

func JobGenerateThumbnail(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error {

	var (
		job   = opts.String("job")
		jobID = ctx.Value(JobIDKey{}).(int64)
	)

	hash, err := utils.ParseHexEncodedSHA1String(job)
	if err != nil {
		return utils.Wrap(err, "parse object sha1")
	}

	logger = logger.With(log.SHA1("object_sha1", hash))

	lockKey := "bg:" + string(postgres.BG_JOB_FN_KEY_GENERATE_THUMBNAIL) + ":" + utils.HexEncodeSHA1(hash)

	lock, err := rmc.Lock(ctx, lockKey, strconv.FormatInt(jobID, 10), 2*time.Minute)
	if err != nil {
		if errors.Is(err, redismain.ErrLockConflict) {
			logger.Info("lock conflict", log.String("lock_key", lockKey))
			return nil
		}
		return utils.Wrap(err, "acquire lock")
	}
	logger.Debug("lock acquired", log.String("lock_key", lockKey), log.String("lock_value", lock))

	obj, err := pg.GetObjectBySHA1(ctx, hash)
	if err != nil {
		return utils.Wrap(err, "query object by sha1")
	}
	if obj == nil {
		return fmt.Errorf("object not found by sha1 %s", utils.HexEncodeSHA1(hash))
	}

	logger = logger.With(
		log.Int64("object_id", obj.ID),
	)

	if !obj.MatchContentTypes(config.SupportedThumbnailContentTypes()) {
		return fmt.Errorf("unsupported content-type: %s/%s", obj.ContentType, obj.ContentSubtype)
	}

	srcDBStorages, err := pg.GetStorageListByObjectID(ctx, obj.ID)
	if err != nil {
		return utils.Wrap(err, "query storage list by object id")
	}
	if len(srcDBStorages) == 0 {
		return fmt.Errorf("no storages found by object id %d", obj.ID)
	}

	srcDBStorages.SortByAccessPriority()
	srcDBStorage := srcDBStorages[0]

	srcStorage, err := objectstorage.NewStorage(srcDBStorage.ID, srcDBStorage.Type, srcDBStorage.Opts, logger)
	if err != nil {
		return utils.Wrap(err, "initialize source object storage")
	}

	dir, err := TempDir()
	if err != nil {
		return utils.Wrap(err, "make temporary directory")
	}
	defer os.RemoveAll(dir)

	path := filepath.Join(dir, "download")
	n, err := srcStorage.PullObject(ctx, obj.SHA1, path)
	if err != nil {
		return utils.Wrap(err, "pull object")
	}
	if n != obj.Size {
		return fmt.Errorf("expected object of %d bytes, got %d", obj.Size, n)
	}

	switch ct := obj.ContentType + "/" + obj.ContentSubtype; ct {
	case "image/jpeg":
		return generateThumbnailImageJPEG(ctx, thumbs, obj, path, logger)
	case "image/png":
		return generateThumbnailImagePNG(ctx, thumbs, obj, path, logger)
	case "video/mp4", "video/quicktime":
		return generateThumbnailVideo(ctx, thumbs, obj, path, logger)
	default:
		return fmt.Errorf("unsupported content-type: %s", ct)
	}
}

func JobImportUpload(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error {
	var (
		job                               = opts.String("job")
		uploadDir                         = opts.String("upload_dir")
		thumbnailQueue, thumbnailQueueSet = opts.OptionalString("thumbnail_queue")
	)

	sum, err := utils.ParseHexEncodedSHA1String(job)
	if err != nil {
		return utils.Wrap(err, "parse upload sha1")
	}

	// upload from db
	upload, err := pg.GetUploadViewBySHA1(ctx, sum)
	if err != nil {
		return utils.Wrap(err, "get upload view by sha1")
	}
	if upload == nil {
		return fmt.Errorf("upload not found by sha1 %s", job)
	}
	if upload.Progress != upload.Size {
		return fmt.Errorf("expected upload progress %d, got %d", upload.Size, upload.Progress)
	}

	defer func() {
		if err = pg.InvalidateUpload(ctx, upload.SHA1); err != nil {
			logger.Error("delete upload", log.Err(err))
		}
	}()

	// object from fs
	fpath := filepath.Join(uploadDir, upload.HexSHA1())

	if !utils.IsRegularFile(fpath) {
		return fmt.Errorf("not a regular file: %s", fpath)
	}
	defer os.Remove(fpath)

	obj, err := postgres.ObjectFromLocalFile(fpath)
	if err != nil {
		return utils.Wrap(err, "object from local file")
	}

	if obj.SHA1 != upload.SHA1 {
		return fmt.Errorf(
			"object sha1 mismatch, expected %s, got %s",
			hex.EncodeToString(upload.SHA1[:]),
			hex.EncodeToString(obj.SHA1[:]),
		)
	}
	if obj.Size != upload.Size {
		return fmt.Errorf("object size mismatch, expected %d, got %d", upload.Size, obj.Size)
	}

	for i := range upload.Tags {
		upload.Tags[i] = strings.ToLower(upload.Tags[i])
	}

	obj.Basename = upload.Basename
	obj.ModifiedAt = upload.ModifiedAt
	obj.Owner = &upload.Owner
	obj.Tags = upload.Tags

	// make sure object does not already exist
	existing, err := pg.GetObjectBySHA1(ctx, obj.SHA1)
	if err != nil {
		return utils.Wrap(err, "get object by sha1")
	}
	if existing != nil {
		return fmt.Errorf("object %s already exists", obj.HexSHA1())
	}

	// figure out and initialize suitable storage
	storages, err := pg.GetStorageList(ctx)
	if err != nil {
		return utils.Wrap(err, "get storage list")
	}
	storages = storages.Filter(postgres.StorageListPredicateHasQuota)
	storages.SortByStoragePriority()
	if len(storages) == 0 {
		return fmt.Errorf("no storages found")
	}

	storage, err := objectstorage.NewStorage(storages[0].ID, storages[0].Type, storages[0].Opts, logger)
	if err != nil {
		return utils.Wrap(err, "initialize object storage")
	}

	// push object to storage, upsert, publish and request thumbnail
	if n, err := storage.PushObject(ctx, obj.SHA1, fpath); err != nil {
		return utils.Wrap(err, "push object")
	} else if n != obj.Size {
		return fmt.Errorf("expected push object of %d bytes, got %d", obj.Size, n)
	}

	if err = pg.UpsertStorageObject(ctx, storage.ID(), obj); err != nil {
		return utils.Wrap(err, "upsert object")
	}

	if err = pg.SetObjectPublicByID(ctx, obj.ID, upload.Public); err != nil {
		return utils.Wrap(err, "set object public by id")
	}

	if thumbnailQueueSet && obj.MatchContentTypes(config.SupportedThumbnailContentTypes()) {
		if err = rmc.PushBackgroundQueueItems(ctx, thumbnailQueue, obj.HexSHA1()); err != nil {
			logger.Error("push thumbnail queue item", log.Err(err))
		}
	}

	return nil
}

func JobCleanUploads(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error {
	var (
		uploadDir = opts.String("upload_dir")
	)

	knownSums := make(map[[sha1.Size]byte]struct{})

	if err := pg.ListEveryUploadView(ctx, func(uploads postgres.UploadViewList) error {
		for _, upload := range uploads {
			knownSums[upload.SHA1] = struct{}{}
		}
		return nil
	}); err != nil {
		return utils.Wrap(err, "list every upload view")
	}

	now := time.Now()

	infos, err := os.ReadDir(uploadDir)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return utils.Wrap(pg.DeleteExpiredUploads(ctx), "delete expired uploads")
		}
		return utils.Wrap(err, "read upload directory")
	}

	for _, entry := range infos {
		fpath := filepath.Join(uploadDir, entry.Name())

		if entry.IsDir() {
			logger.Debug("removing directory", log.String("path", fpath))

			if err = os.RemoveAll(fpath); err != nil {
				logger.Error("remove directory", log.String("path", fpath), log.Err(err))
			}

			continue
		}

		sum, err := utils.ParseHexEncodedSHA1String(entry.Name())
		if err != nil {
			logger.Debug("removing file", log.String("path", fpath))

			if err = os.Remove(fpath); err != nil {
				logger.Error("remove file", log.String("path", fpath), log.Err(err))
			}

			continue
		}

		if _, isKnown := knownSums[sum]; isKnown {
			continue
		}

		info, err := entry.Info()
		if err != nil {
			return utils.Wrap(err, "file info")
		}

		if age := now.Sub(info.ModTime()); age < time.Hour {
			logger.Debug(
				"not removing upload yet",
				log.SHA1("sha1", sum),
				log.Duration("age", age),
				log.Duration("max_age", time.Hour),
			)

			continue
		}

		logger.Debug("removing upload", log.SHA1("sha1", sum))

		if err = os.Remove(fpath); err != nil {
			logger.Error("remove upload", log.SHA1("sha1", sum), log.Err(err))
		}
	}

	return utils.Wrap(pg.DeleteExpiredUploads(ctx), "delete expired uploads")
}

func JobSyncStorage(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error {
	var (
		storageID = opts.Int64("storage_id")
	)

	dbStorage, err := pg.GetStorageByID(ctx, storageID)
	if err != nil {
		return utils.Wrap(err, "get storage by id")
	}
	if dbStorage == nil {
		return fmt.Errorf("storage not found by id %d", storageID)
	}

	switch dbStorage.Type {
	case postgres.StorageTypeS3:
		return syncS3Storage(ctx, pg, dbStorage, logger)
	case postgres.StorageTypeLocal:
		return syncLocalStorage(ctx, pg, dbStorage, logger)
	default:
		return fmt.Errorf("unsupported storage type '%s'", dbStorage.Type)
	}
}

// TODO detect files still in-transfer (modtime? flock? posixlock?)
func JobImportObjects(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error {
	var (
		userID                            = opts.Int64("user_id")
		uploadDir                         = opts.NonEmptyString("upload_dir")
		storageID, storageIDSet           = opts.OptionalInt64("storage_id")
		storage                           objectstorage.Storage
		thumbnailQueue, thumbnailQueueSet = opts.OptionalString("thumbnail_queue")
	)

	if storageIDSet {
		dbStorage, err := pg.GetStorageByID(ctx, storageID)
		if err != nil {
			return utils.Wrap(err, "get storage by id")
		}
		if dbStorage == nil {
			return fmt.Errorf("storage not found by id %d", storageID)
		}
		storage, err = objectstorage.NewStorage(dbStorage.ID, dbStorage.Type, dbStorage.Opts, logger)
		if err != nil {
			return utils.Wrap(err, "initialize object storage")
		}
	} else {
		storages, err := pg.GetStorageList(ctx)
		if err != nil {
			return utils.Wrap(err, "get storage list")
		}
		storages = storages.Filter(postgres.StorageListPredicateHasQuota)
		storages.SortByStoragePriority()
		if len(storages) == 0 {
			return fmt.Errorf("no storages found")
		}
		storage, err = objectstorage.NewStorage(storages[0].ID, storages[0].Type, storages[0].Opts, logger)
		if err != nil {
			return utils.Wrap(err, "initialize object storage")
		}
	}

	user, err := pg.GetUserViewByID(ctx, userID)
	if err != nil {
		return utils.Wrap(err, "get user by id")
	}
	if !user.IsValid() {
		return fmt.Errorf("user not found by id %d", userID)
	}

	if !utils.IsDirectory(uploadDir) {
		return fmt.Errorf("not a directory: %s", uploadDir)
	}

	var (
		now       = time.Now()
		atRoot    = true
		emptyDirs = map[string]bool{}
	)

	if err = filepath.Walk(uploadDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if err = ctx.Err(); err != nil {
			return err
		}

		if atRoot {
			atRoot = false
			return nil
		}

		if dir := filepath.Dir(path); emptyDirs[dir] {
			emptyDirs[dir] = false
		}

		if info.Mode()&os.ModeType != 0 {
			if info.IsDir() && now.Sub(info.ModTime()) > time.Hour {
				emptyDirs[path] = true
			}
			return nil
		}

		remove := func(path string) {
			if err = os.Remove(path); err != nil {
				logger.Error("remove", log.String("path", path), log.Err(err))
			}
		}

		if info.Size() <= 0 {
			remove(path)
			return nil
		}

		obj, err := postgres.ObjectFromLocalFile(path)
		if err != nil {
			return err
		}
		obj.Owner = &user.ID

		existing, err := pg.GetObjectBySHA1(ctx, obj.SHA1)
		if err != nil {
			return utils.Wrap(err, "get object by sha1")
		}
		if existing != nil && existing.Deleted {
			logger.Debug("skip object marked as deleted", log.SHA1("sha1", obj.SHA1), log.String("path", path))
			remove(path)
			return nil
		}

		stored, err := pg.GetStorageObjectListBySHA1(ctx, storage.ID(), [][sha1.Size]byte{obj.SHA1})
		if err != nil {
			return utils.Wrap(err, "get storage object list by sha1")
		}
		if len(stored) != 0 {
			logger.Debug("skip existing stored object", log.Int64("storage_id", storage.ID()), log.SHA1("sha1", obj.SHA1), log.String("path", path))
			remove(path)
			return nil
		}

		logger.Info("import object", log.Int64("storage_id", storage.ID()), log.SHA1("sha1", obj.SHA1), log.String("path", path))

		if n, err := storage.PushObject(ctx, obj.SHA1, path); err != nil {
			return utils.Wrap(err, "push object")
		} else if n != obj.Size {
			return fmt.Errorf("expecting push object of %d bytes, got %d", obj.Size, n)
		}

		if err = pg.UpsertStorageObject(ctx, storage.ID(), obj); err != nil {
			return utils.Wrap(err, "upsert object")
		}

		if thumbnailQueueSet && obj.MatchContentTypes(config.SupportedThumbnailContentTypes()) {
			if err = rmc.PushBackgroundQueueItems(ctx, thumbnailQueue, obj.HexSHA1()); err != nil {
				logger.Error("push thumbnail queue item", log.Err(err))
			}
		}

		remove(path)
		return nil
	}); err != nil {
		return utils.Wrap(err, "walk upload directory")
	}

	for dir, empty := range emptyDirs {
		if !empty {
			continue
		}

		logger.Debug("remove empty directory", log.String("dir", dir))
		if err = os.Remove(dir); err != nil {
			logger.Error("remove empty directory", log.String("dir", dir), log.Err(err))
		}
	}

	return nil
}

func JobSleep(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	opts config.Options,
	logger *log.Logger,
) error {
	var (
		durationLiteral, durationLiteralSet = opts.OptionalString("duration")
	)

	duration := time.Minute
	if durationLiteralSet {
		d, err := time.ParseDuration(durationLiteral)
		if err != nil {
			return utils.Wrap(err, "parse duration")
		}
		duration = d
	}

	select {
	case <-time.After(duration):
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}
