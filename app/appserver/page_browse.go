package appserver

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
)

type BrowsePage struct {
	Page

	srv    *AppServer
	Params postgres.ObjectViewListSearchParams

	Object      *postgres.ObjectView
	ObjectCount uint
	Pagination  *Pagination
}

func BrowsePageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	parser := formparser.NewQueryFormParser(r)

	var (
		search       = parser.String("search").Trim().MaxLength(64)
		contentTypes = parser.StringSlice("content-type").UniqueElements().ElementsMatchOptions([]string{
			"image",
			"video",
			"audio",
		})
		public  = parser.Bool("public")
		tagging = parser.Bool("tagging")
		offset  = parser.Uint("offset").Required()
		orderBy = parser.String("order_by").Options([]string{
			"added_at", "!added_at",
			"modified_at", "!modified_at",
			"size", "!size",
		})
	)

	if err := parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	return &BrowsePage{
		srv: srv,
		Params: postgres.ObjectViewListSearchParams{
			Search:       search.OptionalValue(),
			ContentTypes: contentTypes.Values(),
			Public:       public.OptionalValue(),
			Tagging:      tagging.OptionalValue(),
			Count:        1,
			Offset:       offset.Value(),
			OrderBy:      orderBy.OptionalValue(),
		},
	}, nil
}

func (page *BrowsePage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	page.Params.RestrictByUserID = user.ID

	return true
}

func (page *BrowsePage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	objs, err := srv.pg.SearchObjectViewList(srv.appCtx, &page.Params)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	if len(objs) > 1 {
		srv.handleError(w, errors.New("multiple objects found by search criteria"))
		return
	}
	if len(objs) != 0 {
		page.Object = objs[0]
	}

	count, err := srv.pg.CountObjectViewList(srv.appCtx, &page.Params)
	if err != nil {
		srv.handleError(w, err)
		return
	}

	page.Pagination = &Pagination{
		Count:      page.Params.Count,
		Offset:     page.Params.Offset,
		TotalCount: count,
		LinkFn:     page.Link,
	}

	srv.executePageTemplate(w, "browse.html", page, http.StatusOK)
}

func (page *BrowsePage) Link(offset uint) *url.URL {
	query := url.Values{}

	if page.Params.Search != nil {
		query.Set("search", *page.Params.Search)
	}
	if len(page.Params.ContentTypes) != 0 {
		query["content-type"] = page.Params.ContentTypes
	}
	if page.Params.Public != nil {
		query.Set("public", strconv.FormatBool(*page.Params.Public))
	}
	if page.Params.Tagging != nil {
		query.Set("tagging", strconv.FormatBool(*page.Params.Tagging))
	}
	if page.Params.OrderBy != nil {
		query.Set("order_by", *page.Params.OrderBy)
	}
	query.Set("offset", strconv.FormatUint(uint64(offset), 10))

	return &url.URL{
		Path:     page.srv.path("browse"),
		RawQuery: query.Encode(),
	}
}
