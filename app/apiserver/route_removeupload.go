package apiserver

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type RemoveUploadRoute struct {
	Route

	Upload *postgres.UploadView
}

func RemoveUploadRouteFn(srv *APIServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	sum, err := utils.ParseHexEncodedSHA1String(ps.ByName("sha1"))
	if err != nil {
		return nil, ErrNotFound("invalid sha1")
	}

	upload, err := srv.pg.GetUploadViewBySHA1(srv.appCtx, sum)
	if err != nil {
		return nil, fmt.Errorf("get upload view by sha1: %w", err)
	}
	if upload == nil {
		return nil, ErrNotFound("upload not found by sha1")
	}

	return &RemoveUploadRoute{
		Upload: upload,
	}, nil
}

func (route *RemoveUploadRoute) CanAccess(user *postgres.UserView) bool {
	return route.Upload.Owner == user.ID
}

func (route *RemoveUploadRoute) HandlePOST(srv *APIServer, w http.ResponseWriter, r *http.Request) {
	if route.Upload.Progress >= route.Upload.Size {
		srv.handleError(w, ErrConflict(ERR_CODE_UPLOAD_COMPLETE, "cannot remove completed upload"))
		return
	}

	fpath := filepath.Join(srv.cfg.UploadDir, route.Upload.HexSHA1())

	if err := os.Remove(fpath); err != nil {
		srv.logger.Warn("remove upload file", log.Err(err))
	}

	if err := srv.pg.InvalidateUpload(srv.appCtx, route.Upload.SHA1); err != nil {
		srv.handleError(w, utils.Wrap(err, "delete upload"))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
