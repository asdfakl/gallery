package utils

import (
	"encoding/base64"
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func RandBase64(length int) string {
	b := make([]byte, length)
	n, err := rand.Read(b)
	if err != nil {
		panic(Wrap(err, "rand read"))
	}
	if n != length {
		panic(fmt.Errorf("expected read of %d bytes, got %d", length, n))
	}

	return base64.RawStdEncoding.EncodeToString(b)
}
