package appserver

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type SetObjectTagsPage struct {
	Page

	Object   *postgres.ObjectView
	Next     string
	Fragment bool
}

func SetObjectTagsPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	sum, err := utils.ParseHexEncodedSHA1String(ps.ByName("sha1"))
	if err != nil {
		return nil, ErrNotFound("object not found")
	}

	parser := formparser.NewQueryFormParser(r)

	var (
		fragment = parser.Bool("fragment")
		next     = parser.String("next").Required().Options([]string{"refresh", "reload"})
	)

	if err = parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	// load
	obj, err := srv.pg.GetObjectViewBySHA1(srv.appCtx, sum)
	if err != nil {
		return nil, utils.Wrap(err, "get object view by sha1")
	}
	if obj == nil {
		return nil, ErrNotFound("object not found")
	}

	return &SetObjectTagsPage{
		Object:   obj,
		Fragment: fragment.ValueOr(false),
		Next:     next.Value(),
	}, nil
}

func (page *SetObjectTagsPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.Object.Owner
}

func (page *SetObjectTagsPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	if page.Fragment {
		srv.executePageTemplate(w, "object_tags_form_fragment.html", page, http.StatusOK)
	} else {
		srv.executePageTemplate(w, "object_tags_form.html", page, http.StatusOK)
	}
}

func (page *SetObjectTagsPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		tagsField = parser.String("tags").Trim().MaxLength(256)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	tags := strings.Fields(tagsField.ValueOr(""))
	for i := range tags {
		tags[i] = strings.ToLower(tags[i])
	}
	tags = utils.UniqueStrings(tags)

	if err := srv.pg.SetObjectTags(srv.appCtx, page.Object.ID, tags); err != nil {
		srv.handleError(w, err)
		return
	}

	switch page.Next {
	case "refresh":
		srv.renderJSON(w, &JSONResponse{Refresh: true}, http.StatusOK)
	case "reload":
		srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
	default:
		panic(fmt.Errorf("unsupported requested response: '%s'", page.Next))
	}
}
