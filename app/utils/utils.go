package utils

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/asdfakl/gallery/app/log"
)

const (
	Ki = 1024
	Mi = 1024 * Ki
	Gi = 1024 * Mi
)

var ErrInvalidContentType = errors.New("invalid content-type")

type QualifiedContentType struct {
	Type    string
	Subtype string
	Params  *string
}

func Must(err error, errctx string) {
	if err != nil {
		Die(err, errctx)
	}
}

func Die(err error, errctx string) {
	log.Default.Error("fatal", log.String("context", errctx), log.Err(err))
	os.Exit(2)
}

func Wrap(err error, msg string) error {
	if err == nil {
		return nil
	}

	msg = strings.TrimSpace(msg)
	if msg == "" {
		msg = "unknown error"
	}

	return fmt.Errorf(msg+": %w", err)
}

func MustMarshalIndentJSON(v interface{}) []byte {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		panic(err)
	}
	return b
}

func ParseHexEncodedSHA1String(encoded string) (hash [sha1.Size]byte, err error) {
	if expected := 2 * sha1.Size; len(encoded) != expected {
		err = fmt.Errorf("unexpected hex encoded sha1 string length, expecting %d, got %d", expected, len(encoded))
		return
	}

	b, err := hex.DecodeString(encoded)
	if err != nil {
		return
	}

	n := copy(hash[:], b)
	if n != sha1.Size {
		err = fmt.Errorf("expecting decoded length %d bytes, got %d", sha1.Size, n)
	}
	return
}

func HexEncodeSHA1(sum [sha1.Size]byte) string {
	return hex.EncodeToString(sum[:])
}

func Base64EncodeMD5(sum [md5.Size]byte) string {
	return base64.StdEncoding.EncodeToString(sum[:])
}

func SplitContentType(ct string) (*QualifiedContentType, error) {
	parts := strings.SplitN(ct, "/", 2)

	if len(parts) != 2 {
		return nil, ErrInvalidContentType
	}

	parts[0] = strings.TrimSpace(parts[0])
	if parts[0] == "" {
		return nil, ErrInvalidContentType
	}

	subParts := strings.Split(parts[1], ";")
	if len(subParts) == 0 {
		return nil, ErrInvalidContentType
	}

	subParts[0] = strings.TrimSpace(subParts[0])
	if subParts[0] == "" {
		return nil, ErrInvalidContentType
	}

	var params []string
	for _, param := range subParts[1:] {
		param = strings.TrimSpace(param)
		if param != "" {
			params = append(params, param)
		}
	}

	qct := &QualifiedContentType{
		Type:    parts[0],
		Subtype: subParts[0],
	}
	if len(params) != 0 {
		joined := strings.Join(params, "; ")
		qct.Params = &joined
	}

	return qct, nil
}

func UniqueStrings(ls []string) (uniq []string) {
	m := make(map[string]struct{})

	for _, s := range ls {
		if _, ok := m[s]; ok {
			continue
		}

		uniq = append(uniq, s)
		m[s] = struct{}{}
	}

	return
}
