package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"golang.org/x/sync/errgroup"

	"gitlab.com/asdfakl/gallery/app/utils"
)

func must(err error, errctx string) {
	if err != nil {
		die(err, errctx)
	}
}

func die(err error, errctx string) {
	fmt.Fprintf(os.Stderr, "fatal: %s: %v\n", errctx, err)
	os.Exit(2)
}

func main() {
	startTime := time.Now()

	cfg, err := loadConfiguration()
	must(err, "load configuration")

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	errg, errgCtx := errgroup.WithContext(appCtx)

	uploadsWG := &sync.WaitGroup{}
	objectc := make(chan *Object, cfg.Concurrency.UploadWorkerCount)

	tokenService := NewTokenService(cfg)
	readerService := NewReaderService(cfg, objectc)
	uploadServices := make([]*UploadService, cfg.Concurrency.UploadWorkerCount)
	for i := range uploadServices {
		uploadServices[i] = NewUploadService(cfg, i, uploadsWG, objectc, tokenService)
	}

	go listenSignal(cancelAppCtx, cfg, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	go listenUploadsWG(cancelAppCtx, cfg, uploadsWG)
	tokenService.Start(errgCtx, errg)
	readerService.Start(errgCtx, errg)
	for _, service := range uploadServices {
		service.Start(errgCtx, errg)
	}

	err = errg.Wait()

	cfg.Progress(fmt.Sprintf("Total:          %d %s", cfg.Metrics.Total.Count, utils.HumanFilesize(cfg.Metrics.Total.Bytes)))
	cfg.Progress(fmt.Sprintf("Read Success:   %d %s", cfg.Metrics.ReadSuccess.Count, utils.HumanFilesize(cfg.Metrics.ReadSuccess.Bytes)))
	cfg.Progress(fmt.Sprintf("Read Failure:   %d %s", cfg.Metrics.ReadFailure.Count, utils.HumanFilesize(cfg.Metrics.ReadFailure.Bytes)))
	cfg.Progress(fmt.Sprintf("Read Duplicate: %d %s", cfg.Metrics.ReadDuplicate.Count, utils.HumanFilesize(cfg.Metrics.ReadDuplicate.Bytes)))
	cfg.Progress(fmt.Sprintf("Upload Success: %d %s", cfg.Metrics.UploadSuccess.Count, utils.HumanFilesize(cfg.Metrics.UploadSuccess.Bytes)))
	cfg.Progress(fmt.Sprintf("Upload Failure: %d %s", cfg.Metrics.UploadFailure.Count, utils.HumanFilesize(cfg.Metrics.UploadFailure.Bytes)))
	cfg.Progress(fmt.Sprintf("Upload Skipped: %d %s", cfg.Metrics.UploadSkipped.Count, utils.HumanFilesize(cfg.Metrics.UploadSkipped.Bytes)))
	cfg.Progress(fmt.Sprintf("Total Duration: %v", time.Since(startTime)))

	must(err, "run")
}

func listenSignal(cancel func(), cfg *Configuration, signals ...os.Signal) {
	defer cancel()

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, signals...)

	cfg.Error(fmt.Sprintf("Got signal: %v", <-sigc))
}

func listenUploadsWG(cancel func(), cfg *Configuration, wg *sync.WaitGroup) {
	defer cancel()

	wg.Wait()

	cfg.Success("All upload services complete")
}
