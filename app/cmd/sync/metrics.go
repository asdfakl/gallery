package main

import "sync/atomic"

type Metrics struct {
	Total Metric

	ReadSuccess   Metric
	ReadFailure   Metric
	ReadDuplicate Metric

	UploadSuccess Metric
	UploadFailure Metric
	UploadSkipped Metric
}

type Metric struct {
	Count int32
	Bytes int64
}

func (m *Metric) Increment(bytes int64) {
	atomic.AddInt32(&m.Count, 1)
	atomic.AddInt64(&m.Bytes, bytes)
}
