package redismain

const LUA_DEL_IF_EQUAL = `
if redis.call("get", KEYS[1]) == ARGV[1] then
    return redis.call("del", KEYS[1])
else
    return 0
end
`

const LUA_REFRESH_IF_EQUAL = `
if redis.call("get", KEYS[1]) == ARGV[1] then
	return redis.call("expireat", KEYS[1], ARGV[2])
else
	return 0
end
`
