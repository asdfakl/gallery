package apiserver

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const MAX_UPLOAD_PART_SIZE = 128 * 1024 * 1024

type AddUploadPartRoute struct {
	Route

	Upload   *postgres.UploadView
	Offset   int64
	PartSize int64
}

func AddUploadPartRouteFn(srv *APIServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	sum, err := utils.ParseHexEncodedSHA1String(ps.ByName("sha1"))
	if err != nil {
		return nil, ErrNotFound("invalid sha1")
	}

	upload, err := srv.pg.GetUploadViewBySHA1(srv.appCtx, sum)
	if err != nil {
		return nil, fmt.Errorf("get upload view by sha1: %w", err)
	}
	if upload == nil {
		return nil, ErrNotFound("upload not found by sha1")
	}

	parser := formparser.NewQueryFormParser(r)

	var (
		offset   = parser.Int64("offset").Required().Min(0).Max(upload.Size - 1)
		partSize = parser.Int64("part_size").Required().Min(1).Max(MAX_UPLOAD_PART_SIZE)
	)

	if err := parser.Err(); err != nil {
		return nil, ErrBadRequest(err, "invalid query parameters")
	}

	return &AddUploadPartRoute{
		Upload:   upload,
		Offset:   offset.Value(),
		PartSize: partSize.Value(),
	}, nil
}

func (route *AddUploadPartRoute) CanAccess(user *postgres.UserView) bool {
	return route.Upload.Owner == user.ID
}

func (route *AddUploadPartRoute) HandlePOST(srv *APIServer, w http.ResponseWriter, r *http.Request) {
	if route.Offset != route.Upload.Progress {
		srv.handleError(w, ErrConflict(ERR_CODE_UPLOAD_CONFLICT, fmt.Sprintf("expected offset %d, got %d", route.Upload.Progress, route.Offset)))
		return
	}

	if route.Offset+route.PartSize > route.Upload.Size {
		srv.handleError(w, ErrBadRequest(nil, "part would exceed upload size"))
		return
	}

	if err := os.MkdirAll(srv.cfg.UploadDir, 0755); err != nil {
		srv.handleError(w, fmt.Errorf("mkdir: %w", err))
		return
	}

	fpath := filepath.Join(srv.cfg.UploadDir, route.Upload.HexSHA1())

	var (
		err error
		f   *os.File
	)

	if route.Offset == 0 {
		f, err = os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0644)
		if err != nil {
			os.Remove(fpath)
			srv.handleError(w, fmt.Errorf("open: %w", err))
			return
		}
	} else {
		f, err = os.OpenFile(fpath, os.O_WRONLY, 0644)
		if err != nil {
			srv.handleError(w, fmt.Errorf("open: %w", err))
			return
		}

		offset, err := f.Seek(route.Offset, 0)
		if err != nil {
			srv.handleError(w, fmt.Errorf("seek: %w", err))
			return
		}

		if offset != route.Offset {
			srv.handleError(w, fmt.Errorf("expected offset %d, got %d", route.Offset, offset))
			return
		}
	}
	defer f.Close()

	h := md5.New()
	multi := io.MultiWriter(f, h)

	n, err := io.CopyN(multi, r.Body, route.PartSize)
	if err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}
	if n != route.PartSize {
		srv.handleError(w, ErrBadRequest(nil, fmt.Sprintf("expecting read of %d bytes, got %d", route.PartSize, n)))
		return
	}

	route.Upload.Progress += n

	if err = srv.pg.ModifyUpload(srv.appCtx, route.Upload); err != nil {
		srv.handleError(w, fmt.Errorf("modify upload: %w", err))
		return
	}

	if route.Upload.Progress >= route.Upload.Size {
		queue, err := srv.pg.GetBackgroundJobQueueByFunc(srv.appCtx, postgres.BG_JOB_FN_KEY_IMPORT_UPLOAD)
		if err != nil {
			srv.handleError(w, utils.Wrap(err, "get background job queue by func"))
			return
		}
		if queue != nil {
			if err = srv.rmc.PushBackgroundQueueItems(srv.appCtx, queue.Key, route.Upload.HexSHA1()); err != nil {
				srv.handleError(w, utils.Wrap(err, "push background queue items"))
				return
			}
		}
	}

	p := &struct {
		SHA1     string `json:"sha1"`
		PartMD5  string `json:"part_md5"`
		Progress int64  `json:"progress"`
	}{
		SHA1:     route.Upload.HexSHA1(),
		PartMD5:  hex.EncodeToString(h.Sum(nil)),
		Progress: route.Upload.Progress,
	}

	srv.jsonResponse(w, p)
}
