package utils

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"

	"github.com/gabriel-vasile/mimetype"
)

const (
	DEFAULT_FILE_PERMISSION_BITS      = 0640
	DEFAULT_DIRECTORY_PERMISSION_BITS = 0750
)

func Copy(src, dst string) (int64, error) {
	info, err := os.Lstat(src)
	if err != nil {
		return 0, err
	}

	if info.Mode()&os.ModeType != 0 {
		return 0, fmt.Errorf("not a regular file: %s", src)
	}

	// no copy if hard-linking succeeds
	if err = os.Link(src, dst); err == nil {
		return info.Size(), nil
	}

	srcf, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer srcf.Close()

	dstf, err := os.OpenFile(dst, os.O_CREATE|os.O_WRONLY, DEFAULT_FILE_PERMISSION_BITS)
	if err != nil {
		return 0, err
	}
	defer dstf.Close()

	return io.Copy(dstf, srcf)
}

func IsDirectory(path string) bool {
	info, err := os.Lstat(path)
	if err != nil {
		return false
	}

	return info.IsDir()
}

func IsRegularFile(path string) bool {
	info, err := os.Lstat(path)
	if err != nil {
		return false
	}
	return info.Mode()&os.ModeType == 0
}

func IsSymlink(path string) bool {
	info, err := os.Lstat(path)
	if err != nil {
		return false
	}
	return info.Mode()&os.ModeSymlink == os.ModeSymlink
}

func DetectContentType(path string) (string, error) {
	mtype, err := mimetype.DetectFile(path)
	return mtype.String(), err
}

func HumanFilesize(size int64) string {
	if size < Ki {
		return fmt.Sprintf("%d B", size)
	}

	f := float64(size)
	if size < Mi {
		return fmt.Sprintf("%.2f kiB", f/Ki)
	}
	if size < Gi {
		return fmt.Sprintf("%.2f MiB", f/Mi)
	}
	return fmt.Sprintf("%.2f GiB", f/Gi)
}

func HumanPercentage(f float64) string {
	return fmt.Sprintf("%.1f %%", f*100)
}

func ReadMD5(path string) (sum [md5.Size]byte, err error) {
	f, err := os.Open(path)
	if err != nil {
		return
	}
	defer f.Close()

	h := md5.New()

	_, err = io.Copy(h, f)
	if err != nil {
		return
	}

	copy(sum[:], h.Sum(nil))
	return
}
