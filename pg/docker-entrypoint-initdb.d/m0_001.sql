START TRANSACTION;

---------------------------------------------------------------------------------------------------------------------------------------
-- user

ALTER TABLE gallery.user ADD COLUMN quota_object_count INTEGER;
ALTER TABLE gallery.user ADD COLUMN quota_object_bytes BIGINT;

UPDATE gallery.user SET quota_object_count = 10 * 1000;
UPDATE gallery.user SET quota_object_bytes = 10 * 1024 * 1024;

ALTER TABLE gallery.user ALTER COLUMN quota_object_count SET NOT NULL;
ALTER TABLE gallery.user ALTER COLUMN quota_object_bytes SET NOT NULL;

ALTER TABLE gallery.user ADD CONSTRAINT quota_object_count_check CHECK (quota_object_count >= 0);
ALTER TABLE gallery.user ADD CONSTRAINT quota_object_bytes_check CHECK (quota_object_bytes >= 0);

---------------------------------------------------------------------------------------------------------------------------------------
-- storage

ALTER TABLE gallery.storage ADD COLUMN quota_object_count INTEGER;
ALTER TABLE gallery.storage ADD COLUMN quota_object_bytes BIGINT;

UPDATE gallery.storage SET quota_object_count = 1000 * 1000;
UPDATE gallery.storage SET quota_object_bytes = 100 * 1024 * 1024;

ALTER TABLE gallery.storage ALTER COLUMN quota_object_count SET NOT NULL;
ALTER TABLE gallery.storage ALTER COLUMN quota_object_bytes SET NOT NULL;

ALTER TABLE gallery.storage ADD CONSTRAINT quota_object_count_check CHECK (quota_object_count >= 0);
ALTER TABLE gallery.storage ADD CONSTRAINT quota_object_bytes_check CHECK (quota_object_bytes >= 0);

---------------------------------------------------------------------------------------------------------------------------------------
-- v_storage

CREATE MATERIALIZED VIEW gallery.v_storage AS
	SELECT 
		s.id,
		s.type,
		s.access_priority,
		s.storage_priority,
		s.name,
		COALESCE(s.description, '') AS description,
		s.opts,
		s.quota_object_count,
		s.quota_object_bytes,
		COUNT(o.id)::INTEGER AS object_count,
		COALESCE(SUM(o.size), 0)::BIGINT AS object_bytes
	FROM
		gallery.storage s
	LEFT JOIN
		gallery.object_storage os ON (s.id = os.storage_id)
	LEFT JOIN
		gallery.object o ON (os.object_id = o.id)
	GROUP BY
		s.id, s.type, s.access_priority, s.storage_priority, s.name, description, s.opts, s.quota_object_count, s.quota_object_bytes;

-- required for concurrent refresh
CREATE UNIQUE INDEX v_storage_pkey ON gallery.v_storage (id); 

CREATE FUNCTION gallery.refresh_v_storage()
	RETURNS TRIGGER
	SECURITY DEFINER
	LANGUAGE PLPGSQL
AS $$
BEGIN
	REFRESH MATERIALIZED VIEW CONCURRENTLY gallery.v_storage;
	RETURN NULL;
END;
$$;

CREATE TRIGGER refresh_v_storage
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON gallery.storage
	FOR EACH STATEMENT
	EXECUTE FUNCTION gallery.refresh_v_storage();

CREATE TRIGGER refresh_v_storage
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON gallery.object
	FOR EACH STATEMENT
	EXECUTE FUNCTION gallery.refresh_v_storage();

CREATE TRIGGER refresh_v_storage
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON gallery.object_storage
	FOR EACH STATEMENT
	EXECUTE FUNCTION gallery.refresh_v_storage();

REFRESH MATERIALIZED VIEW gallery.v_storage;

GRANT SELECT ON gallery.v_storage TO gallery_app;

---------------------------------------------------------------------------------------------------------------------------------------
-- v_user

DROP VIEW gallery.v_user;

CREATE MATERIALIZED VIEW gallery.v_user AS
	SELECT
		u.id,
		u.ext_id,
		u.username,
		u.password,
		COALESCE(u.display_name, u.username) AS display_name,
		u.quota_object_count,
		u.quota_object_bytes,
		COUNT(DISTINCT o.id)::INTEGER AS object_count,
		COALESCE(SUM(CASE WHEN os.storage_id IS NULL THEN NULL ELSE o.size END), 0)::BIGINT AS object_bytes
	FROM
		gallery.user u
	LEFT JOIN
		gallery.object o ON (u.id = o.owner)
	LEFT JOIN
		gallery.object_storage os ON (o.id = os.object_id)
	WHERE
		u.validity_period @> now()
	GROUP BY
		u.id, u.ext_id, u.username, u.password, display_name, u.quota_object_count, u.quota_object_bytes;

-- required for concurrent refresh
CREATE UNIQUE INDEX v_user_pkey ON gallery.v_user (id);

CREATE FUNCTION gallery.refresh_v_user()
	RETURNS TRIGGER
	SECURITY DEFINER
	LANGUAGE PLPGSQL
AS $$
BEGIN
	REFRESH MATERIALIZED VIEW CONCURRENTLY gallery.v_user;
	RETURN NULL;
END;
$$;

CREATE TRIGGER refresh_v_user
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON gallery.user
	FOR EACH STATEMENT
	EXECUTE FUNCTION gallery.refresh_v_user();

CREATE TRIGGER refresh_v_user
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON gallery.object
	FOR EACH STATEMENT
	EXECUTE FUNCTION gallery.refresh_v_user();

CREATE TRIGGER refresh_v_user
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON gallery.object_storage
	FOR EACH STATEMENT
	EXECUTE FUNCTION gallery.refresh_v_user();

REFRESH MATERIALIZED VIEW gallery.v_user;

GRANT SELECT ON gallery.v_user TO gallery_app;

---------------------------------------------------------------------------------------------------------------------------------------
-- upload

CREATE TABLE gallery.upload (
	sha1 BYTEA NOT NULL CHECK ( LENGTH(sha1) = 20 ),
	basename TEXT NOT NULL CHECK ( TRIM(basename) != '' AND LENGTH(basename) < 256 ),
	size BIGINT NOT NULL CHECK ( size > 0 ),
  modified_at TIMESTAMPTZ NOT NULL,
  progress BIGINT NOT NULL DEFAULT 0 CHECK ( progress >= 0 AND progress <= size ),
	owner INTEGER NOT NULL,
	public BOOLEAN NOT NULL,
	tags TEXT[],
  validity_period TSTZRANGE NOT NULL CHECK (
    NOT lower_inf(validity_period)
    AND lower_inc(validity_period)
    AND NOT upper_inf(validity_period)
    AND NOT upper_inc(validity_period)
  ),

  EXCLUDE USING GIST (sha1 WITH =, validity_period WITH &&),

  FOREIGN KEY (owner) REFERENCES gallery.user (id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- foreign key indexes
CREATE INDEX ON gallery.upload (owner);

---------------------------------------------------------------------------------------------------------------------------------------
-- v_upload

CREATE OR REPLACE VIEW gallery.v_upload AS
  SELECT
    u.sha1,
    u.basename,
    u.size,
    u.modified_at,
    u.progress,
    u.owner,
    u.public,
    u.tags
  FROM
    gallery.upload u
  WHERE
    u.validity_period @> now();

COMMIT;
