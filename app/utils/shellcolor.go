package utils

type ShellColor string

const (
	ShellColorNone   ShellColor = ""
	ShellColorNormal ShellColor = "\x1b[00m"

	ShellColorBoldCyan      ShellColor = "\x1b[01;36m"
	ShellColorBoldGreen     ShellColor = "\x1b[01;32m"
	ShellColorBoldLightGray ShellColor = "\x1b[01;30m"
	ShellColorBoldRed       ShellColor = "\x1b[01;31m"
	ShellColorBold          ShellColor = "\x1b[01m"
)
