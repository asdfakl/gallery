#!/bin/bash

script_dir="$(dirname "$(realpath "$0")")"

cd "$script_dir" || exit 1

chmod 0644 ./docker-entrypoint-initdb.d/*.sql ./docker-entrypoint-initdb.d/*.conf || exit 1

chmod 0755 ./docker-entrypoint-initdb.d/*.sh || exit 1

docker run -d \
	--net asdfakl-gallery \
	--hostname postgres \
	--env-file ./example.env \
	--name asdfakl-gallery-postgres \
	--mount "type=bind,src=$(pwd)/docker-entrypoint-initdb.d,dst=/docker-entrypoint-initdb.d,readonly" \
	--mount type=volume,src=asdfakl-gallery-postgres-data,dst=/var/lib/postgresql/data \
	--log-opt mode=non-blocking \
	--log-opt max-buffer-size=2m \
	postgres:13.8
