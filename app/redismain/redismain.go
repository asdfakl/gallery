package redismain

import (
	"context"
	"strconv"

	"github.com/go-redis/redis/v8"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type RedisMainConnection struct {
	rdb    *redis.Client
	logger *log.Logger

	cachedRateLimits *rateLimitCache

	scriptDelIfEqual     *redis.Script
	scriptRefreshIfEqual *redis.Script
}

func New(ctx context.Context, cfg *config.RedisConfiguration, logger *log.Logger) (*RedisMainConnection, error) {
	rdb := redis.NewClient(&redis.Options{
		Network:  "tcp",
		Addr:     cfg.Host + ":" + strconv.Itoa(cfg.Port),
		Username: cfg.Username,
		Password: cfg.Password,
		DB:       cfg.Database,
	})

	if err := rdb.Ping(ctx).Err(); err != nil {
		return nil, utils.Wrap(err, "ping")
	}

	return &RedisMainConnection{
		rdb:    rdb,
		logger: logger,

		cachedRateLimits: &rateLimitCache{
			rdb:    rdb,
			logger: logger,
			cached: make(map[string]*rateLimitCacheResource),
		},

		scriptDelIfEqual:     redis.NewScript(LUA_DEL_IF_EQUAL),
		scriptRefreshIfEqual: redis.NewScript(LUA_REFRESH_IF_EQUAL),
	}, nil
}
