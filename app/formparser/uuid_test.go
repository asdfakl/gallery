package formparser

import (
	"errors"
	"testing"

	"github.com/google/uuid"
)

func TestFormUUIDField(t *testing.T) {
	ErrTest := errors.New("some error")

	src := func(key string) ([]string, error) {
		if key == "error" {
			return nil, ErrTest
		}

		return (map[string][]string{
			"invalid": {"clearly not UUID but length does fit"},
			"multiple": {
				"deadbeef-feed-dead-beef-9128ab123932",
				"deadbeef-feed-dead-beef-eff912380128",
			},
			"valid": {"deadbeef-feed-dead-beef-9128ab123932"},
		})[key], nil
	}

	parser := &FormParser{
		src:  src,
		errs: make(map[string][]error),
	}

	for _, tc := range []struct {
		key            string
		field          *FormUUIDField
		expectedValue  uuid.UUID
		expectedErrors []error
	}{
		{
			key:            "error",
			field:          parser.UUID("error"),
			expectedValue:  uuid.Nil,
			expectedErrors: []error{errors.New("parse form: some error")},
		},
		{
			key:           "missing_not_required",
			field:         parser.UUID("missing_not_required"),
			expectedValue: uuid.Nil,
		},
		{
			key:            "missing_required",
			field:          parser.UUID("missing_required").Required(),
			expectedValue:  uuid.Nil,
			expectedErrors: []error{ErrFormParserRequired},
		},
		{
			key:            "invalid",
			field:          parser.UUID("invalid"),
			expectedValue:  uuid.Nil,
			expectedErrors: []error{errors.New("parse uuid: invalid UUID format")},
		},
		{
			key:            "multiple",
			field:          parser.UUID("multiple"),
			expectedValue:  uuid.Nil,
			expectedErrors: []error{ErrFormParserAmbiguous},
		},
		{
			key:           "valid",
			field:         parser.UUID("valid").Required(),
			expectedValue: uuid.MustParse("deadbeef-feed-dead-beef-9128ab123932"),
		},
	} {
		_ = parser.Err() // accessing field value without checking error first will panic

		if tc.field.Value() != tc.expectedValue {
			t.Errorf("key %s expected value mismatch, expected %v, got %v", tc.key, tc.expectedValue, tc.field.Value())
		}

		if len(tc.expectedErrors) != len(parser.errs[tc.key]) {
			t.Errorf("key %s expected error count mismatch, expected %d, got %d", tc.key, len(tc.expectedErrors), len(parser.errs[tc.key]))
			continue
		}

		for i := range tc.expectedErrors {
			expected, got := tc.expectedErrors[i], parser.errs[tc.key][i]

			if expected.Error() != got.Error() {
				t.Errorf("key %s expected error index %d mismatch, expected '%s', got '%s'", tc.key, i, expected.Error(), got.Error())
			}
		}
	}
}
