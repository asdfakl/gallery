package config

import (
	"errors"
	"flag"
	"strings"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type S3Configuration struct {
	AccessKey string
	Endpoint  string
	Region    string
	SecretKey string
}

func (cfg *S3Configuration) InitFlagSet(fs *flag.FlagSet) {
	utils.StringVarOrEnv(fs, &cfg.AccessKey, "s3-access-key", cfg.AccessKey, "S3 object storage access key")
	utils.StringVarOrEnv(fs, &cfg.Endpoint, "s3-endpoint", cfg.Endpoint, "S3 object storage endpoint")
	utils.StringVarOrEnv(fs, &cfg.Region, "s3-region", cfg.Region, "S3 object storage region")
	utils.StringVarOrEnv(fs, &cfg.SecretKey, "s3-secret-key", cfg.SecretKey, "S3 object storage secret key")
}

func (cfg *S3Configuration) Trim() {
	cfg.AccessKey = strings.TrimSpace(cfg.AccessKey)
	cfg.Endpoint = strings.TrimSpace(cfg.Endpoint)
	cfg.Region = strings.TrimSpace(cfg.Region)
	cfg.SecretKey = strings.TrimSpace(cfg.SecretKey)
}

func (cfg *S3Configuration) Validate() error {
	for _, assert := range []assertion{
		{cfg.AccessKey != "", errors.New("s3-access-key empty or undefined")},
		{cfg.Endpoint != "", errors.New("s3-endpoint empty or undefined")},
		{cfg.Region != "", errors.New("s3-region empty or undefined")},
		{cfg.SecretKey != "", errors.New("s3-secret-key empty or undefined")},
	} {
		if !assert.ok {
			return assert.err
		}
	}
	return nil
}
