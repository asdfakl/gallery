"use strict";

var Common = (function() {
	const initJSPostLink = nodes => {
		nodes.forEach(node => {
			const href = node.attributes["href"].nodeValue;

			let form = node.attributes["data-form"];
			form = form && form.nodeValue;

			let next = node.attributes["data-next"];
			next = next && next.nodeValue;
			next = next && JSON.parse(next);

			node.addEventListener("click", e => {
				e.preventDefault();

				const opts = {
					method: "POST",
					credentials: "same-origin",
					redirect: "manual",
					headers: form ? {
						"Content-Type": "application/x-www-form-urlencoded",
					} : undefined,
					body: form || null
				};

				fetch(href, opts).then(Utils.handleFetchError).then(res => res.json()).then(res => {
					Utils.handleFetchSuccess(next || res, node);
				}).catch(err => {
					console.error(`js-post-link fetch failed: ${err}`);
				});
			});
		});
	};

	let jsModalLinkOpener = null;

	const initJSModalLink = nodes => {
		nodes.forEach(node => {
			const href = node.attributes["href"].nodeValue;

			let targetID = node.attributes["data-target-id"];
			targetID = targetID && targetID.nodeValue;

			const modal = document.getElementById("js-content-modal");
			const container = modal.querySelector("div.modal-content");

			node.addEventListener("click", e => {
				e.preventDefault();

				fetch(href).then(Utils.handleFetchError).then(res => res.text()).then(res => {
					const parsed = new DOMParser().parseFromString(res, "text/html");

					const result = targetID ? parsed.getElementById(targetID) : parsed;
					if (!result) {
						throw targetID ? `response element not found by id '${targetID}'` : `no response element`;
					}

					container.replaceChildren(...result.children);

					jsModalLinkOpener = node;
					modal.classList.add("is-active");
					Common.initHTMLElem(container);
				}).catch(err => {
					console.error(`js-modal-link fetch failed: ${err}`);
				});
			});
		});
	};

	const initJSModalForm = nodes => {
		nodes.forEach(node => {
			const action = node.attributes["action"].nodeValue;

			let next = node.attributes["data-next"];
			next = next && next.nodeValue;
			next = next && JSON.parse(next);

			const modal = node.closest("div.modal");

			const hiddenInputs = node.querySelectorAll("input[type=hidden]");
			const textInputs = node.querySelectorAll("input[type=text]");
			const passwordInputs = node.querySelectorAll("input[type=password]");
			const selects = node.querySelectorAll("select");
			const textareas = node.querySelectorAll("textarea");

			const base = {};

			hiddenInputs.forEach(input => {
				base[input.name] = input.value;
			});

			node.addEventListener("submit", e => {
				e.preventDefault();

				const q = { ...base };

				textInputs.forEach(input => {
					const key = input.name;
					const val = input.value.trim();

					if (val) {
						q[key] = val;
					}
				});

				passwordInputs.forEach(input => {
					const key = input.name;
					const val = input.value.trim();

					if (val) {
						q[key] = val;
					}
				});

				selects.forEach(select => {
					const key = select.name;
					const val = select.value;

					if (val) {
						q[key] = val;
					}
				});

				textareas.forEach(textarea => {
					const key = textarea.name;
					const val = textarea.value;

					if (val) {
						q[key] = val;
					}
				});

				const opts = {
					method: "POST",
					credentials: "same-origin",
					redirect: "manual",
					headers: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
					body: Utils.queryString(q),
				};

				fetch(action, opts).then(res => {
					modal.classList.remove("is-active");

					return Utils.handleFetchError(res);
				}).then(res => res.json()).then(res => {
					Utils.handleFetchSuccess(next || res, jsModalLinkOpener || node);
				}).catch(err => {
					console.error(`js-modal-form fetch failed: ${err}`);
				});
			});
		});
	};

	const initJSTabs = nodes => {
		nodes.forEach(node => {
			const tabs = node.querySelectorAll("li");

			tabs.forEach(tab => {
				tab.addEventListener("click", e => {
					const target = e.target.closest("li");

					if (target.classList.contains("is-active")) {
						return;
					}

					tabs.forEach(other => {
						const pane = document.getElementById(other.attributes["data-target"].nodeValue);

						if (other === target) {
							other.classList.add("is-active");
							pane.classList.add("is-active");
							pane.style.display = "block";
						} else {
							other.classList.remove("is-active");
							pane.classList.remove("is-active");
							pane.style.display = "none";
						}
					});
				});
			});
		});
	};

	const initJSPaginationLink = nodes => {
		nodes.forEach(node => {
			let offset = node.attributes["data-offset"];
			offset = offset && Number(offset.nodeValue);

			if (typeof offset !== "number") {
				return;
			}

			node.addEventListener("click", e => {
				e.preventDefault();

				const event = new CustomEvent("RequestPaginationOffset", {
					bubbles: true,
					detail: {
						offset,
					},
				});

				node.dispatchEvent(event);
			});
		});
	};

	const initJSModalClose = nodes => {
		nodes.forEach(node => {
			node.addEventListener("click", e => {
				node.closest("div.modal").classList.remove("is-active");
			});
		});
	};

	const initJSDropdown = (elem, nodes) => {
		if (!nodes.length) {
			return;
		}

		nodes.forEach(node => {
			const trigger = node.querySelector("div.dropdown-trigger");

			const closeExcept = except => {
				nodes.forEach(node => {
					if (node !== except) {
						node.classList.remove("is-active");
					}
				});
			};

			trigger.addEventListener("click", e => {
				e.stopPropagation();

				node.classList.toggle("is-active");

				closeExcept(node);
			});

			elem.addEventListener("click", e => {
				closeExcept(null);
			});
		});
	};

	const initJSNavbarBurger = (elem, burger) => {
		if (!burger) {
			return;
		}

		const target = elem.querySelector(burger.attributes["data-target"].nodeValue);

		burger.addEventListener("click", e => {
			burger.classList.toggle("is-active");
			target.classList.toggle("is-active");
		});
	};

	const initHTMLElemHooks = [];

	// TODO unmountHTMLElem
	return Object.freeze({
		initHTMLElem: elem => {
			initJSPostLink(elem.querySelectorAll(".js-post-link"));

			initJSModalForm(elem.querySelectorAll(".js-modal-form"));

			initJSTabs(elem.querySelectorAll(".js-tabs"));

			initJSPaginationLink(elem.querySelectorAll(".js-pagination-link"));

			initJSModalClose(elem.querySelectorAll(".js-modal-close"));

			initJSModalLink(elem.querySelectorAll(".js-modal-link"));

			initJSDropdown(elem, elem.querySelectorAll(".js-dropdown"));

			initJSNavbarBurger(elem, elem.querySelector(".js-navbar-burger"));

			const autofocus = elem.querySelector(".js-autofocus");
			if (autofocus) {
				autofocus.focus();
			}

			initHTMLElemHooks.forEach(hook => {
				hook(elem);
			});
		},

		registerInitHTMLElemHook: hook => {
			initHTMLElemHooks.push(hook);
		},
	});
})();

document.addEventListener("DOMContentLoaded", () => {
	Common.initHTMLElem(document.querySelector("body"));
});
