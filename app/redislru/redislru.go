package redislru

import (
	"context"
	"errors"
	"strconv"

	"github.com/go-redis/redis/v8"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const MaxValueSize = 64 * 1024

var ErrEmpty = errors.New("value is empty")
var ErrTooBig = errors.New("max value size " + strconv.Itoa(MaxValueSize) + " exceeded")

type RedisLRUConnection struct {
	rdb *redis.Client
}

func New(ctx context.Context, cfg *config.RedisConfiguration) (*RedisLRUConnection, error) {
	rdb := redis.NewClient(&redis.Options{
		Network:  "tcp",
		Addr:     cfg.Host + ":" + strconv.Itoa(cfg.Port),
		Username: cfg.Username,
		Password: cfg.Password,
		DB:       cfg.Database,
	})

	if err := rdb.Ping(ctx).Err(); err != nil {
		return nil, utils.Wrap(err, "ping")
	}

	return &RedisLRUConnection{
		rdb: rdb,
	}, nil
}

func (rlc *RedisLRUConnection) Set(ctx context.Context, key string, value []byte) error {
	if len(value) == 0 {
		return ErrEmpty
	}
	if len(value) > MaxValueSize {
		return ErrTooBig
	}

	_, err := rlc.rdb.SetNX(ctx, key, value, 0).Result()
	return err
}

func (rlc *RedisLRUConnection) Get(ctx context.Context, key string) ([]byte, error) {
	b, err := rlc.rdb.Get(ctx, key).Bytes()
	if errors.Is(err, redis.Nil) {
		return nil, nil
	}
	return b, err
}
