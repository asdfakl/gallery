package appserver

import (
	"bytes"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
)

type HandlerFn func(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error)

type Handler interface {
	CanAccess(*postgres.UserView) bool
	SetUser(*postgres.UserView)
	User() *postgres.UserView
	SetRequest(*http.Request)
	Request() *http.Request
}

type GETHandler interface {
	Handler
	HandleGET(*AppServer, http.ResponseWriter, *http.Request)
}

type POSTHandler interface {
	Handler
	HandlePOST(*AppServer, http.ResponseWriter, *http.Request)
}

type Page struct {
	user *postgres.UserView
	r    *http.Request
}

func (page *Page) SetUser(user *postgres.UserView) {
	page.user = user
}

func (page *Page) User() *postgres.UserView {
	return page.user
}

func (page *Page) SetRequest(r *http.Request) {
	page.r = r
}

func (page *Page) Request() *http.Request {
	return page.r
}

func (srv *AppServer) staticPageFn() httprouter.Handle {
	notFound := func(w http.ResponseWriter) {
		http.Error(w, "Asset Not Found", http.StatusNotFound)
	}
	cacheControl := func(w http.ResponseWriter) {
		w.Header().Set("Cache-Control", "private, max-age=2592000, immutable") // one month
	}

	if srv.cfg.StaticDirectory == "" {
		return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			resource := strings.TrimPrefix(ps.ByName("filepath"), srv.cfg.RootPath)

			f, err := staticFS.Open(filepath.Join("static", resource))
			if err != nil {
				notFound(w)
				return
			}
			defer f.Close()

			info, err := f.Stat()
			if err != nil {
				srv.logger.Error("stat", log.String("resource", resource), log.Err(err))

				notFound(w)
				return
			}

			if info.Mode().IsDir() {
				notFound(w)
				return
			}

			b := &bytes.Buffer{}

			_, err = io.Copy(b, f)
			if err != nil {
				srv.logger.Error("read", log.String("resource", resource), log.Err(err))

				notFound(w)
				return
			}

			cacheControl(w)

			http.ServeContent(w, r, filepath.Base(resource), info.ModTime(), bytes.NewReader(b.Bytes()))
		}
	} else {
		return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			resource := strings.TrimPrefix(ps.ByName("filepath"), srv.cfg.RootPath)

			f, err := os.Open(filepath.Join(srv.cfg.StaticDirectory, resource))
			if err != nil {
				srv.logger.Error("open", log.String("resource", resource), log.Err(err))
				notFound(w)
				return
			}
			defer f.Close()

			info, err := f.Stat()
			if err != nil {
				srv.logger.Error("stat", log.String("resource", resource), log.Err(err))
				notFound(w)
				return
			}

			cacheControl(w)

			http.ServeContent(w, r, filepath.Base(resource), info.ModTime(), f)
		}
	}
}
