package appserver

import (
	"fmt"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/objectstorage"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type ObjectPage struct {
	Page

	Object   *postgres.ObjectView
	Storage  *postgres.StorageView
	Download bool
}

func ObjectPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse path
	sum, err := utils.ParseHexEncodedSHA1String(ps.ByName("sha1"))
	if err != nil {
		return nil, ErrNotFound("object not found")
	}

	// parse query
	parser := formparser.NewQueryFormParser(r)

	download := parser.Bool("download")

	if err = parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	// load
	obj, err := srv.pg.GetObjectViewBySHA1(srv.appCtx, sum)
	if err != nil {
		return nil, utils.Wrap(err, "get object view by sha1")
	}
	if obj == nil {
		return nil, ErrNotFound("object not found")
	}

	storages, err := srv.pg.GetStorageListByObjectID(srv.appCtx, obj.ID)
	if err != nil {
		return nil, utils.Wrap(err, "get storage list by object id")
	}
	if len(storages) == 0 {
		return nil, ErrNotFound("object not found")
	}
	storages.SortByAccessPriority()

	return &ObjectPage{
		Object:   obj,
		Storage:  storages[0],
		Download: download.ValueOr(false),
	}, nil
}

func (page *ObjectPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return page.Object.Public || user.ID == page.Object.Owner
}

func (page *ObjectPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.rateLimited("object", 100, 5*time.Minute, w, r, page.handleGET)
}

func (page *ObjectPage) handleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	storage, err := srv.cachedObjectStorageByID(page.Storage.ID, 1*time.Hour)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	if storage == nil {
		srv.handleError(w, ErrNotFound("object not found"))
		return
	}

	switch s := storage.(type) {
	case *objectstorage.LocalStorage:
		page.handleLocalStorageGET(srv, w, r, s)
	case *objectstorage.S3Storage:
		page.handleS3StorageGET(srv, w, r, s)
	default:
		panic(fmt.Errorf("unsupported storage type: %T", s))
	}

	if _, err = srv.rmc.FreqIncrement(srv.appCtx, "object_access", 1, page.Object.HexSHA1()); err != nil {
		srv.logger.Error("increment object_access frequency", log.Err(err))
	}
}

func (page *ObjectPage) handleLocalStorageGET(srv *AppServer, w http.ResponseWriter, r *http.Request, storage *objectstorage.LocalStorage) {
	f, err := storage.Open(page.Object.SHA1)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	defer f.Close()

	w.Header().Set("Cache-Control", "private, max-age=31536000, immutable") // one year
	if page.Download {
		w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="%s"`, page.Object.Basename))
	}

	http.ServeContent(w, r, page.Object.Basename, page.Object.ModifiedAt, f)
}

func (page *ObjectPage) handleS3StorageGET(srv *AppServer, w http.ResponseWriter, r *http.Request, storage *objectstorage.S3Storage) {
	var (
		signed string
		err    error
	)

	if page.Download {
		signed, err = storage.PresignedObjectDownloadLink(page.Object.SHA1, 5*time.Minute, page.Object.Basename)
	} else {
		signed, err = storage.PresignedObjectLink(page.Object.SHA1, 5*time.Minute)
	}
	if err != nil {
		srv.handleError(w, err)
		return
	}

	http.Redirect(w, r, signed, http.StatusFound)
}
