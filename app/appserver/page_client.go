package appserver

import (
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/postgres"
)

type ClientPage struct {
	Page

	Version string
	APIAddr string
}

func ClientPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &ClientPage{
		Version: config.Version,
		APIAddr: srv.cfg.APIAddr,
	}, nil
}

func (page *ClientPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *ClientPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "client.html", page, http.StatusOK)
}
