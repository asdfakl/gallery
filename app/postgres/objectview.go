package postgres

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"github.com/lib/pq"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type ObjectViewList []*ObjectView
type ObjectView struct {
	ID                   int64
	SHA1                 [sha1.Size]byte
	Basename             string
	Size                 int64
	ContentType          string
	ContentSubtype       string
	ContentSubtypeParams *string
	ModifiedAt           time.Time
	AddedAt              time.Time
	Owner                int64
	Public               bool
	Tags                 []string
}

type ObjectViewListSearchParams struct {
	Search           *string
	ContentTypes     []string
	Count            uint
	Offset           uint
	Public           *bool
	Tagging          *bool
	RestrictByUserID int64
	OrderBy          *string
}

type TagFrequencyList []*TagFrequency
type TagFrequency struct {
	Tag       string
	Frequency int
}

func (obj *ObjectView) HexSHA1() string {
	return hex.EncodeToString(obj.SHA1[:])
}

func (obj *ObjectView) MatchContentType(qct *utils.QualifiedContentType) bool {
	return obj.ContentType == qct.Type && obj.ContentSubtype == qct.Subtype
}

func (obj *ObjectView) MatchContentTypes(qcts []*utils.QualifiedContentType) bool {
	for _, qct := range qcts {
		if obj.MatchContentType(qct) {
			return true
		}
	}
	return false
}

func (pg *PostgresConnection) GetPublicObjectViewTagFrequencyList(ctx context.Context, limit int) (TagFrequencyList, error) {
	q := `
		SELECT tag, COUNT(tag) AS frequency
		FROM gallery.v_object o, UNNEST(tags) tag
		WHERE o.public
		GROUP BY tag
		ORDER BY frequency DESC
		LIMIT $1
	`

	rows, err := pg.cp.Query(ctx, q, limit)
	if err != nil {
		return nil, utils.Wrap(err, "execute query")
	}
	defer rows.Close()

	freqs := TagFrequencyList{}
	for rows.Next() {
		freq, err := scanObjectViewTagFrequencyFromRows(rows)
		if err != nil {
			return nil, utils.Wrap(err, "scan")
		}

		freqs = append(freqs, freq)
	}

	return freqs, rows.Err()
}

func (pg *PostgresConnection) GetObjectViewBySHA1(ctx context.Context, sum [sha1.Size]byte) (*ObjectView, error) {
	q := `
		SELECT
			o.id,
			o.sha1,
			o.basename,
			o.size,
			o.content_type,
			o.content_subtype,
			o.content_subtype_params,
			o.modified_at,
			o.added_at,
			o.owner,
			o.public,
			o.tags
		FROM
			gallery.v_object o
		WHERE
			o.sha1 = $1
	`

	return scanObjectViewFromRow(pg.cp.QueryRow(ctx, q, sum[:]))
}

func (pg *PostgresConnection) SearchObjectViewList(ctx context.Context, params *ObjectViewListSearchParams) (ObjectViewList, error) {
	q, args, err := searchObjectViewListQuery(params, false)
	if err != nil {
		return nil, utils.Wrap(err, "build query")
	}

	rows, err := pg.cp.Query(ctx, q, args...)
	if err != nil {
		return nil, utils.Wrap(err, "execute query")
	}
	defer rows.Close()

	objs := ObjectViewList{}
	for rows.Next() {
		obj, err := scanObjectViewFromRows(rows)
		if err != nil {
			return nil, utils.Wrap(err, "scan")
		}

		objs = append(objs, obj)
	}

	return objs, rows.Err()
}

func (pg *PostgresConnection) CountObjectViewList(ctx context.Context, params *ObjectViewListSearchParams) (count uint, err error) {
	q, args, err := searchObjectViewListQuery(params, true)
	if err != nil {
		return 0, utils.Wrap(err, "build query")
	}

	if err = pg.cp.QueryRow(ctx, q, args...).Scan(&count); err != nil {
		return 0, utils.Wrap(err, "execute query")
	}

	return
}

func searchObjectViewListQuery(params *ObjectViewListSearchParams, aggregate bool) (string, []interface{}, error) {
	dialect := goqu.Dialect("postgres")

	schema := goqu.S("gallery")

	o := schema.Table("v_object").As("o")

	ds := dialect.From(o)

	if params.Search != nil {
		or := []exp.Expression{
			o.Col("basename").ILike("%" + escapeLikePattern(*params.Search) + "%"),
		}

		tags := strings.Fields(*params.Search)
		for i := range tags {
			tags[i] = strings.ToLower(tags[i])
		}
		tags = utils.UniqueStrings(tags)

		if len(tags) != 0 {
			or = append(or, goqu.L("o.tags @> ?", pq.Array(tags)))
		}

		ds = ds.Where(goqu.Or(or...))
	}

	if len(params.ContentTypes) != 0 {
		var or []exp.Expression

		for _, ct := range params.ContentTypes {
			or = append(or, o.Col("content_type").Eq(ct))
		}

		ds = ds.Where(goqu.Or(or...))
	}

	if params.Public != nil {
		if *params.Public {
			ds = ds.Where(o.Col("public").IsTrue())
		} else {
			ds = ds.Where(o.Col("public").IsFalse())
		}
	}

	if params.Tagging != nil {
		if *params.Tagging {
			ds = ds.Where(goqu.L("cardinality(o.tags) != 0"))
		} else {
			ds = ds.Where(goqu.L("cardinality(o.tags) = 0"))
		}
	}

	ds = ds.Where(goqu.Or(
		o.Col("owner").Eq(params.RestrictByUserID),
		o.Col("public").IsTrue(),
	))

	if aggregate {
		ds = ds.Select(goqu.COUNT(o.Col("id")))
	} else {
		ds = ds.Select(
			o.Col("id"),
			o.Col("sha1"),
			o.Col("basename"),
			o.Col("size"),
			o.Col("content_type"),
			o.Col("content_subtype"),
			o.Col("content_subtype_params"),
			o.Col("modified_at"),
			o.Col("added_at"),
			o.Col("owner"),
			o.Col("public"),
			o.Col("tags"),
		).Limit(
			params.Count,
		).Offset(
			params.Offset,
		)

		// always include sha1 in order by to produce unambiguous order
		if params.OrderBy != nil {
			switch *params.OrderBy {
			case "added_at", "modified_at", "size":
				ds = ds.Order(o.Col(*params.OrderBy).Asc(), o.Col("sha1").Asc())
			case "!added_at", "!modified_at", "!size":
				ds = ds.Order(o.Col(strings.TrimPrefix(*params.OrderBy, "!")).Desc(), o.Col("sha1").Asc())
			}
		} else {
			ds = ds.Order(o.Col("sha1").Asc())
		}
	}

	return ds.Prepared(true).ToSQL()
}

func escapeLikePattern(pattern string) string {
	builder := &strings.Builder{}

	for _, r := range pattern {
		switch r {
		case '%':
			builder.WriteString(`\%`)
		case '\\':
			builder.WriteString(`\\`)
		case '_':
			builder.WriteString(`\_`)
		default:
			builder.WriteRune(r)
		}
	}

	return builder.String()
}

func scanObjectViewFromRow(row pgx.Row) (*ObjectView, error) {
	obj := &ObjectView{}
	sum := make([]byte, sha1.Size)

	if err := row.Scan(
		&obj.ID,
		&sum,
		&obj.Basename,
		&obj.Size,
		&obj.ContentType,
		&obj.ContentSubtype,
		&obj.ContentSubtypeParams,
		&obj.ModifiedAt,
		&obj.AddedAt,
		&obj.Owner,
		&obj.Public,
		&obj.Tags,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	copy(obj.SHA1[:], sum)

	return obj, nil
}

func scanObjectViewFromRows(rows pgx.Rows) (*ObjectView, error) {
	obj := &ObjectView{}
	sum := make([]byte, sha1.Size)

	if err := rows.Scan(
		&obj.ID,
		&sum,
		&obj.Basename,
		&obj.Size,
		&obj.ContentType,
		&obj.ContentSubtype,
		&obj.ContentSubtypeParams,
		&obj.ModifiedAt,
		&obj.AddedAt,
		&obj.Owner,
		&obj.Public,
		&obj.Tags,
	); err != nil {
		return nil, err
	}

	copy(obj.SHA1[:], sum)

	return obj, nil
}

func scanObjectViewTagFrequencyFromRows(rows pgx.Rows) (*TagFrequency, error) {
	freq := &TagFrequency{}

	if err := rows.Scan(
		&freq.Tag,
		&freq.Frequency,
	); err != nil {
		return nil, err
	}

	return freq, nil
}
