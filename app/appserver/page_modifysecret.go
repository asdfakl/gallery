package appserver

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
	"golang.org/x/crypto/bcrypt"
)

type ModifySecretPage struct {
	Page

	Secret         *postgres.Secret
	MasterPassword *postgres.MasterPassword
}

func ModifySecretPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("secret not found")
	}

	// load
	secret, err := srv.pg.GetSecretByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get secret by ext_id")
	}
	if secret == nil {
		return nil, ErrNotFound("secret not found")
	}

	password, err := srv.pg.GetMasterPasswordByID(srv.appCtx, secret.MasterPasswordID)
	if err != nil {
		return nil, utils.Wrap(err, "get master password by id")
	}
	if password == nil {
		return nil, ErrNotFound("master password not found")
	}

	return &ModifySecretPage{
		Secret:         secret,
		MasterPassword: password,
	}, nil
}

func (page *ModifySecretPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.MasterPassword.Owner
}

func (page *ModifySecretPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		passwordField = parser.String("password").Required().NonEmpty().MaxLength(64)
		labelField    = parser.String("label").Required().NonEmpty().MaxLength(128)
		secretField   = parser.String("secret").Required().NonEmpty().MaxLength(2048)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(page.MasterPassword.Password), []byte(passwordField.Value())); err != nil {
		srv.handleError(w, ErrForbidden("password mismatch"))
		return
	}

	page.Secret.Label = labelField.Value()

	ciphertext, nonce, err := utils.EncryptAES256([]byte(secretField.Value()), passwordField.Value())
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "encrypt aes256"))
		return
	}

	if err = srv.pg.ModifySecret(srv.appCtx, page.Secret, ciphertext, nonce); err != nil {
		srv.handleError(w, utils.Wrap(err, "modify secret"))
		return
	}

	srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
}
