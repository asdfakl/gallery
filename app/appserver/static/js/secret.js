"use strict";

(function(){
	const initJSSecretViewForm = nodes => {
		nodes.forEach(node => {
			const action = node.attributes["action"].nodeValue;

			const containerID = "secret-container";

			const modal = node.closest("div.modal");

			const csrfInput = node.querySelector("input[name=csrf]");
			const passwordInput = node.querySelector("input[name=password]");

			node.addEventListener("submit", e => {
				e.preventDefault();

				const q = {
					csrf: csrfInput.value,
					password: passwordInput.value,
				};

				const opts = Utils.commonPostFormOpts();
				opts.body = Utils.queryString(q);

				fetch(action, opts).then(res => {
					modal.classList.remove("is-active");

					return Utils.handleFetchError(res);
				}).then(res => res.text()).then(res => {
					const parsed = new DOMParser().parseFromString(res, "text/html");

					const result = parsed.getElementById(containerID);
					if (!result) {
						throw `response element not found by id ${containerID}`
					}

					const modal = document.getElementById("js-content-modal");
					const container = modal.querySelector("div.modal-content");

					container.replaceChildren(...result.children);

					modal.classList.add("is-active");
					Common.initHTMLElem(container);
				}).catch(err => {
					console.error(`js-secret-form fetch failed: ${err}`);
				});
			});
		});
	};

	Common.registerInitHTMLElemHook(elem => {
		initJSSecretViewForm(elem.querySelectorAll(".js-secret-form"));
	});
})();
