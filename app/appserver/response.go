package appserver

import (
	"encoding/json"
	"net/http"

	"gitlab.com/asdfakl/gallery/app/log"
)

func (srv *AppServer) renderLoginForm(w http.ResponseWriter, requestURI string, errors []string) {
	srv.executeTemplate(w, "login_form.html", &struct {
		RequestURI string
		Errors     []string
	}{
		RequestURI: requestURI,
		Errors:     errors,
	}, http.StatusUnauthorized)
}

type JSONResponse struct {
	Redirect string `json:",omitempty"`
	Refresh  bool   `json:",omitempty"`
	Reload   bool   `json:",omitempty"`
}

func (srv *AppServer) renderJSON(w http.ResponseWriter, res *JSONResponse, code int) {
	encoder := json.NewEncoder(w)

	w.Header().Add("Content-Type", "application/json")

	if err := encoder.Encode(res); err != nil {
		srv.logger.Warn("json encoding or response write error", log.Err(err))
	}
}
