package appserver

import (
	"net/http"
	"net/url"
	"time"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
)

const LoginWaitDuration time.Duration = 500 * time.Millisecond

type LoginPage struct {
	Page
}

func LoginPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &LoginPage{}, nil
}

func (page *LoginPage) CanAccess(user *postgres.UserView) bool {
	return true
}

func (page *LoginPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.redirect(w, r, "/", http.StatusMovedPermanently)
}

func (page *LoginPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.rateLimited("login", 10, 10*time.Minute, w, r, page.handlePOST)
}

func (page *LoginPage) handlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()

	parser := formparser.NewBodyFormParser(r, nil)

	var (
		rawRedirect = parser.String("redirect").Required().MaxLength(512)
		username    = parser.String("username").Required().Trim().MaxLength(128)
		password    = parser.String("password").Required().Trim().MaxLength(64)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	redirect, err := url.Parse(rawRedirect.Value())
	if err != nil {
		srv.handleError(w, ErrBadRequest(err, "Invalid Redirect URI"))
		return
	}

	success := true

	user, err := srv.pg.GetUserViewByUsername(srv.appCtx, username.Value())
	if err != nil {
		srv.handleError(w, err)
		return
	}

	if success {
		if user == nil {
			success = false
			srv.logger.Debug("user not found", log.String("username", username.Value()))
		}
	}

	if success {
		if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password.Value())); err != nil {
			success = false
			srv.logger.Debug("password mismatch")
		}
	}

	var session *redismain.Session

	if success {
		session, err = srv.rmc.CreateSession(srv.appCtx, user.ID, srv.cfg.SessionTTL)
		if err != nil {
			srv.handleError(w, err)
			return
		}
	}

	time.Sleep(time.Until(startTime.Add(LoginWaitDuration)))

	if success {
		http.SetCookie(w, &http.Cookie{
			Name:  srv.cfg.SessionCookieName,
			Value: session.String(),

			Path:     srv.cfg.RootPath,
			Domain:   srv.cfg.Domain,
			MaxAge:   int(srv.cfg.SessionTTL.Seconds()),
			Secure:   srv.cfg.SecureCookie,
			HttpOnly: true,
			SameSite: http.SameSiteStrictMode,
		})

		if srv.isRooted(redirect.Path) {
			srv.redirect(w, r, redirect.RequestURI(), http.StatusSeeOther)
		} else {
			srv.redirect(w, r, "/", http.StatusSeeOther)
		}

	} else {
		srv.renderLoginForm(w, redirect.RequestURI(), []string{
			"Username and password mismatch",
		})
	}
}
