#!/bin/bash

usage() {
	cat >&2 <<USAGE
Usage: ./build.sh [TARGET]

Targets: bg bgdev app appdev
USAGE
	exit 1
}

die() {
	echo "$@" >&2
	exit 2
}

test $# -eq 1 || usage

base="$(dirname "$(realpath "$0")")"

cd "$base" || die "cd"

tag=$(git describe --tags)

go test -v -race ./... || die "unit tests"

case "$1" in
"bg")
	BG_DOCKER_REPOSITORY=${BG_DOCKER_REPOSITORY:-'local/gallery-bg'}
	echo -e '\033[01;37mbuilding target bg\033[00m'
	docker build \
		--force-rm \
		--build-arg uid=1000 \
		--build-arg gid=1000 \
		--build-arg version="${tag}" \
		--file ./Dockerfile.bg \
		--tag "${BG_DOCKER_REPOSITORY}:${tag}" \
		.
	;;
"bgdev")
	echo -e '\033[01;37mbuilding target bgdev\033[00m'
	docker build \
		--force-rm \
		--build-arg uid="$(id -u)" \
		--build-arg gid="$(id -g)" \
		--build-arg version="development" \
		--file ./Dockerfile.bg \
		--tag 'local/gallery-bg:development' \
		.
	;;
"appdev")
	echo -e '\033[01;37mbuilding target appdev\033[00m'
	docker build \
		--force-rm \
		--build-arg uid="$(id -u)" \
		--build-arg gid="$(id -g)" \
		--build-arg version="development" \
		--file ./Dockerfile.app \
		--tag 'local/gallery-app:development' \
		.
	;;
"app")
	APP_DOCKER_REPOSITORY=${APP_DOCKER_REPOSITORY:-'local/gallery-app'}
	echo -e '\033[01;37mbuilding target app\033[00m'
	docker build \
		--force-rm \
		--build-arg uid=1000 \
		--build-arg gid=1000 \
		--build-arg version="${tag}" \
		--file ./Dockerfile.app \
		--tag "${APP_DOCKER_REPOSITORY}:${tag}" \
		.
	;;
*)
	usage
	;;
esac
