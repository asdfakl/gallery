package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4"
)

const (
	BG_JOB_FN_KEY_CLEAN_UPLOADS      BackgroundJobFnKey = "clean_uploads"
	BG_JOB_FN_KEY_GENERATE_THUMBNAIL BackgroundJobFnKey = "generate_thumbnail"
	BG_JOB_FN_KEY_IMPORT_OBJECTS     BackgroundJobFnKey = "import_objects"
	BG_JOB_FN_KEY_IMPORT_UPLOAD      BackgroundJobFnKey = "import_upload"
	BG_JOB_FN_KEY_SLEEP              BackgroundJobFnKey = "sleep"
	BG_JOB_FN_KEY_SYNC_STORAGE       BackgroundJobFnKey = "sync_storage"
)

type BackgroundJobFnKey string

type BackgroundJob struct {
	ID   int64
	Func BackgroundJobFnKey
	Opts map[string]interface{}
}

type BackgroundJobTriggerList []*BackgroundJobTrigger
type BackgroundJobTrigger struct {
	BackgroundJob

	Key string
}

type BackgroundJobQueueList []*BackgroundJobQueue
type BackgroundJobQueue struct {
	BackgroundJob

	Key         string
	WorkerCount int
}

func (pg *PostgresConnection) GetEveryBackgroundJobTrigger(ctx context.Context) (jobs BackgroundJobTriggerList, err error) {
	q := `
		SELECT
			j.id,
			j.func,
			j.opts,
			t.key
		FROM
			gallery.background_job_trigger t
		JOIN
			gallery.background_job j ON (t.background_job_id = j.id)
	`

	rows, err := pg.cp.Query(ctx, q)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		job := &BackgroundJobTrigger{}

		if err = rows.Scan(
			&job.ID,
			&job.Func,
			&job.Opts,
			&job.Key,
		); err != nil {
			return
		}

		jobs = append(jobs, job)
	}

	err = rows.Err()
	return
}

func (pg *PostgresConnection) GetEveryBackgroundJobQueue(ctx context.Context) (jobs BackgroundJobQueueList, err error) {
	q := `
		SELECT
			j.id,
			j.func,
			j.opts,
			q.key,
			q.worker_count
		FROM
			gallery.background_job_queue q
		JOIN
			gallery.background_job j ON (q.background_job_id = j.id)
	`

	rows, err := pg.cp.Query(ctx, q)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		job := &BackgroundJobQueue{}

		if err = rows.Scan(
			&job.ID,
			&job.Func,
			&job.Opts,
			&job.Key,
			&job.WorkerCount,
		); err != nil {
			return
		}

		jobs = append(jobs, job)
	}

	err = rows.Err()
	return
}

func (pg *PostgresConnection) GetBackgroundJobQueueByFunc(ctx context.Context, fnKey BackgroundJobFnKey) (*BackgroundJobQueue, error) {
	q := `
		SELECT
			j.id,
			j.func,
			j.opts,
			q.key,
			q.worker_count
		FROM
			gallery.background_job_queue q
		JOIN
			gallery.background_job j ON (q.background_job_id = j.id)
		WHERE
			j.func = $1
		LIMIT 1
	`

	queue := &BackgroundJobQueue{}

	if err := pg.cp.QueryRow(ctx, q, fnKey).Scan(
		&queue.ID,
		&queue.Func,
		&queue.Opts,
		&queue.Key,
		&queue.WorkerCount,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return queue, nil
}
