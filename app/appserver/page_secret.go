package appserver

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type SecretPage struct {
	Page

	Secret         *postgres.Secret
	MasterPassword *postgres.MasterPassword
	Tpl            string
	Plaintext      string
}

func SecretPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("secret not found")
	}

	parser := formparser.NewQueryFormParser(r)

	var (
		tpl = parser.String("tpl").Required().Options([]string{"view", "modify"})
	)

	if err = parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	// load
	secret, err := srv.pg.GetSecretByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get secret by ext_id")
	}
	if secret == nil {
		return nil, ErrNotFound("secret not found")
	}

	password, err := srv.pg.GetMasterPasswordByID(srv.appCtx, secret.MasterPasswordID)
	if err != nil {
		return nil, utils.Wrap(err, "get master password by id")
	}
	if password == nil {
		return nil, ErrNotFound("master password not found")
	}

	return &SecretPage{
		Secret:         secret,
		MasterPassword: password,
		Tpl:            tpl.Value(),
	}, nil
}

func (page *SecretPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.MasterPassword.Owner
}

func (page *SecretPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "secret_open_form.html", page, http.StatusOK)
}

func (page *SecretPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.rateLimited("secret", 10, 10*time.Minute, w, r, page.handlePOST)
}

func (page *SecretPage) handlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		passwordField = parser.String("password").Required().NonEmpty().MaxLength(64)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	plaintext, err := srv.pg.DecryptSecretByID(srv.appCtx, page.Secret.ID, passwordField.Value())
	if err != nil {
		if errors.Is(err, postgres.ErrDecrypt) {
			srv.handleError(w, ErrForbidden("password mismatch"))
		} else {
			srv.handleError(w, utils.Wrap(err, "decrypt secret by id"))
		}
		return
	}
	page.Plaintext = string(plaintext)

	switch page.Tpl {
	case "view":
		srv.executePageTemplate(w, "secret_view.html", page, http.StatusOK)
	case "modify":
		srv.executePageTemplate(w, "secret_modify_form.html", page, http.StatusOK)
	default:
		panic(fmt.Errorf("unsupported template: '%s'", page.Tpl))
	}
}
