package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

const (
	UserViewFields = `
		u.id,
		u.ext_id,
		u.username,
		COALESCE(u.password, ''),
		u.display_name,
		u.quota_object_count,
		u.quota_object_bytes,
		u.object_count,
		u.object_bytes
	`
	UserViewListFields = `
		u.id,
		u.ext_id,
		u.username,
		u.display_name,
		u.quota_object_count,
		u.quota_object_bytes,
		u.object_count,
		u.object_bytes
	`
)

type UserViewList []*UserView
type UserView struct {
	ID               int64
	ExtID            uuid.UUID
	Username         string
	Password         string
	DisplayName      string
	QuotaObjectCount int
	QuotaObjectBytes int64
	ObjectCount      int
	ObjectBytes      int64
}

func (ls UserViewList) IDMap() map[int64]*UserView {
	m := make(map[int64]*UserView)

	for _, user := range ls {
		m[user.ID] = user
	}

	return m
}

func (u *UserView) IsValid() bool {
	return u != nil && u.ID > 0
}

func (pg *PostgresConnection) GetEveryUserView(ctx context.Context) (users UserViewList, err error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_user u
	`, UserViewListFields)

	rows, err := pg.cp.Query(ctx, q)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var user *UserView

		user, err = scanUserViewFromRows(rows)
		if err != nil {
			return
		}

		users = append(users, user)
	}

	err = rows.Err()
	return
}

func (pg *PostgresConnection) GetUserViewByUsername(ctx context.Context, username string) (*UserView, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_user u
		WHERE
			u.username = $1
	`, UserViewFields)

	return scanUserViewFromRow(pg.cp.QueryRow(ctx, q, username))
}

func (pg *PostgresConnection) GetUserViewByID(ctx context.Context, id int64) (*UserView, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_user u
		WHERE
			u.id = $1
	`, UserViewFields)

	return scanUserViewFromRow(pg.cp.QueryRow(ctx, q, id))
}

func (pg *PostgresConnection) GetUserViewByExtID(ctx context.Context, extID uuid.UUID) (*UserView, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_user u
		WHERE
			u.ext_id = $1
	`, UserViewFields)

	return scanUserViewFromRow(pg.cp.QueryRow(ctx, q, extID))
}

func (pg *PostgresConnection) UpdateUser(ctx context.Context, user *UserView) error {
	q := `
		UPDATE
			gallery.user
		SET
			display_name = NULLIF($1, '')
		WHERE
			id = $2
			AND validity_period @> now()
	`

	_, err := pg.cp.Exec(ctx, q, user.DisplayName, user.ID)
	return err
}

func (pg *PostgresConnection) UpdateUserPassword(ctx context.Context, id int64, password string) error {
	q := `
		UPDATE
			gallery.user
		SET
			password = NULLIF($1, '')
		WHERE
			id = $2
			AND validity_period @> now()
	`

	_, err := pg.cp.Exec(ctx, q, password, id)
	return err
}

func scanUserViewFromRow(row pgx.Row) (*UserView, error) {
	user := &UserView{}

	if err := row.Scan(
		&user.ID,
		&user.ExtID,
		&user.Username,
		&user.Password,
		&user.DisplayName,
		&user.QuotaObjectCount,
		&user.QuotaObjectBytes,
		&user.ObjectCount,
		&user.ObjectBytes,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return user, nil
}

func scanUserViewFromRows(rows pgx.Rows) (*UserView, error) {
	user := &UserView{}

	// lists never select password
	if err := rows.Scan(
		&user.ID,
		&user.ExtID,
		&user.Username,
		&user.DisplayName,
		&user.QuotaObjectCount,
		&user.QuotaObjectBytes,
		&user.ObjectCount,
		&user.ObjectBytes,
	); err != nil {
		return nil, err
	}

	return user, nil
}
