START TRANSACTION;

---------------------------------------------------------------------------------------------------------------------------------------
-- master_password

CREATE SEQUENCE gallery.master_password_id_seq;

CREATE TABLE gallery.master_password (
  id INTEGER NOT NULL DEFAULT NEXTVAL('gallery.master_password_id_seq'),
  ext_id UUID NOT NULL DEFAULT uuid_generate_v4() CHECK ( ext_id != uuid_nil() ),
  password TEXT NOT NULL CHECK ( TRIM(password) != '' AND LENGTH(password) <= 60 ),
  owner INTEGER NOT NULL,
  label TEXT NOT NULL CHECK ( TRIM(label) != '' AND LENGTH(label) <= 32 ),

  PRIMARY KEY (id),
  UNIQUE (ext_id),
  UNIQUE (owner, label),

  FOREIGN KEY (owner) REFERENCES gallery.user (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

-- foreign key indexes
CREATE INDEX ON gallery.master_password (owner);

---------------------------------------------------------------------------------------------------------------------------------------
-- secret

CREATE SEQUENCE gallery.secret_id_seq;

CREATE TABLE gallery.secret (
  id INTEGER NOT NULL DEFAULT NEXTVAL('gallery.secret_id_seq'),
  ext_id UUID NOT NULL DEFAULT uuid_generate_v4() CHECK ( ext_id != uuid_nil() ),
  master_password_id INTEGER NOT NULL,
  nonce BYTEA NOT NULL,
  ciphertext BYTEA NOT NULL,
  label TEXT NOT NULL CHECK ( TRIM(label) != '' AND LENGTH(label) <= 128 ),

  PRIMARY KEY (id),
  UNIQUE (ext_id),

  FOREIGN KEY (master_password_id) REFERENCES gallery.master_password (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

-- foreign key indexes
CREATE INDEX ON gallery.secret (master_password_id);

COMMIT;
