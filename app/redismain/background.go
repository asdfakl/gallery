package redismain

import (
	"context"
	"errors"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v8"
)

func (rmc *RedisMainConnection) BackgroundTriggerIsSet(ctx context.Context, key string) (bool, error) {
	k := ns.backgroundJobTrigger(key)

	var exists *redis.IntCmd

	_, err := rmc.rdb.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		exists = pipe.Exists(ctx, k)
		pipe.Del(ctx, k)
		return nil
	})
	if err != nil {
		return false, err
	}

	res, err := exists.Result()
	return res == 1, err
}

func (rmc *RedisMainConnection) PushBackgroundQueueItems(ctx context.Context, key string, vals ...string) (err error) {
	k := ns.backgroundJobQueue(key)

	_, err = rmc.rdb.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		err = pipe.RPush(ctx, k, vals).Err()

		if err == nil && rand.Float64() < BACKGROUND_JOB_QUEUE_LENGTH_ENFORCEMENT_PROBABILITY {
			err = pipe.LTrim(ctx, k, 0, BACKGROUND_JOB_QUEUE_MAX_LENGTH).Err()
		}
		return nil
	})
	return
}

func (rmc *RedisMainConnection) PopBackgroundQueueItem(ctx context.Context, key string, timeout time.Duration) (string, error) {
	k := ns.backgroundJobQueue(key)

	res, err := rmc.rdb.BLPop(ctx, timeout, k).Result()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return "", nil
		}
		return "", err
	}

	return res[1], nil
}
