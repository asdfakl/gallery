package appserver

import (
	"errors"
	"net/http"
	"time"

	"gitlab.com/asdfakl/gallery/app/jwt"
	"gitlab.com/asdfakl/gallery/app/redismain"
)

type CSRFToken struct {
	jwt.JWTPayload

	Path string `json:"path"`
}

func (srv *AppServer) issueCSRFToken(sessionKey redismain.SessionKey, path string, ttl time.Duration) string {
	token := CSRFToken{
		JWTPayload: jwt.JWTPayload{
			Issuer:  srv.cfg.Domain,
			Expiry:  time.Now().Add(ttl).Unix(),
			Subject: sessionKey.String(),
		},
		Path: path,
	}

	return string(jwt.NewJWTHS256(&token, []byte(srv.cfg.Secret)))
}

func (srv *AppServer) validateCSRFToken(r *http.Request) error {
	vals := r.PostForm["csrf"]

	if len(vals) == 0 {
		return errors.New("missing csrf token")
	}
	if len(vals) > 1 {
		return errors.New("ambiguous csrf token")
	}

	token := CSRFToken{}

	if !jwt.VerifyJWTToken([]byte(vals[0]), []byte(srv.cfg.Secret), &token) {
		return errors.New("invalid csrf token")
	}
	if !token.Validate([]string{srv.cfg.Domain}) {
		return errors.New("expired or untrusted csrf token")
	}

	session := r.Context().Value(ContextKeySession{}).(*redismain.Session)

	if token.Subject != session.Key.String() {
		return errors.New("csrf token from another session")
	}
	if token.Path != r.URL.Path {
		return errors.New("csrf token not valid for this request path")
	}

	return nil
}
