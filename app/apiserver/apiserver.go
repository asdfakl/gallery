package apiserver

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"path"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/middleware"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type APIServer struct {
	cfg    *Configuration
	appCtx context.Context
	logger *log.Logger
	pg     *postgres.PostgresConnection
	rmc    *redismain.RedisMainConnection

	once   sync.Once
	server *http.Server
}

func New(
	cfg *Configuration,
	logger *log.Logger,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
) (*APIServer, error) {
	srv := &APIServer{
		cfg:    cfg,
		logger: logger.With(log.String("service", "api")),
		pg:     pg,
		rmc:    rmc,
	}

	router := httprouter.New()

	srv.registerRoutes(router)

	handler := middleware.NewStatusCodeRecorderMiddleware(
		middleware.NewLoggerMiddleWare(
			srv.logger,
			middleware.NewRecovererMiddleware(
				middleware.NewCompressMiddleware(
					NewAuthMiddleware(
						srv,
						router,
					),
					[]string{
						"application/json",
					},
				),
			),
		),
	)

	srv.server = &http.Server{
		Addr:    cfg.ListenAddr,
		Handler: handler,

		ReadTimeout:       cfg.ReadTimeout,
		ReadHeaderTimeout: cfg.ReadHeaderTimeout,
		WriteTimeout:      cfg.WriteTimeout,
		IdleTimeout:       cfg.IdleTimeout,
		MaxHeaderBytes:    cfg.MaxHeaderBytes,
	}

	return srv, nil
}

func (srv *APIServer) Start(ctx context.Context, wg *sync.WaitGroup) {
	srv.once.Do(func() {
		srv.appCtx = ctx

		wg.Add(2)

		go srv.listenAndServe(wg)
		go srv.listenAppContext(wg)
	})
}

func (srv *APIServer) listenAndServe(wg *sync.WaitGroup) {
	defer wg.Done()

	srv.logger.System(
		"listen and serve http",
		log.String("addr", srv.server.Addr),
		log.String("root_path", srv.cfg.RootPath),
	)

	if err := srv.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		utils.Die(err, "listen and serve http")
	}
}

func (srv *APIServer) listenAppContext(wg *sync.WaitGroup) {
	defer wg.Done()

	<-srv.appCtx.Done()

	shutdownCtx, cancelShutdownCtx := context.WithTimeout(context.Background(), srv.cfg.WriteTimeout)
	defer cancelShutdownCtx()

	if err := srv.server.Shutdown(shutdownCtx); err != nil {
		utils.Die(err, "shutdown http server")
	}
}

func (srv *APIServer) registerRoutes(router *httprouter.Router) {
	srv.registerHandler(router, "/token", TokenRouteFn, http.MethodPost)

	srv.registerHandler(router, "/introspect", IntrospectRouteFn, http.MethodGet)

	srv.registerHandler(router, "/add/upload", CreateUploadRouteFn, http.MethodPost)
	srv.registerHandler(router, "/uploads/:sha1/addpart", AddUploadPartRouteFn, http.MethodPost)
	srv.registerHandler(router, "/uploads/:sha1/remove", RemoveUploadRouteFn, http.MethodPost)
}

func (srv *APIServer) registerHandler(router *httprouter.Router, path string, handlerFn HandlerFn, methods ...string) {
	path = srv.path(path)

	fn := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		hnd, err := handlerFn(srv, r, ps)
		if err != nil {
			srv.handleError(w, err)
			return
		}

		user, _ := r.Context().Value(ContextKeyUser{}).(*postgres.UserView)

		hnd.SetRequest(r)
		hnd.SetUser(user)

		canAccess := hnd.CanAccess(user)
		fields := []log.Field{
			log.String("method", r.Method),
			log.String("uri", r.URL.RequestURI()),
			log.Bool("access", canAccess),
		}

		if user == nil {
			fields = append(fields, log.Nil("username"))
		} else {
			fields = append(fields, log.String("username", user.Username))
		}

		srv.logger.Debug("can access", fields...)

		if !canAccess {
			srv.handleError(w, ErrForbidden())
			return
		}

		switch r.Method {
		case http.MethodGet:
			getHnd, ok := hnd.(GETHandler)
			if ok {
				getHnd.HandleGET(srv, w, r)
				return
			}
		case http.MethodPost:
			postHnd, ok := hnd.(POSTHandler)
			if ok {
				postHnd.HandlePOST(srv, w, r)
				return
			}
		}

		srv.handleError(w, ErrMethodNotAllowed())
	}

	for _, method := range methods {
		router.Handle(method, path, fn)
	}
}

func (srv *APIServer) handleError(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json")

	res, ok := err.(*ErrorResponse)
	if !ok {
		srv.logger.Error("internal server error", log.Err(err))
		w.WriteHeader(http.StatusInternalServerError)
		if _, werr := w.Write([]byte(`{"error": "internal server error"}`)); werr != nil {
			srv.logger.Debug("write error response", log.Err(werr))
		}
		return
	}

	switch res.statusCode {
	case http.StatusBadRequest:
		srv.logger.Warn("bad request", log.Err(res))
	}

	w.WriteHeader(res.statusCode)

	if werr := json.NewEncoder(w).Encode(res); werr != nil {
		srv.logger.Debug("write error response", log.Err(werr)) // encoding error unlikely
	}
}

func (srv *APIServer) jsonResponse(w http.ResponseWriter, res interface{}) {
	w.Header().Set("Content-Type", "application/json")

	if werr := json.NewEncoder(w).Encode(res); werr != nil {
		srv.logger.Debug("write response", log.Err(werr)) // encoding error unlikely
	}
}

func (srv *APIServer) path(p string) string {
	return path.Join(srv.cfg.RootPath, p)
}

func (srv *APIServer) rateLimited(resource string, max int, ttl time.Duration, w http.ResponseWriter, r *http.Request, fn func(*APIServer, http.ResponseWriter, *http.Request)) {
	ip := utils.RequestIP(r, srv.cfg.TrustXForwardedFor, srv.cfg.TrustXRealIP)
	if ip == nil {
		srv.handleError(w, ErrBadRequest(errors.New("could not determine request ip"), ""))
		return
	}

	limited, err := srv.rmc.IsRateLimited(srv.appCtx, resource, "remote_ip", ip.String(), max, ttl)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	if limited {
		srv.handleError(w, ErrTooManyRequests())
		return
	}

	fn(srv, w, r)
}
