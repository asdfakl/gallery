package apiserver

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/asdfakl/gallery/app/jwt"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
)

const TokenWaitDuration time.Duration = 500 * time.Millisecond

type TokenRoute struct {
	Route
}

func TokenRouteFn(srv *APIServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &TokenRoute{}, nil
}

func (route *TokenRoute) CanAccess(user *postgres.UserView) bool {
	return true
}

func (route *TokenRoute) HandlePOST(srv *APIServer, w http.ResponseWriter, r *http.Request) {
	srv.rateLimited("token", 10, 5*srv.cfg.TokenTTL, w, r, route.handlePOST)
}

func (route *TokenRoute) handlePOST(srv *APIServer, w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()

	p := &struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		srv.handleError(w, ErrBadRequest(err, "decode json payload"))
		return
	}

	success := true

	user, err := srv.pg.GetUserViewByUsername(srv.appCtx, p.Username)
	if err != nil {
		srv.handleError(w, err)
		return
	}

	if success {
		if user == nil {
			success = false
			srv.logger.Debug("user not found", log.String("username", p.Username))
		}
	}

	if success {
		if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(p.Password)); err != nil {
			success = false
			srv.logger.Debug("password mismatch")
		}
	}

	time.Sleep(time.Until(startTime.Add(TokenWaitDuration)))

	if success {
		token := &TokenPayload{
			JWTPayload: jwt.JWTPayload{
				Issuer:  srv.cfg.Origin,
				Expiry:  time.Now().Add(srv.cfg.TokenTTL).Unix(),
				Subject: user.ExtID.String(),
			},
		}

		srv.jsonResponse(w, string(jwt.NewJWTHS256(token, []byte(srv.cfg.Secret))))
	} else {
		srv.handleError(w, ErrUnauthorized(ERR_CODE_UNAUTHORIZED, "invalid username or password"))
	}
}
