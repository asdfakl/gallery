#!/bin/bash

script_dir="$(dirname "$(realpath "$0")")"

cd "$script_dir" || exit 1

chmod 0644 ./docker-entrypoint-initdb.d/*.conf || exit 1

chmod 0755 ./docker-entrypoint-initdb.d/*.sh || exit 1

container=$(docker container ls -aq -f 'name=asdfakl-gallery-postgres')

volume=$(docker volume ls -q -f 'name=asdfakl-gallery-postgres-data')

if [ -n "$container" ]; then
	docker container rm -f "$container" || exit 1
fi

if [ -n "$volume" ]; then
	docker volume rm "$volume" || exit 1
fi

docker run -d \
	--net asdfakl-gallery \
	--hostname postgres \
	--env-file ./example.env \
	--name asdfakl-gallery-postgres \
	--mount "type=bind,src=$(pwd)/docker-entrypoint-initdb.d/00_init.sh,dst=/docker-entrypoint-initdb.d/00_init.sh,readonly" \
	--mount "type=bind,src=$(pwd)/docker-entrypoint-initdb.d/90_roles.sql,dst=/docker-entrypoint-initdb.d/90_roles.sql,readonly" \
	--mount "type=bind,src=$(pwd)/docker-entrypoint-initdb.d/pg_hba.conf,dst=/docker-entrypoint-initdb.d/pg_hba.conf,readonly" \
	--mount type=volume,src=asdfakl-gallery-postgres-data,dst=/var/lib/postgresql/data \
	--log-opt mode=non-blocking \
	--log-opt max-buffer-size=2m \
	postgres:13.8
