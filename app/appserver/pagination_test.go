package appserver

import (
	"testing"
)

func TestPaginationItems(t *testing.T) {
	type TestCase struct {
		p        *Pagination
		expected []*PaginationItem
	}

	cases := []TestCase{
		{
			p: &Pagination{
				Count:      0,
				Offset:     0,
				TotalCount: 0,
			},
			expected: nil,
		},

		{
			p: &Pagination{
				Count:      50,
				Offset:     0,
				TotalCount: 50,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: true},
			},
		},
		{
			p: &Pagination{
				Count:      50,
				Offset:     49,
				TotalCount: 50,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: true},
			},
		},

		{
			p: &Pagination{
				Count:      10,
				Offset:     0,
				TotalCount: 30,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: true},
				{Text: "2", Offset: 10, IsDivider: false, IsCurrent: false},
				{Text: "3", Offset: 20, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      10,
				Offset:     10,
				TotalCount: 30,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "2", Offset: 10, IsDivider: false, IsCurrent: true},
				{Text: "3", Offset: 20, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      10,
				Offset:     20,
				TotalCount: 30,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "2", Offset: 10, IsDivider: false, IsCurrent: false},
				{Text: "3", Offset: 20, IsDivider: false, IsCurrent: true},
			},
		},

		{
			p: &Pagination{
				Count:      30,
				Offset:     29,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: true},
				{Text: "2", Offset: 30, IsDivider: false, IsCurrent: false},
				{Text: "3", Offset: 60, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     30,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "2", Offset: 30, IsDivider: false, IsCurrent: true},
				{Text: "3", Offset: 60, IsDivider: false, IsCurrent: false},
				{Text: "4", Offset: 90, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     60,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "2", Offset: 30, IsDivider: false, IsCurrent: false},
				{Text: "3", Offset: 60, IsDivider: false, IsCurrent: true},
				{Text: "4", Offset: 90, IsDivider: false, IsCurrent: false},
				{Text: "5", Offset: 120, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     119,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "2", Offset: 30, IsDivider: false, IsCurrent: false},
				{Text: "3", Offset: 60, IsDivider: false, IsCurrent: false},
				{Text: "4", Offset: 90, IsDivider: false, IsCurrent: true},
				{Text: "5", Offset: 120, IsDivider: false, IsCurrent: false},
				{Text: "6", Offset: 150, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     135,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "3", Offset: 60, IsDivider: false, IsCurrent: false},
				{Text: "4", Offset: 90, IsDivider: false, IsCurrent: false},
				{Text: "5", Offset: 120, IsDivider: false, IsCurrent: true},
				{Text: "6", Offset: 150, IsDivider: false, IsCurrent: false},
				{Text: "7", Offset: 180, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     150,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "4", Offset: 90, IsDivider: false, IsCurrent: false},
				{Text: "5", Offset: 120, IsDivider: false, IsCurrent: false},
				{Text: "6", Offset: 150, IsDivider: false, IsCurrent: true},
				{Text: "7", Offset: 180, IsDivider: false, IsCurrent: false},
				{Text: "8", Offset: 210, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     209,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "5", Offset: 120, IsDivider: false, IsCurrent: false},
				{Text: "6", Offset: 150, IsDivider: false, IsCurrent: false},
				{Text: "7", Offset: 180, IsDivider: false, IsCurrent: true},
				{Text: "8", Offset: 210, IsDivider: false, IsCurrent: false},
				{Text: "9", Offset: 240, IsDivider: false, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     210,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "6", Offset: 150, IsDivider: false, IsCurrent: false},
				{Text: "7", Offset: 180, IsDivider: false, IsCurrent: false},
				{Text: "8", Offset: 210, IsDivider: false, IsCurrent: true},
				{Text: "9", Offset: 240, IsDivider: false, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     249,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "7", Offset: 180, IsDivider: false, IsCurrent: false},
				{Text: "8", Offset: 210, IsDivider: false, IsCurrent: false},
				{Text: "9", Offset: 240, IsDivider: false, IsCurrent: true},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: false},
			},
		},
		{
			p: &Pagination{
				Count:      30,
				Offset:     270,
				TotalCount: 295,
			},
			expected: []*PaginationItem{
				{Text: "1", Offset: 0, IsDivider: false, IsCurrent: false},
				{Text: "", Offset: 0, IsDivider: true, IsCurrent: false},
				{Text: "8", Offset: 210, IsDivider: false, IsCurrent: false},
				{Text: "9", Offset: 240, IsDivider: false, IsCurrent: false},
				{Text: "10", Offset: 270, IsDivider: false, IsCurrent: true},
			},
		},
	}

	for i, tc := range cases {
		items := tc.p.Items()

		if len(items) != len(tc.expected) {
			t.Errorf("test case %d: expected %d items, got %d", i, len(tc.expected), len(items))
			continue
		}
		for j := range items {
			if items[j].Text != tc.expected[j].Text {
				t.Errorf("test case %d: item %d: expected text '%s', got '%s'", i, j, tc.expected[j].Text, items[j].Text)
			}
			if items[j].Offset != tc.expected[j].Offset {
				t.Errorf("test case %d: item %d: expected offset %d got %d", i, j, tc.expected[j].Offset, items[j].Offset)
			}
			if items[j].IsDivider != tc.expected[j].IsDivider {
				t.Errorf("test case %d: item %d: expected is_divider %t, got %t", i, j, tc.expected[j].IsDivider, items[j].IsDivider)
			}
			if items[j].IsCurrent != tc.expected[j].IsCurrent {
				t.Errorf("test case %d: item %d: expected is_current %t, got %t", i, j, tc.expected[j].IsCurrent, items[j].IsCurrent)
			}
		}
	}
}
