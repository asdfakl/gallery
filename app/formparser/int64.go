package formparser

import (
	"fmt"
	"strconv"
)

type FormInt64Field struct {
	parser *FormParser
	key    string
	value  *int64
}

func (parser *FormParser) Int64(key string) (field *FormInt64Field) {
	field = &FormInt64Field{
		parser: parser,
		key:    key,
	}

	vals, err := parser.src(key)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse form: %w", err))
		return
	}

	if len(vals) == 0 {
		return
	}

	if len(vals) > 1 {
		parser.registerError(key, ErrFormParserAmbiguous)
		return
	}

	i, err := strconv.ParseInt(vals[0], 10, 64)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse int64: %w", err))
		return
	}

	field.value = &i
	return
}

func (field *FormInt64Field) Required() *FormInt64Field {
	if field.value == nil {
		field.parser.registerError(field.key, ErrFormParserRequired)
	}
	return field
}

func (field *FormInt64Field) Max(max int64) *FormInt64Field {
	if field.value == nil {
		return field
	}

	if *field.value > max {
		field.parser.registerError(field.key, ErrFormParserMaxValueExceeded)
	}
	return field
}

func (field *FormInt64Field) Min(min int64) *FormInt64Field {
	if field.value == nil {
		return field
	}

	if *field.value < min {
		field.parser.registerError(field.key, ErrFormParserMinValueSubceeded)
	}
	return field
}

func (field *FormInt64Field) Options(opts []int64) *FormInt64Field {
	if field.value == nil {
		return field
	}

	match := false
	for _, opt := range opts {
		if *field.value == opt {
			match = true
			break
		}
	}

	if !match {
		field.parser.registerError(field.key, ErrFormParserDoesNotMatchOptions)
	}
	return field
}

func (field *FormInt64Field) Value() int64 {
	field.parser.mustHaveErrChecked()

	return *field.value
}

func (field *FormInt64Field) OptionalValue() *int64 {
	field.parser.mustHaveErrChecked()

	return field.value
}
