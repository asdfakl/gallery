package config

import (
	"errors"
	"flag"
	"path/filepath"
	"strings"

	"gitlab.com/asdfakl/gallery/app/utils"
)

func SupportedThumbnailContentTypes() []*utils.QualifiedContentType {
	return []*utils.QualifiedContentType{
		{Type: "image", Subtype: "jpeg"},
		{Type: "image", Subtype: "png"},
		{Type: "video", Subtype: "mp4"},
		{Type: "video", Subtype: "quicktime"},
	}
}

type ThumbStorageConfiguration struct {
	Path string
}

func DefaultThumbStorageConfiguration() *ThumbStorageConfiguration {
	return &ThumbStorageConfiguration{
		Path: "/var/lib/gallerybg/thumbs/sha1",
	}
}

func (cfg *ThumbStorageConfiguration) InitFlagSet(fs *flag.FlagSet) {
	utils.StringVarOrEnv(fs, &cfg.Path, "thumbs-path", cfg.Path, "thumb storage filepath")
}

func (cfg *ThumbStorageConfiguration) Trim() {
	cfg.Path = filepath.Clean(strings.TrimSpace(cfg.Path))
}

func (cfg *ThumbStorageConfiguration) Validate() error {
	for _, assert := range []assertion{
		{cfg.Path != "", errors.New("thumbs-path empty or undefined")},
		{cfg.Path != ".", errors.New("thumbs-path empty or undefined")},
	} {
		if !assert.ok {
			return assert.err
		}
	}
	return nil
}
