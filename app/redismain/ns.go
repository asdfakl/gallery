package redismain

import (
	"fmt"
	"regexp"
)

const (
	BACKGROUND_JOB_QUEUE_MAX_LENGTH                     = 1024 * 1024
	BACKGROUND_JOB_QUEUE_LENGTH_ENFORCEMENT_PROBABILITY = 0.01
)

var keyExp = regexp.MustCompile(`^[a-z]+(_[a-z]+)*:[[:graph:]]+$`)

func validateKey(key string) string {
	if !keyExp.MatchString(key) {
		panic(fmt.Errorf("invalid redis-main key: '%s'", key))
	}
	return key
}

var ns = struct {
	lock                 func(string) string
	backgroundJobTrigger func(string) string
	backgroundJobQueue   func(string) string
	session              func(SessionKey) string
	rateLimit            func(string) string
	counter              func(string) string
	freq                 func(string) string
}{
	lock: func(key string) string {
		return validateKey("lock:" + key)
	},

	backgroundJobTrigger: func(key string) string {
		return validateKey("bg:trigger:" + key)
	},

	backgroundJobQueue: func(key string) string {
		return validateKey("bg:queue:" + key)
	},

	session: func(key SessionKey) string {
		return validateKey("session:" + key.String())
	},

	rateLimit: func(resource string) string {
		return validateKey("rate_limit:" + resource)
	},

	counter: func(key string) string {
		return validateKey("counter:" + key)
	},

	freq: func(key string) string {
		return validateKey("freq:" + key)
	},
}
