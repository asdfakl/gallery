package appserver

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type UserModifyPasswordPage struct {
	Page

	ModifyUser *postgres.UserView
}

func UserModifyPasswordPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("user not found")
	}

	// load
	user, err := srv.pg.GetUserViewByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get user view by ext_id")
	}
	if user == nil {
		return nil, ErrNotFound("user not found")
	}

	return &UserModifyPasswordPage{
		ModifyUser: user,
	}, nil
}

func (page *UserModifyPasswordPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.ModifyUser.ID
}

func (page *UserModifyPasswordPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "user_modify_password_form.html", page, http.StatusOK)
}

func (page *UserModifyPasswordPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.rateLimited("modify_password", 6, time.Hour, w, r, page.handlePOST)
}

func (page *UserModifyPasswordPage) handlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		currentPasswordField = parser.String("current_password").Required().Trim().NonEmpty().MaxLength(64)
		newPassword1Field    = parser.String("new_password_1").Required().Trim().NonEmpty().MaxLength(64)
		newPassword2Field    = parser.String("new_password_2").Required().Trim().NonEmpty().MaxLength(64)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	currentPassword := currentPasswordField.Value()
	if err := bcrypt.CompareHashAndPassword([]byte(page.ModifyUser.Password), []byte(currentPassword)); err != nil {
		srv.handleError(w, ErrForbidden("current password does not match"))
		return
	}

	newPassword1 := newPassword1Field.Value()
	newPassword2 := newPassword2Field.Value()

	if newPassword1 != newPassword2 {
		srv.handleError(w, ErrBadRequest(nil, "new password mismatch"))
		return
	}
	if newPassword1 == currentPassword {
		srv.handleError(w, ErrBadRequest(nil, "new password must be distinct from old password"))
		return
	}

	// TODO validate with zxcvbn golang port

	h, err := bcrypt.GenerateFromPassword([]byte(newPassword1), bcrypt.DefaultCost)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "generate bcrypt hash from password"))
		return
	}

	if err := srv.pg.UpdateUserPassword(
		srv.appCtx,
		page.ModifyUser.ID,
		string(h),
	); err != nil {
		srv.handleError(w, utils.Wrap(err, "update user password"))
		return
	}

	srv.renderJSON(w, &JSONResponse{}, http.StatusOK)
}
