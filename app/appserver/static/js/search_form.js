"use strict";

document.addEventListener("DOMContentLoaded", () => {
	const initJSSearchForm = nodes => {
		nodes.forEach(node => {
			const targetID = node.attributes["data-target"].nodeValue;
			const target = document.getElementById(targetID);
			const path = node.attributes["data-path"].nodeValue;

			const textInputs = node.querySelectorAll("input[type=text]");
			const selects = node.querySelectorAll("select");
			const radioInputs = node.querySelectorAll("input[type=radio]");
			const checkboxInputs = node.querySelectorAll("input[type=checkbox]");
			const hiddenInputs = node.querySelectorAll("input[type=hidden]");

			const tags = node.querySelectorAll(".js-search-form-tag");

			const base = {
				offset: 0,
			};

			const refresh = () => {
				const q = { ...base };

				radioInputs.forEach(input => {
					if (!input.checked) {
						return;
					}
					if (input.classList.contains("js-omit-empty") && !input.value) {
						return;
					}

					q[input.name] = input.value;
				});

				checkboxInputs.forEach(input => {
					if (!input.checked) {
						return;
					}

					q[input.name] = q[input.name] || [];
					q[input.name].push(input.value);
				});

				textInputs.forEach(input => {
					const key = input.name;
					const val = input.value.trim();

					if (val) {
						q[key] = val;
						input.classList.add("is-success");
					} else {
						input.classList.remove("is-success");
					}
				});

				selects.forEach(select => {
					const key = select.name;
					const val = select.value;

					if (val) {
						q[key] = val;
						select.closest("div.select").classList.add("is-success");
					} else {
						select.closest("div.select").classList.remove("is-success");
					}
				});

				switch (q["display"]) {
					case "small_icons":
						q["count"] = 120;
						break;
					case "large_icons":
						q["count"] = 54;
						break;
					default:
						q["count"] = 15;
				}

				target.attributes["href"].nodeValue = `${path}?${Utils.queryString(q)}`;
			};

			const slowRefresh = Utils.debounce(refresh, 600);
			const fastRefresh = Utils.debounce(refresh, 100);

			textInputs.forEach(input => {
				input.addEventListener("keyup", e => {
					base.offset = 0;
					slowRefresh();
				});
			});

			selects.forEach(select => {
				select.addEventListener("change", e => {
					base.offset = 0;
					slowRefresh();
				});
			});

			radioInputs.forEach(input => {
				input.addEventListener("change", e => {
					base.offset = 0;
					slowRefresh();
				});
			});

			checkboxInputs.forEach(input => {
				input.addEventListener("change", e => {
					base.offset = 0;
					slowRefresh();
				});
			});

			hiddenInputs.forEach(input => {
				base[input.name] = input.value;
			})

			tags.forEach(tag => {
				const value = tag.attributes["data-value"].nodeValue;
				const targetID = tag.attributes["data-target-id"].nodeValue;

				tag.addEventListener("click", e => {
					const target = document.getElementById(targetID);

					const event = new CustomEvent("keyup", {
						bubbles: false,
					});

					target.value = value;
					target.dispatchEvent(event);
				});
			});

			target.addEventListener("RequestPaginationOffset", e => {
				const offset = e.detail && e.detail.offset;
				if (typeof offset === "number" && offset !== base.offset) {
					base.offset = offset;
					fastRefresh();
				}
			});

			setTimeout(refresh, 0);
		});
	};

	initJSSearchForm(document.querySelectorAll(".js-search-form"));
});
