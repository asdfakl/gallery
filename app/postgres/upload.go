package postgres

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"time"

	"github.com/jackc/pgx/v4"
)

type UploadViewList []*UploadView
type UploadView struct {
	SHA1       [sha1.Size]byte
	Basename   string
	Size       int64
	ModifiedAt time.Time
	Progress   int64
	Owner      int64
	Public     bool
	Tags       []string
}

func (upload *UploadView) HexSHA1() string {
	return hex.EncodeToString(upload.SHA1[:])
}

func (pg *PostgresConnection) ModifyUpload(ctx context.Context, upload *UploadView) error {
	q := `
		UPDATE
			gallery.upload
		SET
			basename = $1,
			modified_at = $2,
			progress = $3,
			public = $4,
			tags = $5
		WHERE
			sha1 = $6
			AND validity_period @> now()
	`

	_, err := pg.cp.Exec(
		ctx, q,
		upload.Basename, upload.ModifiedAt, upload.Progress, upload.Public, upload.Tags,
		upload.SHA1[:],
	)

	return err
}

func (pg *PostgresConnection) InvalidateUpload(ctx context.Context, sum [sha1.Size]byte) error {
	q := `
		UPDATE
			gallery.upload
		SET
			validity_period = TSTZRANGE(lower(validity_period), now())
		WHERE
			sha1 = $1
			AND validity_period @> now()
	`

	_, err := pg.cp.Exec(ctx, q, sum[:])

	return err
}

func (pg *PostgresConnection) InsertUpload(ctx context.Context, upload *UploadView, validity BoundedTimerange) error {
	q := `
		INSERT INTO gallery.upload (
			sha1,
			basename,
			size,
			modified_at,
			owner,
			public,
			tags,
			validity_period
		) VALUES (
			$1, $2, $3, $4, $5, $6, $7, TSTZRANGE($8, $9)
		)
	`

	_, err := pg.cp.Exec(
		ctx, q,
		upload.SHA1[:], upload.Basename, upload.Size, upload.ModifiedAt, upload.Owner, upload.Public, upload.Tags,
		validity[0], validity[1],
	)

	return err
}

func (pg *PostgresConnection) DeleteExpiredUploads(ctx context.Context) error {
	q := `
		DELETE FROM
			gallery.upload
		WHERE
			upper(validity_period) < now()
	`

	_, err := pg.cp.Exec(ctx, q)

	return err
}

func (pg *PostgresConnection) GetUploadViewBySHA1(ctx context.Context, sum [sha1.Size]byte) (*UploadView, error) {
	q := `
		SELECT
			u.sha1,
			u.basename,
			u.size,
			u.modified_at,
			u.progress,
			u.owner,
			u.public,
			u.tags
		FROM
			gallery.v_upload u
		WHERE
			u.sha1 = $1
	`

	return scanUplaodViewFromRow(pg.cp.QueryRow(ctx, q, sum[:]))
}

func (pg *PostgresConnection) CountUploadsByOwner(ctx context.Context, ownerID int64) (int, int64, error) {
	q := `
		SELECT
			COUNT(*) AS count,
			COALESCE(SUM(u.size), 0) AS size
		FROM
			gallery.v_upload u
		WHERE
			u.owner = $1
	`

	count, size := int(0), int64(0)

	if err := pg.cp.QueryRow(ctx, q, ownerID).Scan(&count, &size); err != nil {
		return 0, 0, err
	}
	return count, size, nil
}

func (pg *PostgresConnection) ListEveryUploadView(ctx context.Context, callback func(UploadViewList) error) error {
	q := `
		SELECT
			u.sha1,
			u.basename,
			u.size,
			u.modified_at,
			u.progress,
			u.owner,
			u.public,
			u.tags
		FROM
			gallery.v_upload u
	`

	rows, err := pg.cp.Query(ctx, q)
	if err != nil {
		return err
	}
	defer rows.Close()

	var uploads UploadViewList
	for rows.Next() {
		upload, err := scanUploadViewsFromRows(rows)
		if err != nil {
			return err
		}

		uploads = append(uploads, upload)

		if len(uploads) >= ChunkSize {
			if err = callback(uploads); err != nil {
				return err
			}
			uploads = nil
		}
	}

	if len(uploads) > 0 {
		if err = callback(uploads); err != nil {
			return err
		}
	}

	return rows.Err()
}

func scanUplaodViewFromRow(row pgx.Row) (*UploadView, error) {
	upload := &UploadView{}
	sum := make([]byte, sha1.Size)

	if err := row.Scan(
		&sum,
		&upload.Basename,
		&upload.Size,
		&upload.ModifiedAt,
		&upload.Progress,
		&upload.Owner,
		&upload.Public,
		&upload.Tags,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	copy(upload.SHA1[:], sum)

	return upload, nil
}

func scanUploadViewsFromRows(rows pgx.Rows) (*UploadView, error) {
	upload := &UploadView{}
	sum := make([]byte, sha1.Size)

	if err := rows.Scan(
		&sum,
		&upload.Basename,
		&upload.Size,
		&upload.ModifiedAt,
		&upload.Progress,
		&upload.Owner,
		&upload.Public,
		&upload.Tags,
	); err != nil {
		return nil, err
	}

	copy(upload.SHA1[:], sum)

	return upload, nil
}
