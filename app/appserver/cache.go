package appserver

import (
	"time"

	"gitlab.com/asdfakl/gallery/app/objectstorage"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	CACHED_USER_VIEWS_TTL        time.Duration = 20 * time.Minute
	CACHED_TAG_FREQUENCIES_TTL   time.Duration = 5 * time.Minute
	CACHED_TAG_FREQUENCIES_LIMIT               = 35
)

type CachedObjectStorage struct {
	expires time.Time
	storage objectstorage.Storage
}

func (srv *AppServer) cachedObjectStorageByID(id int64, ttl time.Duration) (objectstorage.Storage, error) {
	srv.cachedStoragesMutex.RLock()
	cached := srv.cachedStorages[id]
	srv.cachedStoragesMutex.RUnlock()

	if cached != nil && cached.expires.After(time.Now()) {
		return cached.storage, nil
	}

	srv.cachedStoragesMutex.Lock()
	defer srv.cachedStoragesMutex.Unlock()

	// cache refreshed while waiting for exclusive lock?
	cached = srv.cachedStorages[id]
	if cached != nil && cached.expires.After(time.Now()) {
		return cached.storage, nil
	}

	dbStorage, err := srv.pg.GetStorageByID(srv.appCtx, id)
	if err != nil {
		return nil, utils.Wrap(err, "query storage by id")
	}
	if dbStorage == nil {
		return nil, nil
	}

	storage, err := objectstorage.NewStorage(id, dbStorage.Type, dbStorage.Opts, srv.logger)
	if err != nil {
		return nil, utils.Wrap(err, "initialize object storage")
	}

	srv.cachedStorages[id] = &CachedObjectStorage{
		expires: time.Now().Add(ttl),
		storage: storage,
	}

	return srv.cachedStorages[id].storage, nil

}

type CachedBackgroundJobQueue struct {
	expires time.Time
	queue   *postgres.BackgroundJobQueue
}

func (srv *AppServer) cachedBackgroundJobQueueByFunc(fnKey postgres.BackgroundJobFnKey, ttl time.Duration) (*postgres.BackgroundJobQueue, error) {
	srv.cachedBackgroundJobQueuesMutex.RLock()
	cached := srv.cachedBackgroundJobQueues[fnKey]
	srv.cachedBackgroundJobQueuesMutex.RUnlock()

	if cached != nil && cached.expires.After(time.Now()) {
		return cached.queue, nil
	}

	srv.cachedBackgroundJobQueuesMutex.Lock()
	defer srv.cachedBackgroundJobQueuesMutex.Unlock()

	// cache refreshed while waiting for exclusive lock?
	cached = srv.cachedBackgroundJobQueues[fnKey]
	if cached != nil && cached.expires.After(time.Now()) {
		return cached.queue, nil
	}

	queue, err := srv.pg.GetBackgroundJobQueueByFunc(srv.appCtx, fnKey)
	if err != nil {
		return nil, utils.Wrap(err, "query bg job queue by func")
	}
	if queue == nil {
		return nil, nil
	}

	srv.cachedBackgroundJobQueues[fnKey] = &CachedBackgroundJobQueue{
		expires: time.Now().Add(ttl),
		queue:   queue,
	}

	return srv.cachedBackgroundJobQueues[fnKey].queue, nil
}

func (srv *AppServer) cachedUserViewByID(id int64) (*postgres.UserView, error) {
	srv.cachedUserViewsMutex.RLock()
	cached, expiry := srv.cachedUserViews[id], srv.cachedUserViewsExpiry
	srv.cachedUserViewsMutex.RUnlock()

	if expiry.After(time.Now()) {
		return cached, nil
	}

	srv.cachedUserViewsMutex.Lock()
	defer srv.cachedUserViewsMutex.Unlock()

	// cache refreshed while waiting for exclusive lock?
	if srv.cachedUserViewsExpiry.After(time.Now()) {
		return srv.cachedUserViews[id], nil
	}

	srv.logger.Debug("refresh user cache")

	users, err := srv.pg.GetEveryUserView(srv.appCtx)
	if err != nil {
		return nil, utils.Wrap(err, "get every user view")
	}

	srv.cachedUserViews = users.IDMap()
	srv.cachedUserViewsExpiry = time.Now().Add(CACHED_USER_VIEWS_TTL)

	return srv.cachedUserViews[id], nil
}

func (srv *AppServer) allCachedTagFrequencies() (postgres.TagFrequencyList, error) {
	srv.cachedTagFrequenciesMutex.RLock()
	cached, expiry := srv.cachedTagFrequencies, srv.cachedTagFrequenciesExpiry
	srv.cachedTagFrequenciesMutex.RUnlock()

	if expiry.After(time.Now()) {
		return cached, nil
	}

	srv.cachedTagFrequenciesMutex.Lock()
	defer srv.cachedTagFrequenciesMutex.Unlock()

	// cache refreshed while waiting for exclusive lock?
	if srv.cachedTagFrequenciesExpiry.After(time.Now()) {
		return srv.cachedTagFrequencies, nil
	}

	srv.logger.Debug("refresh tag frequency cache")

	freqs, err := srv.pg.GetPublicObjectViewTagFrequencyList(srv.appCtx, CACHED_TAG_FREQUENCIES_LIMIT)
	if err != nil {
		return nil, utils.Wrap(err, "get public object view tag frequency list")
	}

	srv.cachedTagFrequencies = freqs
	srv.cachedTagFrequenciesExpiry = time.Now().Add(CACHED_TAG_FREQUENCIES_TTL)

	return srv.cachedTagFrequencies, nil
}
