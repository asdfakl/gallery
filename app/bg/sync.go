package bg

import (
	"context"
	"crypto/sha1"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/objectstorage"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

func syncS3Storage(ctx context.Context, pg *postgres.PostgresConnection, dbStorage *postgres.StorageView, logger *log.Logger) error {
	storage, err := objectstorage.NewS3Storage(dbStorage.ID, dbStorage.Opts, logger)
	if err != nil {
		return utils.Wrap(err, "initialize s3 storage")
	}

	allKnownSums := make(map[[sha1.Size]byte]struct{})
	listStorageObjectsCallback := func(sums [][sha1.Size]byte) error {
		objs, err := pg.GetStorageObjectListBySHA1(ctx, dbStorage.ID, sums)
		if err != nil {
			return utils.Wrap(err, "get db storage object list by sha1 sums")
		}
		objm := objs.SHA1Map()

		for _, sum := range sums {
			allKnownSums[sum] = struct{}{}

			// already imported
			if obj := objm[sum]; obj != nil {
				// check if storage obj should be deleted
				if obj.Deleted {
					logger.Info(
						"deleting storage object marked as deleted",
						log.Int64("object_id", obj.ID),
						log.SHA1("sha1", sum),
					)
					if err = storage.DeleteObject(ctx, sum); err != nil {
						return utils.Wrap(err, "delete storage object")
					}
					delete(allKnownSums, sum)
				}
				continue
			}

			// import object
			logger.Info("importing storage object", log.SHA1("sha1", sum))
			if err = storage.ImportObject(ctx, pg, sum); err != nil {
				return utils.Wrap(err, "import storage object")
			}
		}

		return nil
	}

	if err = storage.ListObjects(ctx, listStorageObjectsCallback); err != nil {
		return utils.Wrap(err, "list db storage objects")
	}

	listDBStorageObjectsCallback := func(objs postgres.ObjectList) error {
		// remove db storage objects which do not exist in the storage
		for _, obj := range objs {
			if _, exists := allKnownSums[obj.SHA1]; !exists {
				if err = pg.DeleteStorageObject(ctx, dbStorage.ID, obj.ID); err != nil {
					return utils.Wrap(err, "delete db storage object")
				}
			}
		}

		return nil
	}

	return utils.Wrap(
		pg.ListEveryStorageObject(ctx, dbStorage.ID, listDBStorageObjectsCallback),
		"list every db storage object",
	)
}

func syncLocalStorage(ctx context.Context, pg *postgres.PostgresConnection, dbStorage *postgres.StorageView, logger *log.Logger) error {
	storage, err := objectstorage.NewLocalStorage(dbStorage.ID, dbStorage.Opts, logger)
	if err != nil {
		return utils.Wrap(err, "initialize local storage")
	}

	allKnownSums := make(map[[sha1.Size]byte]struct{})
	listStorageObjectsCallback := func(sums [][sha1.Size]byte) error {
		objs, err := pg.GetStorageObjectListBySHA1(ctx, dbStorage.ID, sums)
		if err != nil {
			return utils.Wrap(err, "get db storage object list by sha1 sums")
		}
		objm := objs.SHA1Map()

		for _, sum := range sums {
			allKnownSums[sum] = struct{}{}

			if obj := objm[sum]; obj == nil || obj.Deleted {
				// db storage object does not exist or is marked deleted
				// -> remove storage object
				logger.Info("deleting storage object", log.SHA1("sha1", sum))
				if err = storage.DeleteObject(ctx, sum); err != nil {
					return utils.Wrap(err, "delete storage object")
				}
				delete(allKnownSums, sum)
			}
		}

		return nil
	}

	if err = storage.ListObjects(ctx, listStorageObjectsCallback); err != nil {
		return utils.Wrap(err, "list db storage objects")
	}

	listDBStorageObjectsCallback := func(objs postgres.ObjectList) error {
		// remove db storage objects which do not exist in the storage
		for _, obj := range objs {
			if _, exists := allKnownSums[obj.SHA1]; !exists {
				if err = pg.DeleteStorageObject(ctx, dbStorage.ID, obj.ID); err != nil {
					return utils.Wrap(err, "delete db storage object")
				}
			}
		}

		return nil
	}

	return utils.Wrap(
		pg.ListEveryStorageObject(ctx, dbStorage.ID, listDBStorageObjectsCallback),
		"list every db storage object",
	)
}
