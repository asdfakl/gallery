package formparser

import (
	"fmt"

	"github.com/google/uuid"
)

type FormUUIDField struct {
	parser *FormParser
	key    string
	value  uuid.UUID
}

func (parser *FormParser) UUID(key string) (field *FormUUIDField) {
	field = &FormUUIDField{
		parser: parser,
		key:    key,
	}

	vals, err := parser.src(key)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse form: %w", err))
		return
	}

	if len(vals) == 0 {
		return
	}

	if len(vals) > 1 {
		parser.registerError(key, ErrFormParserAmbiguous)
		return
	}

	u, err := uuid.Parse(vals[0])
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse uuid: %w", err))
		return
	}
	field.value = u
	return
}

func (field *FormUUIDField) Required() *FormUUIDField {
	if field.value == uuid.Nil {
		field.parser.registerError(field.key, ErrFormParserRequired)
	}
	return field
}

func (field *FormUUIDField) Value() uuid.UUID {
	field.parser.mustHaveErrChecked()

	return field.value
}
