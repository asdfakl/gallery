package objectstorage

import (
	"context"
	"crypto/sha1"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	ChunkSize = 1000
)

type LocalStorage struct {
	id     int64
	path   string
	logger *log.Logger
}

var _ Storage = (*LocalStorage)(nil)

func NewLocalStorage(id int64, opts config.Options, logger *log.Logger) (*LocalStorage, error) {
	var err error
	storage, ok := &LocalStorage{
		id:     id,
		logger: logger,
	}, false

	storage.path, ok = opts.OptionalNonEmptyString("path")
	if !ok {
		return nil, fmt.Errorf("missing required option string: path")
	}

	if utils.IsSymlink(storage.path) {
		storage.path, err = filepath.EvalSymlinks(storage.path)
		if err != nil {
			return nil, utils.Wrap(err, "eval symlinks")
		}
	}

	storage.path, err = filepath.Abs(storage.path)
	if err != nil {
		return nil, utils.Wrap(err, "abs filepath")
	}

	if err = os.MkdirAll(storage.path, 0750); err != nil {
		return nil, utils.Wrap(err, "mkdir all")
	}

	return storage, nil
}

func (storage *LocalStorage) ID() int64 {
	return storage.id
}

func (storage *LocalStorage) DeleteObject(ctx context.Context, sum [sha1.Size]byte) error {
	return storage.deleteKey(ctx, objectKeyFromSHA1(sum))
}

func (storage *LocalStorage) ListObjects(ctx context.Context, callback func([][sha1.Size]byte) error) error {
	var (
		now       = time.Now()
		atRoot    = true
		emptyDirs = map[string]bool{}
		sums      [][sha1.Size]byte
	)

	if err := filepath.Walk(storage.path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if err = ctx.Err(); err != nil {
			return err
		}

		if atRoot {
			atRoot = false
			return nil
		}

		if dir := filepath.Dir(path); emptyDirs[dir] {
			emptyDirs[dir] = false
		}

		if info.Mode()&os.ModeType != 0 {
			if info.IsDir() && now.Sub(info.ModTime()) > time.Hour {
				emptyDirs[path] = true
			}
			return nil
		}

		rel, err := filepath.Rel(storage.path, path)
		if err != nil {
			return err
		}

		h, err := parseObjectKey(rel)
		if err != nil {
			storage.logger.Warn("deleting object with malformed key", log.String("key", rel))
			if err = storage.deleteKey(ctx, rel); err != nil {
				storage.logger.Error("delete object", log.Err(err))
			}
			return nil
		}

		sums = append(sums, h)

		if len(sums) >= ChunkSize {
			if err = callback(sums); err != nil {
				return err
			}
			sums = nil
		}

		return nil
	}); err != nil {
		return err
	}

	if len(sums) > 0 {
		if err := callback(sums); err != nil {
			return err
		}
	}

	for dir, empty := range emptyDirs {
		if !empty {
			continue
		}

		storage.logger.Debug("deleting empty directory", log.String("dir", dir))
		if err := os.Remove(dir); err != nil {
			storage.logger.Error("delete empty directory", log.String("dir", dir), log.Err(err))
		}
	}

	return nil
}

func (storage *LocalStorage) Open(sum [sha1.Size]byte) (*os.File, error) {
	return os.Open(filepath.Join(storage.path, objectKeyFromSHA1(sum)))
}

func (storage *LocalStorage) deleteKey(ctx context.Context, key string) error {
	return os.Remove(filepath.Join(storage.path, key))
}

func (storage *LocalStorage) PullObject(ctx context.Context, sum [sha1.Size]byte, path string) (int64, error) {
	srcPath := filepath.Join(storage.path, objectKeyFromSHA1(sum))

	return utils.Copy(srcPath, path)
}

func (storage *LocalStorage) PushObject(ctx context.Context, sum [sha1.Size]byte, path string) (int64, error) {
	dstPath := filepath.Join(storage.path, objectKeyFromSHA1(sum))

	if err := os.MkdirAll(filepath.Dir(dstPath), utils.DEFAULT_DIRECTORY_PERMISSION_BITS); err != nil {
		return 0, err
	}

	return utils.Copy(path, dstPath)
}
