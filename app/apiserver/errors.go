package apiserver

import "net/http"

const (
	ERR_CODE_UNAUTHORIZED ErrorResponseCode = "unauthorized"

	ERR_CODE_OBJECT_EXISTS ErrorResponseCode = "object_exists"

	ERR_CODE_QUOTA_EXCEEDED ErrorResponseCode = "quota_exceeded"

	ERR_CODE_EXPIRED_OR_UNTRUSTED_TOKEN ErrorResponseCode = "expired_or_untrusted_token"
	ERR_CODE_INVALID_TOKEN              ErrorResponseCode = "invalid_token"
	ERR_CODE_MISSING_TOKEN              ErrorResponseCode = "missing_token"

	ERR_CODE_UPLOAD_COMPLETE ErrorResponseCode = "upload_complete"
	ERR_CODE_UPLOAD_CONFLICT ErrorResponseCode = "upload_conflict"
)

type ErrorResponseCode string

type ErrorResponse struct {
	ErrorCode   ErrorResponseCode `json:"error_code"`
	ErrorDetail string            `json:"error_detail,omitempty"`

	err        error
	statusCode int
}

func ErrNotFound(detail string) *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:   "not_found",
		ErrorDetail: detail,
		statusCode:  http.StatusNotFound,
	}
}

func ErrBadRequest(err error, detail string) *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:   "bad_request",
		ErrorDetail: detail,
		err:         err,
		statusCode:  http.StatusBadRequest,
	}
}

func ErrUnauthorized(public ErrorResponseCode, detail string) *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:   public,
		ErrorDetail: detail,
		statusCode:  http.StatusUnauthorized,
	}
}

func ErrForbidden() *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:  "forbidden",
		statusCode: http.StatusForbidden,
	}
}

func ErrMethodNotAllowed() *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:  "method_not_allowed",
		statusCode: http.StatusMethodNotAllowed,
	}
}

func ErrTooManyRequests() *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:   "too_many_requests",
		ErrorDetail: "rate limit exceeded for this resource",
		statusCode:  http.StatusTooManyRequests,
	}
}

func ErrConflict(public ErrorResponseCode, detail string) *ErrorResponse {
	return &ErrorResponse{
		ErrorCode:   public,
		ErrorDetail: detail,
		statusCode:  http.StatusConflict,
	}
}

func (e *ErrorResponse) Error() string {
	if e.err != nil {
		return e.err.Error()
	}
	return string(e.ErrorCode)
}
