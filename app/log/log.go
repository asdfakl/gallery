package log

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	ERROR = 0 + iota
	WARN
	INFO
	DEBUG
)

const DatetimeFormat = "2006-01-02T15:04:05.000Z"

var (
	EnvKeyColor = "LOG_COLORS"
	EnvKeyLevel = "LOG_LEVEL"
)

var level int

var debugPrefix = []byte("DEB")
var infoPrefix = []byte("INF")
var warnPrefix = []byte("WAR")
var errorPrefix = []byte("ERR")
var systemPrefix = []byte("SYS")

//nolint:unused
var (
	shellColorNormal []byte = nil
	shellColorBold   []byte = nil

	shellColorBoldRed       []byte = nil
	shellColorBoldGreen     []byte = nil
	shellColorBoldBlue      []byte = nil
	shellColorBoldPurple    []byte = nil
	shellColorBoldCyan      []byte = nil
	shellColorBoldLightGray []byte = nil

	shellColorRed       []byte = nil
	shellColorGreen     []byte = nil
	shellColorBlue      []byte = nil
	shellColorPurple    []byte = nil
	shellColorCyan      []byte = nil
	shellColorLightGray []byte = nil
)

var Default = NewLogger()

func init() {
	if t, _ := strconv.ParseBool(os.Getenv(EnvKeyColor)); t {
		debugPrefix = []byte("\x1b[00;36mDEB\x1b[00m")
		infoPrefix = []byte("\x1b[00;34mINF\x1b[00m")
		warnPrefix = []byte("\x1b[01;33mWAR\x1b[00m")
		errorPrefix = []byte("\x1b[01;31mERR\x1b[00m")
		systemPrefix = []byte("\x1b[01;37mSYS\x1b[00m")

		shellColorNormal = []byte("\x1b[00m")

		shellColorBold = []byte("\x1b[01m")
		shellColorBoldRed = []byte("\x1b[01;31m")
		shellColorBoldGreen = []byte("\x1b[01;32m")
		shellColorBoldBlue = []byte("\x1b[01;34m")
		shellColorBoldPurple = []byte("\x1b[01;35m")
		shellColorBoldCyan = []byte("\x1b[01;36m")
		shellColorBoldLightGray = []byte("\x1b[01;37m")

		shellColorRed = []byte("\x1b[31m")
		shellColorGreen = []byte("\x1b[32m")
		shellColorBlue = []byte("\x1b[34m")
		shellColorPurple = []byte("\x1b[35m")
		shellColorCyan = []byte("\x1b[36m")
		shellColorLightGray = []byte("\x1b[37m")
	}

	switch lvl := strings.ToLower(strings.TrimSpace(os.Getenv(EnvKeyLevel))); lvl {
	case "debug":
		level = DEBUG
	case "", "info":
		level = INFO
	case "warn":
		level = WARN
	case "error":
		level = ERROR
	case "system":
		level = -1
	default:
		panic(fmt.Errorf("unsupported %s value %s, valid values are debug, info, warn, error, system", EnvKeyLevel, lvl))
	}
}

type Logger struct {
	nop    bool
	fields []Field
}

func NewLogger(fields ...Field) *Logger {
	return &Logger{
		fields: fields,
	}
}

func NewNopLogger() *Logger {
	return &Logger{
		nop: true,
	}
}

func (l *Logger) With(fields ...Field) *Logger {
	return &Logger{
		nop:    l.nop,
		fields: append(l.fields, fields...),
	}
}

func (l *Logger) Debug(msg string, fields ...Field) {
	if l.nop || level < DEBUG {
		return
	}

	printRow(
		debugPrefix,
		row(msg, append(l.fields, fields...)...),
	)
}

func (l *Logger) Info(msg string, fields ...Field) {
	if l.nop || level < INFO {
		return
	}

	printRow(
		infoPrefix,
		row(msg, append(l.fields, fields...)...),
	)
}

func (l *Logger) Warn(msg string, fields ...Field) {
	if l.nop || level < WARN {
		return
	}

	printRow(
		warnPrefix,
		row(msg, append(l.fields, fields...)...),
	)
}

func (l *Logger) Error(msg string, fields ...Field) {
	if l.nop || level < ERROR {
		return
	}

	printRow(
		errorPrefix,
		row(msg, append(l.fields, fields...)...),
	)
}

func (l *Logger) Stack() {
	if l.nop || level < ERROR {
		return
	}

	b := make([]byte, 8192)
	n := runtime.Stack(b, false)

	b = b[:n]

	printRows(errorPrefix, bytes.Split(b, []byte{0x0a}))
}

func (l *Logger) System(msg string, fields ...Field) {
	if l.nop {
		return
	}

	printRow(
		systemPrefix,
		row(msg, append(l.fields, fields...)...),
	)
}

func row(msg string, fields ...Field) []byte {
	m := make(map[string]struct{})

	b := &bytes.Buffer{}

	b.Write(shellColorLightGray)
	b.Write([]byte(strings.TrimSpace(msg)))
	b.Write(shellColorNormal)

	if len(fields) == 0 {
		return b.Bytes()
	}

	b.Write([]byte(" {"))

	i := 0
	for _, field := range fields {
		if _, ok := m[field.key]; ok {
			continue
		}
		m[field.key] = struct{}{}

		if i > 0 {
			b.Write([]byte(", "))
		}
		i++

		b.Write(field.buf)
	}

	b.WriteByte('}')

	return b.Bytes()
}

var rowMutex = &sync.Mutex{}

func printRow(prefix, row []byte) {
	if len(row) == 0 {
		return
	}

	now := time.Now()

	if row[len(row)-1] != 0x0a {
		row = append(row, 0x0a)
	}

	line := append(prefix, ' ')
	line = append(line, []byte(now.In(time.UTC).Format(DatetimeFormat))...)
	if _, file, lineNumber, ok := runtime.Caller(2); ok {
		line = append(line, ' ')
		line = append(line, []byte(filepath.Base(file))...)
		line = append(line, ':')
		line = append(line, strconv.Itoa(lineNumber)...)
	}
	line = append(line, ' ')
	line = append(line, row...)

	rowMutex.Lock()
	defer rowMutex.Unlock()

	os.Stderr.Write(line)
}

func printRows(prefix []byte, rows [][]byte) {
	if len(rows) == 0 {
		return
	}

	now := time.Now()

	rowMutex.Lock()
	defer rowMutex.Unlock()

	for _, row := range rows {
		if len(row) == 0 {
			continue
		}

		if row[len(row)-1] != 0x0a {
			row = append(row, 0x0a)
		}

		line := append(prefix, ' ')
		line = append(line, []byte(now.In(time.UTC).Format(DatetimeFormat))...)
		line = append(line, ' ')
		line = append(line, row...)

		os.Stderr.Write(line)
	}
}
