package redismain

import "context"

func (rmc *RedisMainConnection) FreqIncrement(ctx context.Context, key string, increment float64, member string) (float64, error) {
	return rmc.rdb.ZIncrBy(ctx, ns.freq(key), increment, member).Result()
}
