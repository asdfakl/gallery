package apiserver

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const UPLOAD_TTL time.Duration = 4 * time.Hour

type CreateUploadRoute struct {
	Route
}

func CreateUploadRouteFn(srv *APIServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &CreateUploadRoute{}, nil
}

func (route *CreateUploadRoute) CanAccess(user *postgres.UserView) bool {
	return true
}

func (route *CreateUploadRoute) HandlePOST(srv *APIServer, w http.ResponseWriter, r *http.Request) {
	p := &struct {
		SHA1       string    `json:"sha1"`
		Basename   string    `json:"basename"`
		Size       int64     `json:"size"`
		ModifiedAt time.Time `json:"modified_at"`
		Public     bool      `json:"public"`
		Tags       []string  `json:"tags"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		srv.handleError(w, ErrBadRequest(err, "decode json payload"))
		return
	}

	sum, err := utils.ParseHexEncodedSHA1String(p.SHA1)
	if err != nil {
		srv.handleError(w, ErrBadRequest(err, "decode sha1 sum"))
		return
	}

	p.Basename = strings.TrimSpace(p.Basename)
	if p.Basename == "" {
		srv.handleError(w, ErrBadRequest(err, "required: basename"))
		return
	}

	if p.Size <= 0 {
		srv.handleError(w, ErrBadRequest(err, "size must be positive"))
		return
	}

	var tags []string
	tagm := map[string]struct{}{}
	for _, tag := range p.Tags {
		tag = strings.TrimSpace(tag)
		if tag != "" {
			tagm[tag] = struct{}{}
		}
	}
	for tag := range tagm {
		tags = append(tags, tag)
	}

	obj, err := srv.pg.GetObjectBySHA1(srv.appCtx, sum)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get object by sha1"))
		return
	}
	if obj != nil {
		srv.handleError(w, ErrConflict(ERR_CODE_OBJECT_EXISTS, "object already exists"))
		return
	}

	upload, err := srv.pg.GetUploadViewBySHA1(srv.appCtx, sum)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get upload view by sha1"))
		return
	}
	if upload != nil {
		if upload.Owner != route.User().ID {
			srv.handleError(w, ErrForbidden())
			return
		}
		if upload.Progress >= upload.Size {
			srv.handleError(w, ErrConflict(ERR_CODE_UPLOAD_COMPLETE, "upload already completed"))
			return
		}
		if upload.Size != p.Size {
			srv.handleError(w, ErrConflict(ERR_CODE_UPLOAD_CONFLICT, "upload already exists with a different size"))
			return
		}

		upload.Basename = p.Basename
		upload.ModifiedAt = p.ModifiedAt
		upload.Public = p.Public
		upload.Tags = tags

		if err = srv.pg.ModifyUpload(srv.appCtx, upload); err != nil {
			srv.handleError(w, utils.Wrap(err, "modify upload"))
			return
		}

		uploadResponse(srv, w, upload)
		return
	}

	uploadCount, uploadBytes, err := srv.pg.CountUploadsByOwner(srv.appCtx, route.User().ID)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "count uploads by owner"))
		return
	}

	totalCount := uploadCount + route.User().ObjectCount + 1
	totalBytes := uploadBytes + route.User().ObjectBytes + p.Size

	if totalCount > route.User().QuotaObjectCount {
		srv.handleError(w, ErrConflict(ERR_CODE_QUOTA_EXCEEDED, "total object count quota exceeded"))
		return
	}

	if totalBytes > route.User().QuotaObjectBytes {
		srv.handleError(w, ErrConflict(ERR_CODE_QUOTA_EXCEEDED, "total object bytes quota exceeded"))
		return
	}

	upload = &postgres.UploadView{
		SHA1:       sum,
		Basename:   p.Basename,
		Size:       p.Size,
		ModifiedAt: p.ModifiedAt,
		Owner:      route.User().ID,
		Public:     p.Public,
		Tags:       p.Tags,
	}

	if err = srv.pg.InsertUpload(srv.appCtx, upload, postgres.BoundedTimerange{
		time.Now(),
		time.Now().Add(UPLOAD_TTL),
	}); err != nil {
		srv.handleError(w, utils.Wrap(err, "insert upload"))
		return
	}

	uploadResponse(srv, w, upload)
}
