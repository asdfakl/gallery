package main

import (
	"context"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"gitlab.com/asdfakl/gallery/app/bg"
	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redislru"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	SESSION_KEY            = "background"
	SESSION_TTL            = 2 * time.Minute
	SESSION_UNLOCK_TIMEOUT = 3 * time.Second
)

func main() {
	startTime := time.Now()

	logger := log.NewLogger()
	if config.Version != "development" {
		logger = logger.With(log.String("version", config.Version))
	}

	cfg, err := loadConfiguration(logger)
	utils.Must(err, "load configuration")

	connCtx, cancelConnCtx := context.WithTimeout(context.Background(), 5*time.Second)

	pg, err := postgres.New(connCtx, &cfg.Postgres)
	utils.Must(err, "connect to postgres database")
	logger.System("connection established to postgres database")

	rmc, err := redismain.New(connCtx, &cfg.RedisMain, logger)
	utils.Must(err, "connect to main redis database")
	logger.System("connection established to main redis database")

	var rlc *redislru.RedisLRUConnection
	if err = cfg.RedisLRU.Validate(); err != nil {
		logger.System("lru redis disabled")
		logger.Debug("lru redis configuration validation", log.Err(err))
	} else {
		rlc, err = redislru.New(connCtx, &cfg.RedisLRU)
		utils.Must(err, "connect to lru redis database")
		logger.System("connection established to lru redis database")
	}

	thumbs, err := thumbstorage.NewLocalStorage(&cfg.Thumbs, rlc, logger)
	utils.Must(err, "initialize thumb storage")

	counter, err := rmc.Counter(connCtx, "pid:bg")
	utils.Must(err, "get pid")

	pid, err := rmc.Lock(connCtx, SESSION_KEY, strconv.FormatInt(counter, 10), SESSION_TTL)
	utils.Must(err, "lock session")
	logger.System("session locked", log.String("identity", pid))

	cancelConnCtx()

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	fatalc := make(chan error)
	fatalf := func(err error) {
		select {
		case fatalc <- err:
		default:
			logger.Error("fatal", log.Err(err))
		}
	}
	sigc := make(chan os.Signal, 1)
	executor, err := bg.NewExecutor(bg.DefaultFunctionMapping, logger)
	utils.Must(err, "initialize executor")

	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	wg.Add(2)
	go handleTriggers(appCtx, pg, rmc, thumbs, &wg, fatalf, executor, logger)
	go handleQueues(appCtx, pg, rmc, thumbs, &wg, fatalf, executor, logger)
	refreshc := rmc.LockRefresher(appCtx, SESSION_KEY, pid, SESSION_TTL)

	select {
	case sig := <-sigc:
		logger.System("got signal", log.String("signal", sig.String()))
	case err = <-fatalc:
		logger.Error("fatal", log.Err(err))
	case err = <-refreshc:
		logger.Error("refresh session lock", log.Err(err))
	}
	cancelAppCtx()

	wg.Wait()
	for range refreshc {
		// wait for close
	}

	utils.Must(unlockSession(rmc, pid, logger), "unlock session")

	logger.System("graceful shutdown", log.Duration("uptime", time.Since(startTime)))

	utils.Must(err, "exit")
}

func unlockSession(rmc *redismain.RedisMainConnection, pid string, logger *log.Logger) error {
	ctx, cancel := context.WithTimeout(context.Background(), SESSION_UNLOCK_TIMEOUT)
	defer cancel()

	ok, err := rmc.Unlock(ctx, SESSION_KEY, pid)
	if ok {
		logger.System("session unlocked", log.String("identity", pid))
	}

	return err
}
