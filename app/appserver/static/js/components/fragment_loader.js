"use strict";

class FragmentLoader extends HTMLElement {
	constructor() {
		super();

		this.targetID = this.getAttribute("data-target-id");
		if (!this.targetID) {
			throw "missing required attribute: data-target-id";
		}

		this.fetchCount = 0;
	}

	static get observedAttributes() {
		return [
			"href",
		];
	}

	connectedCallback() {
		if (!this.isConnected) {
			return;
		}

		this.addEventListener("RequestComponentRefresh", this.requestComponentRefreshListener);
	}

	disconnectedCallback() {
		this.removeEventListener("RequestComponentRefresh", this.requestComponentRefreshListener);
	}

	requestComponentRefreshListener() {
		this.fetch(this.getAttribute("href"));
	}

	attributeChangedCallback(name, oldValue, newValue) {
		switch (name) {
			case "href":
				if (newValue !== oldValue) {
					this.fetch(newValue);
				}
				break;
		}
	}

	// TODO unmount current elems
	fetch(url) {
		const currentFetchCount = ++this.fetchCount;

		if (!url) {
			this.innerHTML = null;
			return;
		}

		fetch(url).then(res => {
			if (!res.ok) {
				throw `get request failed: ${res.status} ${res.statusText}`;
			}
			return res.text();
		}).then(res => {
			if (currentFetchCount !== this.fetchCount) {
				return;
			}

			const parsed = new DOMParser().parseFromString(res, "text/html");

			const result = parsed.getElementById(this.targetID);
			if (!result) {
				throw `response element not found by id '${this.targetID}'`;
			}

			this.replaceChildren(...result.children);

			Common.initHTMLElem(this);
		}).catch(err => {
			if (currentFetchCount === this.fetchCount) {
				this.innerHTML = null;
			}

			console.error(`fragment loading failed: ${err}`);
		});
	}
}

customElements.define("fragment-loader", FragmentLoader);
