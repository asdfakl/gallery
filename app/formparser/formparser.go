package formparser

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

var (
	ErrFormParserAmbiguous           = errors.New("ambiguous value")
	ErrFormParserDoesNotMatchOptions = errors.New("does not match options")
	ErrFormParserEmpty               = errors.New("empty")
	ErrFormParserErrNotChecked       = errors.New("error not checked")
	ErrFormParserInvalidUTF8         = errors.New("invalid utf-8")
	ErrFormParserMaxLengthExceeded   = errors.New("max length exceeded")
	ErrFormParserMaxValueExceeded    = errors.New("max value exceeded")
	ErrFormParserMinValueSubceeded   = errors.New("min value subceeded")
	ErrFormParserNonUnique           = errors.New("non-unique values")
	ErrFormParserRequired            = errors.New("required")
)

type FormParser struct {
	src        FormSource
	errs       map[string][]error
	errChecked bool
}

type FormSource func(string) ([]string, error)

func NewQueryFormParser(r *http.Request) *FormParser {
	var query url.Values

	src := func(key string) ([]string, error) {
		if query == nil {
			q, err := url.ParseQuery(r.URL.RawQuery)
			if err != nil {
				return nil, fmt.Errorf("parse query: %w", err)
			}
			query = q
		}

		vals, ok := query[key]
		if !ok {
			return nil, nil
		}
		return vals, nil
	}

	return &FormParser{
		src:  src,
		errs: make(map[string][]error),
	}
}

func NewBodyFormParser(r *http.Request, csrfValidationFn func(*http.Request) error) *FormParser {
	parsed := false

	src := func(key string) ([]string, error) {
		if !parsed {
			if err := r.ParseForm(); err != nil {
				return nil, fmt.Errorf("parse form: %w", err)
			}
			if csrfValidationFn != nil {
				if err := csrfValidationFn(r); err != nil {
					return nil, fmt.Errorf("validate csrf token: %w", err)
				}
			}
			parsed = true
		}

		vals, ok := r.PostForm[key]
		if !ok {
			return nil, nil
		}
		return vals, nil
	}

	return &FormParser{
		src:  src,
		errs: make(map[string][]error),
	}
}

func (parser *FormParser) Err() error {
	parser.errChecked = true

	for k, errs := range parser.errs {
		if len(errs) != 0 {
			return fmt.Errorf("%s: %v", k, errs[0])
		}
	}
	return nil
}

func (parser *FormParser) mustHaveErrChecked() {
	if !parser.errChecked {
		panic(ErrFormParserErrNotChecked)
	}
}

func (parser *FormParser) registerError(key string, err error) {
	parser.errs[key] = append(parser.errs[key], err)
}
