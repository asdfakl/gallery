package main

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"

	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	ERR_CODE_OBJECT_EXISTS   = "object_exists"
	ERR_CODE_UPLOAD_COMPLETE = "upload_complete"
)

var ErrUploadSkipped = errors.New("upload skipped")

type UploadService struct {
	cfg    *Configuration
	index  int
	tokens *TokenService
	wg     *sync.WaitGroup

	once    sync.Once
	objectc <-chan *Object
	client  *http.Client
}

func NewUploadService(cfg *Configuration, index int, wg *sync.WaitGroup, objectc <-chan *Object, tokens *TokenService) *UploadService {
	return &UploadService{
		cfg:    cfg,
		index:  index,
		tokens: tokens,
		wg:     wg,

		objectc: objectc,
		client:  &http.Client{},
	}
}

func (u *UploadService) Start(ctx context.Context, errg *errgroup.Group) {
	u.once.Do(func() {
		u.wg.Add(1)

		errg.Go(func() error {
			return utils.Wrap(u.Run(ctx), "upload service")
		})
	})
}

func (u *UploadService) Run(ctx context.Context) error {
	defer u.wg.Done()

	defer u.cfg.Success(fmt.Sprintf("Upload service %d complete", u.index))

	for obj := range u.objectc {
		if err := u.Upload(ctx, obj); err != nil {
			if errors.Is(err, ErrUploadSkipped) {
				u.cfg.Metrics.UploadSkipped.Increment(obj.Size)
			} else {
				u.cfg.Metrics.UploadFailure.Increment(obj.Size)
				u.cfg.ObjError(obj.Base, fmt.Sprintf("upload failed: %v", err))
			}
			continue
		}
		u.cfg.Metrics.UploadSuccess.Increment(obj.Size)
	}

	return nil
}

func (u *UploadService) Upload(ctx context.Context, obj *Object) error {
	upload, err := u.InitiateUpload(ctx, obj)
	if err != nil {
		if res, ok := err.(*ErrorResponse); ok && res.StatusCode == http.StatusConflict {
			if res.ErrorCode == ERR_CODE_OBJECT_EXISTS || res.ErrorCode == ERR_CODE_UPLOAD_COMPLETE {
				if res.ErrorDetail == "" {
					u.cfg.ObjSuccess(obj.Base, "upload already completed")
				} else {
					u.cfg.ObjSuccess(obj.Base, res.ErrorDetail)
				}
				return ErrUploadSkipped
			}
		}

		return fmt.Errorf("initiate upload: %w", err)
	}

	if upload.Progress <= 0 {
		u.cfg.ObjProgress(obj.Base, "initiated new upload")
	} else {
		u.cfg.ObjProgress(obj.Base, fmt.Sprintf(
			"continuing previous upload, progress %s / %s",
			utils.HumanFilesize(upload.Progress),
			utils.HumanFilesize(upload.Size),
		))
	}

	f, err := os.Open(obj.Abs)
	if err != nil {
		return fmt.Errorf("open: %w", err)
	}
	defer f.Close()

	lastHumanProgress := ""
	humanUploadSize := utils.HumanFilesize(upload.Size)

	for upload.Progress < upload.Size {
		if err = u.UploadPart(ctx, f, obj, upload); err != nil {
			return fmt.Errorf("upload part: %w", err)
		}

		if upload.Progress < upload.Size {
			if humanProgress := utils.HumanFilesize(upload.Progress); humanProgress != lastHumanProgress {
				lastHumanProgress = humanProgress

				u.cfg.ObjProgress(obj.Base, fmt.Sprintf(
					"upload progress %s / %s (%s)",
					humanProgress,
					humanUploadSize,
					utils.HumanPercentage(float64(upload.Progress)/float64(upload.Size)),
				))
			}
		} else {
			u.cfg.ObjSuccess(obj.Base, "upload complete")
		}
	}

	return nil
}

func (u *UploadService) InitiateUpload(ctx context.Context, obj *Object) (*postgres.UploadView, error) {
	p := &struct {
		SHA1       string    `json:"sha1"`
		Basename   string    `json:"basename"`
		Size       int64     `json:"size"`
		ModifiedAt time.Time `json:"modified_at"`
		Public     bool      `json:"public"`
		Tags       []string  `json:"tags"`
	}{
		SHA1:       hex.EncodeToString(obj.SHA1[:]),
		Basename:   obj.Base,
		Size:       obj.Size,
		ModifiedAt: obj.ModTime,
		Public:     *u.cfg.Publish,
		Tags:       u.cfg.Tags,
	}

	timeoutCtx, cancelTimeoutCtx := context.WithTimeout(ctx, 10*time.Second)
	defer cancelTimeoutCtx()

	token := u.tokens.Token(timeoutCtx)
	if token == nil {
		return nil, fmt.Errorf("could not acquire token")
	}

	body := &bytes.Buffer{}

	if err := json.NewEncoder(body).Encode(p); err != nil {
		return nil, fmt.Errorf("encode json: %w", err)
	}

	url := *u.cfg.APIURL
	url.Path = path.Join(u.cfg.APIURL.Path, "add", "upload")

	req, err := http.NewRequestWithContext(timeoutCtx, http.MethodPost, url.String(), body)
	if err != nil {
		return nil, fmt.Errorf("build add upload request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token.Raw)

	res, err := u.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("add upload request: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode < 200 || res.StatusCode > 299 {
		return nil, readErrorResponse(res)
	}

	decoded := &struct {
		SHA1       string    `json:"sha1"`
		Basename   string    `json:"basename"`
		Size       int64     `json:"size"`
		ModifiedAt time.Time `json:"modified_at"`
		Progress   int64     `json:"progress"`
		Public     bool      `json:"public"`
		Tags       []string  `json:"tags"`
	}{}

	if err := json.NewDecoder(res.Body).Decode(decoded); err != nil {
		return nil, fmt.Errorf("decode response json: %w", err)
	}

	sum, err := utils.ParseHexEncodedSHA1String(decoded.SHA1)
	if err != nil {
		return nil, fmt.Errorf("decode response sha1: %w", err)
	}

	return &postgres.UploadView{
		SHA1:       sum,
		Basename:   decoded.Basename,
		Size:       decoded.Size,
		ModifiedAt: decoded.ModifiedAt,
		Progress:   decoded.Progress,
		Public:     decoded.Public,
		Tags:       decoded.Tags,
	}, nil
}

func (u *UploadService) UploadPart(ctx context.Context, f *os.File, obj *Object, upload *postgres.UploadView) error {
	offset, err := f.Seek(upload.Progress, 0)
	if err != nil {
		return fmt.Errorf("seek: %w", err)
	}
	if offset != upload.Progress {
		return fmt.Errorf("expecting offset %d, got %d", upload.Progress, offset)
	}

	token := u.tokens.Token(ctx)
	if token == nil {
		return fmt.Errorf("could not acquire token")
	}

	partSize := u.cfg.BlockSize
	if partSize+offset > upload.Size {
		partSize = upload.Size - offset
	}

	rdr := io.LimitReader(f, partSize)
	h := md5.New()
	tee := io.TeeReader(rdr, h)

	endpoint := *u.cfg.APIURL
	endpoint.Path = path.Join(u.cfg.APIURL.Path, "uploads", upload.HexSHA1(), "addpart")

	q := url.Values{}
	q.Set("offset", strconv.FormatInt(offset, 10))
	q.Set("part_size", strconv.FormatInt(partSize, 10))

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint.String()+"?"+q.Encode(), tee)
	if err != nil {
		return fmt.Errorf("build add upload part request: %w", err)
	}

	req.Header.Set("Authorization", "Bearer "+token.Raw)

	res, err := u.client.Do(req)
	if err != nil {
		return fmt.Errorf("add upload part request: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode < 200 || res.StatusCode > 299 {
		return readErrorResponse(res)
	}

	decoded := &struct {
		PartMD5  string `json:"part_md5"`
		Progress int64  `json:"progress"`
	}{}

	if err = json.NewDecoder(res.Body).Decode(decoded); err != nil {
		return fmt.Errorf("decode response json: %w", err)
	}

	if hexMD5 := hex.EncodeToString(h.Sum(nil)); decoded.PartMD5 != hexMD5 {
		if err = u.RemoveUpload(ctx, upload); err != nil {
			u.cfg.ObjError(obj.Base, fmt.Sprintf("could not remove corrupted upload: %v", err))
		}

		return fmt.Errorf("part md5 mismatch, expected '%s', got '%s'", hexMD5, decoded.PartMD5)
	}

	upload.Progress = decoded.Progress

	return nil
}

func (u *UploadService) RemoveUpload(ctx context.Context, upload *postgres.UploadView) error {
	timeoutCtx, cancelTimeoutCtx := context.WithTimeout(ctx, 2*time.Second)
	defer cancelTimeoutCtx()

	token := u.tokens.Token(timeoutCtx)
	if token == nil {
		return fmt.Errorf("could not acquire token")
	}

	endpoint := *u.cfg.APIURL
	endpoint.Path = path.Join(u.cfg.APIURL.Path, "uploads", upload.HexSHA1(), "remove")

	req, err := http.NewRequestWithContext(timeoutCtx, http.MethodPost, endpoint.String(), nil)
	if err != nil {
		return fmt.Errorf("build remove upload request: %w", err)
	}

	req.Header.Set("Authorization", "Bearer "+token.Raw)

	res, err := u.client.Do(req)
	if err != nil {
		return fmt.Errorf("remove upload request: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode < 200 || res.StatusCode > 299 {
		return readErrorResponse(res)
	}

	return nil
}
