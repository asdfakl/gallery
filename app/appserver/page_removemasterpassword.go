package appserver

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type RemoveMasterPasswordPage struct {
	Page

	MasterPassword *postgres.MasterPassword
	Next           string
}

func RemoveMasterPasswordPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("master password not found")
	}

	parser := formparser.NewQueryFormParser(r)

	var (
		next = parser.String("next").Required().Options([]string{"refresh", "reload"})
	)

	if err = parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	// load
	password, err := srv.pg.GetMasterPasswordByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get master password by ext_id")
	}
	if password == nil {
		return nil, ErrNotFound("master password not found")
	}

	return &RemoveMasterPasswordPage{
		MasterPassword: password,
		Next:           next.Value(),
	}, nil
}

func (page *RemoveMasterPasswordPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.MasterPassword.Owner
}

func (page *RemoveMasterPasswordPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "master_password_remove_form.html", page, http.StatusOK)
}

func (page *RemoveMasterPasswordPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	parser.String("confirmation").Required().Options([]string{"remove"})

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	if err := srv.pg.DeleteMasterPassword(srv.appCtx, page.MasterPassword.ID); err != nil {
		srv.handleError(w, utils.Wrap(err, "delete master password"))
		return
	}

	switch page.Next {
	case "refresh":
		srv.renderJSON(w, &JSONResponse{Refresh: true}, http.StatusOK)
	case "reload":
		srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
	default:
		panic(fmt.Errorf("unsupported requested response: '%s'", page.Next))
	}
}
