package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"sync"
	"time"

	"github.com/google/uuid"
	"golang.org/x/sync/errgroup"

	"gitlab.com/asdfakl/gallery/app/jwt"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type TokenUser struct {
	ExtID            uuid.UUID `json:"ext_id"`
	Username         string    `json:"username"`
	DisplayName      string    `json:"display_name"`
	QuotaObjectCount int       `json:"quota_object_count"`
	QuotaObjectBytes int64     `json:"quota_object_bytes"`
	ObjectCount      int       `json:"object_count"`
	ObjectBytes      int64     `json:"object_bytes"`
}

type TokenPayload struct {
	jwt.JWTPayload
}

type Token struct {
	Raw     string       `json:"-"`
	User    TokenUser    `json:"user"`
	Payload TokenPayload `json:"token_payload"`
}

type ErrorResponse struct {
	StatusCode int `json:"-"`

	ErrorCode   string `json:"error_code"`
	ErrorDetail string `json:"error_detail,omitempty"`
}

func (e *ErrorResponse) Error() string {
	if e.ErrorDetail != "" {
		return fmt.Sprintf("%d %s: %s", e.StatusCode, e.ErrorCode, e.ErrorDetail)
	}
	return fmt.Sprintf("%d %s", e.StatusCode, e.ErrorCode)
}

type TokenService struct {
	cfg *Configuration

	token  *Token
	reqc   chan chan *Token
	once   sync.Once
	client *http.Client
}

func NewTokenService(cfg *Configuration) *TokenService {
	return &TokenService{
		cfg:    cfg,
		reqc:   make(chan chan *Token),
		client: &http.Client{},
	}
}

func (t *TokenService) Start(ctx context.Context, errg *errgroup.Group) {
	t.once.Do(func() {
		errg.Go(func() error {
			return utils.Wrap(t.Run(ctx, errg), "token service")
		})
	})
}

func (t *TokenService) Run(ctx context.Context, errg *errgroup.Group) error {
	var pending []chan<- *Token
	defer func() {
		for _, req := range pending {
			close(req)
		}
	}()

	fetchInProgress := false
	tokenc := make(chan *Token)

	triggerFetch := func() {
		if fetchInProgress {
			return
		}
		fetchInProgress = true

		errg.Go(func() error {
			err := t.fetch(ctx, tokenc)
			if err != nil {
				t.cfg.Error(err.Error())
			}
			return err
		})
	}

	for {
		select {
		case <-ctx.Done():
			return nil
		case req := <-t.reqc:
			if t.token == nil {
				pending = append(pending, req)
				triggerFetch()
				break
			}
			if t.token.Payload.Expiry < time.Now().Unix()+60 {
				pending = append(pending, req)
				triggerFetch()
				break
			}
			req <- t.token
		case token := <-tokenc:
			fetchInProgress = false
			t.token = token

			t.cfg.Info(fmt.Sprintf(
				"Got token for %s, issuer %s, expiry %s local, object count %d / %d, object bytes %s / %s",
				token.User.DisplayName, token.Payload.Issuer,
				time.Unix(token.Payload.Expiry, 0).Format("2006-01-02 15:04"),
				token.User.ObjectCount, token.User.QuotaObjectCount,
				utils.HumanFilesize(token.User.ObjectBytes), utils.HumanFilesize(token.User.QuotaObjectBytes),
			))

			for _, req := range pending {
				req <- token
				close(req)
			}
			pending = nil
		}
	}
}

func (t *TokenService) Token(ctx context.Context) *Token {
	req := make(chan *Token)

	select {
	case <-ctx.Done():
		return nil
	case t.reqc <- req:
	}

	select {
	case <-ctx.Done():
		return nil
	case token := <-req:
		return token
	}
}

func (t *TokenService) fetch(ctx context.Context, tokenc chan<- *Token) error {
	timeoutCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	p := &struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{
		Username: t.cfg.Username,
		Password: t.cfg.Password,
	}

	body := &bytes.Buffer{}
	if err := json.NewEncoder(body).Encode(p); err != nil {
		return fmt.Errorf("encode json: %w", err)
	}

	u := *t.cfg.APIURL
	u.Path = path.Join(t.cfg.APIURL.Path, "token")

	req, err := http.NewRequestWithContext(timeoutCtx, http.MethodPost, u.String(), body)
	if err != nil {
		return fmt.Errorf("build token request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")

	tokenRes, err := t.client.Do(req)
	if err != nil {
		return fmt.Errorf("token request: %w", err)
	}
	defer tokenRes.Body.Close()

	if tokenRes.StatusCode < 200 || tokenRes.StatusCode > 299 {
		return readErrorResponse(tokenRes)
	}

	raw := ""
	if err = json.NewDecoder(tokenRes.Body).Decode(&raw); err != nil {
		return fmt.Errorf("decode token response: %w", err)
	}

	u.Path = path.Join(t.cfg.APIURL.Path, "introspect")

	req, err = http.NewRequestWithContext(timeoutCtx, http.MethodGet, u.String(), nil)
	if err != nil {
		return fmt.Errorf("build introspect request: %w", err)
	}

	req.Header.Set("Authorization", "Bearer "+raw)

	introspectRes, err := t.client.Do(req)
	if err != nil {
		return fmt.Errorf("introspect request: %w", err)
	}
	defer introspectRes.Body.Close()

	if introspectRes.StatusCode < 200 || introspectRes.StatusCode > 299 {
		return readErrorResponse(introspectRes)
	}

	token := &Token{
		Raw: raw,
	}
	if err = json.NewDecoder(introspectRes.Body).Decode(token); err != nil {
		return fmt.Errorf("decode introspect response: %w", err)
	}

	tokenc <- token

	return nil
}

func readErrorResponse(res *http.Response) error {
	errRes := &ErrorResponse{
		StatusCode: res.StatusCode,
	}

	if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
		return fmt.Errorf("unknown error response: error decoding failed")
	}

	return errRes
}
