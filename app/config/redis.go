package config

import (
	"flag"
	"fmt"
	"strings"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type RedisConfiguration struct {
	Host     string
	Port     int
	Database int
	Username string
	Password string

	prefix string
}

func DefaultRedisConfiguration() *RedisConfiguration {
	return &RedisConfiguration{
		Host: "127.0.0.1",
		Port: 6379,
	}
}

func (cfg *RedisConfiguration) InitFlagSet(fs *flag.FlagSet, prefix string) {
	cfg.prefix = prefix

	utils.StringVarOrEnv(fs, &cfg.Host, cfg.option("host"), cfg.Host, "redis host")
	utils.IntVarOrEnv(fs, &cfg.Port, cfg.option("port"), cfg.Port, "redis port")
	utils.IntVarOrEnv(fs, &cfg.Database, cfg.option("database"), cfg.Database, "redis database number")
	utils.StringVarOrEnv(fs, &cfg.Username, cfg.option("username"), cfg.Username, "redis username")
	utils.StringVarOrEnv(fs, &cfg.Password, cfg.option("password"), cfg.Password, "redis password")
}

func (cfg *RedisConfiguration) Trim() {
	cfg.Host = strings.TrimSpace(cfg.Host)
	cfg.Username = strings.TrimSpace(cfg.Username)
}

func (cfg *RedisConfiguration) Validate() error {
	for _, assert := range []assertion{
		{cfg.Host != "", fmt.Errorf("%s empty or undefined", cfg.option("host"))},
		{cfg.Port > 0, fmt.Errorf("%s must be positive", cfg.option("port"))},
		{cfg.Database >= 0, fmt.Errorf("%s must be non-negative", cfg.option("database"))},
		{cfg.Username != "", fmt.Errorf("%s empty or undefined", cfg.option("username"))},
		{cfg.Password != "", fmt.Errorf("%s empty or undefined", cfg.option("password"))},
	} {
		if !assert.ok {
			return assert.err
		}
	}
	return nil
}

func (cfg *RedisConfiguration) option(opt string) string {
	if cfg.prefix == "" {
		return "redis-" + opt
	}
	return cfg.prefix + "-" + opt
}
