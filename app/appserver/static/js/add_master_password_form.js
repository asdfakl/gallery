"use strict";

(function(){
	const initJSAddMasterPasswordForm = nodes => {
		nodes.forEach(node => {
			const labelInput = node.querySelector("input[name=label]");
			const labelHelp = node.querySelector("#help_label");

			const passwordInput1 = node.querySelector("input[name=password_1]");
			const passwordHelp1 = node.querySelector("#help_password_1");
			const passwordProgress1 = node.querySelector("#progress_password_1");

			const passwordInput2 = node.querySelector("input[name=password_2]");
			const passwordHelp2 = node.querySelector("#help_password_2");
			const passwordProgress2 = node.querySelector("#progress_password_2");

			const submit = node.querySelector("button[type=submit]");

			const progressClass = (progress) => {
				if (progress < 30) {
					return "is-danger";
				}
				if (progress < 70) {
					return "is-warning";
				}
				if (progress < 100) {
					return "is-info";
				}
				return "is-success";
			};

			const labelOK = (input) => {
				const val = input.value.trim();

				if (!val) {
					return [false, "Required"];
				}

				return [true, ""];
			};

			const refresh = (e) => {
				let allOK = true;

				// label
				let [ok, help, progress] = labelOK(labelInput);
				allOK = allOK && ok;
				labelHelp.innerHTML = help;
				labelInput.classList.remove(ok ? "is-danger" : "is-success");
				labelInput.classList.add(ok ? "is-success" : "is-danger");

				// password 1
				[ok, help, progress] = Utils.newPasswordOK(passwordInput1.value);
				allOK = allOK && ok;
				passwordHelp1.innerHTML = help;
				passwordProgress1.setAttribute("value", progress);
				passwordProgress1.classList.remove("is-danger", "is-warning", "is-info", "is-success");
				passwordProgress1.classList.add(progressClass(progress));
				passwordInput1.classList.remove(ok ? "is-danger" : "is-success");
				passwordInput1.classList.add(ok ? "is-success" : "is-danger");

				// password 2
				[ok, help, progress] = Utils.newPasswordOK(passwordInput2.value);
				allOK = allOK && ok;
				passwordHelp2.innerHTML = help;
				passwordProgress2.setAttribute("value", progress);
				passwordProgress2.classList.remove("is-danger", "is-warning", "is-info", "is-success");
				passwordProgress2.classList.add(progressClass(progress));
				passwordInput2.classList.remove(ok ? "is-danger" : "is-success");
				passwordInput2.classList.add(ok ? "is-success" : "is-danger");

				if (!allOK) {
					submit.setAttribute("disabled", "");
					return;
				}

				const password1 = passwordInput1.value.trim();
				const password2 = passwordInput2.value.trim();

				if (password1 !== password2) {
					submit.setAttribute("disabled", "");
					passwordHelp1.innerHTML = "Does not match";
					passwordHelp2.innerHTML = "Does not match";
					passwordInput1.classList.remove("is-success");
					passwordInput1.classList.add("is-danger");
					passwordInput2.classList.remove("is-success");
					passwordInput2.classList.add("is-danger");
					return;
				}

				submit.removeAttribute("disabled");
			};

			labelInput.addEventListener("keyup", refresh);
			labelInput.addEventListener("change", refresh);

			passwordInput1.addEventListener("keyup", refresh);
			passwordInput1.addEventListener("change", refresh);

			passwordInput2.addEventListener("keyup", refresh);
			passwordInput2.addEventListener("change", refresh);

			node.addEventListener("submit", e => {
				e.preventDefault();

				const action = node.attributes["action"].nodeValue;
				const modal = node.closest("div.modal");

				const hiddenInputs = node.querySelectorAll("input[type=hidden]");

				const q = {};

				hiddenInputs.forEach(input => {
					q[input.name] = input.value;
				});

				q[labelInput.name] = labelInput.value.trim();
				q[passwordInput1.name] = passwordInput1.value.trim();
				q[passwordInput2.name] = passwordInput2.value.trim();

				const opts = {
					method: "POST",
					credentials: "same-origin",
					redirect: "manual",
					headers: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
					body: Utils.queryString(q),
				};

				fetch(action, opts).then(res => {
					modal.classList.remove("is-active");

					return Utils.handleFetchError(res);
				}).then(res => res.json()).then(res => {
					window.setTimeout(() => {
						window.location.reload();
					}, 0);
				}).catch(err => {
					console.error(`add-master-password-form fetch failed: ${err}`);
				});
			});
		});
	};

	Common.registerInitHTMLElemHook(elem => {
		initJSAddMasterPasswordForm(elem.querySelectorAll(".js-add-master-password-form"));
	});
})();
