package redismain

import (
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"

	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	SESSION_KEY_LENGTH             = 8
	SESSION_CREATE_MAX_RETRY_COUNT = 5
)

var ErrMaxRetry = errors.New("max retry count exceeded")
var ErrRetry = errors.New("retry")

type SessionKey [SESSION_KEY_LENGTH]byte

type Session struct {
	Key       SessionKey
	UserID    int64
	CreatedAt time.Time
}

type rawSession struct {
	UserID    int64 `redis:"user_id"`
	CreatedAt int64 `redis:"created_at"`
}

func (key SessionKey) String() string {
	return hex.EncodeToString(key[:])
}

func (session *Session) String() string {
	return session.Key.String()
}

func (session *Session) IsValid() bool {
	return session.UserID > 0
}

func (raw *rawSession) session(key SessionKey) *Session {
	return &Session{
		Key:       key,
		UserID:    raw.UserID,
		CreatedAt: time.Unix(raw.CreatedAt, 0),
	}
}

func SessionKeyFromHex(encoded string) (SessionKey, error) {
	if len(encoded) != hex.EncodedLen(SESSION_KEY_LENGTH) {
		return SessionKey{}, fmt.Errorf("expected session key of length %d, got %d", hex.EncodedLen(SESSION_KEY_LENGTH), len(encoded))
	}

	b := make([]byte, SESSION_KEY_LENGTH)

	n, err := hex.Decode(b, []byte(encoded))
	if err != nil {
		return SessionKey{}, utils.Wrap(err, "decode hex")
	}
	if n != SESSION_KEY_LENGTH {
		return SessionKey{}, fmt.Errorf("expected decoded %d bytes, got %d", SESSION_KEY_LENGTH, n)
	}

	key := SessionKey{}
	copy(key[:], b)

	return key, nil
}

func (rmc *RedisMainConnection) CreateSession(ctx context.Context, userID int64, ttl time.Duration) (*Session, error) {
	b := make([]byte, SESSION_KEY_LENGTH)

	for i := 0; i < SESSION_CREATE_MAX_RETRY_COUNT; i++ {
		now := time.Now()

		n, err := rand.Read(b)
		if err != nil {
			return nil, utils.Wrap(err, "rand read")
		}
		if n != SESSION_KEY_LENGTH {
			return nil, fmt.Errorf("expected rand read of %d bytes, got %d", SESSION_KEY_LENGTH, n)
		}

		key := SessionKey{}
		copy(key[:], b)

		k := ns.session(key)

		if err = rmc.rdb.Watch(ctx, func(tx *redis.Tx) error {
			exists, err := tx.Exists(ctx, k).Result()
			if err != nil {
				return err
			}
			if exists != 0 {
				return ErrRetry
			}

			_, err = tx.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
				pipe.HSet(
					ctx, k,
					"user_id", strconv.FormatInt(userID, 10),
					"created_at", strconv.FormatInt(now.Unix(), 10),
				)

				pipe.Expire(ctx, k, ttl)

				return nil
			})

			return err
		}, k); err == nil {
			return &Session{
				Key:       key,
				UserID:    userID,
				CreatedAt: now,
			}, nil
		} else if !errors.Is(err, redis.TxFailedErr) && !errors.Is(err, ErrRetry) {
			return nil, err
		}
	}

	return nil, ErrMaxRetry
}

func (rmc *RedisMainConnection) GetSession(ctx context.Context, key SessionKey) (*Session, error) {
	k := ns.session(key)

	hmget := rmc.rdb.HMGet(
		ctx, k,
		"user_id", "created_at",
	)

	if err := hmget.Err(); err != nil {
		return nil, utils.Wrap(err, "hmget")
	}

	raw := &rawSession{}

	if err := hmget.Scan(raw); err != nil {
		return nil, utils.Wrap(err, "scan")
	}

	return raw.session(key), nil
}

func (rmc *RedisMainConnection) RemoveSession(ctx context.Context, key SessionKey) (bool, error) {
	k := ns.session(key)

	numRemoved, err := rmc.rdb.Del(ctx, k).Result()

	return numRemoved > 0, utils.Wrap(err, "del")
}
