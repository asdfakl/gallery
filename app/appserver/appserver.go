package appserver

import (
	"context"
	"errors"
	"html/template"
	"net/http"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/middleware"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type AppServer struct {
	cfg    *Configuration
	appCtx context.Context
	logger *log.Logger
	pg     *postgres.PostgresConnection
	rmc    *redismain.RedisMainConnection
	thumbs thumbstorage.Storage

	once   sync.Once
	server *http.Server
	tpls   *template.Template

	cachedStoragesMutex sync.RWMutex
	cachedStorages      map[int64]*CachedObjectStorage

	cachedBackgroundJobQueuesMutex sync.RWMutex
	cachedBackgroundJobQueues      map[postgres.BackgroundJobFnKey]*CachedBackgroundJobQueue

	cachedUserViewsMutex  sync.RWMutex
	cachedUserViewsExpiry time.Time
	cachedUserViews       map[int64]*postgres.UserView

	cachedTagFrequenciesMutex  sync.RWMutex
	cachedTagFrequenciesExpiry time.Time
	cachedTagFrequencies       postgres.TagFrequencyList
}

func New(
	cfg *Configuration,
	logger *log.Logger,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
) (*AppServer, error) {
	srv := &AppServer{
		cfg:    cfg,
		logger: logger.With(log.String("service", "app")),
		pg:     pg,
		rmc:    rmc,
		thumbs: thumbs,

		cachedStorages:            make(map[int64]*CachedObjectStorage),
		cachedBackgroundJobQueues: make(map[postgres.BackgroundJobFnKey]*CachedBackgroundJobQueue),
		cachedUserViews:           make(map[int64]*postgres.UserView),
	}

	router := httprouter.New()

	srv.registerRoutes(router)

	handler := middleware.NewStatusCodeRecorderMiddleware(
		middleware.NewLoggerMiddleWare(
			srv.logger,
			middleware.NewRecovererMiddleware(
				middleware.NewCompressMiddleware(
					NewAuthMiddleware(
						srv,
						router,
					),
					[]string{
						"text/*",
						"application/json",
					},
				),
			),
		),
	)

	srv.server = &http.Server{
		Addr:    cfg.ListenAddr,
		Handler: handler,

		ReadTimeout:       cfg.ReadTimeout,
		ReadHeaderTimeout: cfg.ReadHeaderTimeout,
		WriteTimeout:      cfg.WriteTimeout,
		IdleTimeout:       cfg.IdleTimeout,
		MaxHeaderBytes:    cfg.MaxHeaderBytes,
	}

	if err := srv.loadTemplates(); err != nil {
		return nil, utils.Wrap(err, "load html templates")
	}

	return srv, nil
}

func (srv *AppServer) Start(ctx context.Context, wg *sync.WaitGroup) {
	srv.once.Do(func() {
		srv.appCtx = ctx

		wg.Add(2)

		go srv.listenAndServe(wg)
		go srv.listenAppContext(wg)
	})
}

func (srv *AppServer) listenAndServe(wg *sync.WaitGroup) {
	defer wg.Done()

	srv.logger.System(
		"listen and serve http",
		log.String("addr", srv.server.Addr),
		log.String("root_path", srv.cfg.RootPath),
	)

	if err := srv.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		utils.Die(err, "listen and serve http")
	}
}

func (srv *AppServer) listenAppContext(wg *sync.WaitGroup) {
	defer wg.Done()

	<-srv.appCtx.Done()

	shutdownCtx, cancelShutdownCtx := context.WithTimeout(context.Background(), srv.cfg.WriteTimeout)
	defer cancelShutdownCtx()

	if err := srv.server.Shutdown(shutdownCtx); err != nil {
		utils.Die(err, "shutdown http server")
	}
}

func (srv *AppServer) registerRoutes(router *httprouter.Router) {
	router.GET(srv.path("/static/*filepath"), srv.staticPageFn())

	srv.registerHandler(router, "/", IndexPageFn, http.MethodGet)
	srv.registerHandler(router, "/objects/:sha1", ObjectPageFn, http.MethodGet)
	srv.registerHandler(router, "/objects/:sha1/publish", PublishObjectPageFn, http.MethodPost)
	srv.registerHandler(router, "/objects/:sha1/unpublish", PublishObjectPageFn, http.MethodPost)
	srv.registerHandler(router, "/objects/:sha1/tags", SetObjectTagsPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/search", SearchPageFn, http.MethodGet)
	srv.registerHandler(router, "/browse", BrowsePageFn, http.MethodGet)

	srv.registerHandler(router, "/users/:ext_id/profile", UserProfilePageFn, http.MethodGet)
	srv.registerHandler(router, "/users/:ext_id/modify", UserModifyPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/users/:ext_id/modifypassword", UserModifyPasswordPageFn, http.MethodGet, http.MethodPost)

	srv.registerHandler(router, "/secretslist", SecretsListPageFn, http.MethodGet)
	srv.registerHandler(router, "/addmasterpassword", AddMasterPasswordPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/masterpasswords/:ext_id/remove", RemoveMasterPasswordPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/addsecret", AddSecretPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/secrets/:ext_id", SecretPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/secrets/:ext_id/modify", ModifySecretPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/secrets/:ext_id/changemasterpassword", ChangeSecretMasterPasswordPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/secrets/:ext_id/remove", RemoveSecretPageFn, http.MethodGet, http.MethodPost)

	srv.registerHandler(router, "/login", LoginPageFn, http.MethodGet, http.MethodPost)
	srv.registerHandler(router, "/logout", LogoutPageFn, http.MethodPost)

	srv.registerHandler(router, "/thumbnails/:size/:sha1", ThumbnailPageFn, http.MethodGet)

	srv.registerHandler(router, "/client", ClientPageFn, http.MethodGet)

	srv.registerHandler(router, "/download/*filepath", DownloadPageFn, http.MethodGet)
}

func (srv *AppServer) registerHandler(router *httprouter.Router, path string, handlerFn HandlerFn, methods ...string) {
	path = srv.path(path)

	fn := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		hnd, err := handlerFn(srv, r, ps)
		if err != nil {
			srv.handleError(w, err)
			return
		}

		user, _ := r.Context().Value(ContextKeyUser{}).(*postgres.UserView)

		hnd.SetRequest(r)
		hnd.SetUser(user)

		canAccess := hnd.CanAccess(user)
		fields := []log.Field{
			log.String("method", r.Method),
			log.String("uri", r.URL.RequestURI()),
			log.Bool("access", canAccess),
		}

		if user == nil {
			fields = append(fields, log.Nil("username"))
		} else {
			fields = append(fields, log.String("username", user.Username))
		}

		srv.logger.Debug("can access", fields...)

		if !canAccess {
			srv.handleError(w, ErrForbidden(""))
			return
		}

		switch r.Method {
		case http.MethodGet:
			getHnd, ok := hnd.(GETHandler)
			if ok {
				getHnd.HandleGET(srv, w, r)
				return
			}
		case http.MethodPost:
			postHnd, ok := hnd.(POSTHandler)
			if ok {
				postHnd.HandlePOST(srv, w, r)
				return
			}
		}

		srv.handleError(w, ErrMethodNotAllowed())
	}

	for _, method := range methods {
		router.Handle(method, path, fn)
	}
}

func (srv *AppServer) handleError(w http.ResponseWriter, err error) {
	res, ok := err.(*ErrorResponse)
	if !ok {
		srv.logger.Error("internal server error", log.Err(err))
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	switch res.statusCode {
	case http.StatusBadRequest:
		srv.logger.Warn("bad request", log.Err(res))
	}

	http.Error(w, res.public, res.statusCode)
}

func (srv *AppServer) redirect(w http.ResponseWriter, r *http.Request, path string, code int) {
	http.Redirect(w, r, srv.path(path), code)
}

func (srv *AppServer) isRooted(path string) bool {
	root := srv.cfg.RootPath

	if !strings.HasPrefix(path, root) {
		return false
	}

	if root == "/" || path == root {
		return true
	}

	path = strings.TrimPrefix(path, root)

	return strings.HasPrefix(path, "/")
}

func (srv *AppServer) path(p string) string {
	if srv.isRooted(p) {
		return p
	}
	return path.Join(srv.cfg.RootPath, p)
}

func (srv *AppServer) rateLimited(resource string, max int, ttl time.Duration, w http.ResponseWriter, r *http.Request, fn func(*AppServer, http.ResponseWriter, *http.Request)) {
	ip := utils.RequestIP(r, srv.cfg.TrustXForwardedFor, srv.cfg.TrustXRealIP)
	if ip == nil {
		srv.handleError(w, ErrBadRequest(errors.New("could not determine request ip"), ""))
		return
	}

	limited, err := srv.rmc.IsRateLimited(srv.appCtx, resource, "remote_ip", ip.String(), max, ttl)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	if limited {
		srv.handleError(w, ErrTooManyRequests(""))
		return
	}

	fn(srv, w, r)
}
