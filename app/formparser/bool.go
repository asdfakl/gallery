package formparser

import (
	"fmt"
	"strconv"
)

type FormBoolField struct {
	parser *FormParser
	key    string
	value  *bool
}

func (parser *FormParser) Bool(key string) (field *FormBoolField) {
	field = &FormBoolField{
		parser: parser,
		key:    key,
	}

	vals, err := parser.src(key)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse form: %w", err))
		return
	}

	if len(vals) == 0 {
		return
	}

	if len(vals) > 1 {
		parser.registerError(key, ErrFormParserAmbiguous)
		return
	}

	b, err := strconv.ParseBool(vals[0])
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse bool: %w", err))
		return
	}

	field.value = &b
	return
}

func (field *FormBoolField) Required() *FormBoolField {
	if field.value == nil {
		field.parser.registerError(field.key, ErrFormParserRequired)
	}
	return field
}

func (field *FormBoolField) Value() bool {
	field.parser.mustHaveErrChecked()

	return *field.value
}

func (field *FormBoolField) OptionalValue() *bool {
	field.parser.mustHaveErrChecked()

	return field.value
}

func (field *FormBoolField) ValueOr(def bool) bool {
	field.parser.mustHaveErrChecked()

	if field.value == nil {
		return def
	}
	return *field.value
}
