package middleware

import (
	"compress/flate"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"path"
	"strings"
	"time"

	"gitlab.com/asdfakl/gallery/app/log"
)

type ContextKeyUser struct{}
type ContextKeySession struct{}

type RecovererMiddleware struct {
	handler http.Handler
}

type LoggerMiddleware struct {
	logger  *log.Logger
	handler http.Handler
}

type StatusCodeRecorderMiddleware struct {
	handler http.Handler
}

type StatusCodeRecorder struct {
	w              http.ResponseWriter
	statusCode     int
	headersWritten bool
}

type CompressResponseWriter struct {
	w                   http.ResponseWriter
	encoding            string
	compressor          io.Writer
	contentTypePatterns []string
	headersWritten      bool
	doCompress          bool
}

type CompressMiddleware struct {
	handler             http.Handler
	contentTypePatterns []string
}

func NewLoggerMiddleWare(logger *log.Logger, handler http.Handler) http.Handler {
	return &LoggerMiddleware{
		logger:  logger,
		handler: handler,
	}
}

func NewRecovererMiddleware(handler http.Handler) http.Handler {
	return &RecovererMiddleware{
		handler: handler,
	}
}

func NewStatusCodeRecorderMiddleware(handler http.Handler) http.Handler {
	return &StatusCodeRecorderMiddleware{
		handler: handler,
	}
}

func NewCompressMiddleware(handler http.Handler, contentTypePatterns []string) http.Handler {
	return &CompressMiddleware{
		handler:             handler,
		contentTypePatterns: contentTypePatterns,
	}
}

func (mw *RecovererMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			log.Default.Error("http panic", log.Err(fmt.Errorf("%v", r)))

			log.Default.Stack()

			if ww, ok := w.(*StatusCodeRecorder); ok && !ww.headersWritten {
				http.Error(w, "internal server error", http.StatusInternalServerError)
			}
		}
	}()

	mw.handler.ServeHTTP(w, r)
}

func (mw *LoggerMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()

	mw.handler.ServeHTTP(w, r)

	fields := []log.Field{
		log.String("method", r.Method),
		log.String("uri", r.URL.RequestURI()),
		log.Duration("elapsed", time.Since(startTime)),
	}

	if ww, ok := w.(*StatusCodeRecorder); ok {
		fields = append(fields, log.Int("status", ww.statusCode))
	}

	mw.logger.Info("http", fields...)
}

func (mw *StatusCodeRecorderMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ww := &StatusCodeRecorder{
		w:              w,
		statusCode:     http.StatusOK,
		headersWritten: false,
	}

	mw.handler.ServeHTTP(ww, r)
}

// source: https://github.com/gorilla/handlers/blob/master/compress.go
func (mw *CompressMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	const (
		acceptEncoding = "Accept-Encoding"
		encodingGzip   = "gzip"
		encodingFlate  = "deflate"
	)

	encoding := ""
	for _, candidate := range strings.Split(r.Header.Get(acceptEncoding), ",") {
		candidate = strings.TrimSpace(candidate)
		if candidate == encodingGzip || candidate == encodingFlate {
			encoding = candidate
			break
		}
	}

	w.Header().Add("Vary", acceptEncoding)

	if encoding == "" {
		mw.handler.ServeHTTP(w, r)
		return
	}

	if r.Header.Get("Upgrade") != "" {
		mw.handler.ServeHTTP(w, r)
		return
	}

	var writer io.WriteCloser
	switch encoding {
	case encodingGzip:
		writer = gzip.NewWriter(w)
	case encodingFlate:
		writer, _ = flate.NewWriter(w, flate.DefaultCompression)
	}

	r.Header.Del(acceptEncoding)

	cw := &CompressResponseWriter{
		w:                   w,
		encoding:            encoding,
		compressor:          writer,
		contentTypePatterns: mw.contentTypePatterns,
	}

	mw.handler.ServeHTTP(cw, r)
	// do not defer in case the above call panics without writing anything
	if cw.headersWritten && cw.doCompress {
		writer.Close()
	}
}

func (w *StatusCodeRecorder) Header() http.Header {
	return w.w.Header()
}

func (w *StatusCodeRecorder) Write(b []byte) (int, error) {
	w.headersWritten = true

	return w.w.Write(b)
}

func (w *StatusCodeRecorder) WriteHeader(statusCode int) {
	w.headersWritten = true
	w.statusCode = statusCode

	w.w.WriteHeader(statusCode)
}

func (w *CompressResponseWriter) Header() http.Header {
	return w.w.Header()
}

func (w *CompressResponseWriter) Write(b []byte) (int, error) {
	if !w.headersWritten {
		w.firstWrite()
	}

	if w.doCompress {
		return w.compressor.Write(b)
	} else {
		return w.w.Write(b)
	}
}

func (w *CompressResponseWriter) WriteHeader(statusCode int) {
	if !w.headersWritten {
		w.firstWrite()
	}

	w.w.WriteHeader(statusCode)
}

func (w *CompressResponseWriter) firstWrite() {
	w.headersWritten = true

	for _, pattern := range w.contentTypePatterns {
		if match, _ := path.Match(pattern, w.w.Header().Get("Content-Type")); match {
			w.doCompress = true
			break
		}
	}

	if w.doCompress {
		w.w.Header().Del("Content-Length")
		w.Header().Add("Content-Encoding", w.encoding)
	}
}
