package main

import (
	"context"
	"reflect"
	"sync"
	"time"

	"gitlab.com/asdfakl/gallery/app/bg"
	"gitlab.com/asdfakl/gallery/app/bg/queuelistener"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

func handleQueues(
	ctx context.Context,
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	wg *sync.WaitGroup,
	fatalf func(error),
	executor *bg.Executor,
	logger *log.Logger,
) {

	defer wg.Done()
	defer logger.System("stop")

	listenedQueues := make(map[string]*postgres.BackgroundJobQueue)
	listeners := make(map[string]*queuelistener.Listener)
	donec := make(chan string)
	defer close(donec) // not necessary but exposes wait failures

	refresh := func() error {
		queues, err := pg.GetEveryBackgroundJobQueue(ctx)
		if err != nil {
			return utils.Wrap(err, "query postgres background job queues")
		}

		knownKeys := make(map[string]bool)
		for _, queue := range queues {
			knownKeys[queue.Key] = true

			if _, ok := listenedQueues[queue.Key]; ok {
				if !reflect.DeepEqual(listenedQueues[queue.Key], queue) {
					listeners[queue.Key].Stop()
				}
			} else {
				listenedQueues[queue.Key] = queue

				listener := queuelistener.New(pg, rmc, thumbs, queue, executor, logger)
				listener.Start(ctx, donec)
				listeners[queue.Key] = listener
			}
		}

		for _, queue := range listenedQueues {
			if !knownKeys[queue.Key] {
				listeners[queue.Key].Stop()
			}
		}

		return nil
	}

	if err := refresh(); err != nil {
		fatalf(err)
		return
	}

	ticker := time.NewTicker(time.Minute)
	defer ticker.Stop()

	logger.System("run")

queue_select_loop:
	for {
		select {
		case <-ctx.Done():
			break queue_select_loop

		case <-ticker.C:
			if err := refresh(); err != nil {
				fatalf(err)
				return
			}

		case key := <-donec:
			delete(listeners, key)
			delete(listenedQueues, key)
		}
	}

	for len(listenedQueues) > 0 {
		key := <-donec
		delete(listeners, key)
		delete(listenedQueues, key)
	}
}
