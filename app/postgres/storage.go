package postgres

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"github.com/jackc/pgx/v4"
)

const (
	StorageTypeS3     StorageType = "s3"
	StorageTypeLocal  StorageType = "local"
	StorageViewFields             = `
		s.id,
		s.type,
		s.access_priority,
		s.storage_priority,
		s.name,
		s.description,
		s.opts,
		s.quota_object_count,
		s.quota_object_bytes,
		s.object_count,
		s.object_bytes
	`
)

type StorageType string

type StorageList []*StorageView
type StorageView struct {
	ID               int64
	Type             StorageType
	AccessPriority   int
	StoragePriority  int
	Name             string
	Description      string
	Opts             map[string]interface{}
	QuotaObjectCount int
	QuotaObjectBytes int64
	ObjectCount      int
	ObjectBytes      int64
}

func (ls StorageList) SortByAccessPriority() {
	sort.Slice(ls, func(i, j int) bool {
		return ls[i].AccessPriority < ls[j].AccessPriority
	})
}

func (ls StorageList) SortByStoragePriority() {
	sort.Slice(ls, func(i, j int) bool {
		return ls[i].StoragePriority < ls[j].StoragePriority
	})
}

func (ls StorageList) Filter(predicate func(*StorageView) bool) (filtered StorageList) {
	for _, item := range ls {
		if predicate(item) {
			filtered = append(filtered, item)
		}
	}
	return
}

func StorageListPredicateIsLocal(item *StorageView) bool {
	return item.Type == StorageTypeLocal
}

func StorageListPredicateHasQuota(item *StorageView) bool {
	return item.ObjectCount < item.QuotaObjectCount && item.ObjectBytes < item.QuotaObjectBytes
}

func (pg *PostgresConnection) GetStorageList(ctx context.Context) (StorageList, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_storage s
	`, StorageViewFields)

	rows, err := pg.cp.Query(ctx, q)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var storages StorageList

	for rows.Next() {
		storage, err := scanStorageFromRows(rows)
		if err != nil {
			return nil, err
		}

		storages = append(storages, storage)
	}

	return storages, rows.Err()
}

func (pg *PostgresConnection) GetStorageListByObjectID(ctx context.Context, objectID int64) (StorageList, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_storage s
		WHERE
			s.id IN (
				SELECT storage_id
				FROM gallery.object_storage
				WHERE object_id = $1
			)
	`, StorageViewFields)

	rows, err := pg.cp.Query(ctx, q, objectID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var storages StorageList

	for rows.Next() {
		storage, err := scanStorageFromRows(rows)
		if err != nil {
			return nil, err
		}

		storages = append(storages, storage)
	}

	return storages, rows.Err()
}

func (pg *PostgresConnection) GetStorageByName(ctx context.Context, name string) (*StorageView, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_storage s
		WHERE
			s.name = $1
	`, StorageViewFields)

	return scanStorageFromRow(pg.cp.QueryRow(ctx, q, name))
}

func (pg *PostgresConnection) GetStorageByID(ctx context.Context, id int64) (*StorageView, error) {
	q := fmt.Sprintf(`
		SELECT
			%s
		FROM
			gallery.v_storage s
		WHERE
			s.id = $1
	`, StorageViewFields)

	return scanStorageFromRow(pg.cp.QueryRow(ctx, q, id))
}

func scanStorageFromRow(row pgx.Row) (*StorageView, error) {
	storage := &StorageView{}

	if err := row.Scan(
		&storage.ID,
		&storage.Type,
		&storage.AccessPriority,
		&storage.StoragePriority,
		&storage.Name,
		&storage.Description,
		&storage.Opts,
		&storage.QuotaObjectCount,
		&storage.QuotaObjectBytes,
		&storage.ObjectCount,
		&storage.ObjectBytes,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return storage, nil
}

func scanStorageFromRows(rows pgx.Rows) (*StorageView, error) {
	storage := &StorageView{}

	if err := rows.Scan(
		&storage.ID,
		&storage.Type,
		&storage.AccessPriority,
		&storage.StoragePriority,
		&storage.Name,
		&storage.Description,
		&storage.Opts,
		&storage.QuotaObjectCount,
		&storage.QuotaObjectBytes,
		&storage.ObjectCount,
		&storage.ObjectBytes,
	); err != nil {
		return nil, err
	}

	return storage, nil
}
