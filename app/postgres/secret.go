package postgres

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"

	"gitlab.com/asdfakl/gallery/app/utils"
)

var ErrDecrypt = errors.New("decrypt failed")

type MasterPasswordList []*MasterPassword
type MasterPassword struct {
	ID       int64
	ExtID    uuid.UUID
	Password string
	Owner    int64
	Label    string
}

type SecretList []*Secret
type Secret struct {
	ID               int64
	ExtID            uuid.UUID
	MasterPasswordID int64
	Label            string
}

func (ls MasterPasswordList) IDMap() map[int64]*MasterPassword {
	m := make(map[int64]*MasterPassword)

	for _, mp := range ls {
		m[mp.ID] = mp
	}

	return m
}

func (ls MasterPasswordList) Filter(predicate func(*MasterPassword) bool) (filtered MasterPasswordList) {
	for _, mp := range ls {
		if predicate(mp) {
			filtered = append(filtered, mp)
		}
	}
	return
}

func (ls SecretList) CountByMasterPasswordID() map[int64]int {
	m := make(map[int64]int)

	for _, s := range ls {
		m[s.MasterPasswordID]++
	}

	return m
}

func (pg *PostgresConnection) InsertMasterPassword(ctx context.Context, mp *MasterPassword) error {
	q := `
		INSERT INTO gallery.master_password (
			password,
			owner,
			label
		) VALUES (
			$1, $2, $3
		) RETURNING id, ext_id;
	`

	return pg.cp.QueryRow(ctx, q, mp.Password, mp.Owner, mp.Label).Scan(&mp.ID, &mp.ExtID)
}

func (pg *PostgresConnection) InsertSecret(ctx context.Context, secret *Secret, ciphertext, nonce []byte) error {
	q := `
		INSERT INTO gallery.secret (
			master_password_id,
			nonce,
			ciphertext,
			label
		) VALUES (
			$1, $2, $3, $4
		) RETURNING id, ext_id;
	`

	return pg.cp.QueryRow(ctx, q, secret.MasterPasswordID, nonce, ciphertext, secret.Label).Scan(
		&secret.ID,
		&secret.ExtID,
	)
}

func (pg *PostgresConnection) ModifySecret(ctx context.Context, secret *Secret, ciphertext, nonce []byte) error {
	q := `
		UPDATE
			gallery.secret
		SET
			master_password_id = $1,
			nonce = $2,
			ciphertext = $3,
			label = $4
		WHERE
			id = $5
	`

	_, err := pg.cp.Exec(ctx, q, secret.MasterPasswordID, nonce, ciphertext, secret.Label, secret.ID)
	return err
}

func (pg *PostgresConnection) DeleteSecret(ctx context.Context, id int64) error {
	q := `
		DELETE FROM
			gallery.secret
		WHERE
			id = $1
	`

	_, err := pg.cp.Exec(ctx, q, id)
	return err
}

func (pg *PostgresConnection) DeleteMasterPassword(ctx context.Context, id int64) error {
	q := `
		DELETE FROM
			gallery.master_password
		WHERE
			id = $1
	`

	_, err := pg.cp.Exec(ctx, q, id)
	return err
}

func (pg *PostgresConnection) GetMasterPasswordByID(ctx context.Context, id int64) (*MasterPassword, error) {
	q := `
		SELECT
			mp.id,
			mp.ext_id,
			mp.password,
			mp.owner,
			mp.label
		FROM
			gallery.master_password mp
		WHERE
			mp.id = $1
	`

	return scanMasterPasswordFromRow(pg.cp.QueryRow(ctx, q, id))
}

func (pg *PostgresConnection) GetMasterPasswordByExtID(ctx context.Context, extID uuid.UUID) (*MasterPassword, error) {
	q := `
		SELECT
			mp.id,
			mp.ext_id,
			mp.password,
			mp.owner,
			mp.label
		FROM
			gallery.master_password mp
		WHERE
			mp.ext_id = $1
	`

	return scanMasterPasswordFromRow(pg.cp.QueryRow(ctx, q, extID))
}

func (pg *PostgresConnection) GetMasterPasswordListByOwner(ctx context.Context, ownerID int64) (MasterPasswordList, error) {
	q := `
		SELECT
			mp.id,
			mp.ext_id,
			mp.owner,
			mp.label
		FROM
			gallery.master_password mp
		WHERE
			mp.owner = $1
		ORDER BY
			mp.label ASC
	`

	rows, err := pg.cp.Query(ctx, q, ownerID)
	if err != nil {
		return nil, utils.Wrap(err, "query")
	}
	defer rows.Close()

	var passwords MasterPasswordList

	for rows.Next() {
		password, err := scanMasterPasswordFromRows(rows)
		if err != nil {
			return nil, utils.Wrap(err, "scan")
		}

		passwords = append(passwords, password)
	}

	return passwords, rows.Err()
}

func (pg *PostgresConnection) GetSecretListByOwner(ctx context.Context, ownerID int64) (SecretList, error) {
	q := `
		SELECT
			s.id,
			s.ext_id,
			s.master_password_id,
			s.label
		FROM
			gallery.secret s, gallery.master_password mp
		WHERE
			s.master_password_id = mp.id
			AND mp.owner = $1
		ORDER BY
			s.label ASC, s.id ASC
	`

	rows, err := pg.cp.Query(ctx, q, ownerID)
	if err != nil {
		return nil, utils.Wrap(err, "query")
	}
	defer rows.Close()

	var secrets SecretList

	for rows.Next() {
		secret, err := scanSecretFromRows(rows)
		if err != nil {
			return nil, utils.Wrap(err, "scan")
		}

		secrets = append(secrets, secret)
	}

	return secrets, rows.Err()
}

func (pg *PostgresConnection) GetSecretByExtID(ctx context.Context, extID uuid.UUID) (*Secret, error) {
	q := `
		SELECT
			s.id,
			s.ext_id,
			s.master_password_id,
			s.label
		FROM
			gallery.secret s
		WHERE
			s.ext_id = $1
	`

	return scanSecretFromRow(pg.cp.QueryRow(ctx, q, extID))
}

func (pg *PostgresConnection) DecryptSecretByID(ctx context.Context, id int64, passphrase string) ([]byte, error) {
	var ciphertext, nonce []byte

	q := `
		SELECT
			s.ciphertext,
			s.nonce
		FROM
			gallery.secret s
		WHERE
			s.id = $1
	`

	if err := pg.cp.QueryRow(ctx, q, id).Scan(&ciphertext, &nonce); err != nil {
		return nil, utils.Wrap(err, "query")
	}

	plaintext, err := utils.DecryptAES256(ciphertext, nonce, passphrase)
	if err != nil {
		return nil, ErrDecrypt
	}

	return plaintext, nil
}

func scanMasterPasswordFromRow(row pgx.Row) (*MasterPassword, error) {
	password := &MasterPassword{}

	if err := row.Scan(
		&password.ID,
		&password.ExtID,
		&password.Password,
		&password.Owner,
		&password.Label,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return password, nil
}

func scanMasterPasswordFromRows(rows pgx.Rows) (*MasterPassword, error) {
	password := &MasterPassword{}

	// lists never select password
	if err := rows.Scan(
		&password.ID,
		&password.ExtID,
		&password.Owner,
		&password.Label,
	); err != nil {
		return nil, err
	}

	return password, nil
}

func scanSecretFromRow(row pgx.Row) (*Secret, error) {
	secret := &Secret{}

	if err := row.Scan(
		&secret.ID,
		&secret.ExtID,
		&secret.MasterPasswordID,
		&secret.Label,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return secret, nil
}

func scanSecretFromRows(rows pgx.Rows) (*Secret, error) {
	secret := &Secret{}

	if err := rows.Scan(
		&secret.ID,
		&secret.ExtID,
		&secret.MasterPasswordID,
		&secret.Label,
	); err != nil {
		return nil, err
	}

	return secret, nil
}
