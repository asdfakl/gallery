package formparser

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

type FormStringSliceField struct {
	parser *FormParser
	key    string
	values []string
}

func (parser *FormParser) StringSlice(key string) (field *FormStringSliceField) {
	field = &FormStringSliceField{
		parser: parser,
		key:    key,
	}

	vals, err := parser.src(key)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse form: %w", err))
		return
	}

	for _, val := range vals {
		if !utf8.ValidString(val) {
			parser.registerError(key, ErrFormParserInvalidUTF8)
			return
		}
	}

	field.values = vals
	return
}

func (field *FormStringSliceField) TrimElements() *FormStringSliceField {
	for i := range field.values {
		field.values[i] = strings.TrimSpace(field.values[i])
	}
	return field
}

func (field *FormStringSliceField) RemoveEmptyElements() *FormStringSliceField {
	var vals []string
	for _, val := range field.values {
		if val != "" {
			vals = append(vals, val)
		}
	}
	field.values = vals
	return field
}

func (field *FormStringSliceField) NonEmpty() *FormStringSliceField {
	if len(field.values) == 0 {
		field.parser.registerError(field.key, ErrFormParserEmpty)
	}
	return field
}

func (field *FormStringSliceField) MaxLength(maxLength int) *FormStringSliceField {
	if len(field.values) > maxLength {
		field.parser.registerError(field.key, ErrFormParserMaxLengthExceeded)
	}
	return field
}

func (field *FormStringSliceField) MaxElementLength(maxLength int) *FormStringSliceField {
	for _, val := range field.values {
		if runeLen := len([]rune(val)); runeLen > maxLength {
			field.parser.registerError(field.key, ErrFormParserMaxLengthExceeded)
			return field
		}
	}
	return field
}

func (field *FormStringSliceField) UniqueElements() *FormStringSliceField {
	uniq := make(map[string]struct{})

	for _, val := range field.values {
		if _, exists := uniq[val]; exists {
			field.parser.registerError(field.key, ErrFormParserNonUnique)
			return field
		}
		uniq[val] = struct{}{}
	}

	return field
}

func (field *FormStringSliceField) ElementsMatchOptions(opts []string) *FormStringSliceField {
	match := make(map[string]bool)

	for _, opt := range opts {
		match[opt] = true
	}

	for _, val := range field.values {
		if !match[val] {
			field.parser.registerError(field.key, ErrFormParserDoesNotMatchOptions)
			return field
		}
	}

	return field
}

func (field *FormStringSliceField) Values() []string {
	field.parser.mustHaveErrChecked()

	return field.values
}
