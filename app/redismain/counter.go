package redismain

import "context"

func (rmc *RedisMainConnection) Counter(ctx context.Context, key string) (int64, error) {
	return rmc.rdb.Incr(ctx, ns.counter(key)).Result()
}
