package postgres

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/jackc/pgx/v4"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type ObjectList []*Object
type Object struct {
	ID                   int64
	SHA1                 [sha1.Size]byte
	Basename             string
	Size                 int64
	ContentType          string
	ContentSubtype       string
	ContentSubtypeParams *string
	ModifiedAt           time.Time
	AddedAt              time.Time
	Owner                *int64
	Public               bool
	Tags                 []string
	Deleted              bool
}

func (ls ObjectList) SHA1Map() map[[sha1.Size]byte]*Object {
	m := make(map[[sha1.Size]byte]*Object)

	for _, obj := range ls {
		m[obj.SHA1] = obj
	}

	return m
}

func (ls ObjectList) UniqueSHA1() (uniq [][sha1.Size]byte) {
	m := make(map[[sha1.Size]byte]struct{})

	for _, obj := range ls {
		m[obj.SHA1] = struct{}{}
	}

	for id := range m {
		uniq = append(uniq, id)
	}

	return
}

func (obj *Object) HexSHA1() string {
	return hex.EncodeToString(obj.SHA1[:])
}

func (obj *Object) MatchContentType(qct *utils.QualifiedContentType) bool {
	return obj.ContentType == qct.Type && obj.ContentSubtype == qct.Subtype
}

func (obj *Object) MatchContentTypes(qcts []*utils.QualifiedContentType) bool {
	for _, qct := range qcts {
		if obj.MatchContentType(qct) {
			return true
		}
	}
	return false
}

func (pg *PostgresConnection) GetObjectBySHA1(ctx context.Context, hash [sha1.Size]byte) (*Object, error) {
	q := `
		SELECT
			o.id,
			o.sha1,
			o.basename,
			o.size,
			o.content_type,
			o.content_subtype,
			o.content_subtype_params,
			o.modified_at,
			o.added_at,
			o.owner,
			o.public,
			o.tags,
			o.deleted
		FROM
			gallery.object o
		WHERE
			o.sha1 = $1
	`

	return scanObjectFromRow(pg.cp.QueryRow(ctx, q, hash[:]))
}

func (pg *PostgresConnection) GetObjectByID(ctx context.Context, id int64) (*Object, error) {
	q := `
		SELECT
			o.id,
			o.sha1,
			o.basename,
			o.size,
			o.content_type,
			o.content_subtype,
			o.content_subtype_params,
			o.modified_at,
			o.added_at,
			o.owner,
			o.public,
			o.tags,
			o.deleted
		FROM
			gallery.object o
		WHERE
			o.id = $1
	`

	return scanObjectFromRow(pg.cp.QueryRow(ctx, q, id))
}

func (pg *PostgresConnection) SetObjectPublicByID(ctx context.Context, id int64, public bool) error {
	q := `
		UPDATE
			gallery.object
		SET
			public = $1
		WHERE
			id = $2
	`

	_, err := pg.cp.Exec(ctx, q, public, id)
	return err
}

func (pg *PostgresConnection) SetObjectTags(ctx context.Context, objID int64, tags []string) error {
	q := `
		UPDATE
			gallery.object
		SET
			tags = NULLIF($1, '{}'::TEXT[])
		WHERE
			id = $2
	`

	_, err := pg.cp.Exec(ctx, q, tags, objID)
	return err
}

func objectSHA1ArraysToSlices(ids [][sha1.Size]byte) (slices [][]byte) {
	for _, id := range ids {
		b := make([]byte, sha1.Size)
		copy(b, id[:])
		slices = append(slices, b)
	}
	return
}

func scanObjectFromRow(row pgx.Row) (*Object, error) {
	obj := &Object{}
	hash := make([]byte, sha1.Size)

	if err := row.Scan(
		&obj.ID,
		&hash,
		&obj.Basename,
		&obj.Size,
		&obj.ContentType,
		&obj.ContentSubtype,
		&obj.ContentSubtypeParams,
		&obj.ModifiedAt,
		&obj.AddedAt,
		&obj.Owner,
		&obj.Public,
		&obj.Tags,
		&obj.Deleted,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	copy(obj.SHA1[:], hash)

	return obj, nil
}

func scanObjectFromRows(rows pgx.Rows) (*Object, error) {
	obj := &Object{}
	hash := make([]byte, sha1.Size)

	if err := rows.Scan(
		&obj.ID,
		&hash,
		&obj.Basename,
		&obj.Size,
		&obj.ContentType,
		&obj.ContentSubtype,
		&obj.ContentSubtypeParams,
		&obj.ModifiedAt,
		&obj.AddedAt,
		&obj.Owner,
		&obj.Public,
		&obj.Tags,
		&obj.Deleted,
	); err != nil {
		return nil, err
	}

	copy(obj.SHA1[:], hash)

	return obj, nil
}

func ObjectFromLocalFile(path string) (*Object, error) {
	info, err := os.Lstat(path)
	if err != nil {
		return nil, err
	}

	if info.Mode()&os.ModeType != 0 {
		return nil, errors.New("not a regular file")
	}
	if info.Size() <= 0 {
		return nil, errors.New("empty file")
	}

	contentType, err := utils.DetectContentType(path)
	if err != nil {
		return nil, utils.Wrap(err, "detect content type")
	}

	qct, err := utils.SplitContentType(contentType)
	if err != nil {
		return nil, utils.Wrap(err, "split content type")
	}

	obj := &Object{
		Basename:             info.Name(),
		Size:                 info.Size(),
		ContentType:          qct.Type,
		ContentSubtype:       qct.Subtype,
		ContentSubtypeParams: qct.Params,
		ModifiedAt:           info.ModTime(),
	}

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	h := sha1.New()

	n, err := io.Copy(h, f)
	if err != nil {
		return nil, err
	}
	if n != obj.Size {
		return nil, fmt.Errorf("expect read of %d bytes, got %d", obj.Size, n)
	}
	copy(obj.SHA1[:], h.Sum(nil))

	return obj, nil
}
