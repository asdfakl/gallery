package config

import (
	"fmt"
	"strings"
)

type Options map[string]interface{}

func (opts Options) Int64(key string) int64 {
	switch val := opts[key].(type) {
	case int64:
		return val
	case float64:
		return int64(val)
	default:
		panic(fmt.Errorf(
			"parse float64 from options key '%s': value is %T",
			key,
			val,
		))
	}
}

func (opts Options) String(key string) string {
	val, ok := opts[key].(string)
	if !ok {
		panic(fmt.Errorf(
			"parse string from options key '%s': value is %T",
			key,
			opts[key],
		))
	}
	return val
}

func (opts Options) NonEmptyString(key string) string {
	val := strings.TrimSpace(opts.String(key))
	if val == "" {
		panic(fmt.Errorf(
			"parse non-empty string from options key '%s': value is empty",
			key,
		))
	}
	return val
}

func (opts Options) OptionalInt64(key string) (int64, bool) {
	switch val := opts[key].(type) {
	case int64:
		return val, true
	case float64:
		return int64(val), true
	default:
		return 0, false
	}
}

func (opts Options) OptionalString(key string) (string, bool) {
	val, ok := opts[key].(string)
	return val, ok
}

func (opts Options) OptionalNonEmptyString(key string) (string, bool) {
	val, ok := opts.OptionalString(key)

	val = strings.TrimSpace(val)

	return val, ok && val != ""
}

func (opts Options) Int64Or(key string, def int64) int64 {
	val, ok := opts.OptionalInt64(key)
	if !ok {
		return def
	}
	return val
}

func (opts Options) StringOr(key string, def string) string {
	val, ok := opts.OptionalString(key)
	if !ok {
		return def
	}
	return val
}

func (opts Options) NonEmptyStringOr(key string, def string) string {
	val, ok := opts.OptionalNonEmptyString(key)
	if !ok {
		return def
	}
	return val
}
