package apiserver

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/postgres"
)

type IntrospectRoute struct {
	Route
}

func IntrospectRouteFn(srv *APIServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &IntrospectRoute{}, nil
}

func (route *IntrospectRoute) CanAccess(user *postgres.UserView) bool {
	return true
}

func (route *IntrospectRoute) HandleGET(srv *APIServer, w http.ResponseWriter, r *http.Request) {
	type publicUser struct {
		ExtID            uuid.UUID `json:"ext_id"`
		Username         string    `json:"username"`
		DisplayName      string    `json:"display_name"`
		QuotaObjectCount int       `json:"quota_object_count"`
		QuotaObjectBytes int64     `json:"quota_object_bytes"`
		ObjectCount      int       `json:"object_count"`
		ObjectBytes      int64     `json:"object_bytes"`
	}

	user := route.User()

	token := r.Context().Value(ContextKeyToken{}).(*TokenPayload)

	p := &struct {
		User  publicUser   `json:"user"`
		Token TokenPayload `json:"token_payload"`
	}{
		User: publicUser{
			ExtID:            user.ExtID,
			Username:         user.Username,
			DisplayName:      user.DisplayName,
			QuotaObjectCount: user.QuotaObjectCount,
			QuotaObjectBytes: user.QuotaObjectBytes,
			ObjectCount:      user.ObjectCount,
			ObjectBytes:      user.ObjectBytes,
		},
		Token: *token,
	}

	srv.jsonResponse(w, p)
}
