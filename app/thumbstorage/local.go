package thumbstorage

import (
	"bytes"
	"context"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"sync/atomic"

	"github.com/gabriel-vasile/mimetype"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/redislru"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type LocalStorage struct {
	cfg    *config.ThumbStorageConfiguration
	rlc    *redislru.RedisLRUConnection
	logger *log.Logger

	path        string
	cacheHits   int64
	cacheMisses int64
}

var _ Storage = (*LocalStorage)(nil)

func NewLocalStorage(cfg *config.ThumbStorageConfiguration, rlc *redislru.RedisLRUConnection, logger *log.Logger) (*LocalStorage, error) {
	var err error
	storage := &LocalStorage{
		cfg:    cfg,
		rlc:    rlc,
		logger: logger,
		path:   cfg.Path,
	}

	if utils.IsSymlink(storage.path) {
		storage.path, err = filepath.EvalSymlinks(storage.path)
		if err != nil {
			return nil, utils.Wrap(err, "eval symlinks")
		}
	}

	storage.path, err = filepath.Abs(storage.path)
	if err != nil {
		return nil, utils.Wrap(err, "abs filepath")
	}

	if err = os.MkdirAll(storage.path, utils.DEFAULT_DIRECTORY_PERMISSION_BITS); err != nil {
		return nil, utils.Wrap(err, "mkdir all")
	}

	return storage, nil
}

func (storage *LocalStorage) ReadThumb(ctx context.Context, sum [sha1.Size]byte, size ThumbnailSize) (MimeReadSeekCloser, error) {
	mustValidateSize(size)

	if storage.rlc != nil {
		b, err := storage.fromCache(ctx, sum, size)
		if len(b) != 0 && err == nil {
			atomic.AddInt64(&storage.cacheHits, 1)

			return &thumbnailReader{
				ReadSeeker: bytes.NewReader(b),
				mime:       mimetype.Detect(b),
			}, nil
		}

		if err == nil {
			atomic.AddInt64(&storage.cacheMisses, 1)
		} else {
			storage.logger.Warn("read from cache", log.Err(err))
		}
	}

	thumbPath := filepath.Join(storage.thumbnailDir(sum), string(size))
	info, err := os.Lstat(thumbPath)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return nil, ErrNotExist
		}
		return nil, utils.Wrap(err, "stat")
	}

	if !info.Mode().IsRegular() {
		_ = os.Remove(thumbPath)
		return nil, fmt.Errorf("not a regular file: %q", thumbPath)
	}

	if info.Size() == 0 {
		_ = os.Remove(thumbPath)
		return nil, fmt.Errorf("empty file: %q", thumbPath)
	}

	f, err := os.Open(thumbPath)
	if err != nil {
		return nil, utils.Wrap(err, "open")
	}

	mtype, err := mimetype.DetectReader(f)
	if err != nil {
		return nil, utils.Wrap(err, "detect mimetype")
	}

	if _, err = f.Seek(0, io.SeekStart); err != nil {
		return nil, utils.Wrap(err, "seek")
	}

	if storage.rlc != nil && info.Size() <= redislru.MaxValueSize {
		b, err := io.ReadAll(f)
		if err != nil {
			return nil, utils.Wrap(err, "read")
		}

		if err = storage.toCache(ctx, sum, size, b); err != nil {
			storage.logger.Warn("write to cache", log.Err(err))
		}

		return &thumbnailReader{
			ReadSeeker: bytes.NewReader(b),
			mime:       mtype,
		}, nil
	}

	return &thumbnailReader{
		ReadSeeker: f,
		mime:       mtype,
	}, nil
}

func (storage *LocalStorage) CopyThumb(ctx context.Context, sum [sha1.Size]byte, size ThumbnailSize, srcPath string) error {
	mustValidateSize(size)

	dir := storage.thumbnailDir(sum)

	if err := os.MkdirAll(dir, utils.DEFAULT_DIRECTORY_PERMISSION_BITS); err != nil {
		return utils.Wrap(err, "mkdir all")
	}

	dstPath := filepath.Join(dir, string(size))
	_ = os.Remove(dstPath)

	_, err := utils.Copy(srcPath, dstPath)

	return utils.Wrap(err, "copy")
}

func (storage *LocalStorage) fromCache(ctx context.Context, sum [sha1.Size]byte, size ThumbnailSize) ([]byte, error) {
	return storage.rlc.Get(ctx, redisCacheKey(sum, size))
}

func (storage *LocalStorage) toCache(ctx context.Context, sum [sha1.Size]byte, size ThumbnailSize, b []byte) error {
	return storage.rlc.Set(ctx, redisCacheKey(sum, size), b)
}

func (storage *LocalStorage) thumbnailDir(sum [sha1.Size]byte) string {
	encoded := utils.HexEncodeSHA1(sum)

	return filepath.Join(
		storage.path,
		fmt.Sprintf("v%d", Version),
		encoded[0:2],
		encoded[0:4],
		encoded,
	)
}

func redisCacheKey(sum [sha1.Size]byte, size ThumbnailSize) string {
	return fmt.Sprintf(
		"%s:v%d:%s:%s",
		redisCachePrefix,
		Version,
		utils.HexEncodeSHA1(sum),
		size,
	)
}

func mustValidateSize(size ThumbnailSize) {
	switch size {
	case ThumbnailSize64x64, ThumbnailSize128x128:
		return
	default:
		panic(fmt.Errorf("unsupported thumbnail size: %q", size))
	}
}
