package appserver

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type UserProfilePage struct {
	Page

	ProfileUser *postgres.UserView
}

func UserProfilePageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("user not found")
	}

	// load
	user, err := srv.pg.GetUserViewByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get user view by ext_id")
	}
	if user == nil {
		return nil, ErrNotFound("user not found")
	}
	user.Password = "" // ensure this is not rendered!

	return &UserProfilePage{
		ProfileUser: user,
	}, nil
}

func (page *UserProfilePage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.ProfileUser.ID
}

func (page *UserProfilePage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "user_profile.html", page, http.StatusOK)
}
