package bg

import (
	"bytes"
	"context"
	"os/exec"
	"path/filepath"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

func generateThumbnailImageJPEG(ctx context.Context, thumbs thumbstorage.Storage, obj *postgres.Object, path string, logger *log.Logger) error {
	dir := filepath.Dir(path)

	path64 := filepath.Join(dir, "thumbnail_64x64.jpg")
	cmd64 := exec.CommandContext(
		ctx, "/usr/bin/convert",
		path,
		"-resize", "64x64!",
		"-quality", "100",
		path64,
	)
	cmd64.Stderr = &bytes.Buffer{}

	path128 := filepath.Join(dir, "thumbnail_128x128.jpg")
	cmd128 := exec.CommandContext(
		ctx, "/usr/bin/convert",
		path,
		"-resize", "128x128!",
		"-quality", "100",
		path128,
	)
	cmd128.Stderr = &bytes.Buffer{}

	return generateThumbnail(
		ctx, thumbs, obj,
		path64, cmd64,
		path128, cmd128,
		logger,
	)
}

func generateThumbnailImagePNG(ctx context.Context, thumbs thumbstorage.Storage, obj *postgres.Object, path string, logger *log.Logger) error {
	dir := filepath.Dir(path)

	path64 := filepath.Join(dir, "thumbnail_64x64.png")
	cmd64 := exec.CommandContext(
		ctx, "/usr/bin/convert",
		path,
		"-resize", "64x64!",
		"-quality", "100",
		path64,
	)
	cmd64.Stderr = &bytes.Buffer{}

	path128 := filepath.Join(dir, "thumbnail_128x128.png")
	cmd128 := exec.CommandContext(
		ctx, "/usr/bin/convert",
		path,
		"-resize", "128x128!",
		"-quality", "100",
		path128,
	)
	cmd128.Stderr = &bytes.Buffer{}

	return generateThumbnail(
		ctx, thumbs, obj,
		path64, cmd64,
		path128, cmd128,
		logger,
	)
}

func generateThumbnailVideo(ctx context.Context, thumbs thumbstorage.Storage, obj *postgres.Object, path string, logger *log.Logger) error {
	dir := filepath.Dir(path)

	path64 := filepath.Join(dir, "thumbnail_64x64.jpg")
	cmd64 := exec.CommandContext(
		ctx, "/usr/bin/ffmpeg",
		"-loglevel", "level+error",
		"-i", path,
		"-vf", "thumbnail,scale=64x64",
		"-frames:v", "1",
		path64,
	)
	cmd64.Stderr = &bytes.Buffer{}

	path128 := filepath.Join(dir, "thumbnail_128x128.jpg")
	cmd128 := exec.CommandContext(
		ctx, "/usr/bin/ffmpeg",
		"-loglevel", "level+error",
		"-i", path,
		"-vf", "thumbnail,scale=128x128",
		"-frames:v", "1",
		path128,
	)
	cmd128.Stderr = &bytes.Buffer{}

	return generateThumbnail(
		ctx, thumbs, obj,
		path64, cmd64,
		path128, cmd128,
		logger,
	)
}

func generateThumbnail(
	ctx context.Context,
	thumbs thumbstorage.Storage,
	obj *postgres.Object,
	path64 string, cmd64 *exec.Cmd,
	path128 string, cmd128 *exec.Cmd,
	logger *log.Logger,
) error {

	logger.Debug("command 64x64", log.String("command", cmd64.String()))
	runErr := cmd64.Run()
	if buf, ok := cmd64.Stderr.(*bytes.Buffer); ok {
		if buf.Len() != 0 {
			logger.Warn("stderr 64x64", log.String("stderr", buf.String())) // TODO limit
		}
	}
	if runErr != nil {
		return utils.Wrap(runErr, "generate 64x64 thumbnail")
	}

	logger.Debug("command 128x128", log.String("command", cmd128.String()))
	runErr = cmd128.Run()
	if buf, ok := cmd128.Stderr.(*bytes.Buffer); ok {
		if buf.Len() != 0 {
			logger.Warn("stderr 128x128", log.String("stderr", buf.String())) // TODO limit
		}
	}
	if runErr != nil {
		return utils.Wrap(runErr, "generate 128x128 thumbnail")
	}

	if err := thumbs.CopyThumb(ctx, obj.SHA1, thumbstorage.ThumbnailSize64x64, path64); err != nil {
		return utils.Wrap(err, "copy 64x64 thumbnail")
	}

	if err := thumbs.CopyThumb(ctx, obj.SHA1, thumbstorage.ThumbnailSize128x128, path128); err != nil {
		return utils.Wrap(err, "copy 128x128 thumbnail")
	}

	return nil
}
