package appserver

import (
	"net/http"
)

type ErrorResponse struct {
	err        error
	public     string
	statusCode int
}

func ErrNotFound(public string) *ErrorResponse {
	e := &ErrorResponse{
		public:     public,
		statusCode: http.StatusNotFound,
	}

	if e.public == "" {
		e.public = "Not Found"
	}

	return e
}

func ErrBadRequest(err error, public string) *ErrorResponse {
	e := &ErrorResponse{
		err:        err,
		public:     public,
		statusCode: http.StatusBadRequest,
	}

	if e.public == "" {
		e.public = "Bad Request"
	}

	return e
}

func ErrTooManyRequests(public string) *ErrorResponse {
	e := &ErrorResponse{
		public:     public,
		statusCode: http.StatusTooManyRequests,
	}

	if e.public == "" {
		e.public = "Too Many Requests"
	}

	return e
}

func ErrForbidden(public string) *ErrorResponse {
	e := &ErrorResponse{
		public:     public,
		statusCode: http.StatusForbidden,
	}

	if e.public == "" {
		e.public = "Forbidden"
	}

	return e
}

func ErrMethodNotAllowed() *ErrorResponse {
	return &ErrorResponse{
		public:     "Method Not Allowed",
		statusCode: http.StatusMethodNotAllowed,
	}
}

func ErrConflict(public string) *ErrorResponse {
	e := &ErrorResponse{
		public:     public,
		statusCode: http.StatusConflict,
	}

	if e.public == "" {
		e.public = "Conflict"
	}

	return e
}

func (e *ErrorResponse) Error() string {
	if e.err != nil {
		return e.err.Error()
	}
	return e.public
}
