START TRANSACTION;

---------------------------------------------------------------------------------------------------------------------------------------
--             Table "gallery.object_storage"
--    Column   |  Type   | Collation | Nullable | Default 
-- ------------+---------+-----------+----------+---------
--  object_id  | integer |           | not null | 
--  storage_id | integer |           | not null | 
-- Indexes:
--     "object_storage_pkey" PRIMARY KEY, btree (object_id, storage_id)
-- Foreign-key constraints:
--     "object_storage_object_id_fkey" FOREIGN KEY (object_id) REFERENCES object(id) ON UPDATE CASCADE ON DELETE RESTRICT
--     "object_storage_storage_id_fkey" FOREIGN KEY (storage_id) REFERENCES storage(id) ON UPDATE CASCADE ON DELETE RESTRICT
-- Triggers:
--     refresh_v_storage AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON object_storage FOR EACH STATEMENT EXECUTE FUNCTION refresh_v_storage()
--     refresh_v_user AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON object_storage FOR EACH STATEMENT EXECUTE FUNCTION refresh_v_user()

DELETE FROM
  gallery.object_storage os
USING
  gallery.object o1,
  gallery.object o2
WHERE
  os.object_id = o1.id
  AND (o1.id = o2.thumbnail_64x64 OR o1.id = o2.thumbnail_128x128);

---------------------------------------------------------------------------------------------------------------------------------------
-- CREATE OR REPLACE VIEW gallery.v_object AS
--  SELECT o.id,
--     o.sha1,
--     o.basename,
--     o.size,
--     o.content_type,
--     o.content_subtype,
--     o.content_subtype_params,
--     o.modified_at,
--     o.added_at,
--     o.owner,
--     o.public,
--     o.thumbnail_64x64,
--     o.thumbnail_128x128,
--     COALESCE(o.tags, '{}'::text[]) AS tags
--    FROM object o
--      LEFT JOIN object thumbnail_of ON o.id = thumbnail_of.thumbnail_64x64 OR o.id = thumbnail_of.thumbnail_128x128
--   WHERE NOT o.deleted AND o.owner IS NOT NULL AND thumbnail_of.id IS NULL AND (EXISTS ( SELECT 1
--            FROM object_storage
--           WHERE object_storage.object_id = o.id))

DROP VIEW IF EXISTS gallery.v_object;

CREATE VIEW gallery.v_object AS
  SELECT
    o.id,
    o.sha1,
    o.basename,
    o.size,
    o.content_type,
    o.content_subtype,
    o.content_subtype_params,
    o.modified_at,
    o.added_at,
    o.owner,
    o.public,
    COALESCE(o.tags, '{}'::text[]) AS tags
  FROM gallery.object o
  WHERE
    NOT o.deleted
    AND o.owner IS NOT NULL
    AND EXISTS (
      SELECT 1
      FROM gallery.object_storage
          WHERE object_id = o.id
    );

---------------------------------------------------------------------------------------------------------------------------------------
--                                             Table "gallery.object"
--          Column         |           Type           | Collation | Nullable |              Default               
-- ------------------------+--------------------------+-----------+----------+------------------------------------
--  id                     | integer                  |           | not null | nextval('object_id_seq'::regclass)
--  sha1                   | bytea                    |           | not null | 
--  basename               | text                     |           | not null | 
--  size                   | bigint                   |           | not null | 
--  content_type           | text                     |           | not null | 
--  content_subtype        | text                     |           | not null | 
--  content_subtype_params | text                     |           |          | 
--  modified_at            | timestamp with time zone |           | not null | 
--  added_at               | timestamp with time zone |           | not null | now()
--  owner                  | integer                  |           |          | 
--  public                 | boolean                  |           | not null | false
--  thumbnail_64x64        | integer                  |           |          | 
--  thumbnail_128x128      | integer                  |           |          | 
--  tags                   | text[]                   |           |          | 
--  deleted                | boolean                  |           | not null | false
-- Indexes:
--     "object_pkey" PRIMARY KEY, btree (id)
--     "object_added_at_idx" btree (added_at)
--     "object_basename_idx" btree (basename)
--     "object_content_type_idx" btree (content_type)
--     "object_modified_at_idx" btree (modified_at)
--     "object_owner_idx" btree (owner)
--     "object_sha1_key" UNIQUE CONSTRAINT, btree (sha1)
--     "object_tags_idx" gin (tags)
--     "object_thumbnail_128x128_idx" btree (thumbnail_128x128)
--     "object_thumbnail_64x64_idx" btree (thumbnail_64x64)
-- Check constraints:
--     "object_basename_check" CHECK (btrim(basename) <> ''::text AND length(basename) < 256)
--     "object_content_subtype_check" CHECK (btrim(content_subtype) <> ''::text AND length(content_subtype) <= 64)
--     "object_content_subtype_params_check" CHECK (btrim(content_subtype_params) <> ''::text AND length(content_subtype_params) <= 64)
--     "object_content_type_check" CHECK (btrim(content_type) <> ''::text AND length(content_type) <= 32)
--     "object_sha1_check" CHECK (length(sha1) = 20)
--     "object_size_check" CHECK (size > 0)
-- Foreign-key constraints:
--     "object_owner_fkey" FOREIGN KEY (owner) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE RESTRICT
--     "object_thumbnail_128x128_fkey" FOREIGN KEY (thumbnail_128x128) REFERENCES object(id) ON UPDATE CASCADE ON DELETE SET NULL
--     "object_thumbnail_64x64_fkey" FOREIGN KEY (thumbnail_64x64) REFERENCES object(id) ON UPDATE CASCADE ON DELETE SET NULL
-- Referenced by:
--     TABLE "object_storage" CONSTRAINT "object_storage_object_id_fkey" FOREIGN KEY (object_id) REFERENCES object(id) ON UPDATE CASCADE ON DELETE RESTRICT
--     TABLE "object" CONSTRAINT "object_thumbnail_128x128_fkey" FOREIGN KEY (thumbnail_128x128) REFERENCES object(id) ON UPDATE CASCADE ON DELETE SET NULL
--     TABLE "object" CONSTRAINT "object_thumbnail_64x64_fkey" FOREIGN KEY (thumbnail_64x64) REFERENCES object(id) ON UPDATE CASCADE ON DELETE SET NULL
-- Triggers:
--     refresh_v_storage AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON object FOR EACH STATEMENT EXECUTE FUNCTION refresh_v_storage()
--     refresh_v_user AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON object FOR EACH STATEMENT EXECUTE FUNCTION refresh_v_user()

DELETE FROM
  gallery.object o1
USING
  gallery.object o2
WHERE
  o1.id = o2.thumbnail_64x64 OR o1.id = o2.thumbnail_128x128;

ALTER TABLE gallery.object
  DROP COLUMN thumbnail_64x64,
  DROP COLUMN thumbnail_128x128;

---------------------------------------------------------------------------------------------------------------------------------------

UPDATE gallery.background_job SET opts = opts - 'storage_id' WHERE func = 'generate_thumbnail';

DELETE FROM
  gallery.background_job_trigger trg
USING
  gallery.background_job job
WHERE
  trg.background_job_id = job.id
  AND job.func = 'check_thumbnails';

DELETE FROM gallery.background_job WHERE func = 'check_thumbnails';

COMMIT;
