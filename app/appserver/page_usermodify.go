package appserver

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type UserModifyPage struct {
	Page

	ModifyUser *postgres.UserView
	Next       string
}

func UserModifyPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	// parse
	extID, err := uuid.Parse(ps.ByName("ext_id"))
	if err != nil {
		return nil, ErrNotFound("user not found")
	}

	parser := formparser.NewQueryFormParser(r)

	var (
		next = parser.String("next").Required().Options([]string{"refresh", "reload"})
	)

	if err = parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	// load
	user, err := srv.pg.GetUserViewByExtID(srv.appCtx, extID)
	if err != nil {
		return nil, utils.Wrap(err, "get user view by ext_id")
	}
	if user == nil {
		return nil, ErrNotFound("user not found")
	}
	user.Password = "" // ensure this is not rendered!

	return &UserModifyPage{
		ModifyUser: user,
		Next:       next.Value(),
	}, nil
}

func (page *UserModifyPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return user.ID == page.ModifyUser.ID
}

func (page *UserModifyPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "user_modify_form.html", page, http.StatusOK)
}

func (page *UserModifyPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		displayNameField = parser.String("display_name").Required().Trim().NonEmpty().MaxLength(64)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	page.ModifyUser.DisplayName = displayNameField.Value()

	if err := srv.pg.UpdateUser(srv.appCtx, page.ModifyUser); err != nil {
		srv.handleError(w, err)
		return
	}

	switch page.Next {
	case "refresh":
		srv.renderJSON(w, &JSONResponse{Refresh: true}, http.StatusOK)
	case "reload":
		srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
	default:
		panic(fmt.Errorf("unsupported requested response: '%s'", page.Next))
	}
}
