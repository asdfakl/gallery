package appserver

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
)

type SearchPage struct {
	Page

	srv      *AppServer
	Params   postgres.ObjectViewListSearchParams
	Display  string
	Fragment bool

	Objects     postgres.ObjectViewList
	ObjectCount uint
}

func SearchPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	parser := formparser.NewQueryFormParser(r)

	var (
		search       = parser.String("search").Trim().MaxLength(64)
		display      = parser.String("display").Required().Options([]string{"small_icons", "large_icons", "large_cards"})
		contentTypes = parser.StringSlice("content-type").UniqueElements().ElementsMatchOptions([]string{
			"image",
			"video",
			"audio",
		})
		public   = parser.Bool("public")
		tagging  = parser.Bool("tagging")
		count    = parser.Uint("count").Required().Min(10).Max(120)
		offset   = parser.Uint("offset").Required()
		fragment = parser.Bool("fragment")
		orderBy  = parser.String("order_by").Options([]string{
			"added_at", "!added_at",
			"modified_at", "!modified_at",
			"size", "!size",
		})
	)

	if err := parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	return &SearchPage{
		srv: srv,
		Params: postgres.ObjectViewListSearchParams{
			Search:       search.OptionalValue(),
			ContentTypes: contentTypes.Values(),
			Public:       public.OptionalValue(),
			Tagging:      tagging.OptionalValue(),
			Count:        count.Value(),
			Offset:       offset.Value(),
			OrderBy:      orderBy.OptionalValue(),
		},
		Display:  display.Value(),
		Fragment: fragment.ValueOr(false),
	}, nil
}

func (page *SearchPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	page.Params.RestrictByUserID = user.ID

	return true
}

func (page *SearchPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	objs, err := srv.pg.SearchObjectViewList(srv.appCtx, &page.Params)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	page.Objects = objs

	count, err := srv.pg.CountObjectViewList(srv.appCtx, &page.Params)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	page.ObjectCount = count

	if page.Fragment {
		srv.executePageTemplate(w, "search_fragment.html", page, http.StatusOK)
	} else {
		srv.executePageTemplate(w, "search.html", page, http.StatusOK)
	}
}

func (page *SearchPage) ObjectBrowseLink(index int) *url.URL {
	if index < 0 {
		panic(fmt.Errorf("object browse link index out of range: %d", index))
	}

	return (&BrowsePage{
		srv:    page.srv,
		Params: page.Params,
	}).Link(page.Params.Offset + uint(index))
}
