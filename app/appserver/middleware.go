package appserver

import (
	"context"
	"net/http"
	"path"

	"gitlab.com/asdfakl/gallery/app/redismain"
)

type ContextKeyUser struct{}
type ContextKeySession struct{}

type AuthMiddleware struct {
	srv            *AppServer
	handler        http.Handler
	publicPatterns []string
}

func NewAuthMiddleware(srv *AppServer, handler http.Handler) http.Handler {
	return &AuthMiddleware{
		srv:     srv,
		handler: handler,
		publicPatterns: []string{
			srv.path("/static/favicon.ico"),
			srv.path("/static/bulma/*/bulma.min.css"),
			srv.path("/static/fa/*/css/all.min.css"),
			srv.path("/static/fa/*/webfonts/*"),
			srv.path("/login"),
		},
	}
}

// TODO origin and referer whitelisting
func (mw *AuthMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, pattern := range mw.publicPatterns {
		if match, _ := path.Match(pattern, r.URL.Path); match {
			mw.handler.ServeHTTP(w, r)
			return
		}
	}

	cookie, err := r.Cookie(mw.srv.cfg.SessionCookieName)
	if err != nil {
		mw.srv.renderLoginForm(w, r.URL.RequestURI(), nil)
		return
	}

	key, err := redismain.SessionKeyFromHex(cookie.Value)
	if err != nil {
		mw.srv.renderLoginForm(w, r.URL.RequestURI(), nil)
		return
	}

	session, err := mw.srv.rmc.GetSession(mw.srv.appCtx, key)
	if err != nil {
		mw.srv.handleError(w, err)
		return
	}

	if !session.IsValid() {
		mw.srv.renderLoginForm(w, r.URL.RequestURI(), nil)
		return
	}

	user, err := mw.srv.pg.GetUserViewByID(mw.srv.appCtx, session.UserID)
	if err != nil {
		mw.srv.handleError(w, err)
		return
	}

	if user == nil {
		mw.srv.renderLoginForm(w, r.URL.RequestURI(), nil)
		return
	}

	rr := r.Clone(
		context.WithValue(
			context.WithValue(
				r.Context(),
				ContextKeyUser{},
				user,
			),
			ContextKeySession{},
			session,
		),
	)

	mw.handler.ServeHTTP(w, rr)
}
