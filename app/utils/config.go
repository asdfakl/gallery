package utils

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

var envKeyReplacer = strings.NewReplacer(
	"-", "_",
)

func flagKeyToEnv(key string) string {
	return strings.ToUpper(envKeyReplacer.Replace(key))
}

func loadEnvString(target *string, key string) {
	if val := os.Getenv(key); val != "" {
		*target = val
	}
}

func trimKey(key string) string {
	key = strings.TrimSpace(key)
	if key == "" {
		panic(errors.New("empty key"))
	}
	return key
}

func loadEnvInt(target *int, key string) {
	val := strings.TrimSpace(os.Getenv(key))
	if val == "" {
		return
	}

	i, err := strconv.Atoi(val)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading int value from environment variable '%s': %v\n", key, err)
		return
	}
	*target = i
}

func loadEnvUint(target *uint, key string) {
	val := strings.TrimSpace(os.Getenv(key))
	if val == "" {
		return
	}

	u, err := strconv.ParseUint(val, 10, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading uint value from environment variable '%s': %v\n", key, err)
		return
	}
	*target = uint(u)
}

func loadEnvInt64(target *int64, key string) {
	val := strings.TrimSpace(os.Getenv(key))
	if val == "" {
		return
	}

	i, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading int64 value from environment variable '%s': %v\n", key, err)
		return
	}
	*target = i
}

func loadEnvDuration(target *time.Duration, key string) {
	val := strings.TrimSpace(os.Getenv(key))
	if val == "" {
		return
	}

	d, err := time.ParseDuration(val)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading duration value from environment variable '%s': %v\n", key, err)
		return
	}
	*target = d
}

func loadEnvBool(target *bool, key string) {
	val := strings.TrimSpace(os.Getenv(key))
	if val == "" {
		return
	}

	b, err := strconv.ParseBool(val)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading bool value from environment variable '%s': %v\n", key, err)
		return
	}
	*target = b
}

func StringVarOrEnv(fs *flag.FlagSet, target *string, key, def, desc string) {
	key = trimKey(key)

	// environment variable overwrites default value
	loadEnvString(&def, flagKeyToEnv(key))

	// flag overwrites all
	fs.StringVar(target, key, def, desc)
}

func IntVarOrEnv(fs *flag.FlagSet, target *int, key string, def int, desc string) {
	key = trimKey(key)

	// environment variable overwrites default value
	loadEnvInt(&def, flagKeyToEnv(key))

	// flag overwrites all
	fs.IntVar(target, key, def, desc)
}

func UintVarOrEnv(fs *flag.FlagSet, target *uint, key string, def uint, desc string) {
	key = trimKey(key)

	// environment variable overwrites default value
	loadEnvUint(&def, flagKeyToEnv(key))

	// flag overwrites all
	fs.UintVar(target, key, def, desc)
}

func Int64VarOrEnv(fs *flag.FlagSet, target *int64, key string, def int64, desc string) {
	key = trimKey(key)

	// environment variable overwrites default value
	loadEnvInt64(&def, flagKeyToEnv(key))

	// flag overwrites all
	fs.Int64Var(target, key, def, desc)
}

func DurationVarOrEnv(fs *flag.FlagSet, target *time.Duration, key string, def time.Duration, desc string) {
	key = trimKey(key)

	// environment variable overwrites default value
	loadEnvDuration(&def, flagKeyToEnv(key))

	// flag overwrites all
	fs.DurationVar(target, key, def, desc)
}

func BoolVarOrEnv(fs *flag.FlagSet, target *bool, key string, def bool, desc string) {
	key = trimKey(key)

	// environment variable overwrites default value
	loadEnvBool(&def, flagKeyToEnv(key))

	// flag overwrites all
	fs.BoolVar(target, key, def, desc)
}
