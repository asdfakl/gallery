package appserver

import (
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type AddMasterPasswordPage struct {
	Page
}

func AddMasterPasswordPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &AddMasterPasswordPage{}, nil
}

func (page *AddMasterPasswordPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *AddMasterPasswordPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	srv.executePageTemplate(w, "master_password_add_form.html", page, http.StatusOK)
}

func (page *AddMasterPasswordPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	masterPasswords, err := srv.pg.GetMasterPasswordListByOwner(srv.appCtx, page.User().ID)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get master password list by owner"))
		return
	}
	if len(masterPasswords) >= srv.cfg.Limits.UpperMasterPasswordCount {
		srv.handleError(w, ErrForbidden("Too many master passwords"))
		return
	}

	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		labelField     = parser.String("label").Required().Trim().NonEmpty().MaxLength(64)
		password1Field = parser.String("password_1").Required().Trim().NonEmpty().MaxLength(64)
		password2Field = parser.String("password_2").Required().Trim().NonEmpty().MaxLength(64)
	)

	if err = parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	var (
		label                = labelField.Value()
		password1, password2 = password1Field.Value(), password2Field.Value()
	)

	for _, mp := range masterPasswords {
		if strings.EqualFold(label, mp.Label) {
			srv.handleError(w, ErrConflict("The label already exists, labels must be unique"))
			return
		}
	}

	if password1 != password2 {
		srv.handleError(w, ErrBadRequest(nil, "password mismatch"))
		return
	}

	h, err := bcrypt.GenerateFromPassword([]byte(password1), bcrypt.DefaultCost)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "generate bcrypt hash from password"))
		return
	}

	if err = srv.pg.InsertMasterPassword(srv.appCtx, &postgres.MasterPassword{
		Password: string(h),
		Owner:    page.User().ID,
		Label:    label,
	}); err != nil {
		srv.handleError(w, utils.Wrap(err, "insert master password"))
		return
	}

	srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
}
