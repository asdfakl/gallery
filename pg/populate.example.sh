#!/bin/bash

: "${S3_ENDPOINT:?required}"
: "${S3_REGION:?required}"
: "${S3_BUCKET:?required}"
: "${S3_ACCESS_KEY:?required}"
: "${S3_SECRET_KEY:?required}"
: "${APP_USERNAME:?required}"
: "${APP_PASSWORD_BCRYPT:?required}"

run='docker exec -i -e PGPASSWORD=salakala asdfakl-gallery-postgres psql gallery postgres'

$run <<SQL

START TRANSACTION;

-------------------------------------------------------------------------------------------------------------------
-- truncate

TRUNCATE gallery.object CASCADE;
TRUNCATE gallery.storage RESTART IDENTITY CASCADE;
TRUNCATE gallery.background_job RESTART IDENTITY CASCADE;
TRUNCATE gallery.user RESTART IDENTITY CASCADE;

ALTER SEQUENCE gallery.object_id_seq RESTART;
ALTER SEQUENCE gallery.storage_id_seq RESTART;
ALTER SEQUENCE gallery.background_job_id_seq RESTART;

-------------------------------------------------------------------------------------------------------------------
-- user

INSERT INTO
	gallery.user (username, password, quota_object_count, quota_object_bytes)
VALUES
	('${APP_USERNAME}', '${APP_PASSWORD_BCRYPT}', 100, 50 * 2^20);

-------------------------------------------------------------------------------------------------------------------
-- storage

INSERT INTO
	gallery.storage (type, access_priority, storage_priority, name, description, quota_object_count, quota_object_bytes)
VALUES
	('s3', 10, 0, 'primary_s3', 'Primary S3 object storage.', 1000, 1000 * 2^20),
	('local', 0, 10, 'primary_local', 'Primary local file system storage for thumbnails.', 1000, 10 * 2^20);

UPDATE gallery.storage SET opts = opts || '{
"s3_endpoint": "${S3_ENDPOINT}",
"s3_region": "${S3_REGION}",
"s3_bucket": "${S3_BUCKET}",
"s3_prefix": "storage/sha1",
"s3_access_key": "${S3_ACCESS_KEY}",
"s3_secret_key": "${S3_SECRET_KEY}"
}' WHERE name = 'primary_s3';

UPDATE gallery.storage SET opts = opts || '{
"path": "/var/lib/gallerybg/storage/sha1"
}' WHERE name = 'primary_local';

-------------------------------------------------------------------------------------------------------------------
-- background_job

INSERT INTO
	gallery.background_job (func, opts)
VALUES
	(
		'generate_thumbnail', '{}'
	),
	(
		'sync_storage',
		json_build_object('storage_id', (SELECT id FROM gallery.storage WHERE name = 'primary_s3'))
	),
	(
		'sync_storage',
		json_build_object('storage_id', (SELECT id FROM gallery.storage WHERE name = 'primary_local'))
	),
	(
		'import_objects',
		json_build_object(
			'user_id', (SELECT id FROM gallery.user WHERE username = '${APP_USERNAME}'),
			'upload_dir', CONCAT('/var/tmp/gallerybg/transfer/', (SELECT id FROM gallery.user WHERE username = '${APP_USERNAME}'), '/upload'),
			'thumbnail_queue', 'generate_thumbnail'
		)
	),
	(
		'import_upload',
		json_build_object(
			'upload_dir', '/var/lib/gallerybg/upload',
			'thumbnail_queue', 'generate_thumbnail'
		)
	),
	(
		'clean_uploads',
		json_build_object(
			'upload_dir', '/var/lib/gallerybg/upload'
		)
	);

-------------------------------------------------------------------------------------------------------------------
-- background_job_trigger

INSERT INTO
	gallery.background_job_trigger (key, background_job_id)
VALUES
	('sync_primary_s3', (SELECT id FROM gallery.background_job WHERE func = 'sync_storage' AND (opts->'storage_id')::INTEGER = (SELECT id FROM gallery.storage WHERE name = 'primary_s3'))),
	('sync_primary_local', (SELECT id FROM gallery.background_job WHERE func = 'sync_storage' AND (opts->'storage_id')::INTEGER = (SELECT id FROM gallery.storage WHERE name = 'primary_local'))),
	('import_${APP_USERNAME}', (SELECT id FROM gallery.background_job WHERE func = 'import_objects')),
	('clean_uploads', (SELECT id FROM gallery.background_job WHERE func = 'clean_uploads'));

-------------------------------------------------------------------------------------------------------------------
-- background_job_queue

INSERT INTO
	gallery.background_job_queue (key, background_job_id, worker_count)
VALUES
	('generate_thumbnail', (SELECT id FROM gallery.background_job WHERE func = 'generate_thumbnail'), 5),
	('import_upload', (SELECT id FROM gallery.background_job WHERE func = 'import_upload'), 2);


COMMIT;

SQL
