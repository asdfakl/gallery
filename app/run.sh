#!/bin/bash

usage() {
	cat >&2 <<USAGE
Usage: ./run.sh [TARGET]

Targets: bg app
USAGE
	exit 1
}

die() {
	echo "$@" >&2
	exit 2
}

test $# -eq 1 || usage

base="$(dirname "$(realpath "$0")")"

cd "$base" || die "cd"

case "$1" in
"bg")
	transfer_dir=/tmp/asdfakl/gallery/transfer

	echo -e '\033[01;37mcreating transfer directory' $transfer_dir '\033[00m'
	mkdir -p "$transfer_dir" || die "mkdir"
	echo -e '\033[01;37msetting permissions for' $transfer_dir '\033[00m'
	chmod 0750 "$transfer_dir" || die "chmod"

	echo -e '\033[01;37mrunning target bg\033[00m'
	docker run --rm -t \
		--net asdfakl-gallery \
		--env-file ../dev/env \
		--name asdfakl-gallery-bg \
		--mount type=volume,src=asdfakl-gallery-bg-data,dst=/var/lib/gallerybg \
		--mount "type=bind,src=$transfer_dir,dst=/var/tmp/gallerybg/transfer" \
		--log-opt mode=non-blocking \
		--log-opt max-buffer-size=2m \
		local/gallery-bg:development
	;;
"app")
	echo -e '\033[01;37mrunning target app\033[00m'
	docker run --rm -t \
		-p 127.0.0.1:3000:8080 \
		-p 127.0.0.1:3001:8081 \
		--net asdfakl-gallery \
		--env-file ../dev/env \
		--env APP_SERVER_STATIC_DIRECTORY=/opt/galleryapp/static \
		--env APP_SERVER_TEMPLATE_DIRECTORY=/opt/galleryapp/tpl \
		--name asdfakl-gallery-app \
		--mount type=volume,src=asdfakl-gallery-bg-data,dst=/var/lib/gallerybg \
		--mount "type=bind,src=$(pwd)/appserver/static,dst=/opt/galleryapp/static,readonly" \
		--mount "type=bind,src=$(pwd)/appserver/tpl,dst=/opt/galleryapp/tpl,readonly" \
		--log-opt mode=non-blocking \
		--log-opt max-buffer-size=2m \
		local/gallery-app:development
	;;
*)
	usage
	;;
esac
