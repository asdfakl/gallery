package queuelistener

import (
	"context"
	"errors"
	"sync"
	"time"

	"gitlab.com/asdfakl/gallery/app/bg"
	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
)

type Listener struct {
	pg       *postgres.PostgresConnection
	rmc      *redismain.RedisMainConnection
	thumbs   thumbstorage.Storage
	queue    *postgres.BackgroundJobQueue
	executor *bg.Executor
	logger   *log.Logger

	once sync.Once
	stop context.CancelFunc
}

func New(
	pg *postgres.PostgresConnection,
	rmc *redismain.RedisMainConnection,
	thumbs thumbstorage.Storage,
	queue *postgres.BackgroundJobQueue,
	executor *bg.Executor,
	logger *log.Logger,
) *Listener {

	return &Listener{
		pg:       pg,
		rmc:      rmc,
		thumbs:   thumbs,
		queue:    queue,
		executor: executor,
		logger:   logger.With(log.String("queue", queue.Key)),
	}
}

func (listener *Listener) Start(ctx context.Context, donec chan<- string) {
	listener.once.Do(func() {
		runctx, cancel := context.WithCancel(ctx)
		listener.stop = cancel

		go listener.run(runctx, donec)
	})
}

func (listener *Listener) Stop() {
	listener.stop() // panics if called without first calling Start
}

func (listener *Listener) run(ctx context.Context, donec chan<- string) {
	defer func() {
		listener.logger.System("stop")

		donec <- listener.queue.Key
	}()

	listener.logger.System("run", log.Int("worker_count", listener.queue.WorkerCount))

	throttle := make(chan struct{}, listener.queue.WorkerCount)
	wg := sync.WaitGroup{}
	defer wg.Wait()

	for {
		select {
		case <-ctx.Done():
			return

		default:
			job, err := listener.rmc.PopBackgroundQueueItem(ctx, listener.queue.Key, 5*time.Minute)
			if err != nil {
				if !errors.Is(err, context.Canceled) {
					listener.logger.Error("pop background queue item", log.Err(err))
				}
				return
			}
			if job == "" {
				break
			}

			// NOTE: any pending jobs are silently dropped when ctx is closed
			throttle <- struct{}{}
			wg.Add(1)
			go listener.execute(ctx, &wg, job, throttle)
		}
	}
}

func (listener *Listener) execute(ctx context.Context, wg *sync.WaitGroup, job string, throttle <-chan struct{}) {
	defer func() {
		<-throttle
		wg.Done()
	}()

	// abort if context was closed while waiting for throttle
	if err := ctx.Err(); err != nil {
		return
	}

	opts := config.Options{}

	for k, v := range listener.queue.Opts {
		opts[k] = v
	}
	opts["job"] = job

	listener.executor.Execute(ctx, listener.queue.Func, listener.pg, listener.rmc, listener.thumbs, opts, nil)
}
