package jwt

import (
	"testing"
)

func TestNewJWTHS256(t *testing.T) {
	for i, tc := range []struct {
		payload       JWTPayload
		secret        string
		expectedToken string
	}{
		{
			JWTPayload{
				Issuer:  "me",
				Expiry:  1929839200,
				Subject: "that guy over there",
			},
			"salakala",
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.ekweGH8wsJnzqUK3bhAYR5dx-N8MruyP2FsPfoYrFFw",
		},
		{
			JWTPayload{
				Issuer:  "me",
				Expiry:  1929839200,
				Subject: "that guy over there",
			},
			"kirokala",
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.KAdAbO6Bdl9xW886E6wl0EPCIHdOe1ScIRjL9oZlGBM",
		},
		{
			JWTPayload{
				Issuer:  "you",
				Expiry:  1829329100,
				Subject: "me",
			},
			"salakala",
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ5b3UiLCJleHAiOjE4MjkzMjkxMDAsInN1YiI6Im1lIn0.R8H6AWoikauDqM0BbALnrBFEfzgVHOmKk8DPtR8Szg0",
		},
		{
			JWTPayload{
				Issuer:  "you",
				Expiry:  1829329100,
				Subject: "me",
			},
			"kirokala",
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ5b3UiLCJleHAiOjE4MjkzMjkxMDAsInN1YiI6Im1lIn0.v_ciZyWwUYQj7tG_ItKXTOW7ZwafqtqsSVgE7EZaOjo",
		},
	} {
		token := NewJWTHS256(&tc.payload, []byte(tc.secret))

		if string(token) != tc.expectedToken {
			t.Errorf("test case %d: expected '%s', got '%s'", i, tc.expectedToken, string(token))
		}
	}
}

func TestVerifyJWTToken(t *testing.T) {
	for i, tc := range []struct {
		token           string
		secret          string
		expectedResult  bool
		expectedPayload *JWTPayload
	}{
		{
			expectedResult: false,
		},

		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.ekweGH8wsJnzqUK3bhAYR5dx-N8MruyP2FsPfoYrFFw",
			secret:         "salakala",
			expectedResult: true,
			expectedPayload: &JWTPayload{
				Issuer:  "me",
				Expiry:  1929839200,
				Subject: "that guy over there",
			},
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.ekweGH8wsJnzqUK3bhAYR5dx-N8MruyP2FsPfoYrFFw",
			secret:         "salakalax",
			expectedResult: false,
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXvCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.ekweGH8wsJnzqUK3bhAYR5dx-N8MruyP2FsPfoYrFFw",
			secret:         "salakala",
			expectedResult: false,
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyjpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.ekweGH8wsJnzqUK3bhAYR5dx-N8MruyP2FsPfoYrFFw",
			secret:         "salakala",
			expectedResult: false,
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.ekweGH8wsJnzqUK3bhAYR5dx-N8MruyP2FsPfoYrFFW",
			secret:         "salakala",
			expectedResult: false,
		},

		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.KAdAbO6Bdl9xW886E6wl0EPCIHdOe1ScIRjL9oZlGBM",
			secret:         "kirokala",
			expectedResult: true,
			expectedPayload: &JWTPayload{
				Issuer:  "me",
				Expiry:  1929839200,
				Subject: "that guy over there",
			},
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.KAdAbO6Bdl9xW886E6wl0EPCIHdOe1ScIRjL9oZlGBM",
			secret:         " ",
			expectedResult: false,
		},
		{
			token:          "eyJhbGciOi?IUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.KAdAbO6Bdl9xW886E6wl0EPCIHdOe1ScIRjL9oZlGBM",
			secret:         "kirokala",
			expectedResult: false,
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..KAdAbO6Bdl9xW886E6wl0EPCIHdOe1ScIRjL9oZlGBM",
			secret:         "kirokala",
			expectedResult: false,
		},
		{
			token:          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJtZSIsImV4cCI6MTkyOTgzOTIwMCwic3ViIjoidGhhdCBndXkgb3ZlciB0aGVyZSJ9.KadAbO6Bdl9xW886E6wl0EPCIHdOe1ScIRjL9oZlGBM",
			secret:         "kirokala",
			expectedResult: false,
		},
	} {
		payload := JWTPayload{}

		if result := VerifyJWTToken([]byte(tc.token), []byte(tc.secret), &payload); result != tc.expectedResult {
			t.Errorf("test case %d: expected result %t, got %t", i, tc.expectedResult, result)
		}
		if tc.expectedPayload != nil {
			if payload.Issuer != tc.expectedPayload.Issuer {
				t.Errorf("test case %d: issuer mismatch, expected '%s', got '%s'", i, tc.expectedPayload.Issuer, payload.Issuer)
			}
			if payload.Expiry != tc.expectedPayload.Expiry {
				t.Errorf("test case %d: expiry mismatch, expected %d, got %d", i, tc.expectedPayload.Expiry, payload.Expiry)
			}
			if payload.Subject != tc.expectedPayload.Subject {
				t.Errorf("test case %d: subject mismatch, expected '%s', got '%s'", i, tc.expectedPayload.Subject, payload.Subject)
			}
		}
	}
}
