package apiserver

import (
	"flag"
	"fmt"
	"path"
	"strings"
	"time"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type Configuration struct {
	ListenAddr string
	StartTime  time.Time

	IdleTimeout       time.Duration
	MaxHeaderBytes    int
	ReadHeaderTimeout time.Duration
	ReadTimeout       time.Duration
	WriteTimeout      time.Duration

	Origin             string
	RootPath           string
	Secret             string
	TokenTTL           time.Duration
	TrustXForwardedFor bool
	TrustXRealIP       bool
	UploadDir          string
}

func DefaultConfiguration() *Configuration {
	return &Configuration{
		ListenAddr: "127.0.0.1:8081",
		StartTime:  time.Now(),

		IdleTimeout:       1 * time.Minute,
		MaxHeaderBytes:    2 * 1024,
		ReadHeaderTimeout: 1 * time.Second,
		ReadTimeout:       2 * time.Minute,
		WriteTimeout:      5 * time.Minute,

		Origin:             "http://127.0.0.1:8081",
		RootPath:           "/",
		TrustXForwardedFor: false,
		TrustXRealIP:       false,
		UploadDir:          "/var/lib/gallerybg/upload",
	}
}

func (cfg *Configuration) InitFlagSet(fs *flag.FlagSet) {
	utils.StringVarOrEnv(fs, &cfg.ListenAddr, cfg.option("http-listen-addr"), cfg.ListenAddr, "http listen address, host:port")

	utils.DurationVarOrEnv(fs, &cfg.IdleTimeout, cfg.option("http-idle-timeout"), cfg.IdleTimeout, "maximum duration to wait for the next request when keep-alives are enabled")
	utils.IntVarOrEnv(fs, &cfg.MaxHeaderBytes, cfg.option("http-max-header-bytes"), cfg.MaxHeaderBytes, "maximum amount of http header bytes read")
	utils.DurationVarOrEnv(fs, &cfg.ReadHeaderTimeout, cfg.option("http-read-header-timeout"), cfg.ReadHeaderTimeout, "maximum duration for reading the headers")
	utils.DurationVarOrEnv(fs, &cfg.ReadTimeout, cfg.option("http-read-timeout"), cfg.ReadTimeout, "maximum duration for reading the entire request, including body")
	utils.DurationVarOrEnv(fs, &cfg.WriteTimeout, cfg.option("http-write-timeout"), cfg.WriteTimeout, "maximum duration before timing out response writes")

	utils.StringVarOrEnv(fs, &cfg.Origin, cfg.option("origin"), cfg.Origin, "external origin, scheme://host:port")
	utils.StringVarOrEnv(fs, &cfg.RootPath, cfg.option("root-path"), cfg.RootPath, "http root path")
	utils.StringVarOrEnv(fs, &cfg.Secret, cfg.option("secret"), cfg.Secret, "token issuer secret")
	utils.DurationVarOrEnv(fs, &cfg.TokenTTL, cfg.option("token-ttl"), cfg.TokenTTL, "token ttl (time to live)")
	utils.BoolVarOrEnv(fs, &cfg.TrustXForwardedFor, cfg.option("trust-x-forwarded-for"), cfg.TrustXForwardedFor, "trust X-Forwarded-For header (from reverse proxy)")
	utils.BoolVarOrEnv(fs, &cfg.TrustXRealIP, cfg.option("trust-x-real-ip"), cfg.TrustXRealIP, "trust X-Real-IP header (from reverse proxy)")
	utils.StringVarOrEnv(fs, &cfg.UploadDir, cfg.option("upload-dir"), cfg.UploadDir, "directory where client uploads are stored")
}

func (cfg *Configuration) Trim() {
	for _, str := range []*string{
		&cfg.ListenAddr,
		&cfg.Origin,
		&cfg.Secret,
		&cfg.UploadDir,
	} {
		*str = strings.TrimSpace(*str)
	}

	cfg.RootPath = path.Clean(cfg.RootPath)
}

func (cfg *Configuration) Validate() error {
	for _, assert := range []struct {
		ok  bool
		err error
	}{
		{cfg.ListenAddr != "", fmt.Errorf("%s empty or undefined", cfg.option("http-listen-addr"))},

		{cfg.IdleTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-idle-timeout"))},
		{cfg.MaxHeaderBytes > 0, fmt.Errorf("%s must be a positive integer", cfg.option("http-max-header-bytes"))},
		{cfg.ReadHeaderTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-read-header-timeout"))},
		{cfg.ReadTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-read-timeout"))},
		{cfg.WriteTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-write-timeout"))},

		{cfg.Origin != "", fmt.Errorf("%s empty or undefined", cfg.option("origin"))},
		{path.IsAbs(cfg.RootPath), fmt.Errorf("%s must be an absolute path", cfg.option("root-path"))},
		{cfg.Secret != "", fmt.Errorf("%s empty or undefined", cfg.option("secret"))},
		{cfg.TokenTTL >= time.Minute, fmt.Errorf("%s must be greater than 1m", cfg.option("token-ttl"))},
		{cfg.UploadDir != "", fmt.Errorf("%s empty or undefined", cfg.option("upload-dir"))},
	} {
		if !assert.ok {
			return assert.err
		}
	}

	return nil
}

func (cfg *Configuration) option(opt string) string {
	return "api-server-" + opt
}
