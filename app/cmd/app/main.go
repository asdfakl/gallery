package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"gitlab.com/asdfakl/gallery/app/apiserver"
	"gitlab.com/asdfakl/gallery/app/appserver"
	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redislru"
	"gitlab.com/asdfakl/gallery/app/redismain"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

func main() {
	startTime := time.Now()

	logger := log.NewLogger()
	if config.Version != "development" {
		logger = logger.With(log.String("version", config.Version))
	}

	cfg, err := loadConfiguration(logger)

	utils.Must(err, "load configuration")

	connCtx, cancelConnCtx := context.WithTimeout(context.Background(), 5*time.Second)

	pg, err := postgres.New(connCtx, &cfg.Postgres)
	utils.Must(err, "connect to postgres database")
	logger.System("connection established to postgres database")

	rmc, err := redismain.New(connCtx, &cfg.RedisMain, logger)
	utils.Must(err, "connect to main redis database")
	logger.System("connection established to main redis database")

	var rlc *redislru.RedisLRUConnection
	if err = cfg.RedisLRU.Validate(); err != nil {
		logger.System("lru redis disabled")
		logger.Debug("lru redis configuration validation", log.Err(err))
	} else {
		rlc, err = redislru.New(connCtx, &cfg.RedisLRU)
		utils.Must(err, "connect to lru redis database")
		logger.System("connection established to lru redis database")
	}

	thumbs, err := thumbstorage.NewLocalStorage(&cfg.Thumbs, rlc, logger)
	utils.Must(err, "initialize thumb storage")

	cancelConnCtx()

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	sigc := make(chan os.Signal, 1)

	appServer, err := appserver.New(&cfg.AppServer, logger, pg, rmc, thumbs)
	utils.Must(err, "initialize app server")

	apiServer, err := apiserver.New(&cfg.APIServer, logger, pg, rmc)
	utils.Must(err, "initialize api server")

	appServer.Start(appCtx, wg)
	apiServer.Start(appCtx, wg)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	sig := <-sigc
	logger.System("got signal", log.String("signal", sig.String()))
	cancelAppCtx()

	wg.Wait()

	logger.System("graceful shutdown", log.Duration("uptime", time.Since(startTime)))
}
