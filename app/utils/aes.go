package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
)

func EncryptAES256(plaintext []byte, passphrase string) (ciphertext, nonce []byte, err error) {
	key := SHA256Sum([]byte(passphrase))

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, nil, fmt.Errorf("new aes block cipher: %w", err)
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, nil, fmt.Errorf("new gcm block cipher: %w", err)
	}

	nonce = make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		return nil, nil, fmt.Errorf("rand read: %w", err)
	}

	ciphertext = gcm.Seal(plaintext[:0], nonce, plaintext, nil)

	return
}

func DecryptAES256(ciphertext, nonce []byte, passphrase string) (plaintext []byte, err error) {
	key := SHA256Sum([]byte(passphrase))

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("new aes block cipher: %w", err)
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("new gcm block cipher: %w", err)
	}

	plaintext, err = gcm.Open(ciphertext[:0], nonce, ciphertext, nil)

	return
}

func SHA256Sum(b []byte) []byte {
	h := sha256.New()

	h.Write(b)

	return h.Sum(nil)
}
