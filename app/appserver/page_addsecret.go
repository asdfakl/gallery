package appserver

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type AddSecretPage struct {
	Page

	MasterPasswords postgres.MasterPasswordList
}

func AddSecretPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &AddSecretPage{}, nil
}

func (page *AddSecretPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *AddSecretPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	var err error

	page.MasterPasswords, err = srv.pg.GetMasterPasswordListByOwner(srv.appCtx, page.User().ID)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get master password list by owner"))
		return
	}

	if len(page.MasterPasswords) == 0 {
		srv.handleError(w, ErrConflict("No master passwords, create a master password first"))
		return
	}

	srv.executePageTemplate(w, "secret_add_form.html", page, http.StatusOK)
}

func (page *AddSecretPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		passwordIDField = parser.UUID("password_id").Required()
		passwordField   = parser.String("password").Required().NonEmpty().MaxLength(64)
		labelField      = parser.String("label").Required().NonEmpty().MaxLength(128)
		secretField     = parser.String("secret").Required().NonEmpty().MaxLength(2048)
	)

	if err := parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	mp, err := srv.pg.GetMasterPasswordByExtID(srv.appCtx, passwordIDField.Value())
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get master password by ext_id"))
		return
	}
	if mp == nil {
		srv.handleError(w, ErrNotFound("master password not found"))
		return
	}
	if mp.Owner != page.User().ID {
		srv.handleError(w, ErrForbidden("master password access denied"))
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(mp.Password), []byte(passwordField.Value())); err != nil {
		srv.handleError(w, ErrForbidden("password mismatch"))
		return
	}

	ciphertext, nonce, err := utils.EncryptAES256([]byte(secretField.Value()), passwordField.Value())
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "encrypt aes256"))
		return
	}

	secret := &postgres.Secret{
		MasterPasswordID: mp.ID,
		Label:            labelField.Value(),
	}

	if err = srv.pg.InsertSecret(srv.appCtx, secret, ciphertext, nonce); err != nil {
		srv.handleError(w, utils.Wrap(err, "insert secret"))
		return
	}

	srv.renderJSON(w, &JSONResponse{Reload: true}, http.StatusOK)
}
