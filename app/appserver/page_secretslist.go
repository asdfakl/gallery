package appserver

import (
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type SecretsListPage struct {
	Page

	Search                        string
	MasterPasswords               postgres.MasterPasswordList
	Secrets                       postgres.SecretList
	SecretCountByMasterPasswordID map[int64]int
}

func SecretsListPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	parser := formparser.NewQueryFormParser(r)

	var (
		search = parser.String("search").Trim().MaxLength(128)
	)

	if err := parser.Err(); err != nil {
		return nil, ErrBadRequest(err, err.Error())
	}

	return &SecretsListPage{
		Search: search.ValueOr(""),
	}, nil
}

func (page *SecretsListPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *SecretsListPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	passwords, err := srv.pg.GetMasterPasswordListByOwner(srv.appCtx, page.User().ID)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get master password list by owner"))
		return
	}
	page.MasterPasswords = passwords

	secrets, err := srv.pg.GetSecretListByOwner(srv.appCtx, page.User().ID)
	if err != nil {
		srv.handleError(w, utils.Wrap(err, "get secret list by owner"))
		return
	}
	page.Secrets = secrets
	page.SecretCountByMasterPasswordID = secrets.CountByMasterPasswordID()

	srv.executePageTemplate(w, "secrets_list.html", page, http.StatusOK)
}

func (page *SecretsListPage) FilteredSecrets() (filtered postgres.SecretList) {
	if page.Search == "" {
		return page.Secrets
	}

	search := strings.ToLower(page.Search)

	for _, secret := range page.Secrets {
		label := strings.ToLower(secret.Label)

		if strings.Contains(label, search) {
			filtered = append(filtered, secret)
		}
	}

	return filtered
}
