package appserver

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"path/filepath"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/thumbstorage"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	thumbnailCacheControlHit  = "private, max-age=2592000, immutable" // one month
	thumbnailCacheControlMiss = "private, max-age=300, immutable"     // five minutes
)

type ThumbnailPage struct {
	Page

	Size   thumbstorage.ThumbnailSize
	Object *postgres.ObjectView
}

func ThumbnailPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	sum, err := utils.ParseHexEncodedSHA1String(ps.ByName("sha1"))
	if err != nil {
		return nil, ErrBadRequest(err, "invalid hex-encoded sha1 sum")
	}

	obj, err := srv.pg.GetObjectViewBySHA1(srv.appCtx, sum)
	if err != nil {
		return nil, err
	}
	if obj == nil {
		return nil, ErrNotFound(fmt.Sprintf("object not found by sha1 sum %s", utils.HexEncodeSHA1(sum)))
	}

	page := &ThumbnailPage{
		Object: obj,
		Size:   thumbstorage.ThumbnailSize(ps.ByName("size")),
	}

	if !thumbstorage.IsValidThumbnailSize(page.Size) {
		return nil, ErrBadRequest(nil, fmt.Sprintf("unsupported thumbnail size: %q", page.Size))
	}

	return page, nil
}

func (page *ThumbnailPage) CanAccess(user *postgres.UserView) bool {
	if !user.IsValid() {
		return false
	}

	return page.Object.Public || user.ID == page.Object.Owner
}

func (page *ThumbnailPage) HandleGET(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	rdr, err := srv.thumbs.ReadThumb(srv.appCtx, page.Object.SHA1, page.Size)
	if err != nil {
		if errors.Is(err, thumbstorage.ErrNotExist) {
			page.defaultThumbnail(srv, w, r)
			return
		}
		srv.handleError(w, utils.Wrap(err, "read thumbnail"))
		return
	}
	defer rdr.Close()

	w.Header().Set("Content-Type", rdr.MIME().String())
	w.Header().Set("Cache-Control", thumbnailCacheControlHit)

	http.ServeContent(w, r, "", time.Time{}, rdr)
}

func (page *ThumbnailPage) defaultThumbnail(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	var resource string

	switch page.Object.ContentType {
	case "audio":
		resource = fmt.Sprintf("static/img/thumbnails/wave-%s.png", page.Size)
	case "video":
		resource = fmt.Sprintf("static/img/thumbnails/video-%s.png", page.Size)
	default:
		resource = fmt.Sprintf("static/img/thumbnails/default-%s.png", page.Size)
	}

	f, err := staticFS.Open(resource)
	if err != nil {
		srv.handleError(w, err)
		return
	}
	defer f.Close()

	b := &bytes.Buffer{}
	_, err = io.Copy(b, f)
	if err != nil {
		srv.handleError(w, err)
		return
	}

	w.Header().Set("Cache-Control", thumbnailCacheControlMiss)
	http.ServeContent(w, r, filepath.Base(resource), time.Time{}, bytes.NewReader(b.Bytes()))

	// request thumbnail generation
	if page.Object.MatchContentTypes(config.SupportedThumbnailContentTypes()) {
		queue, err := srv.cachedBackgroundJobQueueByFunc(postgres.BG_JOB_FN_KEY_GENERATE_THUMBNAIL, 15*time.Minute)
		if err != nil {
			srv.logger.Error("query cached bg job queue by func", log.Err(err))
			return
		}
		if queue == nil {
			return
		}

		if err = srv.rmc.PushBackgroundQueueItems(srv.appCtx, queue.Key, page.Object.HexSHA1()); err != nil {
			srv.logger.Error("push bg queue items", log.Err(err))
			return
		}

		srv.logger.Debug("request thumbnail generation", log.SHA1("object_sha1", page.Object.SHA1))
	}
}
