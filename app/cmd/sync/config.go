package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"

	"github.com/BurntSushi/toml"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/utils"
)

var ErrUnrecognizedAnswer = errors.New("unrecognized answer")

type URL struct {
	Scheme string `toml:"scheme"`
	Host   string `toml:"host"`
	Path   string `toml:"path,omitempty"`
}

type Concurrency struct {
	ReadWorkerCount   int `toml:"read_workers"`
	UploadWorkerCount int `toml:"upload_workers"`
}

type Configuration struct {
	Targets           []string `toml:"-"`
	ConfigurationFile string   `toml:"-"`
	Tags              []string `toml:"-"`
	Password          string   `toml:"-"`

	APIURL      *URL        `toml:"api_url"`
	Username    string      `toml:"username"`
	Publish     *bool       `toml:"publish,omitempty"`
	Color       *bool       `toml:"color,omitempty"`
	Hidden      *bool       `toml:"hidden,omitempty"`
	Concurrency Concurrency `toml:"concurrency"`
	BlockSize   int64       `toml:"block_size"`

	Metrics Metrics

	printMutex sync.Mutex
}

func (u *URL) String() string {
	return (&url.URL{
		Scheme: u.Scheme,
		Host:   u.Host,
		Path:   u.Path,
	}).String()
}

func (cfg *Configuration) IncludeHidden() bool {
	if cfg.Hidden == nil {
		return false
	}
	return *cfg.Hidden
}

func loadConfiguration() (*Configuration, error) {
	cfg, err := loadConfigurationFromFlags()
	if err != nil {
		return nil, fmt.Errorf("load command-line configuration: %w", err)
	}
	trimConfiguration(cfg)

	if cfg.ConfigurationFile != "" {
		fromFile, err := loadConfigurationFromFile(cfg.ConfigurationFile)
		if err != nil {
			return nil, fmt.Errorf("load configuration file %s: %w", cfg.ConfigurationFile, err)
		}
		trimConfiguration(fromFile)

		cfg = mergeConfigurations(cfg, fromFile)
	}

	if cfg.Password == "" {
		cfg.Password, err = ask(fmt.Sprintf("Password for user %s:", cfg.Username))
		if err != nil {
			return nil, fmt.Errorf("ask password: %w", err)
		}
	}

	if err := validateConfiguration(cfg); err != nil {
		return nil, fmt.Errorf("validate configuration: %w", err)
	}

	return cfg, nil
}

func defaultConfiguration() *Configuration {
	cfg := &Configuration{}

	if cfgDir, err := os.UserConfigDir(); err == nil {
		cfg.ConfigurationFile = filepath.Join(cfgDir, "asdfakl", "gallery", "config.toml")
	}

	return cfg
}

func trimConfiguration(cfg *Configuration) {
	cfg.Username = strings.TrimSpace(cfg.Username)

	var tags []string
	for _, tag := range cfg.Tags {
		tag = strings.TrimSpace(tag)
		if tag != "" {
			tags = append(tags, tag)
		}
	}
	cfg.Tags = tags
}

func mergeConfigurations(primary, secondary *Configuration) *Configuration {
	cfg := &Configuration{
		Targets:           primary.Targets,
		ConfigurationFile: primary.ConfigurationFile,
		Tags:              primary.Tags,
		Password:          primary.Password,

		APIURL:      primary.APIURL,
		Username:    primary.Username,
		Publish:     primary.Publish,
		Color:       primary.Color,
		Hidden:      primary.Hidden,
		Concurrency: primary.Concurrency,
		BlockSize:   primary.BlockSize,
	}

	if len(cfg.Targets) == 0 {
		cfg.Targets = secondary.Targets
	}

	if cfg.ConfigurationFile == "" {
		cfg.ConfigurationFile = secondary.ConfigurationFile
	}

	if len(cfg.Tags) == 0 {
		cfg.Tags = secondary.Tags
	}

	if cfg.Password == "" {
		cfg.Password = secondary.Password
	}

	if cfg.APIURL == nil {
		cfg.APIURL = secondary.APIURL
	}

	if cfg.Username == "" {
		cfg.Username = secondary.Username
	}

	if cfg.Publish == nil {
		cfg.Publish = secondary.Publish
	}

	if cfg.Color == nil {
		cfg.Color = secondary.Color
	}

	if cfg.Hidden == nil {
		cfg.Hidden = secondary.Hidden
	}

	if cfg.Concurrency.ReadWorkerCount <= 0 {
		cfg.Concurrency.ReadWorkerCount = secondary.Concurrency.ReadWorkerCount
	}

	if cfg.Concurrency.UploadWorkerCount <= 0 {
		cfg.Concurrency.UploadWorkerCount = secondary.Concurrency.UploadWorkerCount
	}

	if cfg.BlockSize <= 0 {
		cfg.BlockSize = secondary.BlockSize
	}

	return cfg
}

func validateConfiguration(cfg *Configuration) error {
	for _, assert := range []struct {
		ok  func() bool
		err error
	}{
		{
			ok:  func() bool { return len(cfg.Targets) != 0 },
			err: errors.New("at least one target required, got none"),
		},
		{
			ok:  func() bool { return cfg.Password != "" },
			err: errors.New("required: password"),
		},
		{
			ok:  func() bool { return cfg.APIURL != nil },
			err: errors.New("required: api url"),
		},
		{
			ok:  func() bool { return cfg.APIURL.Scheme != "" },
			err: errors.New("required: api url scheme"),
		},
		{
			ok:  func() bool { return cfg.APIURL.Host != "" },
			err: errors.New("required: api url host"),
		},
		{
			ok:  func() bool { return cfg.Username != "" },
			err: errors.New("required: username"),
		},
		{
			ok:  func() bool { return cfg.Publish != nil },
			err: errors.New("publish must be defined"),
		},
		{
			ok:  func() bool { return cfg.Color != nil },
			err: errors.New("color must be defined"),
		},
		{
			ok:  func() bool { return cfg.Concurrency.ReadWorkerCount > 0 },
			err: errors.New("read worker count must be positive"),
		},
		{
			ok:  func() bool { return cfg.Concurrency.UploadWorkerCount > 0 },
			err: errors.New("upload worker count must be positive"),
		},
		{
			ok:  func() bool { return cfg.BlockSize > 0 && cfg.BlockSize <= 1024*1024*1024 },
			err: errors.New("block size must be positive and less than or equal to 1 GiB"),
		},
	} {
		if !assert.ok() {
			return assert.err
		}
	}

	return nil
}

func loadConfigurationFromFlags() (*Configuration, error) {
	cfg := defaultConfiguration()

	fs := flag.NewFlagSet("", flag.ContinueOnError)
	fs.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: sync [OPTIONS] [TARGET FILE/DIRECTORY...]\n")

		fs.PrintDefaults()
	}

	var (
		version bool
		tags    string
		apiURL  string
		publish string
		color   string
		hidden  string
	)

	fs.StringVar(&cfg.ConfigurationFile, "c", cfg.ConfigurationFile, "Path to configuration file, leave empty to skip loading")
	fs.StringVar(&tags, "tags", "", "Whitespace-separated tag list")
	fs.StringVar(&cfg.Password, "p", "", "Gallery password")
	fs.BoolVar(&version, "version", false, "Print version and exit")
	fs.StringVar(&apiURL, "api", "", "URL of a gallery API server, scheme://host[:port][/path]")
	fs.StringVar(&cfg.Username, "u", "", "Gallery username")
	fs.StringVar(&publish, "publish", "", "Publish all uploads, yes/no")
	fs.StringVar(&color, "color", "", "Colorized output, yes/no")
	fs.StringVar(&hidden, "hidden", "", "Include hidden files and contents of hidden directories, yes/no")
	fs.IntVar(&cfg.Concurrency.ReadWorkerCount, "read-workers", cfg.Concurrency.ReadWorkerCount, "Concurrent read count")
	fs.IntVar(&cfg.Concurrency.UploadWorkerCount, "upload-workers", cfg.Concurrency.UploadWorkerCount, "Concurrent upload count")
	fs.Int64Var(&cfg.BlockSize, "block-size", cfg.BlockSize, "Upload block size in bytes")

	if err := fs.Parse(os.Args[1:]); err != nil {
		return nil, fmt.Errorf("parse options: %w", err)
	}

	if version {
		fmt.Println(config.Version)
		os.Exit(0)
	}

	args := fs.Args()
	if len(args) == 0 {
		fs.Usage()

		return nil, fmt.Errorf("expecting at least one target argument, got none")
	}
	cfg.Targets = args

	cfg.Tags = strings.Split(tags, " ")

	if parsed, err := parseURL(apiURL); err == nil {
		cfg.APIURL = parsed
	}

	cfg.Publish = parseBool(publish, cfg.Publish)
	cfg.Color = parseBool(color, cfg.Color)
	cfg.Hidden = parseBool(hidden, cfg.Hidden)

	return cfg, nil
}

func loadConfigurationFromFile(fpath string) (*Configuration, error) {
	f, err := os.Open(fpath)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return setup(fpath)
		}
		return nil, err
	}
	defer f.Close()

	cfg := defaultConfiguration()
	decoder := toml.NewDecoder(f)
	if _, err := decoder.Decode(cfg); err != nil {
		return nil, err
	}

	fmt.Printf("Configuration loaded from %s\n", cfg.ConfigurationFile)

	return cfg, nil
}

func setup(fpath string) (*Configuration, error) {
	cfg := defaultConfiguration()

	bTrue := true
	bFalse := false

	max := func(a, b int) int {
		if b > a {
			return b
		}
		return a
	}

	defaultReadWorkerCount := max(runtime.NumCPU()/3, 1)
	defaultUploadWorkerCount := max(runtime.NumCPU()/4, 1)
	defaultBlockSize := 4 * 1024 * 1024 // 2 MiB

	input, err := ask("First time setup, create a configuration file? [Y/n]")
	if err != nil {
		return nil, err
	}
	answerBool := parseBool(input, &bTrue)
	if answerBool == nil {
		return nil, ErrUnrecognizedAnswer
	}
	if !*answerBool {
		fmt.Println("Skipping setup")
		return cfg, nil
	}

	input, err = ask("API URL (usually of form https://somesite.com/api):")
	if err != nil {
		return nil, err
	}
	if input == "" {
		fmt.Println("Skipping setup")
		return cfg, nil
	}
	apiURL, err := parseURL(input)
	if err != nil {
		return nil, err
	}
	cfg.APIURL = apiURL

	input, err = ask("Your gallery username:")
	if err != nil {
		return nil, err
	}
	if input == "" {
		fmt.Println("Skipping setup")
		return cfg, nil
	}
	cfg.Username = input

	input, err = ask("Publish all synced objects by default? [y/N]")
	if err != nil {
		return nil, err
	}
	cfg.Publish = parseBool(input, &bFalse)

	input, err = ask("Use colorized output by default? [Y/n]")
	if err != nil {
		return nil, err
	}
	cfg.Color = parseBool(input, &bTrue)

	input, err = ask("Include hidden files and contents of hidden directories by default? [y/N]")
	if err != nil {
		return nil, err
	}
	cfg.Hidden = parseBool(input, &bFalse)

	input, err = ask(fmt.Sprintf("Default count of concurrent reads (default %d):", defaultReadWorkerCount))
	if err != nil {
		return nil, err
	}
	answerInt, ok := parseInt(input, defaultReadWorkerCount)
	if !ok {
		return nil, ErrUnrecognizedAnswer
	}
	cfg.Concurrency.ReadWorkerCount = answerInt

	input, err = ask(fmt.Sprintf("Default count of concurrent uploads (default %d):", defaultUploadWorkerCount))
	if err != nil {
		return nil, err
	}
	answerInt, ok = parseInt(input, defaultUploadWorkerCount)
	if !ok {
		return nil, ErrUnrecognizedAnswer
	}
	cfg.Concurrency.UploadWorkerCount = answerInt

	input, err = ask(fmt.Sprintf("Default upload block size (default %d):", defaultBlockSize))
	if err != nil {
		return nil, err
	}
	answerInt, ok = parseInt(input, defaultBlockSize)
	if !ok {
		return nil, ErrUnrecognizedAnswer
	}
	cfg.BlockSize = int64(answerInt)

	input, err = ask(fmt.Sprintf("Configuration file path (default %s):", fpath))
	if err != nil {
		return nil, err
	}
	if input != "" {
		fpath = input
	}

	dir := filepath.Dir(fpath)
	if err = os.MkdirAll(dir, 0755); err != nil {
		return nil, err
	}

	f, err := os.Create(fpath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	encoder := toml.NewEncoder(f)
	if err = encoder.Encode(cfg); err != nil {
		return nil, err
	}

	fmt.Printf("Successfully created configuration file %s\n", fpath)

	return cfg, nil
}

func ask(msg string) (string, error) {
	fmt.Printf("%s ", strings.TrimSpace(msg))

	scanner := bufio.NewScanner(os.Stdin)

	scanner.Scan()

	return strings.TrimSpace(scanner.Text()), scanner.Err()
}

func parseBool(s string, def *bool) *bool {
	if s == "" {
		return def
	}
	if strings.EqualFold(s, "y") || strings.EqualFold(s, "yes") {
		b := true
		return &b
	}
	if strings.EqualFold(s, "n") || strings.EqualFold(s, "no") {
		b := false
		return &b
	}
	return nil
}

func parseInt(s string, def int) (int, bool) {
	if s == "" {
		return def, true
	}
	i, err := strconv.Atoi(s)
	if err != nil {
		return 0, false
	}
	return i, true
}

func parseURL(s string) (*URL, error) {
	u, err := url.Parse(s)
	if err != nil {
		return nil, err
	}

	if u.Scheme == "" {
		return nil, errors.New("required url component: scheme")
	}
	if u.Host == "" {
		return nil, errors.New("required url component: host")
	}

	return &URL{
		Scheme: u.Scheme,
		Host:   u.Host,
		Path:   u.Path,
	}, nil
}

func (cfg *Configuration) Info(msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(msg)
		return
	}
	cfg.colorPrintln(utils.ShellColorBoldLightGray, msg)
}

func (cfg *Configuration) Progress(msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(msg)
		return
	}
	cfg.colorPrintln(utils.ShellColorBoldCyan, msg)
}

func (cfg *Configuration) Success(msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(msg)
		return
	}
	cfg.colorPrintln(utils.ShellColorBoldGreen, msg)
}

func (cfg *Configuration) Error(msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(msg)
		return
	}
	cfg.colorPrintln(utils.ShellColorBoldRed, msg)
}

func (cfg *Configuration) ObjProgress(base, msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(base + " - " + msg)
		return
	}
	cfg.colorPrint(utils.ShellColorBold, base)
	fmt.Print(" - ")
	cfg.colorPrintln(utils.ShellColorBoldCyan, msg)
}

func (cfg *Configuration) ObjSuccess(base, msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(base + " - " + msg)
		return
	}
	cfg.colorPrint(utils.ShellColorBold, base)
	fmt.Print(" - ")
	cfg.colorPrintln(utils.ShellColorBoldGreen, msg)
}
func (cfg *Configuration) ObjError(base, msg string) {
	cfg.printMutex.Lock()
	defer cfg.printMutex.Unlock()

	if !*cfg.Color {
		cfg.println(base + " - " + msg)
		return
	}
	cfg.colorPrint(utils.ShellColorBold, base)
	fmt.Print(" - ")
	cfg.colorPrintln(utils.ShellColorBoldRed, msg)
}

func (cfg *Configuration) println(msg string) {
	fmt.Println(strings.TrimSpace(msg))
}

func (cfg *Configuration) colorPrint(color utils.ShellColor, msg string) {
	fmt.Printf(
		"%s%s%s",
		color,
		strings.TrimSpace(msg),
		utils.ShellColorNormal,
	)
}

func (cfg *Configuration) colorPrintln(color utils.ShellColor, msg string) {
	fmt.Printf(
		"%s%s%s\n",
		color,
		strings.TrimSpace(msg),
		utils.ShellColorNormal,
	)
}
