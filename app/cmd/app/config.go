package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/asdfakl/gallery/app/apiserver"
	"gitlab.com/asdfakl/gallery/app/appserver"
	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
)

type Configuration struct {
	APIServer apiserver.Configuration
	AppServer appserver.Configuration
	Postgres  config.PostgresConfiguration
	RedisLRU  config.RedisConfiguration
	RedisMain config.RedisConfiguration
	Thumbs    config.ThumbStorageConfiguration
}

func loadConfiguration(logger *log.Logger) (*Configuration, error) {
	cfg := defaultConfiguration()

	fs := flag.NewFlagSet("", flag.ExitOnError)
	fs.Usage = func() {
		fmt.Fprintln(os.Stderr, "Usage: app [OPTIONS]")

		fs.PrintDefaults()
	}

	cfg.APIServer.InitFlagSet(fs)
	cfg.AppServer.InitFlagSet(fs)
	cfg.Postgres.InitFlagSet(fs)
	cfg.RedisLRU.InitFlagSet(fs, "redis-lru")
	cfg.RedisMain.InitFlagSet(fs, "redis-main")
	cfg.Thumbs.InitFlagSet(fs)

	if err := fs.Parse(os.Args[1:]); err != nil {
		return nil, fmt.Errorf("parse command-line arguments: %w", err)
	}

	trimConfiguration(cfg)
	return validateConfiguration(cfg, logger)
}

func trimConfiguration(cfg *Configuration) {
	cfg.APIServer.Trim()
	cfg.AppServer.Trim()
	cfg.Postgres.Trim()
	cfg.RedisLRU.Trim()
	cfg.RedisMain.Trim()
	cfg.Thumbs.Trim()
}

func validateConfiguration(cfg *Configuration, logger *log.Logger) (*Configuration, error) {
	failed := 0

	for _, sub := range []error{
		cfg.APIServer.Validate(),
		cfg.AppServer.Validate(),
		cfg.Postgres.Validate(),
		cfg.RedisMain.Validate(),
		cfg.Thumbs.Validate(),
	} {
		if sub != nil {
			logger.Error("configuration validation", log.Err(sub))
			failed++
		}
	}

	if failed > 0 {
		return nil, fmt.Errorf("configuration validation failed with %d errors", failed)
	}
	return cfg, nil
}

func defaultConfiguration() *Configuration {
	return &Configuration{
		APIServer: *apiserver.DefaultConfiguration(),
		AppServer: *appserver.DefaultConfiguration(),
		Postgres:  *config.DefaultPostgresConfiguration(),
		RedisLRU:  *config.DefaultRedisConfiguration(),
		RedisMain: *config.DefaultRedisConfiguration(),
		Thumbs:    *config.DefaultThumbStorageConfiguration(),
	}
}
