package objectstorage

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"regexp"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
)

var (
	ErrObjectKeyParse = fmt.Errorf("could not parse object key")
	ErrNotImplemented = fmt.Errorf("not implemented")
	objectKeyExp      = regexp.MustCompile(`^[0-9a-f]{2}/[0-9a-f]{4}/([0-9a-f]{40})$`)
)

type Storage interface {
	ID() int64
	DeleteObject(ctx context.Context, sum [sha1.Size]byte) error
	ListObjects(ctx context.Context, callback func([][sha1.Size]byte) error) error
	PullObject(ctx context.Context, sum [sha1.Size]byte, path string) (int64, error)
	PushObject(ctx context.Context, sum [sha1.Size]byte, path string) (int64, error)
}

func NewStorage(id int64, t postgres.StorageType, opts map[string]interface{}, logger *log.Logger) (Storage, error) {
	switch t {
	case postgres.StorageTypeS3:
		return NewS3Storage(id, opts, logger)
	case postgres.StorageTypeLocal:
		return NewLocalStorage(id, opts, logger)
	default:
		return nil, fmt.Errorf("unsupported storage type '%s'", t)
	}
}

func objectKeyFromSHA1(sum [sha1.Size]byte) string {
	encoded := hex.EncodeToString(sum[:])

	return fmt.Sprintf(
		"%s/%s/%s",
		encoded[0:2],
		encoded[0:4],
		encoded,
	)
}

func parseObjectKey(key string) ([sha1.Size]byte, error) {
	subs := objectKeyExp.FindStringSubmatch(key)
	if len(subs) != 2 {
		return [sha1.Size]byte{}, ErrObjectKeyParse
	}

	b, err := hex.DecodeString(subs[1])
	if err != nil {
		return [sha1.Size]byte{}, err
	}
	if len(b) != 20 {
		panic(fmt.Errorf("parse object key: expecting 20 bytes decoded, got %d", len(b)))
	}

	res := [sha1.Size]byte{}
	copy(res[:], b)

	return res, nil
}
