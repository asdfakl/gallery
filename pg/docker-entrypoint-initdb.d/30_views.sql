------------------------------------------------------------------------------------------------------------------
-- v_object

CREATE OR REPLACE VIEW gallery.v_object AS
	SELECT
		o.id,
		o.sha1,
		o.basename,
		o.size,
		o.content_type,
		o.content_subtype,
		o.content_subtype_params,
		o.modified_at,
		o.added_at,
		o.owner,
		o.public,
		o.thumbnail_64x64,
		o.thumbnail_128x128,
		COALESCE(o.tags, '{}') AS tags
	FROM
		gallery.object o
	LEFT JOIN
		gallery.object thumbnail_of ON (o.id = thumbnail_of.thumbnail_64x64 OR o.id = thumbnail_of.thumbnail_128x128)
	WHERE
		NOT o.deleted
		AND o.owner IS NOT NULL
		AND thumbnail_of.id IS NULL
		AND EXISTS (
			SELECT 1
			FROM gallery.object_storage
			WHERE object_id = o.id
		);

------------------------------------------------------------------------------------------------------------------
-- v_object

CREATE VIEW gallery.v_user AS
	SELECT
		u.id,
		u.ext_id,
		u.username,
		u.password,
		COALESCE(u.display_name, u.username) AS display_name
	FROM
		gallery.user u
	WHERE
		u.validity_period @> now();
