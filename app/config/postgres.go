package config

import (
	"errors"
	"flag"
	"fmt"
	"strings"

	"gitlab.com/asdfakl/gallery/app/utils"
)

type PostgresConfiguration struct {
	Host     string
	Port     int
	Database string
	Username string
	Password string
}

func DefaultPostgresConfiguration() *PostgresConfiguration {
	return &PostgresConfiguration{
		Host:     "127.0.0.1",
		Port:     5432,
		Database: "gallery",
	}
}

func (cfg *PostgresConfiguration) InitFlagSet(fs *flag.FlagSet) {
	utils.StringVarOrEnv(fs, &cfg.Host, "postgres-host", cfg.Host, "postgres host")
	utils.IntVarOrEnv(fs, &cfg.Port, "postgres-port", cfg.Port, "postgres port")
	utils.StringVarOrEnv(fs, &cfg.Database, "postgres-database", cfg.Database, "postgres database")
	utils.StringVarOrEnv(fs, &cfg.Username, "postgres-username", cfg.Username, "postgres username")
	utils.StringVarOrEnv(fs, &cfg.Password, "postgres-password", cfg.Password, "postgres password")
}

func (cfg *PostgresConfiguration) Trim() {
	cfg.Host = strings.TrimSpace(cfg.Host)
	cfg.Database = strings.TrimSpace(cfg.Database)
	cfg.Username = strings.TrimSpace(cfg.Username)
}

func (cfg *PostgresConfiguration) Validate() error {
	for _, assert := range []assertion{
		{cfg.Host != "", errors.New("postgres-host empty or undefined")},
		{cfg.Port > 0, errors.New("postgres-port must be positive")},
		{cfg.Database != "", errors.New("postgres-database empty or undefined")},
		{cfg.Username != "", errors.New("postgres-username empty or undefined")},
		{cfg.Password != "", errors.New("postgres-password empty or undefined")},
	} {
		if !assert.ok {
			return assert.err
		}
	}
	return nil
}

func (cfg *PostgresConfiguration) ConnectionString() string {
	return fmt.Sprintf(
		"user=%s password=%s host=%s port=%d dbname=%s pool_max_conns=2",
		cfg.Username,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.Database,
	)
}
