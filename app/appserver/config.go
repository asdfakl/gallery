package appserver

import (
	"flag"
	"fmt"
	"path"
	"strings"
	"time"

	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/utils"
)

type Configuration struct {
	ListenAddr        string
	APIAddr           string
	StartTime         time.Time
	StaticDirectory   string
	TemplateDirectory string
	DownloadDirectory string

	IdleTimeout       time.Duration
	MaxHeaderBytes    int
	ReadHeaderTimeout time.Duration
	ReadTimeout       time.Duration
	WriteTimeout      time.Duration

	Domain             string // TODO csrf replace with origin
	RootPath           string
	Secret             string
	SecureCookie       bool
	SessionCookieName  string
	SessionTTL         time.Duration
	TrustXForwardedFor bool
	TrustXRealIP       bool

	Limits Limits
}

type Limits struct {
	UpperMasterPasswordCount int
}

func DefaultConfiguration() *Configuration {
	return &Configuration{
		ListenAddr:        "127.0.0.1:8080",
		StartTime:         time.Now(),
		DownloadDirectory: "/opt/galleryapp/download",

		IdleTimeout:       1 * time.Minute,
		MaxHeaderBytes:    2 * 1024,
		ReadHeaderTimeout: 1 * time.Second,
		ReadTimeout:       3 * time.Second,
		WriteTimeout:      5 * time.Second,

		Domain:             "localhost",
		RootPath:           "/",
		SecureCookie:       true,
		SessionCookieName:  "gallery-session",
		SessionTTL:         time.Hour,
		TrustXForwardedFor: false,
		TrustXRealIP:       false,

		Limits: Limits{
			UpperMasterPasswordCount: 10,
		},
	}
}

func (cfg *Configuration) InitFlagSet(fs *flag.FlagSet) {
	utils.StringVarOrEnv(fs, &cfg.ListenAddr, cfg.option("http-listen-addr"), cfg.ListenAddr, "http listen address, host:port")
	utils.StringVarOrEnv(fs, &cfg.APIAddr, cfg.option("api-addr"), cfg.APIAddr, "api address, scheme://host[:port][/path]")
	utils.StringVarOrEnv(fs, &cfg.StaticDirectory, cfg.option("static-directory"), cfg.StaticDirectory, "serve static files from directory instead of embedded (optional)")
	utils.StringVarOrEnv(fs, &cfg.TemplateDirectory, cfg.option("template-directory"), cfg.TemplateDirectory, "load templates from directory instead of embedded (optional)")
	utils.StringVarOrEnv(fs, &cfg.DownloadDirectory, cfg.option("download-directory"), cfg.DownloadDirectory, "serve downloads from this directory")

	utils.DurationVarOrEnv(fs, &cfg.IdleTimeout, cfg.option("http-idle-timeout"), cfg.IdleTimeout, "maximum duration to wait for the next request when keep-alives are enabled")
	utils.IntVarOrEnv(fs, &cfg.MaxHeaderBytes, cfg.option("http-max-header-bytes"), cfg.MaxHeaderBytes, "maximum amount of http header bytes read")
	utils.DurationVarOrEnv(fs, &cfg.ReadHeaderTimeout, cfg.option("http-read-header-timeout"), cfg.ReadHeaderTimeout, "maximum duration for reading the headers")
	utils.DurationVarOrEnv(fs, &cfg.ReadTimeout, cfg.option("http-read-timeout"), cfg.ReadTimeout, "maximum duration for reading the entire request, including body")
	utils.DurationVarOrEnv(fs, &cfg.WriteTimeout, cfg.option("http-write-timeout"), cfg.WriteTimeout, "maximum duration before timing out response writes")

	utils.StringVarOrEnv(fs, &cfg.Domain, cfg.option("domain"), cfg.Domain, "domain name")
	utils.StringVarOrEnv(fs, &cfg.RootPath, cfg.option("root-path"), cfg.RootPath, "http root path")
	utils.StringVarOrEnv(fs, &cfg.Secret, cfg.option("secret"), cfg.Secret, "token issuer secret")
	utils.BoolVarOrEnv(fs, &cfg.SecureCookie, cfg.option("secure-cookie"), cfg.SecureCookie, "set cookies as secure (https only)")
	utils.StringVarOrEnv(fs, &cfg.SessionCookieName, cfg.option("session-cookie-name"), cfg.SessionCookieName, "session cookie name")
	utils.DurationVarOrEnv(fs, &cfg.SessionTTL, cfg.option("session-ttl"), cfg.SessionTTL, "session ttl (time to live)")
	utils.BoolVarOrEnv(fs, &cfg.TrustXForwardedFor, cfg.option("trust-x-forwarded-for"), cfg.TrustXForwardedFor, "trust X-Forwarded-For header (from reverse proxy)")
	utils.BoolVarOrEnv(fs, &cfg.TrustXRealIP, cfg.option("trust-x-real-ip"), cfg.TrustXRealIP, "trust X-Real-IP header (from reverse proxy)")

	utils.IntVarOrEnv(fs, &cfg.Limits.UpperMasterPasswordCount, cfg.option("limits-upper-master-password-count"), cfg.Limits.UpperMasterPasswordCount, "upper bound on a user's master password count")
}

func (cfg *Configuration) Trim() {
	for _, str := range []*string{
		&cfg.ListenAddr,
		&cfg.APIAddr,
		&cfg.Domain,
		&cfg.SessionCookieName,
		&cfg.Secret,
		&cfg.StaticDirectory,
		&cfg.TemplateDirectory,
		&cfg.DownloadDirectory,
	} {
		*str = strings.TrimSpace(*str)
	}

	cfg.RootPath = path.Clean(cfg.RootPath)
}

func (cfg *Configuration) Validate() error {
	for _, assert := range []struct {
		ok  bool
		err error
	}{
		{cfg.ListenAddr != "", fmt.Errorf("%s empty or undefined", cfg.option("http-listen-addr"))},
		{cfg.APIAddr != "", fmt.Errorf("%s empty or undefined", cfg.option("api-addr"))},
		{
			cfg.StaticDirectory == "" || utils.IsDirectory(cfg.StaticDirectory),
			fmt.Errorf("%s is not a directory: '%s'", cfg.option("static-directory"), cfg.StaticDirectory),
		},
		{
			cfg.TemplateDirectory == "" || utils.IsDirectory(cfg.TemplateDirectory),
			fmt.Errorf("%s is not a directory: '%s'", cfg.option("template-directory"), cfg.TemplateDirectory),
		},

		{cfg.IdleTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-idle-timeout"))},
		{cfg.MaxHeaderBytes > 0, fmt.Errorf("%s must be a positive integer", cfg.option("http-max-header-bytes"))},
		{cfg.ReadHeaderTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-read-header-timeout"))},
		{cfg.ReadTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-read-timeout"))},
		{cfg.WriteTimeout > 0, fmt.Errorf("%s must be a positive duration", cfg.option("http-write-timeout"))},

		{cfg.Domain != "", fmt.Errorf("%s empty or undefined", cfg.option("domain"))},
		{path.IsAbs(cfg.RootPath), fmt.Errorf("%s must be an absolute path", cfg.option("root-path"))},
		{cfg.SessionCookieName != "", fmt.Errorf("%s empty or undefined", cfg.option("session-cookie-name"))},
		{cfg.SessionTTL >= 15*time.Minute, fmt.Errorf("%s must be greater than 15m", cfg.option("session-ttl"))},
		{cfg.Secret != "", fmt.Errorf("%s empty or undefined", cfg.option("secret"))},
	} {
		if !assert.ok {
			return assert.err
		}
	}

	if err := cfg.validateLimits(); err != nil {
		return err
	}

	for _, warning := range []struct {
		ok     bool
		msg    string
		fields []log.Field
	}{
		{cfg.StaticDirectory == "", "serving static files from directory", []log.Field{log.String("directory", cfg.StaticDirectory)}},
		{cfg.TemplateDirectory == "", "loading templates from directory", []log.Field{log.String("directory", cfg.TemplateDirectory)}},
	} {
		if !warning.ok {
			log.Default.Warn(warning.msg, warning.fields...)
		}
	}

	return nil
}

func (cfg *Configuration) validateLimits() error {
	for _, assert := range []struct {
		ok  bool
		err error
	}{
		{
			cfg.Limits.UpperMasterPasswordCount > 0 && cfg.Limits.UpperMasterPasswordCount <= 1000,
			fmt.Errorf("%s must be in the range (0, 1000]", cfg.option("limits-upper-master-password-count")),
		},
	} {
		if !assert.ok {
			return assert.err
		}
	}

	return nil
}

func (cfg *Configuration) option(opt string) string {
	return "app-server-" + opt
}
