"use strict";

var Utils = (function() {
	return Object.freeze({
		debounce: (fn, wait) => {
			if (typeof fn !== "function") {
				throw `Utils.debounce arg fn: expect function, got ${typeof fn}`;
			}

			wait = Number(wait);
			wait = wait > 0 ? wait : 0;

			let debounce = null;

			return () => {
				if (debounce) {
					clearTimeout(debounce);
				}

				debounce = setTimeout(fn, wait);
			};
		},

		queryString: obj => {
			if (typeof obj !== "object") {
				throw `Utils.queryString arg obj: expect object, got ${typeof obj}`;
			}

			let qs = [];

			const encodeKeyValue = (k, v) => {
				switch (typeof v) {
					case "number":
					case "string":
					case "boolean":
						break;
					default:
						throw `Utils.queryString unsupported value type for key ${k}: ${typeof v}`;
				}

				qs.push(`${encodeURIComponent(k)}=${encodeURIComponent(v)}`);
			};


			for (const [k, v] of Object.entries(obj)) {
				if (Array.isArray(v)) {
					v.forEach(vv => {
						encodeKeyValue(k, vv);
					});
				} else {
					encodeKeyValue(k, v);
				}
			}

			return qs.join("&");
		},

		showErrorModal: content => {
			content = String(content);

			const modal = document.getElementById("js-error-modal");
			const container = modal.querySelector("div.modal-content");

			// TODO mitigate xss risk
			container.innerHTML = `
				<div class="notification is-danger">
					${content}
				</div>
			`;
			modal.classList.add("is-active");
		},

		showContentModal: (content, isClass) => {
			content = String(content);
			isClass = isClass || "is-info";

			const modal = document.getElementById("js-content-modal");
			const container = modal.querySelector("div.modal-content");

			// TODO mitigate xss risk
			container.innerHTML = `
				<div class="notification is-light ${isClass}">
					${content}
				</div>
			`;
			modal.classList.add("is-active");
		},

		handleFetchError: res => {
			if (res.ok) {
				return res;
			}

			const exception = `fetch request failed: ${res.status} ${res.statusText}`;

			const contentType = res.headers.get("Content-Type");
			if (contentType) {
				switch (contentType.split(";")[0]) {
					case "text/plain":
						return res.text().then(text => {
							Utils.showErrorModal(`${res.status} ${res.statusText} : ${text}`);
							throw exception;
						});
				}
			}

			throw exception;
		},

		handleFetchSuccess: (res, node) => {
			if (res.Redirect) {
				window.location = String(res.Redirect);
				return;
			}
			if (res.Reload) {
				window.location.reload();
				return;
			}
			if (res.Refresh) {
				const event = new CustomEvent("RequestComponentRefresh", {
					bubbles: true
				});

				node.dispatchEvent(event);
				return;
			}
		},

		newPasswordOK: val => {
			if (!zxcvbn) {
				throw "missing dependency: zxcvbn";
			}

			val = val.trim();
			if (!val) {
				return [false, "Required", 0];
			}

			const report = zxcvbn(val);

			const requiredStrength = 0.12;

			const progress = Math.min(Math.round(report.guesses_log10 * (1 / requiredStrength)), 100);

			if (report.feedback.warning) {
				return [false, report.feedback.warning, progress];
			}
			if (progress < 100) {
				return [false, "Too weak", progress];
			}

			return [true, "", progress];
		},

		commonPostFormOpts: () => ({
			method: "POST",
			credentials: "same-origin",
			redirect: "manual",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
		}),
	});
})();
