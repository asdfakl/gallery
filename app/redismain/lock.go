package redismain

import (
	"context"
	"errors"
	"time"
)

var ErrLockConflict = errors.New("lock conflict")

func (rmc *RedisMainConnection) Lock(ctx context.Context, key, value string, exp time.Duration) (string, error) {
	k := ns.lock(key)

	res := rmc.rdb.SetNX(ctx, k, value, exp)

	set, err := res.Result()
	if err != nil {
		return value, err
	}

	if !set {
		return value, ErrLockConflict
	}

	return value, nil
}

func (rmc *RedisMainConnection) Unlock(ctx context.Context, key, value string) (bool, error) {
	k := ns.lock(key)

	res, err := rmc.scriptDelIfEqual.Run(ctx, rmc.rdb, []string{k}, value).Result()
	if err != nil {
		return false, err
	}

	return res.(int64) == 1, nil
}

func (rmc *RedisMainConnection) RefreshLock(ctx context.Context, key, value string, expat time.Time) (bool, error) {
	k := ns.lock(key)

	res, err := rmc.scriptRefreshIfEqual.Run(ctx, rmc.rdb, []string{k}, value, expat.Unix()).Result()
	if err != nil {
		return false, err
	}

	return res.(int64) == 1, nil
}

func (rmc *RedisMainConnection) LockRefresher(ctx context.Context, key, value string, exp time.Duration) <-chan error {
	errc := make(chan error, 1)

	go func() {
		defer close(errc)

		if exp < 5*time.Second {
			errc <- errors.New("expiry period must be at least 5 seconds")
			return
		}

		ticker := time.NewTicker(exp / 2)
		defer ticker.Stop()

		for {
			select {
			case <-ctx.Done():
				return

			case t := <-ticker.C:
				success, err := rmc.RefreshLock(ctx, key, value, t.Add(exp))
				if err != nil {
					errc <- err
					return
				}
				if !success {
					errc <- errors.New("conflict")
					return
				}
			}
		}
	}()

	return errc
}
