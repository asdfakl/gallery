---------------------------------------------------------------------------------------------------------------------------------------
-- user

CREATE SEQUENCE gallery.user_id_seq;

CREATE TABLE gallery.user (
	id INTEGER NOT NULL DEFAULT NEXTVAL('gallery.user_id_seq'),
	ext_id UUID NOT NULL DEFAULT uuid_generate_v4() CHECK ( ext_id != uuid_nil() ),
	username TEXT NOT NULL CHECK ( TRIM(username) != '' AND LENGTH(username) <= 64 ),
	password TEXT CHECK ( LENGTH(password) <= 60),
	display_name TEXT CHECK ( TRIM(display_name) != '' AND LENGTH(display_name) <= 64 ),
	validity_period TSTZRANGE NOT NULL CHECK (
		NOT lower_inf(validity_period)
		AND lower_inc(validity_period)
		AND NOT upper_inc(validity_period)
	) DEFAULT TSTZRANGE(now(), NULL),

	PRIMARY KEY (id),
	UNIQUE (ext_id),
	EXCLUDE USING GIST (username WITH =, validity_period WITH &&),
	EXCLUDE USING GIST (display_name WITH =, validity_period WITH &&)
);

---------------------------------------------------------------------------------------------------------------------------------------
-- object

CREATE SEQUENCE gallery.object_id_seq;

CREATE TABLE gallery.object (
	id INTEGER NOT NULL DEFAULT NEXTVAL('gallery.object_id_seq'),
	sha1 BYTEA NOT NULL CHECK ( LENGTH(sha1) = 20 ),
	basename TEXT NOT NULL CHECK ( TRIM(basename) != '' AND LENGTH(basename) < 256 ),
	size BIGINT NOT NULL CHECK ( size > 0 ),
	content_type TEXT NOT NULL CHECK ( TRIM(content_type) != '' AND LENGTH(content_type) <= 32 ),
	content_subtype TEXT NOT NULL CHECK ( TRIM(content_subtype) != '' AND LENGTH(content_subtype) <= 64 ),
	content_subtype_params TEXT CHECK ( TRIM(content_subtype_params) != '' AND LENGTH(content_subtype_params) <= 64 ),
	modified_at TIMESTAMPTZ NOT NULL,
	added_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	owner INTEGER,
	public BOOLEAN NOT NULL DEFAULT FALSE,
	thumbnail_64x64 INTEGER,
	thumbnail_128x128 INTEGER,
	tags TEXT[],
	deleted BOOLEAN NOT NULL DEFAULT FALSE,

	PRIMARY KEY (id),
	UNIQUE (sha1),

	FOREIGN KEY (owner) REFERENCES gallery.user (id) ON UPDATE CASCADE ON DELETE RESTRICT,

	FOREIGN KEY (thumbnail_64x64) REFERENCES gallery.object (id) ON UPDATE CASCADE ON DELETE SET NULL,

	FOREIGN KEY (thumbnail_128x128) REFERENCES gallery.object (id) ON UPDATE CASCADE ON DELETE SET NULL
);

-- foreign key indexes
CREATE INDEX ON gallery.object (owner);
CREATE INDEX ON gallery.object (thumbnail_64x64);
CREATE INDEX ON gallery.object (thumbnail_128x128);

CREATE INDEX ON gallery.object (basename);

CREATE INDEX ON gallery.object (content_type);

CREATE INDEX ON gallery.object (modified_at);

CREATE INDEX ON gallery.object (added_at);

CREATE INDEX ON gallery.object USING GIN (tags);

---------------------------------------------------------------------------------------------------------------------------------------
-- storage

CREATE SEQUENCE gallery.storage_id_seq;

CREATE TYPE gallery.storage_type AS ENUM ('local', 's3');

CREATE TABLE gallery.storage (
	id INTEGER NOT NULL DEFAULT NEXTVAL('gallery.storage_id_seq'),
	type gallery.storage_type NOT NULL,
	access_priority INTEGER NOT NULL CHECK ( access_priority >= 0 ),
	storage_priority INTEGER NOT NULL CHECK ( storage_priority >= 0 ),
	name TEXT NOT NULL CHECK ( TRIM(name) != '' ),
	description TEXT,
	opts JSONB NOT NULL DEFAULT '{}' CHECK ( jsonb_typeof(opts) = 'object'),

	PRIMARY KEY (id),
	UNIQUE (name),
	UNIQUE (access_priority),
	UNIQUE (storage_priority)
);

---------------------------------------------------------------------------------------------------------------------------------------
-- object_storage

CREATE TABLE gallery.object_storage (
	object_id INTEGER NOT NULL,
	storage_id INTEGER NOT NULL,

	PRIMARY KEY (object_id, storage_id),

	FOREIGN KEY (object_id) REFERENCES gallery.object (id) ON UPDATE CASCADE ON DELETE RESTRICT,

	FOREIGN KEY (storage_id) REFERENCES gallery.storage (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

---------------------------------------------------------------------------------------------------------------------------------------
-- background_job

CREATE SEQUENCE gallery.background_job_id_seq;

CREATE TABLE gallery.background_job (
	id INTEGER NOT NULL DEFAULT NEXTVAL('gallery.background_job_id_seq'),
	func TEXT NOT NULL CHECK ( TRIM(func) != '' ),
	opts JSONB NOT NULL DEFAULT '{}' CHECK (jsonb_typeof(opts) = 'object'),

	PRIMARY KEY (id)
);

---------------------------------------------------------------------------------------------------------------------------------------
-- background_job_trigger

CREATE TABLE gallery.background_job_trigger (
	key TEXT NOT NULL CHECK ( TRIM(key) != '' ),
	background_job_id INTEGER NOT NULL,

	PRIMARY KEY (key),

	UNIQUE (background_job_id),

	FOREIGN KEY (background_job_id) REFERENCES gallery.background_job (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

---------------------------------------------------------------------------------------------------------------------------------------
-- background_job_queue

CREATE TABLE gallery.background_job_queue (
	key TEXT NOT NULL CHECK ( TRIM(key) != '' ),
	background_job_id INTEGER NOT NULL,
	worker_count INTEGER NOT NULL CHECK ( worker_count > 0 ),

	PRIMARY KEY (key),

	UNIQUE (background_job_id),

	FOREIGN KEY (background_job_id) REFERENCES gallery.background_job (id) ON UPDATE CASCADE ON DELETE RESTRICT
);
