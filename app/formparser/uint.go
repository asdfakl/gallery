package formparser

import (
	"fmt"
	"math/bits"
	"strconv"
)

type FormUintField struct {
	parser *FormParser
	key    string
	value  *uint
}

func (parser *FormParser) Uint(key string) (field *FormUintField) {
	field = &FormUintField{
		parser: parser,
		key:    key,
	}

	vals, err := parser.src(key)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse form: %w", err))
		return
	}

	if len(vals) == 0 {
		return
	}

	if len(vals) > 1 {
		parser.registerError(key, ErrFormParserAmbiguous)
		return
	}

	u64, err := strconv.ParseUint(vals[0], 10, bits.UintSize)
	if err != nil {
		parser.registerError(key, fmt.Errorf("parse uint: %w", err))
		return
	}
	u := uint(u64)

	field.value = &u
	return
}

func (field *FormUintField) Required() *FormUintField {
	if field.value == nil {
		field.parser.registerError(field.key, ErrFormParserRequired)
	}
	return field
}

func (field *FormUintField) Max(max uint) *FormUintField {
	if field.value == nil {
		return field
	}

	if *field.value > max {
		field.parser.registerError(field.key, ErrFormParserMaxValueExceeded)
	}
	return field
}

func (field *FormUintField) Min(min uint) *FormUintField {
	if field.value == nil {
		return field
	}

	if *field.value < min {
		field.parser.registerError(field.key, ErrFormParserMinValueSubceeded)
	}
	return field
}

func (field *FormUintField) Options(opts []uint) *FormUintField {
	if field.value == nil {
		return field
	}

	match := false
	for _, opt := range opts {
		if *field.value == opt {
			match = true
			break
		}
	}

	if !match {
		field.parser.registerError(field.key, ErrFormParserDoesNotMatchOptions)
	}
	return field
}

func (field *FormUintField) Value() uint {
	field.parser.mustHaveErrChecked()

	return *field.value
}

func (field *FormUintField) OptionalValue() *uint {
	field.parser.mustHaveErrChecked()

	return field.value
}
