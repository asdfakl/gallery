package appserver

import (
	"errors"
	"net/http"
	"net/url"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/asdfakl/gallery/app/formparser"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/redismain"
)

type LogoutPage struct {
	Page
}

func LogoutPageFn(srv *AppServer, r *http.Request, ps httprouter.Params) (Handler, error) {
	return &LogoutPage{}, nil
}

func (page *LogoutPage) CanAccess(user *postgres.UserView) bool {
	return user.IsValid()
}

func (page *LogoutPage) HandlePOST(srv *AppServer, w http.ResponseWriter, r *http.Request) {
	parser := formparser.NewBodyFormParser(r, srv.validateCSRFToken)

	var (
		err         error
		redirect    *url.URL
		rawRedirect = parser.String("redirect").MaxLength(4 * 1024)
	)

	if err = parser.Err(); err != nil {
		srv.handleError(w, ErrBadRequest(err, err.Error()))
		return
	}

	if val := rawRedirect.OptionalValue(); val != nil {
		redirect, err = url.Parse(rawRedirect.Value())
		if err != nil {
			srv.handleError(w, ErrBadRequest(err, "parse redirect uri"))
			return
		}
	}

	session, ok := r.Context().Value(ContextKeySession{}).(*redismain.Session)
	if !ok {
		srv.handleError(w, errors.New("missing request context value: session"))
		return
	}

	removed, err := srv.rmc.RemoveSession(srv.appCtx, session.Key)
	if err != nil {
		srv.handleError(w, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:  srv.cfg.SessionCookieName,
		Value: "",

		Path:   srv.cfg.RootPath,
		MaxAge: -1,
	})

	srv.logger.Debug("logout", log.String("session", session.String()), log.Bool("removed", removed))

	redirectURI := srv.path("/")

	if redirect != nil && srv.isRooted(redirect.Path) {
		redirectURI = redirect.RequestURI()
	}

	srv.renderJSON(w, &JSONResponse{
		Redirect: redirectURI,
	}, http.StatusOK)
}
