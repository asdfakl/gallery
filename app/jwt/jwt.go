package jwt

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"time"
)

// {"alg":"HS256","typ":"JWT"}
var encodedHS256Header = []byte("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")

type JWTHeader struct {
	Alg string `json:"alg"`
	Typ string `json:"typ"`
}

type JWTPayload struct {
	Issuer  string `json:"iss"`
	Expiry  int64  `json:"exp"`
	Subject string `json:"sub"`
}

func (p *JWTPayload) Validate(trustedIssuers []string) bool {
	if !time.Unix(p.Expiry, 0).After(time.Now()) {
		return false
	}

	for _, iss := range trustedIssuers {
		if p.Issuer == iss {
			return true
		}
	}

	return false
}

func NewJWTHS256(payload interface{}, secret []byte) (token []byte) {
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		panic(err)
	}

	encodedPayload := make([]byte, base64.RawURLEncoding.EncodedLen(len(jsonPayload)))
	base64.RawURLEncoding.Encode(encodedPayload, jsonPayload)

	h := hmac.New(sha256.New, secret)

	token = append(encodedHS256Header, '.')
	token = append(token, encodedPayload...)

	if _, err = h.Write(token); err != nil {
		panic(err)
	}

	signature := h.Sum(nil)

	encodedSignature := make([]byte, base64.RawURLEncoding.EncodedLen(len(signature)))
	base64.RawURLEncoding.Encode(encodedSignature, signature)

	token = append(token, '.')
	token = append(token, encodedSignature...)

	return
}

func VerifyJWTToken(token, secret []byte, v interface{}) bool {
	parts := bytes.Split(token, []byte{'.'})

	if len(parts) != 3 {
		return false
	}

	decodedHeader := make([]byte, base64.RawURLEncoding.DecodedLen(len(parts[0])))
	if _, err := base64.RawURLEncoding.Decode(decodedHeader, parts[0]); err != nil {
		return false
	}

	header := JWTHeader{}
	if err := json.Unmarshal(decodedHeader, &header); err != nil {
		return false
	}

	if header.Typ != "JWT" {
		return false
	}
	if header.Alg != "HS256" {
		return false
	}

	decodedSignature := make([]byte, base64.RawURLEncoding.DecodedLen(len(parts[2])))
	if _, err := base64.RawURLEncoding.Decode(decodedSignature, parts[2]); err != nil {
		return false
	}

	toSign := bytes.Join(parts[0:2], []byte{'.'})

	h := hmac.New(sha256.New, secret)
	if _, err := h.Write(toSign); err != nil {
		panic(err)
	}

	if !bytes.Equal(h.Sum(nil), decodedSignature) {
		return false
	}

	decodedPayload := make([]byte, base64.RawURLEncoding.DecodedLen(len(parts[1])))
	if _, err := base64.RawURLEncoding.Decode(decodedPayload, parts[1]); err != nil {
		return false
	}

	if err := json.Unmarshal(decodedPayload, v); err != nil {
		return false
	}

	return true
}
