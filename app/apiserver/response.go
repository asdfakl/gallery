package apiserver

import (
	"net/http"
	"time"

	"gitlab.com/asdfakl/gallery/app/postgres"
)

func uploadResponse(srv *APIServer, w http.ResponseWriter, upload *postgres.UploadView) {
	p := &struct {
		SHA1       string    `json:"sha1"`
		Basename   string    `json:"basename"`
		Size       int64     `json:"size"`
		ModifiedAt time.Time `json:"modified_at"`
		Progress   int64     `json:"progress"`
		Public     bool      `json:"public"`
		Tags       []string  `json:"tags"`
	}{
		SHA1:       upload.HexSHA1(),
		Basename:   upload.Basename,
		Size:       upload.Size,
		ModifiedAt: upload.ModifiedAt,
		Progress:   upload.Progress,
		Public:     upload.Public,
		Tags:       upload.Tags,
	}

	srv.jsonResponse(w, p)
}
