package appserver

import (
	"net/url"
	"strconv"
)

type Pagination struct {
	Count      uint
	Offset     uint
	TotalCount uint
	LinkFn     func(uint) *url.URL
}

type PaginationItem struct {
	Text      string
	Offset    uint
	IsDivider bool
	IsCurrent bool
}

func (p *Pagination) Items() (items []*PaginationItem) {
	if p.TotalCount == 0 {
		return
	}

	currentPage := p.Offset / p.Count
	pageCount := p.TotalCount / p.Count
	if remainder := p.TotalCount % p.Count; remainder != 0 {
		pageCount++
	}

	page := uint(0)
	for page < pageCount {
		items = append(items, &PaginationItem{
			Text:      strconv.FormatUint(uint64(page+1), 10),
			Offset:    page * p.Count,
			IsCurrent: page == currentPage,
		})

		page++
		if int(page) < int(currentPage-2) {
			page = currentPage - 2
			items = append(items, &PaginationItem{IsDivider: true})
		} else if page > currentPage+2 && page < pageCount-1 {
			page = pageCount - 1
			items = append(items, &PaginationItem{IsDivider: true})
		}
	}

	return
}

func (p *Pagination) Prev() *PaginationItem {
	if p.TotalCount == 0 {
		return nil
	}

	currentPage := p.Offset / p.Count
	if currentPage == 0 {
		return nil
	}

	return &PaginationItem{
		Offset: (currentPage - 1) * p.Count,
	}
}

func (p *Pagination) Next() *PaginationItem {
	if p.TotalCount == 0 {
		return nil
	}

	currentPage := p.Offset / p.Count
	pageCount := p.TotalCount / p.Count
	if remainder := p.TotalCount % p.Count; remainder != 0 {
		pageCount++
	}

	if currentPage >= pageCount-1 {
		return nil
	}

	return &PaginationItem{
		Offset: (currentPage + 1) * p.Count,
	}
}

func (p *Pagination) Link(offset uint) *url.URL {
	return p.LinkFn(offset)
}
