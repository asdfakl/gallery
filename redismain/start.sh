#!/bin/bash

script_dir="$(dirname "$(realpath "$0")")"

cd "$script_dir" || exit 1

chmod 0644 redis.conf || exit 1
chmod 0644 users.acl || exit 1

docker run -d \
	--net asdfakl-gallery \
	--hostname redis-main \
	--name asdfakl-gallery-redis-main \
	--mount "type=bind,src=$(pwd)/redis.conf,dst=/etc/redis/redis.conf,readonly" \
	--mount "type=bind,src=$(pwd)/users.acl,dst=/etc/redis/users.acl,readonly" \
	--mount type=volume,src=asdfakl-gallery-redis-main-data,dst=/data \
	--log-opt mode=non-blocking \
	--log-opt max-buffer-size=2m \
	redis:6.2.7-bullseye redis-server /etc/redis/redis.conf
