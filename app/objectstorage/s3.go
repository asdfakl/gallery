package objectstorage

import (
	"bytes"
	"context"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	"gitlab.com/asdfakl/gallery/app/config"
	"gitlab.com/asdfakl/gallery/app/log"
	"gitlab.com/asdfakl/gallery/app/postgres"
	"gitlab.com/asdfakl/gallery/app/utils"
)

const (
	ERR_CODE_NOT_FOUND         = "NotFound"
	MULTIPART_UPLOAD_MAX_PARTS = 10000
)

type S3Storage struct {
	id     int64
	logger *log.Logger

	endpoint string
	region   string
	bucket   string
	prefix   string

	accessKey string
	secretKey string

	multipartPartSize    int64
	multipartUploadLimit int64

	svc      *s3.S3
	sess     *session.Session
	sessErr  error
	sessOnce sync.Once
}

var _ Storage = (*S3Storage)(nil)

func NewS3Storage(id int64, opts config.Options, logger *log.Logger) (*S3Storage, error) {
	const (
		MULTIPART_PART_SIZE_MIN = 5 * 1024 * 1024
		MULTIPART_PART_SIZE_MAX = 1024 * 1024 * 1024
	)

	storage, ok := &S3Storage{
		id:                   id,
		logger:               logger,
		multipartPartSize:    opts.Int64Or("multipart_part_size", 16*1024*1024),
		multipartUploadLimit: opts.Int64Or("multipart_upload_limit", 4),
	}, false

	if storage.multipartPartSize < MULTIPART_PART_SIZE_MIN {
		return nil, fmt.Errorf("multipart_part_size must be gte %s", utils.HumanFilesize(MULTIPART_PART_SIZE_MIN))
	}
	if storage.multipartPartSize > MULTIPART_PART_SIZE_MAX {
		return nil, fmt.Errorf("multipart_part_size must be lte %s", utils.HumanFilesize(MULTIPART_PART_SIZE_MAX))
	}

	if storage.multipartUploadLimit <= 0 {
		return nil, fmt.Errorf("multipart_upload_limit must be gt 0")
	}

	storage.endpoint, ok = opts.OptionalNonEmptyString("s3_endpoint")
	if !ok {
		return nil, fmt.Errorf("missing required option string: s3_endpoint")
	}

	storage.region, ok = opts.OptionalNonEmptyString("s3_region")
	if !ok {
		return nil, fmt.Errorf("missing required option string: s3_region")
	}

	storage.bucket, ok = opts.OptionalNonEmptyString("s3_bucket")
	if !ok {
		return nil, fmt.Errorf("missing required option string: s3_bucket")
	}

	storage.prefix, ok = opts.OptionalNonEmptyString("s3_prefix")
	if !ok {
		return nil, fmt.Errorf("missing required option string: s3_prefix")
	}
	storage.prefix = strings.TrimRight(storage.prefix, "/") + "/"

	storage.accessKey, ok = opts.OptionalNonEmptyString("s3_access_key")
	if !ok {
		return nil, fmt.Errorf("missing required option string: s3_access_key")
	}

	storage.secretKey, ok = opts.OptionalNonEmptyString("s3_secret_key")
	if !ok {
		return nil, fmt.Errorf("missing required option string: s3_secret_key")
	}

	return storage, nil
}

func (storage *S3Storage) ID() int64 {
	return storage.id
}

func (storage *S3Storage) DeleteObject(ctx context.Context, sum [sha1.Size]byte) error {
	return storage.deleteKey(ctx, objectKeyFromSHA1(sum))
}

func (storage *S3Storage) ListObjects(ctx context.Context, callback func([][sha1.Size]byte) error) error {
	if err := storage.initiateSession(); err != nil {
		return utils.Wrap(err, "initiate session")
	}

	var (
		err               error
		output            *s3.ListObjectsV2Output
		continuationToken *string
	)
	for output == nil || *output.IsTruncated {
		output, err = storage.svc.ListObjectsV2WithContext(ctx, &s3.ListObjectsV2Input{
			Bucket:            aws.String(storage.bucket),
			Prefix:            aws.String(storage.prefix),
			ContinuationToken: continuationToken,
		})
		if err != nil {
			return utils.Wrap(err, "list objects")
		}
		continuationToken = output.NextContinuationToken

		hashes := [][sha1.Size]byte{}
		for _, obj := range output.Contents {
			key := strings.TrimPrefix(*obj.Key, storage.prefix)
			h, err := parseObjectKey(key)
			if err != nil {
				storage.logger.Warn("deleting object with malformed key", log.String("key", key))
				if err = storage.deleteKey(ctx, key); err != nil {
					storage.logger.Error("delete object", log.Err(err))
				}
				continue
			}

			hashes = append(hashes, h)
		}

		if err = callback(hashes); err != nil {
			return err
		}
	}

	return nil
}

func (storage *S3Storage) PullObject(ctx context.Context, sum [sha1.Size]byte, path string) (int64, error) {
	if err := storage.initiateSession(); err != nil {
		return 0, utils.Wrap(err, "initiate session")
	}

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0640)
	if err != nil {
		return 0, utils.Wrap(err, "open target file")
	}
	defer f.Close()

	h := sha1.New()

	multi := io.MultiWriter(f, h)

	output, err := storage.svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(storage.bucket),
		Key:    aws.String(storage.prefixedKey(sum)),
	})
	if err != nil {
		return 0, utils.Wrap(err, "get object")
	}
	defer output.Body.Close()

	n, err := io.Copy(multi, output.Body)
	if err != nil {
		return n, utils.Wrap(err, "pull object")
	}

	var calculated [sha1.Size]byte
	copy(calculated[:], h.Sum(nil))

	if calculated != sum {
		return n, fmt.Errorf("sha1 sum mismatch")
	}

	return n, nil
}

func (storage *S3Storage) PushObject(ctx context.Context, sum [sha1.Size]byte, path string) (int64, error) {
	info, err := os.Lstat(path)
	if err != nil {
		return 0, utils.Wrap(err, "lstat")
	}
	if info.Mode()&os.ModeType != 0 {
		return 0, fmt.Errorf("not a regular file: %s", path)
	}
	if info.Size() == 0 {
		return 0, fmt.Errorf("empty file: %s", path)
	}

	exists, err := storage.objectExists(ctx, sum)
	if err != nil {
		return 0, utils.Wrap(err, "query object exists")
	}
	if exists {
		return info.Size(), nil
	}

	if info.Size() > storage.multipartPartSize*storage.multipartUploadLimit {
		return storage.pushObjectMultipart(ctx, sum, path, info)
	}

	if err = storage.initiateSession(); err != nil {
		return 0, utils.Wrap(err, "initiate session")
	}

	sumMD5, err := utils.ReadMD5(path)
	if err != nil {
		return 0, utils.Wrap(err, "read md5")
	}

	ct, err := utils.DetectContentType(path)
	if err != nil {
		return 0, utils.Wrap(err, "detect content type")
	}

	f, err := os.Open(path)
	if err != nil {
		return 0, utils.Wrap(err, "open")
	}
	defer f.Close()

	_, err = storage.svc.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Body:          f,
		Bucket:        aws.String(storage.bucket),
		ContentLength: aws.Int64(info.Size()),
		ContentType:   aws.String(ct),
		ContentMD5:    aws.String(utils.Base64EncodeMD5(sumMD5)),
		Key:           aws.String(storage.prefixedKey(sum)),
		Metadata: map[string]*string{
			"base":  aws.String(filepath.Base(path)),
			"mtime": aws.String(strconv.FormatInt(info.ModTime().Unix(), 10)),
		},
	})
	if err != nil {
		return 0, utils.Wrap(err, "put object")
	}

	return info.Size(), nil
}

func (storage *S3Storage) pushObjectMultipart(ctx context.Context, sum [sha1.Size]byte, path string, info os.FileInfo) (int64, error) {
	if err := storage.initiateSession(); err != nil {
		return 0, utils.Wrap(err, "initiate session")
	}

	logger := storage.logger.With(
		log.String("sha1", utils.HexEncodeSHA1(sum)),
		log.String("path", path),
	)

	if max := MULTIPART_UPLOAD_MAX_PARTS * storage.multipartPartSize; info.Size() > max {
		return 0, fmt.Errorf(
			"file size %s exceeds maximum multipart upload size of %s",
			utils.HumanFilesize(info.Size()),
			utils.HumanFilesize(max),
		)
	}

	f, err := os.Open(path)
	if err != nil {
		return 0, utils.Wrap(err, "open")
	}
	defer f.Close()

	uploadID, isNew, err := storage.getMultipartUploadID(ctx, sum, path, info)
	if err != nil {
		return 0, utils.Wrap(err, "get multipart upload id")
	}

	prefixedKey := storage.prefixedKey(sum)

	var existingParts map[int64]*s3.Part
	if !isNew {
		existingParts, err = storage.listMultipartUploadParts(ctx, sum, uploadID)
		if err != nil {
			return 0, utils.Wrap(err, "list multipart upload parts")
		}
	}

	numParts := info.Size()/storage.multipartPartSize + 1
	completedParts := []*s3.CompletedPart{}
	uploadedBytes := int64(0)

	for i := int64(0); i < numParts; i++ {
		var partSize int64
		if i == numParts-1 {
			partSize = info.Size() % storage.multipartPartSize
		} else {
			partSize = storage.multipartPartSize
		}

		desiredOffset := i * storage.multipartPartSize
		offset, err := f.Seek(desiredOffset, 0)
		if err != nil {
			return uploadedBytes, utils.Wrap(err, "seek")
		}
		if offset != desiredOffset {
			return uploadedBytes, fmt.Errorf("expecting offset %d, got %d", desiredOffset, offset)
		}

		h := md5.New()
		buf := &bytes.Buffer{}
		mw := io.MultiWriter(buf, h)
		n, err := io.CopyN(mw, f, partSize)
		if err != nil {
			return uploadedBytes, utils.Wrap(err, "read")
		}
		if n != partSize {
			return uploadedBytes, fmt.Errorf("expecting read of %d bytes, got %d bytes", partSize, n)
		}

		if existing := existingParts[i+1]; existing != nil {
			if *existing.Size == partSize &&
				*existing.PartNumber == i+1 &&
				strings.Trim(*existing.ETag, `"`) == hex.EncodeToString(h.Sum(nil)) {

				logger.Debug("part exists", log.Int64("part_number", *existing.PartNumber), log.Int64("num_parts", numParts))
				completedParts = append(completedParts, &s3.CompletedPart{
					ETag:       existing.ETag,
					PartNumber: existing.PartNumber,
				})
				uploadedBytes += partSize
				continue
			}
		}

		logger.Debug("upload part", log.Int64("part_number", i+1), log.Int64("num_parts", numParts))
		uploadPartOutput, err := storage.svc.UploadPartWithContext(ctx, &s3.UploadPartInput{
			Body:          bytes.NewReader(buf.Bytes()),
			Bucket:        aws.String(storage.bucket),
			ContentLength: aws.Int64(partSize),
			ContentMD5:    aws.String(base64.StdEncoding.EncodeToString(h.Sum(nil))),
			Key:           aws.String(prefixedKey),
			PartNumber:    aws.Int64(i + 1),
			UploadId:      uploadID,
		})
		if err != nil {
			return uploadedBytes, utils.Wrap(err, "upload part")
		}

		completedParts = append(completedParts, &s3.CompletedPart{
			ETag:       uploadPartOutput.ETag,
			PartNumber: aws.Int64(i + 1),
		})
		uploadedBytes += partSize
	}

	completedUpload := &s3.CompletedMultipartUpload{
		Parts: completedParts,
	}

	_, err = storage.svc.CompleteMultipartUploadWithContext(ctx, &s3.CompleteMultipartUploadInput{
		Bucket:          aws.String(storage.bucket),
		Key:             aws.String(prefixedKey),
		MultipartUpload: completedUpload,
		UploadId:        uploadID,
	})
	if err != nil {
		return uploadedBytes, utils.Wrap(err, "complete multipart upload")
	}

	return uploadedBytes, nil
}

func (storage *S3Storage) ImportObject(ctx context.Context, pg *postgres.PostgresConnection, sum [sha1.Size]byte) error {
	if err := storage.initiateSession(); err != nil {
		return utils.Wrap(err, "initiate session")
	}

	prefixedKey := storage.prefixedKey(sum)

	output, err := storage.svc.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(storage.bucket),
		Key:    aws.String(prefixedKey),
	})
	if err != nil {
		return utils.Wrap(err, "head object")
	}

	basename := output.Metadata["Base"]
	if basename == nil {
		return fmt.Errorf("object %s metadata missing key: 'Base'", prefixedKey)
	}

	mtime := output.Metadata["Mtime"]
	if mtime == nil {
		return fmt.Errorf("object %s metadata missing key: 'Mtime'", prefixedKey)
	}
	parsedMtime, err := strconv.ParseInt(*mtime, 10, 64)
	if err != nil {
		return fmt.Errorf("object %s metadata key 'Mtime' cannot be parsed as int64: %v", prefixedKey, err)
	}

	qct, err := utils.SplitContentType(*output.ContentType)
	if err != nil {
		return fmt.Errorf("object %s content-type cannot be parsed: %v", prefixedKey, err)
	}

	return pg.UpsertStorageObject(ctx, storage.id, &postgres.Object{
		SHA1:                 sum,
		Basename:             *basename,
		Size:                 *output.ContentLength,
		ContentType:          qct.Type,
		ContentSubtype:       qct.Subtype,
		ContentSubtypeParams: qct.Params,
		ModifiedAt:           time.Unix(parsedMtime, 0),
	})
}

func (storage *S3Storage) PresignedObjectLink(sum [sha1.Size]byte, duration time.Duration) (string, error) {
	if err := storage.initiateSession(); err != nil {
		return "", utils.Wrap(err, "initiate session")
	}

	req, _ := storage.svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(storage.bucket),
		Key:    aws.String(storage.prefixedKey(sum)),
	})

	return req.Presign(duration)
}

func (storage *S3Storage) PresignedObjectDownloadLink(sum [sha1.Size]byte, duration time.Duration, fname string) (string, error) {
	if err := storage.initiateSession(); err != nil {
		return "", utils.Wrap(err, "initiate session")
	}

	req, _ := storage.svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket:                     aws.String(storage.bucket),
		Key:                        aws.String(storage.prefixedKey(sum)),
		ResponseContentDisposition: aws.String(fmt.Sprintf(`attachment; filename="%s"`, fname)),
	})

	return req.Presign(duration)
}

func (storage *S3Storage) objectExists(ctx context.Context, sum [sha1.Size]byte) (bool, error) {
	if err := storage.initiateSession(); err != nil {
		return false, utils.Wrap(err, "initiate session")
	}

	_, err := storage.svc.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(storage.bucket),
		Key:    aws.String(storage.prefixedKey(sum)),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			if aerr.Code() == ERR_CODE_NOT_FOUND {
				return false, nil
			}
		}

		return false, utils.Wrap(err, "head object")
	}

	return true, nil
}

func (storage *S3Storage) getMultipartUploadID(ctx context.Context, sum [sha1.Size]byte, path string, info os.FileInfo) (*string, bool, error) {
	if err := storage.initiateSession(); err != nil {
		return nil, false, utils.Wrap(err, "initiate session")
	}

	var (
		err                                 error
		prefixedKey                         = storage.prefixedKey(sum)
		listOutput                          *s3.ListMultipartUploadsOutput
		uploadID, keyMarker, uploadIDMarker *string
		latestInitiated                     time.Time
	)

	for listOutput == nil || *listOutput.IsTruncated {
		listOutput, err = storage.svc.ListMultipartUploadsWithContext(ctx, &s3.ListMultipartUploadsInput{
			Bucket:         aws.String(storage.bucket),
			Prefix:         aws.String(prefixedKey),
			KeyMarker:      keyMarker,
			UploadIdMarker: uploadIDMarker,
		})
		if err != nil {
			return nil, false, utils.Wrap(err, "list multipart uploads")
		}

		keyMarker, uploadIDMarker = listOutput.NextKeyMarker, listOutput.NextUploadIdMarker

		for _, upload := range listOutput.Uploads {
			if *upload.Key == prefixedKey && upload.Initiated.After(latestInitiated) {
				latestInitiated = *upload.Initiated
				uploadID = upload.UploadId
			}
		}
	}
	if uploadID != nil {
		return uploadID, false, nil
	}

	ct, err := utils.DetectContentType(path)
	if err != nil {
		return nil, false, utils.Wrap(err, "detect content type")
	}

	createOutput, err := storage.svc.CreateMultipartUploadWithContext(ctx, &s3.CreateMultipartUploadInput{
		Bucket:      aws.String(storage.bucket),
		ContentType: aws.String(ct),
		Key:         aws.String(prefixedKey),
		Metadata: map[string]*string{
			"base":  aws.String(filepath.Base(path)),
			"mtime": aws.String(strconv.FormatInt(info.ModTime().Unix(), 10)),
		},
	})
	if err != nil {
		return nil, false, utils.Wrap(err, "create multipart upload")
	}

	return createOutput.UploadId, true, nil
}

func (storage *S3Storage) listMultipartUploadParts(ctx context.Context, sum [sha1.Size]byte, uploadID *string) (map[int64]*s3.Part, error) {
	if err := storage.initiateSession(); err != nil {
		return nil, utils.Wrap(err, "initiate session")
	}

	var (
		err              error
		parts            = make(map[int64]*s3.Part)
		partNumberMarker *int64
		output           *s3.ListPartsOutput
	)
	for output == nil || *output.IsTruncated {
		output, err = storage.svc.ListPartsWithContext(ctx, &s3.ListPartsInput{
			Bucket:           aws.String(storage.bucket),
			Key:              aws.String(storage.prefixedKey(sum)),
			PartNumberMarker: partNumberMarker,
			UploadId:         uploadID,
		})
		if err != nil {
			return nil, utils.Wrap(err, "list parts")
		}

		partNumberMarker = output.NextPartNumberMarker

		for _, part := range output.Parts {
			parts[*part.PartNumber] = part
		}
	}

	return parts, nil
}

func (storage *S3Storage) deleteKey(ctx context.Context, key string) error {
	if err := storage.initiateSession(); err != nil {
		return utils.Wrap(err, "initiate session")
	}

	_, err := storage.svc.DeleteObjectWithContext(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String(storage.bucket),
		Key:    aws.String(storage.prefix + key),
	})

	return err
}

func (storage *S3Storage) initiateSession() error {
	storage.sessOnce.Do(func() {
		storage.sess, storage.sessErr = session.NewSession(&aws.Config{
			Region:      aws.String(storage.region),
			Endpoint:    aws.String(storage.endpoint),
			Credentials: credentials.NewStaticCredentials(storage.accessKey, storage.secretKey, ""),
		})
		if storage.sessErr == nil {
			storage.svc = s3.New(storage.sess)
		}
	})
	return storage.sessErr
}

func (storage *S3Storage) prefixedKey(sum [sha1.Size]byte) string {
	return storage.prefix + objectKeyFromSHA1(sum)
}
