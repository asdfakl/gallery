package appserver

import (
	"testing"
)

func TestAppServerIsRooted(t *testing.T) {
	helper := func(root string, cases map[string]bool) {
		srv := &AppServer{
			cfg: &Configuration{
				RootPath: root,
			},
		}

		for path, rooted := range cases {
			if r := srv.isRooted(path); r != rooted {
				t.Errorf(
					"expecting root path '%s', path '%s' rooted %t, got %t",
					root, path, rooted, r,
				)
			}
		}
	}

	helper("/", map[string]bool{
		"":           false,
		"abc/bdf":    false,
		"/":          true,
		"/foo":       true,
		"/bar":       true,
		"/a/b/c/d/e": true,
		"///////":    true,
	})

	helper("/app", map[string]bool{
		"":           false,
		"/":          false,
		"app":        false,
		"/apply":     false,
		"/api/bar":   false,
		"/app":       true,
		"/app/ly":    true,
		"/app/a/b/c": true,
		"/app///":    true,
	})
}
