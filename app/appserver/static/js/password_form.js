"use strict";

(function(){
	const initJSPasswordForm = nodes => {
		nodes.forEach(node => {
			const currentPasswordInput = node.querySelector("input[name=current_password]");
			const currentPasswordHelp = node.querySelector("#help_current_password");

			const newPasswordInput1 = node.querySelector("input[name=new_password_1]");
			const newPasswordHelp1 = node.querySelector("#help_new_password_1");
			const newPasswordProgress1 = node.querySelector("#progress_new_password_1");

			const newPasswordInput2 = node.querySelector("input[name=new_password_2]");
			const newPasswordHelp2 = node.querySelector("#help_new_password_2");
			const newPasswordProgress2 = node.querySelector("#progress_new_password_2");

			const submit = node.querySelector("button[type=submit]");

			const progressClass = (progress) => {
				if (progress < 30) {
					return "is-danger";
				}
				if (progress < 70) {
					return "is-warning";
				}
				if (progress < 100) {
					return "is-info";
				}
				return "is-success";
			};

			const currentPasswordOK = (input) => {
				const val = input.value.trim();

				if (!val) {
					return [false, "Required"];
				}

				return [true, ""];
			};

			const refresh = (e) => {
				let allOK = true;

				// current password
				let [ok, help, progress] = currentPasswordOK(currentPasswordInput);
				allOK = allOK && ok;
				currentPasswordHelp.innerHTML = help;
				currentPasswordInput.classList.remove(ok ? "is-danger" : "is-success");
				currentPasswordInput.classList.add(ok ? "is-success" : "is-danger");

				// new password 1
				[ok, help, progress] = Utils.newPasswordOK(newPasswordInput1.value);
				allOK = allOK && ok;
				newPasswordHelp1.innerHTML = help;
				newPasswordProgress1.setAttribute("value", progress);
				newPasswordProgress1.classList.remove("is-danger", "is-warning", "is-info", "is-success");
				newPasswordProgress1.classList.add(progressClass(progress));
				newPasswordInput1.classList.remove(ok ? "is-danger" : "is-success");
				newPasswordInput1.classList.add(ok ? "is-success" : "is-danger");

				// new password 2
				[ok, help, progress] = Utils.newPasswordOK(newPasswordInput2.value);
				allOK = allOK && ok;
				newPasswordHelp2.innerHTML = help;
				newPasswordProgress2.setAttribute("value", progress);
				newPasswordProgress2.classList.remove("is-danger", "is-warning", "is-info", "is-success");
				newPasswordProgress2.classList.add(progressClass(progress));
				newPasswordInput2.classList.remove(ok ? "is-danger" : "is-success");
				newPasswordInput2.classList.add(ok ? "is-success" : "is-danger");

				if (!allOK) {
					submit.setAttribute("disabled", "");
					return;
				}

				const currentPassword = currentPasswordInput.value.trim();
				const newPassword1 = newPasswordInput1.value.trim();
				const newPassword2 = newPasswordInput2.value.trim();

				if (newPassword1 !== newPassword2) {
					submit.setAttribute("disabled", "");
					newPasswordHelp1.innerHTML = "Does not match";
					newPasswordHelp2.innerHTML = "Does not match";
					newPasswordInput1.classList.remove("is-success");
					newPasswordInput1.classList.add("is-danger");
					newPasswordInput2.classList.remove("is-success");
					newPasswordInput2.classList.add("is-danger");
					return;
				}

				if (newPassword1 === currentPassword) {
					submit.setAttribute("disabled", "");
					newPasswordHelp1.innerHTML = "Must be distinct from old password";
					newPasswordHelp2.innerHTML = "Must be distinct from old password";
					newPasswordInput1.classList.remove("is-success");
					newPasswordInput1.classList.add("is-danger");
					newPasswordInput2.classList.remove("is-success");
					newPasswordInput2.classList.add("is-danger");
					return;
				}

				submit.removeAttribute("disabled");
			};

			currentPasswordInput.addEventListener("keyup", refresh);
			currentPasswordInput.addEventListener("change", refresh);

			newPasswordInput1.addEventListener("keyup", refresh);
			newPasswordInput1.addEventListener("change", refresh);

			newPasswordInput2.addEventListener("keyup", refresh);
			newPasswordInput2.addEventListener("change", refresh);

			node.addEventListener("submit", e => {
				e.preventDefault();

				const action = node.attributes["action"].nodeValue;
				const modal = node.closest("div.modal");

				const hiddenInputs = node.querySelectorAll("input[type=hidden]");

				const q = {};

				hiddenInputs.forEach(input => {
					q[input.name] = input.value;
				});

				q[currentPasswordInput.name] = currentPasswordInput.value.trim();
				q[newPasswordInput1.name] = newPasswordInput1.value.trim();
				q[newPasswordInput2.name] = newPasswordInput2.value.trim();

				const opts = {
					method: "POST",
					credentials: "same-origin",
					redirect: "manual",
					headers: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
					body: Utils.queryString(q),
				};

				fetch(action, opts).then(res => {
					modal.classList.remove("is-active");

					return Utils.handleFetchError(res);
				}).then(res => res.json()).then(res => {
					Utils.showContentModal("Password changed successfully", "is-success");
				}).catch(err => {
					console.error(`js-password-form fetch failed: ${err}`);
				});
			});
		});
	};

	Common.registerInitHTMLElemHook(elem => {
		initJSPasswordForm(elem.querySelectorAll(".js-password-form"));
	});
})();
