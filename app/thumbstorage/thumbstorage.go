package thumbstorage

import (
	"context"
	"crypto/sha1"
	"errors"
	"io"
	"os"

	"github.com/gabriel-vasile/mimetype"
)

const Version = 1

const redisCachePrefix = "thumbnail"

const (
	ThumbnailSize64x64   ThumbnailSize = "64x64"
	ThumbnailSize128x128 ThumbnailSize = "128x128"
)

var ErrNotExist = errors.New("thumbnail not found")

type ThumbnailSize string

type MimeReadSeekCloser interface {
	io.ReadSeekCloser

	MIME() *mimetype.MIME
}

type Storage interface {
	ReadThumb(context.Context, [sha1.Size]byte, ThumbnailSize) (MimeReadSeekCloser, error)

	CopyThumb(context.Context, [sha1.Size]byte, ThumbnailSize, string) error
}

func IsValidThumbnailSize(size ThumbnailSize) bool {
	switch size {
	case ThumbnailSize64x64, ThumbnailSize128x128:
		return true
	default:
		return false
	}
}

type thumbnailReader struct {
	io.ReadSeeker
	mime *mimetype.MIME
}

func (t *thumbnailReader) MIME() *mimetype.MIME {
	return t.mime
}

func (t *thumbnailReader) Close() error {
	if f, ok := t.ReadSeeker.(*os.File); ok {
		return f.Close()
	}
	return nil
}
