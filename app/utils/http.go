package utils

import (
	"net"
	"net/http"
	"strings"
)

func RequestIP(r *http.Request, trustXForwardedFor, trustXRealIP bool) net.IP {
	if trustXRealIP {
		if ip := net.ParseIP(r.Header.Get("X-Real-IP")); ip != nil {
			return ip
		}
	}

	if trustXForwardedFor {
		for _, xff := range strings.Split(r.Header.Get("X-Forwarded-For"), ", ") {
			if ip := net.ParseIP(xff); ip != nil {
				return ip
			}
		}
	}

	host, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return nil
	}

	return net.ParseIP(host)
}
