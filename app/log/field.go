package log

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"strconv"
	"strings"
	"time"
)

type Field struct {
	key string
	buf []byte
}

func Bool(key string, val bool) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	if val {
		b.Write(shellColorGreen)
		b.Write([]byte("true"))
	} else {
		b.Write(shellColorRed)
		b.Write([]byte("false"))
	}
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func String(key string, val string) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	encoded, err := json.Marshal(val)
	if err != nil {
		panic(err)
	}

	b.Write(shellColorLightGray)
	b.Write(encoded)
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func Int64(key string, val int64) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	b.Write(shellColorBlue)
	b.Write([]byte(strconv.FormatInt(val, 10)))
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func Int(key string, val int) Field {
	return Int64(key, int64(val))
}

func Float64(key string, val float64) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	b.Write(shellColorCyan)
	b.Write([]byte(strconv.FormatFloat(val, 'f', -1, 64)))
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func Duration(key string, val time.Duration) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	b.Write(shellColorLightGray)
	b.WriteByte('"')
	b.Write([]byte(val.String()))
	b.WriteByte('"')
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func Time(key string, val time.Time) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	b.Write(shellColorLightGray)
	b.WriteByte('"')
	b.Write([]byte(val.In(time.UTC).Format("2006-01-02T15:04:05Z")))
	b.WriteByte('"')
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func SHA1(key string, val [sha1.Size]byte) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	encoded := make([]byte, hex.EncodedLen(sha1.Size))
	hex.Encode(encoded, val[:])

	b.Write(shellColorLightGray)
	b.WriteByte('"')
	b.Write(encoded)
	b.WriteByte('"')
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}

}

func Err(err error) Field {
	b := &bytes.Buffer{}

	key := writeKey("error", b)

	encoded, err := json.Marshal(err.Error())
	if err != nil {
		panic(err)
	}

	b.Write(shellColorBoldRed)
	b.Write(encoded)
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func Any(key string, val interface{}) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	encoded, err := json.Marshal(val)
	if err != nil {
		panic(err)
	}

	b.Write(shellColorPurple)
	b.Write(encoded)
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func Nil(key string) Field {
	b := &bytes.Buffer{}

	key = writeKey(key, b)

	b.Write(shellColorPurple)
	b.Write([]byte("null"))
	b.Write(shellColorNormal)

	return Field{
		key: key,
		buf: b.Bytes(),
	}
}

func writeKey(key string, b *bytes.Buffer) string {
	key = strings.TrimSpace(key)
	if key == "" {
		panic(errors.New("empty key"))
	}

	b.WriteByte('"')
	b.Write(shellColorBold)
	b.Write([]byte(key))
	b.Write(shellColorNormal)
	b.Write([]byte("\": "))

	return key
}
